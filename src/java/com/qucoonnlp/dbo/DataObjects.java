/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoonnlp.dbo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User001
 */
public class DataObjects {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    
      public static ArrayList getNlp_banks(){
        String query = "from Nlp_banks";
            Nlp_banks obj = new Nlp_banks();
            //obj.setStatus(status);
            List<Nlp_banks> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadQuery(query);
            }catch(Exception s){
                s.printStackTrace();
            }
            return (ArrayList) objList;
    }
      
      public static ArrayList getNlp_actions(){
        String query = "from Nlp_actions";
            Nlp_actions obj = new Nlp_actions();
            //obj.setStatus(status);
            List<Nlp_actions> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadQuery(query);
            }catch(Exception s){
                s.printStackTrace();
            }
            return (ArrayList) objList;
    }
      public static ArrayList getNlp_beneficiary(){
        String query = "from Nlp_beneficiary";
            Nlp_beneficiary obj = new Nlp_beneficiary();
            //obj.setStatus(status);
            List<Nlp_beneficiary> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadQuery(query);
            }catch(Exception s){
                s.printStackTrace();
            }
            return (ArrayList) objList;
    }
      
}
