package com.qucoonnlp.dbo;
// Generated 16-May-2017 00:53:04 by Hibernate Tools 4.3.1



/**
 * iswCategorys generated by hbm2java
 */
public class Nlp_banks  implements java.io.Serializable {


     private Integer id;
     private String alias;
     private String bank_name;
     

    public Nlp_banks() {
    }

    public Nlp_banks(String alias, String bank_name) {
       this.alias = alias;
       this.bank_name = bank_name;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getAlias() {
        return this.alias;
    }
    
    public void setAlias(String alias) {
        this.alias = alias;
    }
    public String getBank_name() {
        return this.bank_name;
    }
    
    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }
    
   
    
    public static void main(String args[]){
        Nlp_banks iswc = new Nlp_banks();
        iswc.setAlias("301");
        //iswc.setCategory_name("wer");
        iswc.setBank_name("2340");
        PersistenceAgent.logTransactions(iswc);
    }



}


