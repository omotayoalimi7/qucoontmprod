/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoonnlp.dbo;

/**
 *
 * @author Neptune
 */

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

public class PersistenceAgent
{
  static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PersistenceAgent.class);
  
   public static boolean saveObject(Object obj)

  {
    Session session = null;
    Transaction tx = null;
    boolean flag = false;
    try
    {
      session = HibernateSessionFactory.getSession();
      tx = session.beginTransaction();
      session.saveOrUpdate(obj);
     // session.merge(obj);
      tx.commit();
      flag = true;
      if ((session != null) && (session.isOpen())) {
        session.close();
      }
      
    }
    catch (HibernateException e)
    {
      tx.rollback();
      //LoggingUtil.logError(e, logger);
      logger.info(">>>ERROR>>> OBJECT:"+obj.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
      //throw new Exception(e);
    }
    catch (Exception e)
    {
      tx.rollback();
      //LoggingUtil.logError(e, logger);
       logger.info(">>>ERROR>>> OBJECT:"+obj.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
     // throw new Exception(e);
    }
    finally
    {
      try
      {
        if ((session != null) && (session.isOpen())) {
          session.close();
        }
      }
      catch (Exception e)
      {
        //LoggingUtil.logError(e, logger);
         logger.info(">>>ERROR>>> OBJECT:"+obj.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
      }
    }
    
    return flag;
  }
  
  public static boolean logTransactions(Object obj)
    //throws Exception
  {
    Session session = null;
    Transaction tx = null;
    boolean flag = false;
    try
    {
      session = HibernateSessionFactory.getSession();
      tx = session.beginTransaction();
      session.saveOrUpdate(obj);
     // session.merge(obj);
      tx.commit();
      flag = true;
//      if ((session != null) && (session.isOpen())) {
//        session.close();
//      }
      
    }
    catch (HibernateException e)
    {
      tx.rollback();
      //LoggingUtil.logError(e, logger);
      logger.info(">>>ERROR>>> OBJECT:"+obj.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
      //throw new Exception(e);
    }
    catch (Exception e)
    {
      tx.rollback();
      //LoggingUtil.logError(e, logger);
       logger.info(">>>ERROR>>> OBJECT:"+obj.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
     // throw new Exception(e);
    }
    finally
    {
      try
      {
        if ((session != null) && (session.isOpen())) {
          session.close();
        }
      }
      catch (Exception e)
      {
        //LoggingUtil.logError(e, logger);
         logger.info(">>>ERROR>>> OBJECT:"+obj.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
      }
    }
    
    return flag;
  }
  
 



  public static List loadTableRecords(Object objName)
    throws Exception
  {
    Session session = null;
    
    String objNameString = objName.toString();
    String query = "from " + objNameString;
    List resultSet  = null;
    try
    {
      System.out.println("query is " + query);
      
      session = HibernateSessionFactory.getSession();
      

      resultSet  = session.createQuery(query).list();
      if ((session != null) && (session.isOpen())) {
        session.close();
      }
      
    }
    catch (HibernateException e)
    {
      //LoggingUtil.logError(e, logger);
      logger.info(">>>ERROR>>> OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
    }
    catch (Exception e)
    {
       logger.info(">>>ERROR>>> OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
      //throw new Exception(e);
    }
    finally
    {
      try
      {
        if ((session != null) && (session.isOpen())) {
          session.close();
        }
      }
      catch (Exception e)
      {
        LoggingUtil.logError(e, logger);
      }
    }
    return resultSet;
  }
  
  public static List loadQuery(String query)
    throws Exception
  {
    Session session = null;
    List resultSet = null;
    try
    {
      logger.info("query is " + query); 
      session = HibernateSessionFactory.getSession();
      resultSet = session.createQuery(query)
     // .setProperties(objName)
      .list();
      if ((session != null) && (session.isOpen())) {
        session.close();
      }
      
    }
    catch (HibernateException e)
    {
       logger.info(">>>ERROR>>> query:"+query );
    //  throw new Exception(e);
    }
    catch (Exception e)
    {
       logger.info(">>>ERROR>>> query:"+query);
    //  throw new Exception(e);
    }
    finally
    {
      try
      {
        if ((session != null) && (session.isOpen())) {
          session.close();
        }
      }
      catch (Exception e)
      {
        logger.info(">>>ERROR>>> query:"+query);;
      }
    }
    return resultSet;
  }
  
  
  public static List loadTableSingleResult(Object objName, String query)
    throws Exception
  {
    Session session = null;
    List resultSet = null;
    try
    {
      logger.info("query is " + query); 
      session = HibernateSessionFactory.getSession();
      resultSet = session.createQuery(query)
      .setProperties(objName)
      .list();
      if ((session != null) && (session.isOpen())) {
        session.close();
      }
      
    }
    catch (HibernateException e)
    {
       logger.info(">>>ERROR>>> query:"+query+"     OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
    //  throw new Exception(e);
    }
    catch (Exception e)
    {
       logger.info(">>>ERROR>>> query:"+query+"     OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
    //  throw new Exception(e);
    }
    finally
    {
      try
      {
        if ((session != null) && (session.isOpen())) {
          session.close();
        }
      }
      catch (Exception e)
      {
        logger.info(">>>ERROR>>> query:"+query+"     OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
      }
    }
    return resultSet;
  }
  
  
  public static List loadTableRecordsQuery(Object objName, String query)
    throws Exception
  {
    Session session = null;
    List resultSet = null;
    try
    {
      logger.info("query is " + query); 
      session = HibernateSessionFactory.getSession();
      resultSet = session.createQuery(query)
      .setProperties(objName)
      .list();
      if ((session != null) && (session.isOpen())) {
        session.close();
      }
      
    }
    catch (HibernateException e)
    {
       logger.info(">>>ERROR>>> query:"+query+"     OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
    //  throw new Exception(e);
    }
    catch (Exception e)
    {
       logger.info(">>>ERROR>>> query:"+query+"     OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
    //  throw new Exception(e);
    }
    finally
    {
      try
      {
        if ((session != null) && (session.isOpen())) {
          session.close();
        }
      }
      catch (Exception e)
      {
        logger.info(">>>ERROR>>> query:"+query+"     OBJECT:"+objName.toString()+" ERRORDESC:"+e.getMessage()+"  "+e.toString());
      }
    }
    return resultSet;
  }
  
  public static List loadTableRecords(Object objName, String whereClause)
    throws Exception
  {
    Session session = null;
    
    String objNameString = objName.toString();
    String query = "from " + objNameString;
    List resultSet = null;
    
    query = query + (whereClause != null ? " " + whereClause : "");
    try
    {
      System.out.println("query is " + query);
      
      session = HibernateSessionFactory.getSession();
      

      resultSet = session.createQuery(query).list();
      if ((session != null) && (session.isOpen())) {
        session.close();
      }
      
    }
    catch (HibernateException e)
    {
      LoggingUtil.logError(e, logger);
    //  throw new Exception(e);
    }
    catch (Exception e)
    {
      LoggingUtil.logError(e, logger);
    //  throw new Exception(e);
    }
    finally
    {
      try
      {
        if ((session != null) && (session.isOpen())) {
          session.close();
        }
      }
      catch (Exception e)
      {
        LoggingUtil.logError(e, logger);
      }
    }
    return resultSet;
  }
  

  

  
//  public static CbaAccountEnquiryData performAccountEnquiry(CbaAccountEnquiryData request)
//    throws Exception
//  {
//    Session session = null;
//    CbaAccountEnquiryData result = new CbaAccountEnquiryData();
//    try
//    {
//      session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
//      session.beginTransaction();
//      
//      String sql = "select cod_ccy,cod_prod, flg_mnt_status,COD_ACCT_TITLE, (bal_available-nvl(amt_hld,0)-nvl(bal_acct_min_reqd,0)+nvl(AMT_OD_LIMIT,0)) as net_balance FROM ch_acct_mast  where cod_acct_no = '" + request.getAccountNo() + "'";
//      
//
//      SQLQuery sq = session.createSQLQuery(sql).addScalar("cod_ccy", Hibernate.STRING);
//      SQLQuery sq1 = session.createSQLQuery(sql).addScalar("flg_mnt_status", Hibernate.STRING);
//      SQLQuery sq2 = session.createSQLQuery(sql).addScalar("net_balance", Hibernate.DOUBLE);
//      SQLQuery sq3 = session.createSQLQuery(sql).addScalar("COD_ACCT_TITLE", Hibernate.STRING);
//      SQLQuery sq4 = session.createSQLQuery(sql).addScalar("cod_prod", Hibernate.STRING);
//      if (sq.uniqueResult() != null)
//      {
//        result.setIsActive(sq.uniqueResult().equals("22"));
//        result.setIsNairaAccount(sq1.uniqueResult().equals("A"));
//        result.setIsValid(true);
//        result.setNetBalance(Double.parseDouble(sq2.uniqueResult().toString()));
//        result.setAccountName(sq3.uniqueResult().toString());
//        result.setIsStaffAccount(sq4.uniqueResult().equals("3"));
//      }
//      else
//      {
//        result.setIsActive(false);
//        result.setIsNairaAccount(false);
//        result.setIsValid(false);
//        result.setNetBalance(0.0D);
//        result.setAccountName("");
//        result.setRequestModule(request.getRequestModule());
//        result.setResponseMessage("Account Does not Exist");
//        result.setIsStaffAccount(false);
//      }
//      if ((session != null) && (session.isOpen())) {
//        session.close();
//      }
//    }
//    catch (Exception e)
//    {
//      result.setIsActive(false);
//      result.setIsNairaAccount(false);
//      result.setIsValid(false);
//      result.setNetBalance(0.0D);
//      result.setRequestModule(request.getRequestModule());
//      result.setResponseMessage(e.getMessage());
//      
//      LoggingUtil.logError(e, logger);
//      throw new Exception(e);
//    }
//    return result;
//  }
//  

  
 
  
  public static void main(String[] args)
  {
    try
    {
      System.out.println("The length is ");
    }
    catch (Exception ex)
    {
      //java.util.logging.Logger.getLogger(PersistenceAgent.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}

