/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoonnlp.dbo;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateSessionFactory
{
  private static String CONFIG_FILE_LOCATION = "/hibernate.cfg.xml";
  private static Logger logger = Logger.getLogger(HibernateSessionFactory.class);
  private static final ThreadLocal threadLocal = new ThreadLocal();
  private static Configuration configuration = new Configuration();
  private static SessionFactory sessionFactory;
  private static String configFile = CONFIG_FILE_LOCATION;
  

  
  static {
            try {
                Configuration cfg = new Configuration().configure("hibernate.cfg.xml");         
                StandardServiceRegistryBuilder sb = new StandardServiceRegistryBuilder();
                sb.applySettings(cfg.getProperties());
                StandardServiceRegistry standardServiceRegistry = sb.build();                   
                sessionFactory = cfg.buildSessionFactory(standardServiceRegistry);              
            } catch (Exception th) {
                    System.err.println("Enitial SessionFactory creation failed" + th);
                    LoggingUtil.logThrowable("%%%% Error Creating SessionFactory in the HibernateSessionFactory class%%%%", th, logger);
                    //throw new ExceptionInInitializerError(th);
            }
    }
  
//  static {
//            try {
//        Configuration configuration = new Configuration();
//        configuration.configure("hibernate.cfg.xml");
//        // Add annotated class
//        //configuration.addAnnotatedClass(RandomNumberPOJO.class);
// 
//        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
//        logger.info("ServiceRegistry created successfully");
//        SessionFactory sessionFactory = configuration
//                .buildSessionFactory(serviceRegistry);
//        logger.info("SessionFactory created successfully");
// 
//        servletContextEvent.getServletContext().setAttribute("SessionFactory", sessionFactory);
//        logger.info("Hibernate SessionFactory Configured successfully");
//  } catch (Exception th) {
//                    System.err.println("Enitial SessionFactory creation failed" + th);
//                    LoggingUtil.logThrowable("%%%% Error Creating SessionFactory in the HibernateSessionFactory class%%%%", th, logger);
//                    //throw new ExceptionInInitializerError(th);
//            }
//    }
  
  
  
  

static {
try {
sessionFactory = new Configuration().configure().buildSessionFactory();
} catch (Throwable ex) {
throw new ExceptionInInitializerError(ex);
}
}
  
  public static Session getSession()
    throws HibernateException
  {
    Session session = (Session)threadLocal.get();
    if ((session == null) || (!session.isOpen()))
    {
      if (sessionFactory == null) {
        rebuildSessionFactory();
      }
      session = sessionFactory != null ? sessionFactory.openSession() : null;
      
      threadLocal.set(session);
    }
    return session;
  }
  
  public static void rebuildSessionFactory()
  {
    try
    {

////  configuration.configure(configFile);
////  ServiceRegistry  serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();        
////  sessionFactory = configuration.buildSessionFactory(serviceRegistry);
      sessionFactory = new Configuration().configure().buildSessionFactory();
    }
    catch (Exception e)
    {
      LoggingUtil.logThrowable("%%%% Error rebuilding SessionFactory in the HibernateSessionFactory class%%%%", e, logger);
    }
  }
  
  public static void closeSession()
    throws HibernateException
  {
    Session session = (Session)threadLocal.get();
    threadLocal.set(null);
    if (session != null) {
      session.close();
    }
  }
  
  public static SessionFactory getSessionFactory()
  {
    return sessionFactory;
  }
  
  public static void setConfigFile(String configFile)
  {
    configFile = configFile;
    sessionFactory = null;
  }
  
  public static Configuration getConfiguration()
  {
    return configuration;
  }
}


