/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoonnlp.processor;

import com.qucoonnlp.dbo.DataObjects;
import com.qucoonnlp.dbo.Nlp_actions;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import com.qucoonnlp.dbo.Nlp_banks;
import com.qucoonnlp.dbo.Nlp_beneficiary;

/**
 *
 * @author DR MRS ALIMI
 */
public class langprocessor {
    static String amt="";
    static String benename="";
    static int action_marker;
    static int amt_marker;
    static int amt_prefix_marker;
    static int bank_marker;
    static int account_marker;
    static int currency_marker;
    public static void main(String args[]) throws JSONException{
        langprocessor lp = new langprocessor();
        System.out.println(lp.runDictionaryCheck("get 30k from  0023113070"));
    }
   
    
    public List <String> LoadWords(String theSentence){
        List <String> words= new ArrayList <String>();
        int k;
        int p;
        int y;
        String thisXtr;
        String thisWrd;
        //remove special xters
        theSentence=theSentence.replace(",", "");
        theSentence=theSentence.replace("'", "");
        theSentence=theSentence.replace(";", "");
        theSentence=theSentence.replace("(", "");
        theSentence=theSentence.replace(")", "");
        theSentence=theSentence.replace("  ", " ");
        theSentence=theSentence.trim();
        k=0;
        p=0;
        y=1;
        thisWrd="";
        do{
           thisXtr=theSentence.substring(k, y);
           //System.out.println(thisXtr);
           if (thisXtr.equals(" ")){
               words.add(thisWrd);
               thisWrd="";
               p=p+1;
           }
           thisWrd=thisWrd + thisXtr;
           k=k+1 ;
           y=y+1 ;
        } 
        while (k<theSentence.length());
        words.add(theSentence.substring(theSentence.lastIndexOf(" "), theSentence.length()));
        
        return words;}
   
    public String runDictionaryCheck(String text) throws JSONException{
        String thisEntry; 
        String keyword;
        String meaning;
        String type;
        String sCurrentLine;
        String action;
        String beneaccount;
        String benebank;
        SimpleDateFormat df = new SimpleDateFormat("YYYYMMdd");
        SimpleDateFormat tf = new SimpleDateFormat("HH:mm:ss");
        Date dateobj = new Date();
        List <String> words= new ArrayList <String>();
        langprocessor obj = new langprocessor();
        langprocessor lp = new langprocessor();
        words=obj.LoadWords(text);
        JSONObject output = new JSONObject();
                
        action="";
        beneaccount="";
        amt="";
        benebank="";
        try{
            //detect amount
            for(int j=0;j<words.size();j++){
                detectAmount(words.get(j),j,words);
            }
            //detect action
            int count=0;
            Nlp_actions nlpa;
            ArrayList objList = DataObjects.getNlp_actions();
            while(count <objList.size()){
                 nlpa =  (Nlp_actions)objList.get(count);
                 keyword = nlpa.getKeyword();
                 for(int j=0;j<words.size();j++){
                     if (keyword.equals(words.get(j).toLowerCase().trim())) {
                         meaning = nlpa.getActions();
                         action=meaning;
                         type=nlpa.getTrans_type();
                         j=j+1;

                     }
                 }
                  count=count+1;
            }
            if(action.equals("")){
                action=lp.detectAction(words);
            }
            //detect account and bank
            String acc;
            acc= lp.detectaccount(words);
            String benbank;
            benbank=obj.Loadbanks(words);
            if(benbank.equals("")||!acc.equals("")){
            if(""!=acc){
                beneaccount=acc;
             }else{
                beneaccount=lp.resolvename_account(words);
            }
            }
            //detect bank
            if(acc.equals("")||!benbank.equals("")){
            if(""!=benbank){
                benebank=benbank;
             }else{
                benebank=lp.resolvename_bank(words);
            }
            }
            //detect name
            lp.detectName(words);
            
            if(action.equals("FundsTransfer")){
                output.put("benename", benename);
                output.put("beneaccount", beneaccount);
                output.put("action", action);
                output.put("benebank", benebank);
            }else if(action.equals("VTU")){
                output.put("benename", benename);
                output.put("action", action);
                output.put("benephone", beneaccount);   
            }else if(action.equals("FundsRequest")){
                output.put("sender", benename);
                output.put("action", action);
                output.put("senderaccount", beneaccount);
                output.put("senderbank", benebank);
            }
            output.put("amt", amt);
            output.put("date", df.format(dateobj));
            output.put("domain", "banking");
            output.put("statusmessage", "language processes");
            output.put("narration", "sample language processing");
            output.put("currency", "Naira");
            output.put("time", tf.format(dateobj));
            output.put("currencycode", "N");
            output.put("screentodisplay", "ftsummary");
            output.put("status", "00");
            amt="";
            benename="";
            action_marker=0;
            amt_marker=0;
            amt_prefix_marker=0;
            bank_marker=0;
            account_marker=0;
            int currency_marker;
        }catch(Exception e){
            System.out.println(e.getMessage());
        } 
    return output.toString();}
    
    public void detectAmount(String theWord,int j,List <String> words){
        int word;
        try{
                if(theWord.length()<10||theWord.contains("hundred")||theWord.contains("hundread")||theWord.contains("thousand")||theWord.contains("thousnd")||theWord.contains("thou")||theWord.contains("million")||theWord.contains("milion")||theWord.contains("mil")){
                    
                    word= Integer.parseInt(theWord.trim().substring(0,1));
                    if(theWord.contains("hundr")&&!theWord.contains("thou")||theWord.contains("h")&&!theWord.contains("thou")){
                    theWord = theWord.replace("hundread", "00");
                    theWord = theWord.replace("hundred", "00");
                    theWord = theWord.replace("h", "00");
                    }
                    if(theWord.contains("thous")||theWord.contains("k")){
                    theWord = theWord.replace("thousand", "000");
                    theWord = theWord.replace("thousnd", "000");
                    theWord = theWord.replace("thou", "000");
                    theWord = theWord.replace("k", "000");
                    }
                    if(theWord.contains("mil")||theWord.contains("m")){
                    theWord = theWord.replace("million", "000000");
                    theWord = theWord.replace("milion", "000000");
                    theWord = theWord.replace("mil", "000000");
                    theWord = theWord.replace("m", "000000");
                    }
                    amt=theWord.trim();
                    amt_marker=j;
                    if(words.get(j+1).trim().toLowerCase().equals("naira")){
                       currency_marker =j+1;
                    }
                    if(words.get(j+2).trim().toLowerCase().equals("naira")){
                        currency_marker =j+2;
                    }
                    if(words.get(j+1).trim().toLowerCase().equals("hundred")||words.get(j+1).trim().toLowerCase().equals("hundread")||words.get(j+1).trim().toLowerCase().equals("h")){
                        amt=amt+"00";
                        amt_prefix_marker=j+1;
                    }
                    if(words.get(j+1).trim().toLowerCase().equals("thousand")||words.get(j+1).trim().toLowerCase().equals("thou")||words.get(j+1).trim().toLowerCase().equals("thousnd")||words.get(j+1).trim().toLowerCase().equals("k")){
                        amt=amt+"000";
                        amt_prefix_marker=j+1;
                    }
                    if(words.get(j+1).trim().toLowerCase().equals("million")||words.get(j+1).trim().toLowerCase().equals("milion")||words.get(j+1).trim().toLowerCase().equals("m")){
                        amt=amt+"000000";
                        amt_prefix_marker=j+1;
                    }
                    
                }
           }
        catch(Exception e){
        }
    }
     
    public String resolvename_account(List <String> words){
        String output="";
        String benealias="";
        String beneaccount="";
        String text_name="";
        int word;
        int count=0;
        Nlp_beneficiary nlpb;
        ArrayList objList = DataObjects.getNlp_beneficiary();
        while(count <objList.size()){
            nlpb =  (Nlp_beneficiary)objList.get(count);
            benealias = nlpb.getBenealias();
            beneaccount=nlpb.getBeneaccount();
            for(int l=0; l< words.size(); l++){
               //System.out.println("word "+words.get(l)); 
               if (words.get(l).trim().toLowerCase().equals(benealias.toLowerCase())){
                    benename=benealias;
                    output=beneaccount;
                 }
            }
            count=count+1;
        }
        
    return output;}
    
    public String resolvename_bank(List <String> words){
        String output="";
        String benealias="";
        String benebank="";
        String text_name="";
        int word;
        int count=0;
        Nlp_beneficiary nlpb;
        ArrayList objList = DataObjects.getNlp_beneficiary();
        while(count <objList.size()){
            nlpb =  (Nlp_beneficiary)objList.get(count);
            benealias = nlpb.getBenealias();
            benebank=nlpb.getBenebank();
            for(int l=0; l< words.size(); l++){
                if (words.get(l).trim().toLowerCase().equals(benealias.toLowerCase())){
                    benename=benealias;
                    output=benebank;
                 }
            }
            count=count+1;
        }
        
    return output;}
    
    public String detectaccount(List <String> words){
        int word;
        String acc="";
        try{
            for(int l=0; l< words.size(); l++){
                if(words.get(l).length()>=10){
                    word= Integer.parseInt(words.get(l).trim().substring(0,10));
                    acc=words.get(l).trim();
                    account_marker=l;
                }
            }
        }catch(Exception e){
            acc="";
        }
    return acc;}
   
    public String Loadbanks( List <String> words){
        String output="";
        String alias="";
        String bank_name="";
        int count=0;
        Nlp_banks nlpb;
        ArrayList objList = DataObjects.getNlp_banks();
        while(count <objList.size()){
            nlpb =  (Nlp_banks)objList.get(count);
            alias = nlpb.getAlias();
            bank_name=nlpb.getBank_name();
            for(int l=0; l< words.size(); l++){
               //System.out.println("word "+words.get(l)); 
                if (words.get(l).trim().toLowerCase().equals(alias.toLowerCase())){
                    output=bank_name;
                    bank_marker=l;
                }
            }
            count=count+1;
        }
        
    
      
return output;}
    
    public void detectName(List <String> words){
        int word;
        String text_name="";
        for(int l=0; l< words.size(); l++){
          if(l==bank_marker||l==account_marker||l==amt_marker||l==action_marker||l==amt_prefix_marker||l==currency_marker||words.get(l).trim().equals("to")){
                l=l+1;  
          }else{
              if(!words.get(l).trim().toLowerCase().equals("bank")){
                benename=words.get(l).trim();
              }
          } 
        }
    }
    
    public String detectAction(List <String> words){
        String action="";
        int word;
        String text_name="";
        for(int l=0; l< words.size(); l++){
          if(words.get(l).trim().equals("to")){
            action="FundsTransfer";  
          }else if(words.get(l).trim().equals("from")){
             action="FundsRequest";
          } 
        }
    return action;}
}
