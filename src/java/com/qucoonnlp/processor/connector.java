/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoonnlp.processor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class connector {
     public String langprocessor(String text) throws JSONException{
        String inputtext;
        String responseString;
        String finalString;
        langprocessor nlpOBJ = new langprocessor();
        JSONObject queryOBJ = new JSONObject(text);
        inputtext=queryOBJ.getString("inputtext");
        responseString= nlpOBJ.runDictionaryCheck(inputtext).toString();
        responseString = "["+responseString+"]";
        JSONArray response = new JSONArray(responseString);
        queryOBJ.remove("response");
        finalString = queryOBJ.toString().substring(0, queryOBJ.toString().lastIndexOf("}"));
        finalString = finalString+",\"response\": "+response+"}";
        //JSONObject outputOBJ = new JSONObject(finalString);      
    
        
        return finalString;}
    
}
