/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoonnlp.processor;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.POST;
import org.json.JSONException;
/**
 *
 * @author User001
 */
@Path("/services")
public class service {
    @POST
    @Path("/langprocessor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String langprocessor(String text){
        connector connct = new connector();
        String Output="";
        try{
            Output=connct.langprocessor(text);
        }
        catch(Exception e){

        }
    return Output;
    }
    
    @POST
    @Path("/test")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String test(String text){
    
    return text;}
    
}
