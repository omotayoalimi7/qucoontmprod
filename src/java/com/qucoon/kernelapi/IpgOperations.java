/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.kernelapi;


/**
 *
 * @author User001
 */
public class IpgOperations {
    
    public String addcard(String source, String transid, String sessid,String phonenumber,String cardno,String pin,String expiry,String cvv,String cardname,String country,String currency){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgaddcard(source, transid, sessid, phonenumber, cardno, pin, expiry, cvv, cardname, country, currency);
    return response;
    }
    
    public String enrolphonewithcard(String source, String transid, String sessid,String phonenumber,String enrolphonenumber,String transactionref,String cid){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgenrolphonewithcard(source, transid, sessid, phonenumber, enrolphonenumber, transactionref, cid);
    return response;
    }
    
    public String addcardvalidate(String source, String transid, String sessid,String phonenumber,String cid,String otp){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgaddcardvalidate(source, transid, sessid, phonenumber, cid, otp);
    return response;
    }
    
    public String balance(String source, String transid, String sessid,String phonenumber,String cid){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgbalance(source, transid, sessid, phonenumber, cid);
    return response;
    }
    
    public String balancevalidate(String source, String transid, String sessid,String phonenumber,String cid,String otp){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgbalancevalidate(source, transid, sessid, phonenumber, cid, otp);
    return response;
    }
    
    public String cards(String source, String transid, String sessid,String phonenumber){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgcards(source, transid, sessid, phonenumber);
    return response;
    }
    
    public String removecard(String source, String transid, String sessid,String phonenumber,String cid){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgremovecard(source, transid, sessid, phonenumber, cid);
    return response;
    }
    
    public String debitcard(String source, String transid, String sessid,String phonenumber,String amount,String cid,String cardname,String country,String currency){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgdebitcard(source, transid, sessid, phonenumber, amount, cid, cardname, country, currency);
    return response;
    }
    
    public String debitcardvalidate(String source, String transid, String sessid,String phonenumber,String transactionref,String amount,String cid,String cardname,String country,String currency,String otp,String paymentid){
       kernel kn = new kernel(); 
       String response="";
       response=kn.ipgdebitcardvalidate(source, transid, sessid, phonenumber,transactionref, amount, cid, cardname, country, currency,otp,paymentid);
                                         
       return response;
    }
    
    public static void main(String args[]){
        IpgOperations ipg = new IpgOperations();
        System.out.println(ipg.debitcardvalidate("USSD", "01234567890", "01234567890", "2347066353204", "20171515120838118267", "102", "4b119d1d-235d-4851-8113-5694e9bbe78d", "Tayo Olukoya", "NG", "NGN", "256524",""));
    }
    
   
}
