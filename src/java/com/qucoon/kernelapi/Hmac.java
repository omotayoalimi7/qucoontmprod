/*
 * To change this license header, choose License Hmac in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.kernelapi;
//import com.utilities.PropsReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.time.Instant;
import java.util.Formatter;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
/**
 *
 * @author User001
 */
public class Hmac {
    public static void main(String[] args){
        String dataenc = "{\"pin\":\"6689\",\"expiry\":\"2003\",\"transactionid\":\"000\",\"cardname\":\"Alimi Omotayo\",\"cvv\":\"854\",\"currency\":\"NGN\",\"phonenumber\":\"07038901111\",\"requestor\":\"TELEBOT\",\"cardno\":\"5370100311349442\",\"country\":\"NG\",\"passcode\":\"1111\",\"sessionid\":\"000\"}";
        String key="ukRTG!QQNve8V2gErxFglltc$7_*y1ov";
        Hmac enc = new Hmac();
        System.out.println(enc.getHmac(dataenc,key));
     }


    private static final String HMAC_SHA512 = "HmacSHA512";

    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    public static String computeHMAC(String data, String key)
        throws SignatureException, NoSuchAlgorithmException, InvalidKeyException
    {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), HMAC_SHA512);
        Mac mac = Mac.getInstance(HMAC_SHA512);
        mac.init(secretKeySpec);
        return toHexString(mac.doFinal(data.getBytes()));
    }

    public String getHmac(String data, String key) {
        String hmac="";
        String secretkey = key;//PropsReader.getProperty(key);
        try{
         hmac = computeHMAC(data, secretkey);
        }catch(Exception s){
            s.printStackTrace();
        }
        return hmac;
    }
    
}
