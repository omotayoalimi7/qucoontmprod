/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.kernelapi;

import com.qucoon.hbng.ConnectionClient;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONObject;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;

/**
 *
 * @author User001
 */
public class kernel {
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(kernel.class);
    public String callService_jersey(String url, JSONObject requestbody) {
        String output = "";
        UUID uuid = UUID.randomUUID();
        String nonce = uuid.toString().replaceAll("-", "");
        String username = "QUCOONTM";
        String password = "K89irjru.8jdm546%4";
        String authcipher = username + ":" + password;
        String authcipher64 = new String(Base64.encodeBase64(authcipher.getBytes()));
        // Timestamp
        TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
        Calendar calendar = Calendar.getInstance(lagosTimeZone);
        long timestamp = calendar.getTimeInMillis() / 1000;
        System.out.println("Timestamp: "+timestamp);
        System.out.println("Timestamp: "+authcipher64);
        try {
            String signature =Hmac.computeHMAC(requestbody.toString(), "ukRTG!QQNve8V2gErxFglltc$7_*y1ov");
            Client client = Client.create();
            WebResource webResource = client
                    .resource("http://kernel.octopuszone.com/prod/api/"+url);
            ClientResponse response = webResource.type("application/json")
                    .header("Timestamp", timestamp)
                    .header("Nonce", nonce)
                    .header("Signature", signature)
                    .header("Authorization","Basic "+authcipher64)
                    .header("Terminal","QUCOONTM")
                    .post(ClientResponse.class, requestbody.toString());
           //System.out.println("Output from Server .... \n");
            output = response.getEntity(String.class);
            System.out.println(response.getStatus()+": Output from kernel "+"\n");
            System.out.println(output);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
    
    public String callService(String url, JSONObject requestbody) {
        String output = "";
        try {
            UUID uuid = UUID.randomUUID();
            String nonce = uuid.toString().replaceAll("-", "");
            String username = "QUCOONTM";
            String password = "K89irjru.8jdm546%4";
            String authcipher = username + ":" + password;
            String authcipher64 = new String(Base64.encodeBase64(authcipher.getBytes()));
            // Timestamp
            TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
            Calendar calendar = Calendar.getInstance(lagosTimeZone);
            long timestamp = calendar.getTimeInMillis() / 1000;
            System.out.println("Timestamp: "+timestamp);
            System.out.println("Timestamp: "+authcipher64);
            String signature =Hmac.computeHMAC(requestbody.toString(), "ukRTG!QQNve8V2gErxFglltc$7_*y1ov");
            OkHttpClient client = ConnectionClientkernel.getSecureHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType,requestbody.toString());
            Request request = new Request.Builder()
                .url("http://kernel.octopuszone.com/prod/api/" + url)
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("Timestamp", timestamp+"")
                .addHeader("Nonce", nonce)
                .addHeader("Signature", signature)
                .addHeader("Authorization","Basic "+authcipher64)
                .addHeader("Terminal","QUCOONTM")
                .build();
            Response response = client.newCall(request).execute();
           
//            System.out.println("Output from Server .... \n");
            output = response.body().string();
            System.out.println(response.code()+": Output from kernel "+"\n");
//            System.out.println(output);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
    
    public String registeroctopus(String req){
        String output = "";
        try {
            JSONObject requestbody = new JSONObject(req);
            System.out.println("request \n"+requestbody);
            output = callService("registeroctopus", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    return output;}
    
    public String customerstatus(String req){
        String output = "";
        
        try {
            JSONObject requestbody = new JSONObject(req);
            System.out.println("request \n"+requestbody);
            output = callService("customerstatus", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    return output;}
    
    
    public String etzregisteruser() {
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", "USSD");
            requestbody.put("transactionid", "1929391000009");
            requestbody.put("sessionid", "29293959000041");
            requestbody.put("passcode", "011");
            requestbody.put("fistname", "Bayo");
            requestbody.put("lastname", "Muyiwa");
            requestbody.put("phonenumber", "08037551898");
            requestbody.put("emailaddress", "muyiwa@gmail.com");
            requestbody.put("authorization", "383838jjkk3893jjkjk389");
            output = callService("etzregisteruser", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String etzinitialize(String source,String transid,String sessid,String bank,String acno,String amt,String desc,String phone) {
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("bank", bank);
            requestbody.put("accountnumber", acno);
            requestbody.put("amount", amt);
            requestbody.put("description", desc);
            requestbody.put("phonenumber", phone);
            requestbody.put("authorization", Authorization.getAuth());
            System.out.println("initialize req: "+ requestbody.toString());
            output = callService("etzinitialize", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String etzsendaccount(String source,String transid,String sessid,String accountid,String phone ,String passcode) {
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("accountid", accountid);
            requestbody.put("phonenumber", phone);
            requestbody.put("passcode", passcode);
            requestbody.put("authorization", Authorization.getAuth());
            output = callService("etzsendaccount", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
    
    public String etzreceivetoken(String source,String transid,String sessid,String phone) {
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phone);
            requestbody.put("authorization", Authorization.getAuth());
            output = callService("etzreceivetoken", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
    
    public String etzmakepayment(String source, String transid,String sessid,String phone,String esacode) {
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phone);
            requestbody.put("esacode", esacode);
            requestbody.put("authorization", Authorization.getAuth());
            System.out.println("request :"+requestbody.toString());
            output = callService("etzmakepayment", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
    
    
    public String ipgaddcard(String source, String transid, String sessid,String phonenumber,String cardno,String pin,String expiry,String cvv,String cardname,String country,String currency){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("cardno", cardno);
            requestbody.put("pin", pin);
            requestbody.put("expiry", expiry);
            requestbody.put("cvv", cvv);
            requestbody.put("cardname", cardname);
            requestbody.put("country", country);
            requestbody.put("currency", currency);
            System.out.println(requestbody);
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgaddcard", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
    
    public String ipgenrolphonewithcard(String source, String transid, String sessid,String phonenumber,String enrolphonenumber,String transactionref,String cid){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("enrolphone", enrolphonenumber);
            requestbody.put("transactionref", transactionref);
            requestbody.put("cid", cid);
            System.out.println(requestbody);
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgenrolphonewithcard", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
    
    
    public String ipgaddcardvalidate(String source, String transid, String sessid,String phonenumber,String cid,String otp){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("cid", cid);
            requestbody.put("otp", otp);
            System.out.println("request:\n"+requestbody.toString());
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgaddcardvalidate", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String ipgbalance(String source, String transid, String sessid,String phonenumber,String cid){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("cid", cid);
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgbalance", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String ipgbalancevalidate(String source, String transid, String sessid,String phonenumber,String cid,String otp){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("cid", cid);
            requestbody.put("otp", otp);
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgbalancevalidate", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String ipgcards(String source, String transid, String sessid,String phonenumber){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgcards", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String ipgremovecard(String source, String transid, String sessid,String phonenumber,String cid){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("cid", cid);
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgremovecard", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String ipgdebitcard(String source, String transid, String sessid,String phonenumber,String amount,String cid,String cardname,String country,String currency){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("amount", amount);
            requestbody.put("cid", cid);
            requestbody.put("cardname", cardname);
            requestbody.put("country", country);
            requestbody.put("currency", currency);
            //requestbody.put("authorization", Authorization.getAuth());
            output = callService("ipgdebitcard", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public String ipgdebitcardvalidate(String source, String transid, String sessid,String phonenumber,String transactionRef,String amount,String cid,String cardname,String country,String currency,String otp,String paymentid){
        String output = "";
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("transactionRef", transactionRef);
            requestbody.put("amount", amount);
            requestbody.put("cid", cid);
            requestbody.put("cardname", cardname);
            requestbody.put("country", country);
            requestbody.put("currency", currency);
            requestbody.put("otp", otp);
            requestbody.put("paymentId", paymentid);
            //requestbody.put("authorization", Authorization.getAuth());
            System.out.println("request\n"+requestbody.toString());
            output = callService("ipgdebitcardvalidate", requestbody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    
    public static void main(String args[]) {
        kernel kn = new kernel();
        //System.out.println(kn.ipgaddcard("USSD","012345678900","012345678900","07038901111","5370100311349442","6689","2003","854","Alimi Omotayo","NG","NGN"));
        //System.out.println(kn.ipgaddcardvalidate("USSD","0123456789090","0123456789090","07038901111","cee8ece5-25d5-48f8-95c4-161091ae35f2","667220"));
        System.out.println(kn.ipgbalance("USSD","01234567890","01234567890","07038901111","b54c8543-10c4-412c-a6e3-5bff2baf3c74"));
        //System.out.println(kn.ipgcards("USSD","01234567890","01234567890","07038901111"));
        //System.out.println(kn.ipgdebitcard("USSD","012345678900","012345678900","07038901111","102","ef55215f-1fe1-4738-8a95-275adc7e4753","Tayo Alimi","NG","NGN"));
        JSONObject gh = new JSONObject();
        try {
            gh.put("test", "test");
        } catch (JSONException ex) {
            
        }
        //System.out.println(kn.callService("", gh));
    
    }

}

