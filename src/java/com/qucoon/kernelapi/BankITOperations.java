/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.kernelapi;

import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class BankITOperations {
    public String initialize(String source,String transid,String sessid,String bank,String acno,String amt,String desc,String phone){
        String output="";
        String kernelresponse_str="";
        String responsecode="";
        kernel kn = new kernel();
        JSONObject requestbody = new JSONObject();
        JSONObject kernelresponse ;
        JSONArray accountslist = new JSONArray();
        try {
            //etz initialize
            kernelresponse_str=kn.etzinitialize(source,transid,sessid,bank,acno,amt,desc,phone);
            kernelresponse= new JSONObject(kernelresponse_str);
            accountslist = kernelresponse.getJSONArray("accounts");
            
        }catch(Exception e){
            e.printStackTrace();
        }
    return accountslist.toString();}
    
    public String sendDebit(String source,String transid,String sessid,String accountid,String phone,String passcode,String acno,String amt,String desc){
        String output="";
        String kernelresponse_str="";
        String responsecode="";
        String responsemessage="";
        String otpcode="";
        kernel kn = new kernel();
        JSONObject requestbody = new JSONObject();
        JSONObject kernelresponse;
        JSONObject returnjson = new JSONObject();
         try {
             //etz initialize
             kernelresponse_str=kn.etzinitialize(source,transid,sessid,accountid,acno,amt,desc,phone);
             System.out.println("initialize success");
            // etz send account 
            kernelresponse_str=kn.etzsendaccount(source,transid,sessid,accountid,phone,passcode);
            System.out.println(kernelresponse_str);
            kernelresponse= new JSONObject(kernelresponse_str);
            responsecode= kernelresponse.getString("responsecode");//etz send account responsecode
            responsemessage= kernelresponse.getString("responsemessage");
            System.out.println("send act success");
            System.out.println("res code "+ kernelresponse.toString());
            if(responsecode.equals("Q03")){
                // receive otp
                kernelresponse_str=kn.etzreceivetoken(source,transid,sessid,phone);
                
                kernelresponse= new JSONObject(kernelresponse_str);
                
                responsecode= kernelresponse.getString("responsecode");
                System.out.println("receive token "+kernelresponse.toString());
                if(responsecode.equals("00")){
                    otpcode=kernelresponse.getString("otpcode");
                    System.out.println(" token "+kernelresponse.toString());
                    //makepayment
                    kernelresponse_str=kn.etzmakepayment(source,transid,sessid,phone,otpcode);
                    kernelresponse= new JSONObject(kernelresponse_str);
                    responsecode= kernelresponse.getString("responsecode");
                    System.out.println(" make payment "+responsecode+kernelresponse.toString());
                    if(responsecode.equals("00")){
                        returnjson.put("responsedesc", "debit successful");
                        returnjson.put("responsecode", "00");
                        returnjson.put("bankitresponse", kernelresponse);
                    }else{
                        //cannot make payment
                        returnjson.put("responsedesc", "cannot make etz debit");
                        returnjson.put("responsecode", "11");
                        returnjson.put("bankitresponse", kernelresponse);
                    }
                }else{
                    //cannot receive otp
                    returnjson.put("responsedesc", "cannot receive otp");
                    returnjson.put("responsecode", "22");
                    returnjson.put("bankitresponse", kernelresponse);
                }
            }else{
                //cannot send account
                returnjson.put("responsedesc", "cannot send account ");
                returnjson.put("responsecode", "33");
                returnjson.put("bankitresponse", kernelresponse);
            }
            output=returnjson.toString();
            
        }catch(Exception e){
            e.printStackTrace();
        }
    return output;}
    
    
    public static void main(String args[]) {
       BankITOperations bio = new BankITOperations();
       //System.out.println(bio.initialize("USSD","1234567890987654","1234567890987654","223","0023113070","1","TEST PAYMENT","07038901111"));
       System.out.println(bio.sendDebit("USSD","1234567890987654587","1234567890987654587","538","08185771056","1111","2091021397","1","TEST PAYMENT"));
       
    }
}
