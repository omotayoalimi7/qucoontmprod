/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.isw;

/**
 *
 * @author User001
 */
public class QtServices {
    public String payBill(String source,String transid,String sessid,String paymentcode, String custid,String phonenumber,String amt, String unqref){
        String iswresponse="";
        iswServices isw = new iswServices();
        iswresponse=isw.sendAdviceRequest(source,transid,sessid,paymentcode, custid, phonenumber, amt, unqref);
    return iswresponse;}
    
    public String doTransfer(String source,String transid,String sessid,String amount,String bankcode,String craccountno,String benelastname,String beneothername, String senderphone,String senderemail,String senderothername,String senderlastname, String unqref){
        String iswresponse="";
        iswServices isw = new iswServices();
        iswresponse=isw.doTransfer(source,transid,sessid,amount, bankcode, craccountno, benelastname, beneothername, senderphone, senderemail, senderothername, senderlastname, unqref);
    return iswresponse;}
    
    public String queryTransaction(String phonenumber,String reqref){
        String iswresponse="";
        iswServices isw = new iswServices();
        iswresponse=isw.queryTransaction(phonenumber, reqref);
    return iswresponse;}
    
    public String customerValidation(String phonenumber,String customerid,String paymentcode){
        String iswresponse="";
        iswServices isw = new iswServices();
        iswresponse=isw.customerValidation(phonenumber, customerid, paymentcode);
    return iswresponse;}
}
