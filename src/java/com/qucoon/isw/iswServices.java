/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.isw;

import com.qucoon.dbo.Debitlogs;
import com.qucoon.dbo.Octopusqttransactions;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.Tmlogs;
import com.qucoon.operations.PropsReader;
import com.qucoon.operations.Utilities;
import com.qucoon.slacklogger.SlackLogMessage;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONArray;

/**
 *
 * @author User001
 */
public class iswServices {
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(iswServices.class);
    static String qucktellerbaseurl=PropsReader.getProperty("quicktellerbaseurl");
    SlackLogMessage log = new SlackLogMessage();
    String appname="QucconTM";
    String classname="iswServices";
    public static void main(String args[]){
        iswServices isws = new iswServices();
        //System.out.println(isws.customerValidation("07038901111","1040575719","10401"));
       System.out.println(isws.returnPaymentItems("109"));
        //System.out.println(isws.doTransfer("10000","050","0023113070","Alimi","Ishola","07038901111","tayo.alimi@qucoon.com","Ishola","Alimi","1528000027"));
        //System.out.println(isws.sendAdviceRequest("MOBILE","","","90806","08099444111","08099444111","20000","152803000079"));
    }
    public String returnCategorys(){
        String output="";
        try{
        // Timestamp
        TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
        Calendar calendar = Calendar.getInstance(lagosTimeZone);
        long timestamp = calendar.getTimeInMillis() / 1000;
        // Nonce
        UUID uuid = UUID.randomUUID();
        String nonce = uuid.toString().replaceAll("-", "");
        // Signature Method
        String signatureMethod = "SHA1";
        // Signature
        String httpMethod = "GET"; // HTTP Method of the resource that is being called
        String encodedResourceUrl = qucktellerbaseurl+"categorys";// put the resource URL here
        encodedResourceUrl = URLEncoder.encode(encodedResourceUrl, "ISO-8859-1");
        
        String clientId = "IKIA9677668317335AAD2DEFDE0C2314AB0605AB43B2"; // put your client Id here
        String clientSecretKey = "0QEidNGcNZzgL7WbDz92FHieX916CuUUz2egklnvnLo="; // put your client secret here
        String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&" + timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;
        MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
        byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
        String signature = new String(Base64.encodeBase64(signatureBytes));
        String clientid64 = new String(Base64.encodeBase64(clientId.getBytes()));
        System.out.println(encodedResourceUrl);
        System.out.println(signature);
        System.out.println(nonce);
        //System.out.println(clientid64);
        System.out.println(timestamp);
        //return;
        Client client = Client.create();
        WebResource webResource = client
           .resource(qucktellerbaseurl+"categorys");
        ClientResponse response = webResource
                .header("TIMESTAMP", timestamp)
                .header("NONCE", nonce)
                .header("SIGNATUREMETHOD", signatureMethod)
                .header("SIGNATURE", signature)
                .header("AUTHORIZATION","InterswitchAuth "+clientid64)
                .header("CONTENT-TYPE","application/json")
                .header("TERMINALID","3QUO0001")
                .get(ClientResponse.class);

        System.out.println("Response Code \n"+response.getStatus()+"\n");
        System.out.println("Output from Server .... \n");
        output = response.getEntity(String.class);
        JSONObject json = new JSONObject(output);
        output=json.getString("categorys");
        System.out.println(output);
           }   
        catch(Exception e){  
            e.printStackTrace();
        }

    return output;}
    
    public String returnBillers(){
        String output="";
        try{
        // Timestamp
        TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
        Calendar calendar = Calendar.getInstance(lagosTimeZone);
        long timestamp = calendar.getTimeInMillis() / 1000;
        // Nonce
        UUID uuid = UUID.randomUUID();
        String nonce = uuid.toString().replaceAll("-", "");
        // Signature Method
        String signatureMethod = "SHA1";
        // Signature
        String httpMethod = "GET"; // HTTP Method of the resource that is being called
        String encodedResourceUrl = qucktellerbaseurl+"billers";// put the resource URL here
        encodedResourceUrl = URLEncoder.encode(encodedResourceUrl, "ISO-8859-1");
        
        String clientId = "IKIA9677668317335AAD2DEFDE0C2314AB0605AB43B2"; // put your client Id here
        String clientSecretKey = "0QEidNGcNZzgL7WbDz92FHieX916CuUUz2egklnvnLo="; // put your client secret here
        String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&" + timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;
        MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
        byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
        String signature = new String(Base64.encodeBase64(signatureBytes));
        String clientid64 = new String(Base64.encodeBase64(clientId.getBytes()));
        System.out.println(encodedResourceUrl);
        System.out.println(signature);
        System.out.println(nonce);
        //System.out.println(clientid64);
        System.out.println(timestamp);
        //return;
        Client client = Client.create();
        WebResource webResource = client
           .resource(qucktellerbaseurl+"billers");
        ClientResponse response = webResource
                .header("TIMESTAMP", timestamp)
                .header("NONCE", nonce)
                .header("SIGNATUREMETHOD", signatureMethod)
                .header("SIGNATURE", signature)
                .header("AUTHORIZATION","InterswitchAuth "+clientid64)
                .header("CONTENT-TYPE","application/json")
                .header("TERMINALID","3QUO0001")
                .get(ClientResponse.class);

        System.out.println("Response Code \n"+response.getStatus()+"\n");
        System.out.println("Output from Server .... \n");
        output = response.getEntity(String.class);
        JSONObject json = new JSONObject(output);
        output=json.getString("billers");
        System.out.println(output);
           }   
        catch(Exception e){  
            e.printStackTrace();
        }

    return output;}
    
    public String returnPaymentItems(String id){
        String output="";
        try{
        // Timestamp
        TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
        Calendar calendar = Calendar.getInstance(lagosTimeZone);
        long timestamp = calendar.getTimeInMillis() / 1000;
        // Nonce
        UUID uuid = UUID.randomUUID();
        String nonce = uuid.toString().replaceAll("-", "");
        // Signature Method
        String signatureMethod = "SHA1";
        // Signature
        String httpMethod = "GET"; // HTTP Method of the resource that is being called
        String encodedResourceUrl = qucktellerbaseurl+"billers/"+id+"/paymentitems";// put the resource URL here
        encodedResourceUrl = URLEncoder.encode(encodedResourceUrl, "ISO-8859-1");
        
        String clientId = "IKIA9677668317335AAD2DEFDE0C2314AB0605AB43B2"; // put your client Id here
        String clientSecretKey = "0QEidNGcNZzgL7WbDz92FHieX916CuUUz2egklnvnLo="; // put your client secret here
        String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&" + timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;
        MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
        byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
        String signature = new String(Base64.encodeBase64(signatureBytes));
        String clientid64 = new String(Base64.encodeBase64(clientId.getBytes()));
        System.out.println(encodedResourceUrl);
        System.out.println(signature);
        System.out.println(nonce);
        //System.out.println(clientid64);
        System.out.println(timestamp);
        //return;
        Client client = Client.create();
        WebResource webResource = client
           .resource(qucktellerbaseurl+"billers/"+id+"/paymentitems");
        ClientResponse response = webResource
                .header("TIMESTAMP", timestamp)
                .header("NONCE", nonce)
                .header("SIGNATUREMETHOD", signatureMethod)
                .header("SIGNATURE", signature)
                .header("AUTHORIZATION","InterswitchAuth "+clientid64)
                .header("CONTENT-TYPE","application/json")
                .header("TERMINALID","3QUO0001")
                .get(ClientResponse.class);
        System.out.println("Response Code \n"+response.getStatus()+"\n");
        System.out.println("Output from Server .... \n");
        output = response.getEntity(String.class);
       
        JSONObject json = new JSONObject(output);
        output=json.getString("paymentitems");
        System.out.println(output);
           }   
        catch(Exception e){  
            //e.printStackTrace();
            output="error";
        }

    return output;}
    
    public String sendAdviceRequest(String source,String transid,String sessid,String paymentcode, String custid,String phonenumber,String amt, String unqref){
        Octopusqttransactions qtrans = new Octopusqttransactions();
        String output="";
        Debitlogs dbl = new Debitlogs();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String status="";
        String serviceoutput="";
        JSONObject outputjson = new JSONObject();
        String qtresponsecode="";
        String qtstatus="";
        String rescode="";
        String MscData="";
        String rechargePIN="";
        amt = amt;
        try{
            String resp="{\n" +
            "\"TerminalId\":\"3QUO0001\",\n" +
            "\"paymentCode\":\""+paymentcode+"\",\n" +
            "\"customerId\":\""+custid+"\",\n" +
            "\"customerMobile\":\""+phonenumber+"\",\n" +
            "\"amount\":\""+amt+"\",\n" +
            "\"requestReference\":\""+unqref+"\"\n" +
            "}";
            System.out.println(resp);
            qtrans.setAmount(amt);
            qtrans.setQttransref(unqref);
            qtrans.setSessionid(sessid);
            qtrans.setSource(source);
            qtrans.setCustomerid(custid);
            qtrans.setPaymentcode(paymentcode);
            qtrans.setDescriprequest(resp);
            qtrans.setTransdate(utils.getDate()+" "+utils.getTime());
            qtrans.setTransactionid(transid);
            qtrans.setCreatedat(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(qtrans);
            dbl.setActiondetails("Qt pay bills request: "+resp+" ");
            dbl.setActionperformed("Qt pay bills request");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amt);
            dbl.setDestinationaccount(custid);
            dbl.setDestinationbank(paymentcode);
            dbl.setNarration("QT paybills "+custid+" "+paymentcode);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("");
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourcecard("");
            dbl.setSourcebank("");
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
            logger.info(phonenumber+"Qt pay bills request: "+resp);
            log.info(appname, classname, "Qt pay bills Request at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+resp);
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("Qt pay bills request");
            tmlog.setActiondetails("Qt pay bills request: "+resp);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            //return;
            // Timestamp
            TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
            Calendar calendar = Calendar.getInstance(lagosTimeZone);
            long timestamp = calendar.getTimeInMillis() / 1000;
            // Nonce
            UUID uuid = UUID.randomUUID();
            String nonce = uuid.toString().replaceAll("-", "");
            // Signature Method
            String signatureMethod = "SHA1";
            // Signature
            String httpMethod = "POST"; // HTTP Method of the resource that is being called
            String encodedResourceUrl = qucktellerbaseurl+"sendAdviceRequest";// put the resource URL here
            encodedResourceUrl = URLEncoder.encode(encodedResourceUrl, "ISO-8859-1");
            String clientId = "IKIA9677668317335AAD2DEFDE0C2314AB0605AB43B2"; // put your client Id here
            String clientSecretKey = "0QEidNGcNZzgL7WbDz92FHieX916CuUUz2egklnvnLo="; // put your client secret here
            String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&" + timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;
            MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
            byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
            String signature = new String(Base64.encodeBase64(signatureBytes));
            String clientid64 = new String(Base64.encodeBase64(clientId.getBytes()));
            System.out.println(encodedResourceUrl);
            System.out.println(signature);
            System.out.println(nonce);
            //System.out.println(clientid64);
            System.out.println(timestamp);
            //return;
            Client client = Client.create();
            WebResource webResource = client
               .resource(qucktellerbaseurl+"sendAdviceRequest");
            client.setConnectTimeout(300);
            ClientResponse response = webResource
                    .header("TIMESTAMP", timestamp)
                    .header("NONCE", nonce)
                    .header("SIGNATUREMETHOD", signatureMethod)
                    .header("SIGNATURE", signature)
                    .header("AUTHORIZATION","InterswitchAuth "+clientid64)
                    .header("CONTENT-TYPE","application/json")
                    .header("TERMINALID","3QUO0001")
                    .post(ClientResponse.class,resp);
            logger.info(phonenumber+ "new bill payment");
            System.out.println("Response Code \n"+response.getStatus()+"\n");
            System.out.println("Output from Server .... \n");
            logger.info(phonenumber+ "server response "+response.getStatus());
            qtrans.setResponsecode(response.getStatus()+"");
            PersistenceAgent.logTransactions(qtrans);
            serviceoutput = response.getEntity(String.class);
            qtrans.setDescrip(serviceoutput);
            PersistenceAgent.logTransactions(qtrans);
            log.info(appname, classname, "Qt pay bills Response at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+" "+response.getStatus()+" "+serviceoutput);
            dbl.setActionperformed("Qt pay bills response");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amt);
            dbl.setDestinationaccount(custid);
            dbl.setDestinationbank(paymentcode);
            dbl.setNarration("QT paybills "+custid+" "+paymentcode);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("");
            dbl.setSource(source);
            dbl.setSourcecard("");
            dbl.setSourcebank("");
            dbl.setTransactionid(transid);
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("Qt pay bills response");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            JSONObject serviceoutputjson = new JSONObject(serviceoutput);
            System.out.println(serviceoutputjson.toString());
            rescode="11";
            outputjson.put("responsecode", "11");
            if(serviceoutputjson.has("transactionRef")){
                rescode="00";
                String transactionRef=serviceoutputjson.getString("transactionRef");
                qtrans.setQttransref(transactionRef);
                PersistenceAgent.logTransactions(qtrans);
                outputjson.put("transactionRef", transactionRef);
                outputjson.put("responsecode", "00");
            }
            if(serviceoutputjson.has("MscData")){
                MscData=serviceoutputjson.getString("MscData");
                qtrans.setMiscdata(MscData);
                PersistenceAgent.logTransactions(qtrans);
                outputjson.put("MscData", MscData);
            }
            if(serviceoutputjson.has("rechargePIN")){
                rechargePIN=serviceoutputjson.getString("rechargePIN");
                qtrans.setRechargepins(rechargePIN);
                PersistenceAgent.logTransactions(qtrans);
                outputjson.put("rechargePIN", rechargePIN);
            }
            if(serviceoutputjson.has("transactionResponseCode")){
                String responseCode=serviceoutputjson.getString("transactionResponseCode");
                qtresponsecode=responseCode;
                qtrans.setTransresponsecode(responseCode);
                PersistenceAgent.logTransactions(qtrans);
                dbl.setActiondetails("Qt pay bills response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput+" ");
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
                tmlog.setActiondetails("Qt pay bills response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                logger.info(phonenumber+"Qt pay bills response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                if(responseCode.equals("90000")){
                    rescode="00";
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    if(serviceoutputjson.has("transactionRef")){
                        String transactionRef=serviceoutputjson.getString("transactionRef");
                        qtrans.setQttransref(transactionRef);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                        String transferCodee=serviceoutputjson.getString("transferCode");
                        outputjson.put("transferCode", transferCodee);
                        qtrans.setQttransref(transferCodee);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                    
                }else if(responseCode.equals("90009")||responseCode.equals("900A0")){
                    rescode="00";
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("transactionRef")){
                        String transactionRef=serviceoutputjson.getString("transactionRef");
                        qtrans.setQttransref(transactionRef);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                        String transferCodee=serviceoutputjson.getString("transferCode");
                        outputjson.put("transferCode", transferCodee);
                        qtrans.setQttransref(transferCodee);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                }else{
                    qtrans.setStatus("failed");
                    PersistenceAgent.logTransactions(qtrans);
                    rescode="11";
                    outputjson.put("status", "failed");
                    outputjson.put("responsecode", "11");
                    outputjson.put("qtresponsecode", responseCode);
                    
                }
            }else if(serviceoutputjson.has("responseCode")){
                String responseCode=serviceoutputjson.getString("responseCode");
                qtresponsecode=responseCode;
                dbl.setActiondetails("Qt pay bills response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput+" ");
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
                tmlog.setActiondetails("Qt pay bills response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                logger.info(phonenumber+"Qt pay bills response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                if(responseCode.equals("90000")){
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    if(serviceoutputjson.has("transactionRef")){
                        String transactionRef=serviceoutputjson.getString("transactionRef");
                        qtrans.setQttransref(transactionRef);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                        String transferCodee=serviceoutputjson.getString("transferCode");
                        outputjson.put("transferCode", transferCodee);
                        qtrans.setQttransref(transferCodee);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                }else if(responseCode.equals("90009")||responseCode.equals("900A0")){
                    rescode="00";
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("transactionRef")){
                        String transactionRef=serviceoutputjson.getString("transactionRef");
                        qtrans.setQttransref(transactionRef);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                        String transferCodee=serviceoutputjson.getString("transferCode");
                        outputjson.put("transferCode", transferCodee);
                        qtrans.setQttransref(transferCodee);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                }else{
                    qtrans.setStatus("failed");
                    PersistenceAgent.logTransactions(qtrans);
                    rescode="11";
                    outputjson.put("status", "failed");
                    outputjson.put("responsecode", "11");
                    outputjson.put("qtresponsecode", responseCode);
                }
            }
            
            System.out.println(serviceoutput);
            output=outputjson.toString();
            log.info(appname, classname, "Qt do Transfer Request at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+outputjson.toString());
        }   
        catch(Exception e){  
            logger.info(phonenumber+ "error: "+e.getMessage());
            output="{\"status\":\"error\",\"responsecode\":\""+rescode+"\",\"qtresponsecode\":\""+qtresponsecode+"\",\"MscData\":\""+MscData+"\"}";
            dbl.setActiondetails("Qt pay bills response: "+qtresponsecode+" "+serviceoutput+" "+output);
            dbl.setActionperformed("Qt pay bills response");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amt);
            dbl.setDestinationaccount(custid);
            dbl.setDestinationbank(paymentcode);
            dbl.setNarration("QT paybills "+custid+" "+paymentcode);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("");
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourcecard("");
            dbl.setSourcebank("");
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
            logger.info(phonenumber+"Qt pay bills response: "+qtresponsecode+" "+serviceoutput+" "+output);
            log.error(appname, classname, "pay Bill Request -error parsing json at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+serviceoutput+" "+output+" "+e.getMessage());
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("Qt pay bills response");
            tmlog.setActiondetails("Qt pay bills response: "+qtresponsecode+" "+serviceoutput+" "+output);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
        }

    return output;}
     
    public String  queryTransaction(String phonenumber,String reqref){
        String output="";
        String status="";
        String serviceoutput="";
        JSONObject outputjson = new JSONObject();
        try{
            // Timestamp
            TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
            Calendar calendar = Calendar.getInstance(lagosTimeZone);
            long timestamp = calendar.getTimeInMillis() / 1000;
            // Nonce
            UUID uuid = UUID.randomUUID();
            String nonce = uuid.toString().replaceAll("-", "");
            // Signature Method
            String signatureMethod = "SHA1";
            // Signature
            String httpMethod = "GET"; // HTTP Method of the resource that is being called
            String encodedResourceUrl = qucktellerbaseurl+"transactions?requestreference="+reqref;// put the resource URL here
            encodedResourceUrl = URLEncoder.encode(encodedResourceUrl, "ISO-8859-1");
            String clientId = "IKIA9677668317335AAD2DEFDE0C2314AB0605AB43B2"; // put your client Id here
            String clientSecretKey = "0QEidNGcNZzgL7WbDz92FHieX916CuUUz2egklnvnLo="; // put your client secret here
            String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&" + timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;
            MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
            byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
            String signature = new String(Base64.encodeBase64(signatureBytes));
            String clientid64 = new String(Base64.encodeBase64(clientId.getBytes()));
            System.out.println(encodedResourceUrl);
            System.out.println(signature);
            System.out.println(nonce);
            //System.out.println(clientid64);
            System.out.println(timestamp);
            //return;

            Client client = Client.create();
            WebResource webResource=null;

            webResource=
               client.resource(qucktellerbaseurl+"transactions?requestreference="+reqref);
            ClientResponse response = webResource
                    .header("TIMESTAMP", timestamp)
                    .header("NONCE", nonce)
                    .header("SIGNATUREMETHOD", signatureMethod)
                    .header("SIGNATURE", signature)
                    .header("AUTHORIZATION","InterswitchAuth "+clientid64)
                    .header("CONTENT-TYPE","application/json")
                    .header("TERMINALID","3QUO0001")
                    .get(ClientResponse.class);
            logger.info(phonenumber+ "new query qt trans");
            System.out.println("Response Code \n"+response.getStatus()+"\n");
            System.out.println("Output from Server .... \n");
            logger.info(phonenumber+ "server response "+response.getStatus());
            serviceoutput = response.getEntity(String.class);
            JSONObject serviceoutputjson = new JSONObject(serviceoutput);
            System.out.println(serviceoutputjson.toString());
            status=serviceoutputjson.getString("status");
            String responsecode= serviceoutputjson.getString("transactionResponseCode");
            outputjson.put("transactionResponseCode", responsecode);
            outputjson.put("responsecode", "00");
            outputjson.put("status", status);
            output = outputjson.toString();
            System.out.println(output);
        }   
        catch(Exception e){  
            //e.printStackTrace();
            output="{\"status\":\"error\",\"responsecode\":\"99\"}";
            logger.info(phonenumber+ "error: "+e.getMessage());
        }

        return output;  
    }
    
    public String customerValidation(String phonenumber,String customerid,String paymentcode){
        String status="";
        String serviceoutput="";
        JSONObject outputjson = new JSONObject();
        String output="";
        JSONObject qtresponse = new JSONObject();
        String resp="{\n" +
        "\"customers\":\n" +
        "\n" +
        "   [\n" +
        "     {\n" +
        "        \"customerId\": \""+customerid+"\",\n" +
        "        \"paymentCode\": \""+paymentcode+"\"\n" +
        "    }\n" +
        "  ]\n" +
        "}";
        System.out.println(resp);
        try{
            // Timestamp
            TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
            Calendar calendar = Calendar.getInstance(lagosTimeZone);
            long timestamp = calendar.getTimeInMillis() / 1000;
            // Nonce
            UUID uuid = UUID.randomUUID();
            String nonce = uuid.toString().replaceAll("-", "");
            // Signature Method
            String signatureMethod = "SHA1";
            // Signature
            String httpMethod = "POST"; // HTTP Method of the resource that is being called
            String encodedResourceUrl = qucktellerbaseurl+"customers/validations";// put the resource URL here
            encodedResourceUrl = URLEncoder.encode(encodedResourceUrl, "ISO-8859-1");
            String clientId = "IKIA9677668317335AAD2DEFDE0C2314AB0605AB43B2"; // put your client Id here
            String clientSecretKey = "0QEidNGcNZzgL7WbDz92FHieX916CuUUz2egklnvnLo="; // put your client secret here
            String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&" + timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;
            MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
            byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
            String signature = new String(Base64.encodeBase64(signatureBytes));
            String clientid64 = new String(Base64.encodeBase64(clientId.getBytes()));
            System.out.println(encodedResourceUrl);
            System.out.println(signature);
            System.out.println(nonce);
            //System.out.println(clientid64);
            System.out.println(timestamp);
            //return;
            Client client = Client.create();
            System.setProperty("https.proxyHost", "192.168.100.90");
            System.setProperty("https.proxyPort", "8080");
            WebResource webResource=null;
            webResource=
               client.resource(qucktellerbaseurl+"customers/validations");
            ClientResponse response = webResource
                    .header("TIMESTAMP", timestamp)
                    .header("NONCE", nonce)
                    .header("SIGNATUREMETHOD", signatureMethod)
                    .header("SIGNATURE", signature)
                    .header("AUTHORIZATION","InterswitchAuth "+clientid64)
                    .header("CONTENT-TYPE","application/json")
                    .header("TERMINALID","3QUO0001")
                    .post(ClientResponse.class,resp);
            logger.info(phonenumber+ "new customer validation");
            System.out.println("Response Code \n"+response.getStatus()+"\n");
            System.out.println("Output from Server .... \n");
            logger.info(phonenumber+ "server response "+response.getStatus());
            serviceoutput = response.getEntity(String.class);
            JSONObject serviceoutputjson = new JSONObject(serviceoutput);
            System.out.println(serviceoutputjson.toString());
            JSONArray customers;
            JSONObject customer = new JSONObject();
            if(serviceoutputjson.has("Customers")){
                customers=serviceoutputjson.getJSONArray("Customers");
                customer=customers.getJSONObject(0);
                output=customer.getString("fullName");
                qtresponse.put("fullName", output);
                qtresponse.put("responsecode", "00");
                
            }else{
                qtresponse.put("responsecode", "11");
            }
            output=qtresponse.toString();
            //System.out.println(output);
        }   
        catch(Exception e){  
            //e.printStackTrace();
            logger.info(phonenumber+ "error: "+e.getMessage());
            output="{\"responsecode\":\"11\"}";
        }
    return output;}
    
    public String doTransfer(String source,String transid,String sessid,String amount,String bankcode,String craccountno,String benelastname,String beneothername, String senderphone,String senderemail,String senderothername,String senderlastname, String unqref){
        Octopusqttransactions qtrans = new Octopusqttransactions();
        String output="";
        String status="";
        Debitlogs dbl = new Debitlogs();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String serviceoutput="";
        JSONObject outputjson = new JSONObject();
        String qtresponsecode="";
        String qtstatus="";
        String rescode="";
        String MscData="";
        String rechargePIN="";
        String InitiatingAmount=amount;
        String InitiatingCurrencyCode="566";
        String InitiatingChannel="7"; 
        String InitiatingPaymentMethodCode="CA";
        String initiatingEntityCode="QUO"; 
        String TerminatingAmount=amount;
        String TerminationAccountno=craccountno;
        String TerminationAccountype="10";
        String TerminatingCurrencyCode="566";
        String TerminatingPaymentMethodCode ="AC";
        String TerminatingCountryCode="NG";
        String TerminatingEntityCode=bankcode;
        String firstname="chi";
        String lastname=benelastname;
        String othernames=beneothername;
        String senderEmail=senderemail;
        String senderLastname=senderlastname;
        String senderOthernames=senderothername;
        String senderPhone=senderphone; 
        String transferCode=unqref;
        amount=amount;
        try{
            // Timestamp
            TimeZone lagosTimeZone = TimeZone.getTimeZone("Africa/Lagos");
            Calendar calendar = Calendar.getInstance(lagosTimeZone);
            long timestamp = calendar.getTimeInMillis() / 1000;
            // Nonce
            UUID uuid = UUID.randomUUID();
            String nonce = uuid.toString().replaceAll("-", "");
            // Signature Method
            String signatureMethod = "SHA1";
            String macMethod = "SHA-512";
            // Signature
            String httpMethod = "POST"; // HTTP Method of the resource that is being called
            String encodedResourceUrl = qucktellerbaseurl+"fundsTransfer";// put the resource URL here
            encodedResourceUrl = URLEncoder.encode(encodedResourceUrl, "ISO-8859-1");
            String clientId = "IKIA9677668317335AAD2DEFDE0C2314AB0605AB43B2"; // put your client Id here
            String clientSecretKey = "0QEidNGcNZzgL7WbDz92FHieX916CuUUz2egklnvnLo="; // put your client secret here
            String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&" + timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;
            MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
            byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
            String signature = new String(Base64.encodeBase64(signatureBytes));
            String clientid64 = new String(Base64.encodeBase64(clientId.getBytes()));
            String mac_pre = InitiatingAmount+InitiatingCurrencyCode+InitiatingPaymentMethodCode+TerminatingAmount+TerminatingCurrencyCode+TerminatingPaymentMethodCode+TerminatingCountryCode ;
            MessageDigest messageDigest2 = MessageDigest.getInstance(macMethod);
            byte[] macBytes = messageDigest2.digest(mac_pre.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< macBytes.length ;i++){
               sb.append(Integer.toString((macBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            String mac = sb.toString();
            String beneficiary="{       \n" +
                        "\"lastname\": \""+lastname+"\",       \n" +
                        "\"othernames\": \""+othernames+"\"   \n" +
                        "}";
            String initiation="{     \n" +
                        "\"amount\": \""+InitiatingAmount+"\", \n" +
                        "\"channel\": \""+InitiatingChannel+"\",     \n" +
                        "\"currencyCode\": \""+InitiatingCurrencyCode+"\",     \n" +
                        "\"paymentMethodCode\": \""+InitiatingPaymentMethodCode+"\"   \n" +
                        "}";
            String sender="{     \n" +
                        "\"email\": \""+senderEmail+"\",     \n" +
                        "\"lastname\": \""+senderLastname+"\",     \n" +
                        "\"othernames\": \""+senderOthernames+"\",     \n" +
                        "\"phone\": \""+senderPhone+"\"   \n" +
                        "}";
            String termination="{     \n" +
                        "\"accountReceivable\":{       \n" +
                        "\"accountNumber\": \""+TerminationAccountno+"\",       \n" +
                        "\"accountType\": \""+TerminationAccountype+"\"     \n" +
                        "}, \n"+
                        "\"amount\": \""+TerminatingAmount+"\",     \n" +
                        "\"countryCode\": \""+TerminatingCountryCode+"\",     \n" +
                        "\"currencyCode\": \""+TerminatingCurrencyCode+"\",     \n" +
                        "\"entityCode\": \""+TerminatingEntityCode+"\",     \n" +
                        "\"paymentMethodCode\": \""+TerminatingPaymentMethodCode+"\"   \n" +
                        "}";
            JSONObject jsonresp = new JSONObject();
            String resp= "";
            jsonresp.put("mac", mac);
            jsonresp.put("initiatingEntityCode", initiatingEntityCode);
            jsonresp.put("transferCode", transferCode);

            resp = jsonresp.toString().substring(0,jsonresp.toString().length()-1)+",";
            //System.out.println(resp);
            resp=resp+"\"beneficiary\":"+beneficiary+",";
            resp=resp+"\"initiation\":"+initiation+",";
            resp=resp+"\"sender\":"+sender+",";
            resp=resp+"\"termination\":"+termination+"";
            resp = resp+"\n }";
            jsonresp = new JSONObject(resp);
            System.out.println(resp);
            log.info(appname, classname, "Qt do Transfer Request at"+utils.getDate()+" "+utils.getTime(), senderphone+" "+resp);
            qtrans.setAmount(amount);
            qtrans.setBenelastname(benelastname);
            qtrans.setBeneothernames(beneothername);
            qtrans.setCraccountno(craccountno);
            qtrans.setCrbankcode(bankcode);
            qtrans.setPhonenumber(senderphone);
            qtrans.setQttransref(unqref);
            qtrans.setSenderemail(senderEmail);
            qtrans.setSenderlastname(senderLastname);
            qtrans.setSenderothernames(senderOthernames);
            qtrans.setSenderphone(senderPhone);
            qtrans.setSessionid(sessid);
            qtrans.setSource(source);
            qtrans.setDescriprequest(resp);
            qtrans.setTransactionid(transid);
            qtrans.setTransdate(utils.getDate()+" "+utils.getTime());
            qtrans.setCreatedat(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(qtrans);
            dbl.setActiondetails("Qt do transfer request: "+resp+" ");
            dbl.setActionperformed("Qt do transfer request");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amount);
            dbl.setDestinationaccount(craccountno);
            dbl.setDestinationbank(bankcode);
            dbl.setNarration("QT do transfer "+craccountno+" "+bankcode);
            dbl.setPhonenumber(senderphone);
            dbl.setPreferredmedium("");
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourcecard("");
            dbl.setSourcebank("");
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
            logger.info(senderphone+"Qt do transfer request: "+resp);
            tmlog.setSource(source);
            tmlog.setPhonenumber(senderphone);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("Qt do transfer request");
            tmlog.setActiondetails("Qt do transfer request: "+resp);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            JSONObject respj = new JSONObject(resp);
            System.out.println(encodedResourceUrl);
            System.out.println(signature);
            System.out.println(nonce);
            //System.out.println(clientid64);
            System.out.println(timestamp);
            //return;
            Client client = Client.create();
            WebResource webResource = client
               .resource(qucktellerbaseurl+"fundsTransfer");
            client.setConnectTimeout(300);
            ClientResponse response = webResource
                    .header("TIMESTAMP", timestamp)
                    .header("NONCE", nonce)
                    .header("SIGNATUREMETHOD", signatureMethod)
                    .header("SIGNATURE", signature)
                    .header("AUTHORIZATION","InterswitchAuth "+clientid64)
                    .header("CONTENT-TYPE","application/json")
                    .header("TERMINALID","3QUO0001")
                    .post(ClientResponse.class,resp);
            System.out.println("Response Code \n"+response.getStatus()+"\n");
            System.out.println("Output from Server .... \n");
            logger.info(senderphone+ "server response "+response.getStatus());
            qtrans.setResponsecode(response.getStatus()+"");
            PersistenceAgent.logTransactions(qtrans);
            serviceoutput = response.getEntity(String.class);
            qtrans.setDescrip(serviceoutput);
            PersistenceAgent.logTransactions(qtrans);
            log.info(appname, classname, "Qt do Transfer Response at"+utils.getDate()+" "+utils.getTime(), senderphone+" "+" "+response.getStatus()+" "+serviceoutput);
            JSONObject serviceoutputjson = new JSONObject(serviceoutput);
            System.out.println(serviceoutputjson.toString());
            dbl.setActionperformed("Qt do transfer response");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amount);
            dbl.setDestinationaccount(craccountno);
            dbl.setDestinationbank(bankcode);
            dbl.setNarration("QT do transfer "+craccountno+" "+bankcode);
            dbl.setPhonenumber(senderphone);
            dbl.setPreferredmedium("");
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourcecard("");
            dbl.setSourcebank("");
            dbl.setTransactionid(transid);
            tmlog.setSource(source);
            tmlog.setPhonenumber(senderphone);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("Qt do transfer response");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            rescode="11";
            outputjson.put("responsecode", "11");
            if(serviceoutputjson.has("transactionRef")){
                rescode="00";
                String transactionRef=serviceoutputjson.getString("transactionRef");
                qtrans.setQttransref(transactionRef);
                PersistenceAgent.logTransactions(qtrans);
                outputjson.put("transactionRef", transactionRef);
                outputjson.put("responsecode", "00");
            }
            if(serviceoutputjson.has("MscData")){
                MscData=serviceoutputjson.getString("MscData");
                qtrans.setMiscdata(MscData);
                PersistenceAgent.logTransactions(qtrans);
                outputjson.put("MscData", MscData);
            }
            if(serviceoutputjson.has("rechargePIN")){
                rechargePIN=serviceoutputjson.getString("rechargePIN");
                qtrans.setRechargepins(rechargePIN);
                PersistenceAgent.logTransactions(qtrans);
                outputjson.put("rechargePIN", rechargePIN);
            }
            if(serviceoutputjson.has("transferCode")){
                rescode="00";
                String transactionRef=serviceoutputjson.getString("transferCode");
                qtrans.setQttransref(transactionRef);
                PersistenceAgent.logTransactions(qtrans);
                outputjson.put("transferCode", transactionRef);
                outputjson.put("responsecode", "00");
            }
            if(serviceoutputjson.has("transactionResponseCode")){
                String responseCode=serviceoutputjson.getString("transactionResponseCode");
                qtresponsecode=responseCode;
                qtrans.setTransresponsecode(responseCode);
                PersistenceAgent.logTransactions(qtrans);
                dbl.setActiondetails("Qt do transfer response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput+" ");
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
                tmlog.setActiondetails("Qt do transfer response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                logger.info(senderphone+"Qt do transfer response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                if(responseCode.equals("90000")){
                    if(serviceoutputjson.has("transactionRef")){
                        String transactionRef=serviceoutputjson.getString("transactionRef");
                        qtrans.setQttransref(transactionRef);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                        String transferCodee=serviceoutputjson.getString("transferCode");
                        outputjson.put("transferCode", transferCodee);
                        qtrans.setQttransref(transferCodee);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                    
                }else if(responseCode.equals("90009")||responseCode.equals("900A0")){
                    PersistenceAgent.logTransactions(qtrans);
                    outputjson.put("status", "processing");
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    if(serviceoutputjson.has("transactionRef")){
                        String transactionRef=serviceoutputjson.getString("transactionRef");
                        qtrans.setQttransref(transactionRef);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                        String transferCodee=serviceoutputjson.getString("transferCode");
                        outputjson.put("transferCode", transferCodee);
                        qtrans.setQttransref(transferCodee);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                }else{
                    qtrans.setStatus("failed");
                    PersistenceAgent.logTransactions(qtrans);
                    rescode="11";
                    outputjson.put("status", "failed");
                    outputjson.put("responsecode", "11");
                    outputjson.put("qtresponsecode", responseCode);
                    
                }
            }else if(serviceoutputjson.has("responseCode")){
                String responseCode=serviceoutputjson.getString("responseCode");
                qtresponsecode=responseCode;
                qtrans.setTransresponsecode(responseCode);
                PersistenceAgent.logTransactions(qtrans);
                dbl.setActiondetails("Qt do transfer response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput+" ");
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
                tmlog.setActiondetails("Qt do transfer response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                logger.info(senderphone+"Qt do transfer response: "+response.getStatus()+" "+qtresponsecode+" "+serviceoutput);
                if(responseCode.equals("90000")){
                    rescode="00";
                    if(serviceoutputjson.has("transactionRef")){
                    String transactionRef=serviceoutputjson.getString("transactionRef");
                    outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                    String transferCodee=serviceoutputjson.getString("transferCode");
                    outputjson.put("transferCode", transferCodee);
                    }
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                }else if(responseCode.equals("90009")||responseCode.equals("900A0")){
                    rescode="00";
                    outputjson.put("status", "processing");
                    outputjson.put("responsecode", "00");
                    outputjson.put("qtresponsecode", responseCode);
                    if(serviceoutputjson.has("status")){
                        qtstatus=serviceoutputjson.getString("status");
                        outputjson.put("status", qtstatus);
                        qtrans.setStatus(qtstatus);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                    if(serviceoutputjson.has("MscData")){
                        MscData=serviceoutputjson.getString("MscData");
                        qtrans.setMiscdata(MscData);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("MscData", MscData);
                    }
                    if(serviceoutputjson.has("rechargePIN")){
                        rechargePIN=serviceoutputjson.getString("rechargePIN");
                        qtrans.setRechargepins(rechargePIN);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("rechargePIN", rechargePIN);
                    }
                    if(serviceoutputjson.has("transactionRef")){
                        String transactionRef=serviceoutputjson.getString("transactionRef");
                        qtrans.setQttransref(transactionRef);
                        PersistenceAgent.logTransactions(qtrans);
                        outputjson.put("transactionRef", transactionRef);
                    }
                    if(serviceoutputjson.has("transferCode")){
                        String transferCodee=serviceoutputjson.getString("transferCode");
                        outputjson.put("transferCode", transferCodee);
                        qtrans.setQttransref(transferCodee);
                        PersistenceAgent.logTransactions(qtrans);
                    }
                }else{
                    qtrans.setStatus("failed");
                    PersistenceAgent.logTransactions(qtrans);
                    rescode="11";
                    outputjson.put("status", "failed");
                    outputjson.put("responsecode", "11");
                    outputjson.put("qtresponsecode", responseCode);
                }
            }
            log.info(appname, classname, "Qt do Transfer Request at"+utils.getDate()+" "+utils.getTime(), senderphone+" "+outputjson.toString());
            System.out.println(serviceoutput);
            output=outputjson.toString();
            System.out.println("qt output: "+output);
            
        }   
        catch(Exception e){  
            
            //output="error";
            output="{\"status\":\"error\",\"responsecode\":\""+rescode+"\",\"qtresponsecode\":\""+qtresponsecode+"\",\"MscData\":\""+MscData+"\"}";
            dbl.setActiondetails("Qt do transfer response: "+qtresponsecode+" "+serviceoutput+" "+output);
            dbl.setActionperformed("Qt do transfer response");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amount);
            dbl.setDestinationaccount(craccountno);
            dbl.setDestinationbank(bankcode);
            dbl.setNarration("QT do transfer "+craccountno+" "+bankcode);
            dbl.setPhonenumber(senderphone);
            dbl.setPreferredmedium("");
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourcecard("");
            dbl.setSourcebank("");
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
            logger.info(senderphone+"Qt do transfer response: "+qtresponsecode+" "+serviceoutput+" "+output);
            log.error(appname, classname, "do Transfer Request -error parsing json at"+utils.getDate()+" "+utils.getTime(), senderphone+" "+serviceoutput+" "+output+" "+e.getMessage());
            tmlog.setSource(source);
            tmlog.setPhonenumber(senderphone);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("Qt do transfer response");
            tmlog.setActiondetails("Qt do transfer response: "+qtresponsecode+" "+serviceoutput+" "+output);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            logger.info(senderphone+ "error: "+e.getMessage());
        }

    return output;}
    
    
}
