/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.dbo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User001
 */
public class DataObjects {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       List<Moviescinema> obj;
       obj = DataObjects.getMoviescinema("1");
       Moviescinema b=obj.get(0);
       System.out.println(b.getCinemaname());
        
    }
    
    
    public static StateManager getStatemanager(String reference){
        reference = (reference != null)?reference.trim() :"";
        String query = "from StateManager s where s.reference = :reference";
            StateManager obj = new StateManager();
            obj.setReference(reference);
            List<StateManager> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (StateManager)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
      
    public static Octopuscustomerstm getOctopuscustomerstm_rec(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscustomerstm s where s.phonenumber = :phonenumber";
            Octopuscustomerstm obj = new Octopuscustomerstm();
            obj.setPhonenumber(phonenumber);
            List<Octopuscustomerstm> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerstm)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
    public static List<Octopuspasswordreset> getOctopuspasswordreset (String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuspasswordreset  s where s.phonenumber = :phonenumber";
            Octopuspasswordreset  obj = new Octopuspasswordreset ();
            obj.setPhonenumber(phonenumber);
            List<Octopuspasswordreset > objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuspasswordreset )objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
        }
    
    
    public static Octopuscustomersaccounts getOctopuscustomersaccounts_acc(String accountnumber){
        accountnumber = (accountnumber != null)?accountnumber.trim() :"";
        String query = "from Octopuscustomersaccounts s where s.accountnumber = :accountnumber";
            Octopuscustomersaccounts obj = new Octopuscustomersaccounts();
            obj.setAccountnumber(accountnumber);
            List<Octopuscustomersaccounts> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomersaccounts)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
       
    public static List<Octopuscustomersaccounts> getOctopuscustomersaccounts_rec(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscustomersaccounts s where s.phonenumber = :phonenumber";
            Octopuscustomersaccounts obj = new Octopuscustomersaccounts();
            obj.setPhonenumber(phonenumber);
            List<Octopuscustomersaccounts> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomersaccounts)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
      
    public static List<Octopuscustomerstm> getOctopuscustomerstm(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscustomerstm s where s.phonenumber = :phonenumber";
            Octopuscustomerstm obj = new Octopuscustomerstm();
            obj.setPhonenumber(phonenumber);
            List<Octopuscustomerstm> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerstm)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
      
    public static List<Octopuscustomersaccounts> getOctopuscustomersaccounts(String accountnumber){
        accountnumber = (accountnumber != null)?accountnumber.trim() :"";
        String query = "from Octopuscustomersaccounts s where s.accountnumber = :accountnumber";
            Octopuscustomersaccounts obj = new Octopuscustomersaccounts();
            obj.setAccountnumber(accountnumber);
            List<Octopuscustomersaccounts> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomersaccounts)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
      
    public static List<Bankcodes> getBankcodes(){
        String query = "from Bankcodes ";
           
            List<Bankcodes> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadQuery(query);
                
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static Bankcodes getBankcodes_code(String bankcode){
        String query = "from Bankcodes s where s.bankcode = :bankcode";
           Bankcodes obj = new Bankcodes();
            obj.setBankcode(bankcode);
            List<Bankcodes> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadQuery(query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Bankcodes)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
      
    
    public static List<Octopuscustomerstm> getOctopuscustomerstm_email(String emailaddress)
    {
        emailaddress = (emailaddress != null)?emailaddress.trim() :"";
        String query = "from Octopuscustomerstm s where s.emailaddress = :emailaddress";
            Octopuscustomerstm obj = new Octopuscustomerstm();
            obj.setEmailaddress(emailaddress);
            List<Octopuscustomerstm> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerstm)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopusblacklist> getOctopusblacklist(String phonenumber)
    {
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopusblacklist s where s.phonenumber = :phonenumber";
            Octopusblacklist obj = new Octopusblacklist();
            obj.setPhonenumber(phonenumber);
            List<Octopusblacklist> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusblacklist)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static Octopusblacklist getOctopusblacklist_one(String phonenumber)
    {
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopusblacklist s where s.phonenumber = :phonenumber";
            Octopusblacklist obj = new Octopusblacklist();
            obj.setPhonenumber(phonenumber);
            List<Octopusblacklist> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusblacklist)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
    
    public static List<Octopusinboxtransactions> getOctopusinboxtransactions(String senderid){
        senderid = (senderid != null)?senderid.trim() :"";
        String query = "from Octopusinboxtransactions s where s.senderid = :senderid";
            Octopusinboxtransactions obj = new Octopusinboxtransactions();
            obj.setSenderid(senderid);
            List<Octopusinboxtransactions> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusinboxtransactions)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopusblacklist> getOctopusblacklist_personal(String blacklistedby){
        blacklistedby = (blacklistedby != null)?blacklistedby.trim() :"";
        String query = "from Octopusblacklist s where s.blacklistedby = :blacklistedby";
            Octopusblacklist obj = new Octopusblacklist();
            obj.setBlacklistedby(blacklistedby);
            List<Octopusblacklist> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusblacklist)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
      
    public static Octopusinboxtransactions getOctopusinboxtransactions_rec(String id_s){
        id_s = (id_s != null)?id_s.trim() :"";
        int id = Integer.parseInt(id_s);
        String query = "from Octopusinboxtransactions s where s.id = :id";
            Octopusinboxtransactions obj = new Octopusinboxtransactions();
            obj.setId(id);
            List<Octopusinboxtransactions> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusinboxtransactions)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
      
    public static List<Beneficiaries> getBeneficiaries(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
            
        String query = "from Beneficiaries s where s.phonenumber = :phonenumber";
            Beneficiaries obj = new Beneficiaries();
            obj.setPhonenumber(phonenumber);
            List<Beneficiaries> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
//                if(objList != null){
//                    if(!objList.isEmpty()){
//                   obj =  (Beneficiaries)objList.get(0);
//                    }
//                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
      
    public static List<Movies> getMovies(String cinemaid){
        cinemaid = (cinemaid != null)?cinemaid.trim() :"";
        String query = "from Movies s where s.cinemaid = :cinemaid";
            Movies obj = new Movies();
            obj.setCinemaid(cinemaid);
            List<Movies> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Movies)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Moviescinema> getMoviescinema(String cityid){
        cityid = (cityid != null)?cityid.trim() :"";
        String query = "from Moviescinema s where s.cityid = :cityid";
            Moviescinema obj = new Moviescinema();
            obj.setCityid(cityid);
            List<Moviescinema> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Moviescinema)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Moviescity> getMoviescity(){
        String query = "from Moviescity ";
            Moviescity obj = new Moviescity();
            //obj.setCityid(cityid);
            List<Moviescity> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Moviescity)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Movieslist> getMovies_rec(String movieid){
        movieid = (movieid != null)?movieid.trim() :"";
        String query = "from Movieslist s where s.movieid = :movieid";
            Movieslist obj = new Movieslist();
            obj.setMovieid(movieid);
            List<Movieslist> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Movieslist)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
        }
    
    public static Movieslist getMovieswithviewid(String viewid){
        viewid = (viewid != null)?viewid.trim() :"";
        String query = "from Movieslist s where s.viewid = :viewid";
            Movieslist obj = new Movieslist();
            obj.setViewid(viewid);
            List<Movieslist> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Movieslist)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
    public static List<Octopusexchangerates> getOctopusexchangerates(){
        //phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopusexchangerates";
            Octopusexchangerates obj = new Octopusexchangerates();
            //obj.setPhonenumber(phonenumber);
            List<Octopusexchangerates> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusexchangerates)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopusparties> getOctopusparties(){
        //phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopusparties";
            Octopusparties obj = new Octopusparties();
            //obj.setPhonenumber(phonenumber);
            List<Octopusparties> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusparties)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopusparties> getOctopusparties_owner(String partyownerid){
        partyownerid = (partyownerid != null)?partyownerid.trim() :"";
        String query = "from Octopusparties s where s.partyownerid = :partyownerid";
            Octopusparties obj = new Octopusparties();
            obj.setPartyownerid(partyownerid);
            List<Octopusparties> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusparties)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopuspartymembers> getOctopusparties_member(String memberid){
        memberid = (memberid != null)?memberid.trim() :"";
        String query = "from Octopuspartymembers s where s.memberid = :memberid";
            Octopuspartymembers obj = new Octopuspartymembers();
            obj.setMemberid(memberid);
            List<Octopuspartymembers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuspartymembers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopuspartymembers> getOctopusparties_mymembers(String partyid){
        partyid = (partyid != null)?partyid.trim() :"";
        String query = "from Octopuspartymembers s where s.partyid = :partyid";
            Octopuspartymembers obj = new Octopuspartymembers();
            obj.setPartyid(partyid);
            List<Octopuspartymembers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuspartymembers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    
    public static Octopusparties getOctopusparties_id(String partyid){
        partyid = (partyid != null)?partyid.trim() :"";
        String query = "from Octopusparties s where s.partyid = :partyid";
            Octopusparties obj = new Octopusparties();
            obj.setPartyid(partyid);
            List<Octopusparties> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopusparties)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
    
    public static List<Octopuspartymembers> getOctopuspartiesmembers_id(String partyid){
        partyid = (partyid != null)?partyid.trim() :"";
        String query = "from Octopuspartymembers s where s.partyid = :partyid";
            Octopuspartymembers obj = new Octopuspartymembers();
            obj.setPartyid(partyid);
            List<Octopuspartymembers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuspartymembers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
     
    public static List<Octopuscommunities> getOctopuscommunities(){
        //phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscommunities";
            Octopuscommunities obj = new Octopuscommunities();
            //obj.setPhonenumber(phonenumber);
            List<Octopuscommunities> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscommunities)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static Octopuscommunities getOctopuscommunities_id(String commid){
        commid = (commid != null)?commid.trim() :"";
        String query = "from Octopuscommunities s where s.commid = :commid";
            Octopuscommunities obj = new Octopuscommunities();
            obj.setCommid(commid);
            List<Octopuscommunities> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscommunities)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
    
    public static List<Octopuscommunitymembers> getOctopuscommunitiesmembers_id(String commid){
        commid = (commid != null)?commid.trim() :"";
        String query = "from Octopuscommunitymembers s where s.commid = :commid";
            Octopuscommunitymembers obj = new Octopuscommunitymembers();
            obj.setCommid(commid);
            List<Octopuscommunitymembers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscommunitymembers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static Octopuscommunities getOctopuscommunities_name(String commname){
        commname = (commname != null)?commname.trim() :"";
        String query = "from Octopuscommunities s where s.commname = :commname";
            Octopuscommunities obj = new Octopuscommunities();
            obj.setCommname(commname);
            List<Octopuscommunities> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscommunities)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
    
    public static List<Octopuscommunitymembers> getOctopuscommunitymembers_member(String memberid){
        memberid = (memberid != null)?memberid.trim() :"";
        String query = "from Octopuscommunitymembers s where s.memberid = :memberid";
            Octopuscommunitymembers obj = new Octopuscommunitymembers();
            obj.setMemberid(memberid);
            List<Octopuscommunitymembers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscommunitymembers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopuscommunities> getOctopuscommunities_owner(String commownerid){
        commownerid = (commownerid != null)?commownerid.trim() :"";
        String query = "from Octopuscommunities s where s.commownerid = :commownerid";
            Octopuscommunities obj = new Octopuscommunities();
            obj.setCommownerid(commownerid);
            List<Octopuscommunities> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscommunities)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    public static List<Octopuscommunitymembers> getOctopuscommunities_mymembers(String commid){
        commid = (commid != null)?commid.trim() :"";
        String query = "from Octopuscommunitymembers s where s.commid = :commid";
            Octopuscommunitymembers obj = new Octopuscommunitymembers();
            obj.setCommid(commid);
            List<Octopuscommunitymembers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscommunitymembers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }

    public static List<Octopuscustomerscards> getOctopuscustomerscards_rec(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscustomerscards s where s.phonenumber = :phonenumber";
            Octopuscustomerscards obj = new Octopuscustomerscards();
            obj.setPhonenumber(phonenumber);
            List<Octopuscustomerscards> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerscards)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<iswCategorys> getiswCategorys(){
        //phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from iswCategorys s ";
            iswCategorys obj = new iswCategorys();
            //obj.setPhonenumber(phonenumber);
            List<iswCategorys> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswCategorys)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<iswBillers> getiswBillers(){
        //phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from iswBillers s ";
            iswBillers obj = new iswBillers();
            //obj.setPhonenumber(phonenumber);
            List<iswBillers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswBillers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<iswPaymentitems> getiswPaymentitems(){
        //phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from iswPaymentitems s ";
            iswPaymentitems obj = new iswPaymentitems();
            //obj.setPhonenumber(phonenumber);
            List<iswPaymentitems> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswPaymentitems)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<iswPaymentitems> getiswPaymentitems_billerid(String biller_id){
        biller_id = (biller_id != null)?biller_id.trim() :"";
        String query = "from iswPaymentitems s where s.biller_id = :biller_id";
            iswPaymentitems obj = new iswPaymentitems();
            obj.setBiller_id(biller_id);
            List<iswPaymentitems> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswPaymentitems)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    
    public static List<iswPaymentitems> getiswPaymentitems_paymentcode(String paymentcode){
        paymentcode = (paymentcode != null)?paymentcode.trim() :"";
        String query = "from iswPaymentitems s where s.paymentcode = :paymentcode";
            iswPaymentitems obj = new iswPaymentitems();
            obj.setPaymentcode(paymentcode);
            List<iswPaymentitems> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswPaymentitems)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
     public static iswPaymentitems getiswPaymentitems_paymentcodeObject(String paymentcode){
        paymentcode = (paymentcode != null)?paymentcode.trim() :"";
        String query = "from iswPaymentitems s where s.paymentcode = :paymentcode";
            iswPaymentitems obj = new iswPaymentitems();
            obj.setPaymentcode(paymentcode);
            List<iswPaymentitems> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswPaymentitems)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
    
    public static List<iswPaymentitems> getiswPaymentitems_recharge(String category_id){
        category_id = (category_id != null)?category_id.trim() :"";
        String query = "from iswPaymentitems s where s.category_id = :category_id";
            iswPaymentitems obj = new iswPaymentitems();
            obj.setCategory_id(category_id);
            List<iswPaymentitems> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswPaymentitems)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<iswBillers> getiswBillers_billerid(String category_id){
        category_id = (category_id != null)?category_id.trim() :"";
        String query = "from iswBillers s where s.category_id = :category_id";
            iswBillers obj = new iswBillers();
            obj.setCategory_id(category_id);
            List<iswBillers> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (iswBillers)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static Octopuscustomerscards getOctopuscustomerscards_cid(String cardid){
        cardid = (cardid != null)?cardid.trim() :"";
        String query = "from Octopuscustomerscards s where s.cardid = :cardid";
            Octopuscustomerscards obj = new Octopuscustomerscards();
            obj.setCardid(cardid);
            List<Octopuscustomerscards> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerscards)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
    public static Octopuspndtrans getOctopuspndtrans(String transactionref){
        transactionref = (transactionref != null)?transactionref.trim() :"";
        String query = "from Octopuspndtrans s where s.transactionref = :transactionref";
            Octopuspndtrans obj = new Octopuspndtrans();
            obj.setTransactionref(transactionref);
            List<Octopuspndtrans> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuspndtrans)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
    public static List<Octopuscustomerstmpending> getOctopuscustomerstmpending_email(String emailaddress)
    {
        emailaddress = (emailaddress != null)?emailaddress.trim() :"";
        String query = "from Octopuscustomerstmpending s where s.emailaddress = :emailaddress";
            Octopuscustomerstmpending obj = new Octopuscustomerstmpending();
            obj.setEmailaddress(emailaddress);
            List<Octopuscustomerstmpending> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerstmpending)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static List<Octopuscustomerstmpending> getOctopuscustomerstmpending(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscustomerstmpending s where s.phonenumber = :phonenumber";
            Octopuscustomerstmpending obj = new Octopuscustomerstmpending();
            obj.setphonenumber(phonenumber);
            List<Octopuscustomerstmpending> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerstmpending)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static Octopuscustomerstmpending getOctopuscustomerstmpending_rec(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscustomerstmpending s where s.phonenumber = :phonenumber";
            Octopuscustomerstmpending obj = new Octopuscustomerstmpending();
            obj.setphonenumber(phonenumber);
            List<Octopuscustomerstmpending> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscustomerstmpending)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
    }
     public static Telegram_users getTelegram_user(String chatid){
        chatid = (chatid != null)?chatid.trim() :"";
        String query = "from Telegram_users s where s.chatid = :chatid";
            Telegram_users obj = new Telegram_users();
            obj.setChatid(chatid);
            List<Telegram_users> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Telegram_users)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
   
    public static List<Telegram_users> getTelegram_users(String phoneno){
        phoneno = (phoneno != null)?phoneno.trim() :"";
        String query = "from Telegram_users s where s.phoneno = :phoneno";
            Telegram_users obj = new Telegram_users();
            obj.setPhoneno(phoneno);
            List<Telegram_users> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Telegram_users)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
    
    public static State_manager_telegram getCurrentTelegram_user(String webid){
        webid = (webid != null)?webid.trim() :"";
        String query = "from State_manager_telegram s where s.webid = :webid";
            State_manager_telegram obj = new State_manager_telegram();
            obj.setWebid(webid);
            List<State_manager_telegram> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (State_manager_telegram)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
    
    public static Facebook_users getFacebook_user(String chatid){
        chatid = (chatid != null)?chatid.trim() :"";
        String query = "from Facebook_users s where s.chatid = :chatid";
            Facebook_users obj = new Facebook_users();
            obj.setChatid(chatid);
            List<Facebook_users> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Facebook_users)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
   
    public static List<Facebook_users> getFacebook_users(String phoneno){
        phoneno = (phoneno != null)?phoneno.trim() :"";
        String query = "from Facebook_users s where s.phoneno = :phoneno";
            Facebook_users obj = new Facebook_users();
            obj.setPhoneno(phoneno);
            List<Facebook_users> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Facebook_users)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
      
    public static State_manager_facebook getCurrentFacebook_user(String webid){
        webid = (webid != null)?webid.trim() :"";
        String query = "from State_manager_facebook s where s.webid = :webid";
            State_manager_facebook obj = new State_manager_facebook();
            obj.setWebid(webid);
            List<State_manager_facebook> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (State_manager_facebook)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return obj;
        }
    
    public static List<Octopuscompletetransactions> getOctopuscompletetransactions(String phonenumber){
        phonenumber = (phonenumber != null)?phonenumber.trim() :"";
        String query = "from Octopuscompletetransactions s where s.phonenumber = :phonenumber";
            Octopuscompletetransactions obj = new Octopuscompletetransactions();
            obj.setPhonenumber(phonenumber);
            List<Octopuscompletetransactions> objList = null;
            try{
                objList = (ArrayList)PersistenceAgent.loadTableRecordsQuery(obj, query);
                if(objList != null){
                    if(!objList.isEmpty()){
                   obj =  (Octopuscompletetransactions)objList.get(0);
                    }
                }
            }catch(Exception s){
                s.printStackTrace();
            }
            return objList;
    }
     
    

}
