package com.qucoon.dbo;
// Generated 16-May-2017 00:53:04 by Hibernate Tools 4.3.1



/**
 * StateManager generated by hbm2java
 */
public class Octopusdelcustomerscards  implements java.io.Serializable {


     private Integer id;
     private String phonenumber;
     private String firstname;
     private String lastname;
     private String cardid;
     private String cardno;
     private String cardtype;
     private String cardbrand;
     private String cardcategory;
     private String cardcountry;
     private String cardcountrycode;
     private String cardbank;
     private String cardname;
     private String recordstatus;
     private String createdat;
     private String updatedat;

    public Octopusdelcustomerscards() {
    }

    public Octopusdelcustomerscards(String phonenumber, String firstname,String lastname, String cardid,String cardname,String cardno,String cardtype,String recordstatus,String createdat,String updatedat) {
       this.phonenumber = phonenumber;
       this.firstname = firstname;
       this.lastname=lastname;
       this.cardid = cardid;
       this.cardname= cardname;
       this.cardno=cardno;
       this.cardtype=cardtype;
       this.recordstatus=recordstatus;
       this.createdat=createdat;
       this.updatedat=updatedat;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getPhonenumber() {
        return this.phonenumber;
    }
    
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getCardid() {
        return this.cardid;
    }
    
    public void setCardid(String cardid) {
        this.cardid = cardid;
    }
    
    public String getCardname() {
        return this.cardname;
    }
    
    public void setCardname(String cardname) {
        this.cardname = cardname;
    }
    public String getFirstname() {
        return this.firstname;
    }

    public String getCardno() {
        return this.cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCardtype() {
        return this.cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
    public String getRecordstatus() {
        return this.recordstatus;
    }

    public void setRecordstatus(String recordstatus) {
        this.recordstatus = recordstatus;
    }

    public String getCreatedat() {
        return this.createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getUpdatedat() {
        return this.updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getCardbrand() {
        return this.cardbrand;
    }

    public void setCardbrand(String cardbrand) {
        this.cardbrand = cardbrand;
    }

    public String getCardcategory() {
        return this.cardcategory;
    }

    public void setCardcategory(String cardcategory) {
        this.cardcategory = cardcategory;
    }

    public String getCardcountry() {
        return this.cardcountry;
    }

    public void setCardcountry(String cardcountry) {
        this.cardcountry = cardcountry;
    }

    public String getCardcountrycode() {
        return this.cardcountrycode;
    }

    public void setCardcountrycode(String cardcountrycode) {
        this.cardcountrycode = cardcountrycode;
    }

    public String getCardbank() {
        return this.cardbank;
    }

    public void setCardbank(String cardbank) {
        this.cardbank = cardbank;
    }
    
    

    public static void main(String args[]){
        Octopusdelcustomerscards tm = new Octopusdelcustomerscards();
        tm.setPhonenumber("ussd");
        PersistenceAgent.logTransactions(tm);
    }

}


