/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.dbo;

/**
 *
 * @author User001
 */
public class Requests {
    public String updateState(String unqref, String presentaction, String nextaction,String responsecode,String responsedesc,String updated_at){
        StateManager sm = DataObjects.getStatemanager(unqref);
        sm.setPresentaction(presentaction);
        sm.setResponsecode(responsecode);
        sm.setResponsedesc(responsedesc);
        sm.setNextaction(nextaction);
        sm.setUpdated_at(updated_at);
        PersistenceAgent.logTransactions(sm);
        
    return"";}
    
    public String createState(String unqref, String presentaction, String nextaction,String responsecode,String responsedesc,String created_at,String updated_at){
        StateManager s = new StateManager();
        s.setReference(unqref);
        s.setPresentaction(presentaction);
        s.setNextaction(nextaction);
        s.setResponsecode(responsecode);
        s.setResponsedesc(responsedesc);
        s.setCreated_at(created_at);
        s.setUpdated_at(updated_at);
        PersistenceAgent.logTransactions(s);
        
    return"";}
    
    public String getNextaction(String unqref){
        String nextaction="";
        StateManager sm = DataObjects.getStatemanager(unqref);
        nextaction=sm.getNextaction();
        if(nextaction==null){
            nextaction="none";
        }
    return nextaction;}
    
    
    
    public static void main(String args[]){
        Requests rt = new Requests();
        System.out.println(rt.createState("","","","","","",""));
    }
}
