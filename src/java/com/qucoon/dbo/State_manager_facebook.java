package com.qucoon.dbo;
// Generated 16-May-2017 00:53:04 by Hibernate Tools 4.3.1



/**
 * StateManager generated by hbm2java
 */
public class State_manager_facebook  implements java.io.Serializable {


    private Integer id;
    private String sessionid;
    private String chatid;
    private String phoneno;
    private String presentaction;
    private String nextaction;
    private String responsecode;
    private String responsedesc;
    private String rechargephoneno;
    private String rechargeamt;
    private String beneaccount;
    private String benebank;
    private String benename;
    private String ftamt;
    private String registersource;
    private String registerdestination;
    private String categoryname;
    private String categoryid;
    private String billername;
    private String billerid;
    private String paymentitem;
    private String paymentcode;
    private String billerref;
    private String billamt;
    private String billitemfee;
    private String inboxsno;
    private String movieviewid;
    private String movieprice;
    private String moviename;
    private String moviecinemaname;
    private String partyid;
    private String partyownerid;
    private String partyownername;
    private String partyname;
    private String partydescription;
    private String partyvenue;
    private String partyfee;
    private String partyimage;
    private String partytaggingalong;
    private String commid;
    private String commownerid;
    private String commownername;
    private String commname;
    private String commwelcomemsg;
    private String commdescription;
    private String commtype;
    private String commentry;
    private String commimage;
    private String commnotification;
    private String status;
    private String debitaccount;
    private String debitbankcode;
    private String debitcid;
    private String transactionref;
    private String webid;
    private String created_at;
    private String updated_at;
    private String benebankcode;

    

    public State_manager_facebook() {
    }

    public State_manager_facebook(String sessionid, String chatid, String phoneno, String presentaction, String nextaction, String responsecode, String responsedesc, String rechargephoneno, String rechargeamt, String beneaccount, String benebank, String benename, String ftamt, String registersource, String registerdestination, String categoryname, String categoryid, String billername, String billerid, String paymentitem, String paymentcode, String billerref, String billamt, String billitemfee, String inboxsno, String movieviewid, String movieprice, String moviename, String moviecinemaname, String partyid, String partyownerid, String partyownername, String partyname, String partydescription, String partyvenue, String partyfee, String partyimage, String partytaggingalong, String commid, String commownerid, String commownername, String commname, String commwelcomemsg, String commdescription, String commtype, String commentry, String commimage, String commnotification, String status, String debitaccount, String debitbankcode, String debitcid, String transactionref, String webid, String created_at, String updated_at, String benebankcode) {
        this.sessionid=sessionid;
        this.chatid=chatid;
        this.phoneno=phoneno;
        this.presentaction=presentaction;
        this.nextaction=nextaction;
        this.responsecode=responsecode;
        this.responsedesc=responsedesc;
        this.rechargephoneno=rechargephoneno;
        this.rechargeamt=rechargeamt;
        this.beneaccount=beneaccount;
        this.benebank=benebank;
        this.benename=benename;
        this.ftamt=ftamt;
        this.registersource=registersource;
        this.registerdestination=registerdestination;
        this.categoryname=categoryname;
        this.categoryid=categoryid;
        this.billername=billername;
        this.billerid=billerid;
        this.paymentitem=paymentitem;
        this.paymentcode=paymentcode;
        this.billerref=billerref;
        this.billamt=billamt;
        this.billitemfee=billitemfee;
        this.inboxsno=inboxsno;
        this.movieviewid=movieviewid;
        this.movieprice=movieprice;
        this.moviename=moviename;
        this.moviecinemaname=moviecinemaname;
        this.partyid=partyid;
        this.partyownerid=partyownerid;
        this.partyownername=partyownername;
        this.partyname=partyname;
        this.partydescription=partydescription;
        this.partyvenue=partyvenue;
        this.partyfee=partyfee;
        this.partyimage=partyimage;
        this.partytaggingalong=partytaggingalong;
        this.commid=commid;
        this.commownerid=commownerid;
        this.commownername=commownername;
        this.commname=commname;
        this.commwelcomemsg=commwelcomemsg;
        this.commdescription=commdescription;
        this.commtype=commtype;
        this.commentry=commentry;
        this.commimage=commimage;
        this.commnotification=commnotification;
        this.status=status;
        this.debitaccount=debitaccount;
        this.debitbankcode=debitbankcode;
        this.debitcid=debitcid;
        this.transactionref=transactionref;
        this.webid=webid;
        this.created_at=created_at;
        this.updated_at=updated_at;
        this.benebankcode=benebankcode;


    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getSessionid() {
        return this.sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getChatid() {
        return this.chatid;
    }

    public void setChatid(String chatid) {
        this.chatid = chatid;
    }

    public String getPhoneno() {
        return this.phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getPresentaction() {
        return this.presentaction;
    }

    public void setPresentaction(String presentaction) {
        this.presentaction = presentaction;
    }

    public String getNextaction() {
        return this.nextaction;
    }

    public void setNextaction(String nextaction) {
        this.nextaction = nextaction;
    }

    public String getResponsecode() {
        return this.responsecode;
    }

    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    public String getResponsedesc() {
        return this.responsedesc;
    }

    public void setResponsedesc(String responsedesc) {
        this.responsedesc = responsedesc;
    }

    public String getRechargephoneno() {
        return this.rechargephoneno;
    }

    public void setRechargephoneno(String rechargephoneno) {
        this.rechargephoneno = rechargephoneno;
    }

    public String getRechargeamt() {
        return this.rechargeamt;
    }

    public void setRechargeamt(String rechargeamt) {
        this.rechargeamt = rechargeamt;
    }

    public String getBeneaccount() {
        return this.beneaccount;
    }

    public void setBeneaccount(String beneaccount) {
        this.beneaccount = beneaccount;
    }

    public String getBenebank() {
        return this.benebank;
    }

    public void setBenebank(String benebank) {
        this.benebank = benebank;
    }

    public String getBenename() {
        return this.benename;
    }

    public void setBenename(String benename) {
        this.benename = benename;
    }

    public String getFtamt() {
        return this.ftamt;
    }

    public void setFtamt(String ftamt) {
        this.ftamt = ftamt;
    }

    public String getRegistersource() {
        return this.registersource;
    }

    public void setRegistersource(String registersource) {
        this.registersource = registersource;
    }

    public String getRegisterdestination() {
        return this.registerdestination;
    }

    public void setRegisterdestination(String registerdestination) {
        this.registerdestination = registerdestination;
    }

    public String getCategoryname() {
        return this.categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getCategoryid() {
        return this.categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getBillername() {
        return this.billername;
    }

    public void setBillername(String billername) {
        this.billername = billername;
    }

    public String getBillerid() {
        return this.billerid;
    }

    public void setBillerid(String billerid) {
        this.billerid = billerid;
    }

    public String getPaymentitem() {
        return this.paymentitem;
    }

    public void setPaymentitem(String paymentitem) {
        this.paymentitem = paymentitem;
    }

    public String getPaymentcode() {
        return this.paymentcode;
    }

    public void setPaymentcode(String paymentcode) {
        this.paymentcode = paymentcode;
    }

    public String getBillerref() {
        return this.billerref;
    }

    public void setBillerref(String billerref) {
        this.billerref = billerref;
    }

    public String getBillamt() {
        return this.billamt;
    }

    public void setBillamt(String billamt) {
        this.billamt = billamt;
    }

    public String getBillitemfee() {
        return this.billitemfee;
    }

    public void setBillitemfee(String billitemfee) {
        this.billitemfee = billitemfee;
    }

    public String getInboxsno() {
        return this.inboxsno;
    }

    public void setInboxsno(String inboxsno) {
        this.inboxsno = inboxsno;
    }

    public String getMovieviewid() {
        return this.movieviewid;
    }

    public void setMovieviewid(String movieviewid) {
        this.movieviewid = movieviewid;
    }

    public String getMovieprice() {
        return this.movieprice;
    }

    public void setMovieprice(String movieprice) {
        this.movieprice = movieprice;
    }

    public String getMoviename() {
        return this.moviename;
    }

    public void setMoviename(String moviename) {
        this.moviename = moviename;
    }

    public String getMoviecinemaname() {
        return this.moviecinemaname;
    }

    public void setMoviecinemaname(String moviecinemaname) {
        this.moviecinemaname = moviecinemaname;
    }

    public String getPartyid() {
        return this.partyid;
    }

    public void setPartyid(String partyid) {
        this.partyid = partyid;
    }

    public String getPartyownerid() {
        return this.partyownerid;
    }

    public void setPartyownerid(String partyownerid) {
        this.partyownerid = partyownerid;
    }

    public String getPartyownername() {
        return this.partyownername;
    }

    public void setPartyownername(String partyownername) {
        this.partyownername = partyownername;
    }

    public String getPartyname() {
        return this.partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getPartydescription() {
        return this.partydescription;
    }

    public void setPartydescription(String partydescription) {
        this.partydescription = partydescription;
    }

    public String getPartyvenue() {
        return this.partyvenue;
    }

    public void setPartyvenue(String partyvenue) {
        this.partyvenue = partyvenue;
    }

    public String getPartyfee() {
        return this.partyfee;
    }

    public void setPartyfee(String partyfee) {
        this.partyfee = partyfee;
    }

    public String getPartyimage() {
        return this.partyimage;
    }

    public void setPartyimage(String partyimage) {
        this.partyimage = partyimage;
    }

    public String getPartytaggingalong() {
        return this.partytaggingalong;
    }

    public void setPartytaggingalong(String partytaggingalong) {
        this.partytaggingalong = partytaggingalong;
    }

    public String getCommid() {
        return this.commid;
    }

    public void setCommid(String commid) {
        this.commid = commid;
    }

    public String getCommownerid() {
        return this.commownerid;
    }

    public void setCommownerid(String commownerid) {
        this.commownerid = commownerid;
    }

    public String getCommownername() {
        return this.commownername;
    }

    public void setCommownername(String commownername) {
        this.commownername = commownername;
    }

    public String getCommname() {
        return this.commname;
    }

    public void setCommname(String commname) {
        this.commname = commname;
    }

    public String getCommwelcomemsg() {
        return this.commwelcomemsg;
    }

    public void setCommwelcomemsg(String commwelcomemsg) {
        this.commwelcomemsg = commwelcomemsg;
    }

    public String getCommdescription() {
        return this.commdescription;
    }

    public void setCommdescription(String commdescription) {
        this.commdescription = commdescription;
    }

    public String getCommtype() {
        return this.commtype;
    }

    public void setCommtype(String commtype) {
        this.commtype = commtype;
    }

    public String getCommentry() {
        return this.commentry;
    }

    public void setCommentry(String commentry) {
        this.commentry = commentry;
    }

    public String getCommimage() {
        return this.commimage;
    }

    public void setCommimage(String commimage) {
        this.commimage = commimage;
    }

    public String getCommnotification() {
        return this.commnotification;
    }

    public void setCommnotification(String commnotification) {
        this.commnotification = commnotification;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDebitaccount() {
        return this.debitaccount;
    }

    public void setDebitaccount(String debitaccount) {
        this.debitaccount = debitaccount;
    }

    public String getDebitbankcode() {
        return this.debitbankcode;
    }

    public void setDebitbankcode(String debitbankcode) {
        this.debitbankcode = debitbankcode;
    }

    public String getDebitcid() {
        return this.debitcid;
    }

    public void setDebitcid(String debitcid) {
        this.debitcid = debitcid;
    }

    public String getTransactionref() {
        return this.transactionref;
    }

    public void setTransactionref(String transactionref) {
        this.transactionref = transactionref;
    }

    public String getWebid() {
        return this.webid;
    }

    public void setWebid(String webid) {
        this.webid = webid;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return this.updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBenebankcode() {
        return this.benebankcode;
    }

    public void setBenebankcode(String benebankcode) {
        this.benebankcode = benebankcode;
    }

    
    
    
    
    
   
    

    public static void main(String args[]){
        State_manager_facebook tm = new State_manager_facebook();
        //tm.setcode("ussd");
        PersistenceAgent.logTransactions(tm);
    }

}


