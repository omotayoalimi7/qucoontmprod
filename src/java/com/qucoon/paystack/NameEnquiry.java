/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.paystack;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONObject;

/**
 *
 * @author omotayoalimi
 */
public class NameEnquiry {
    public static String callService_paystack(String accountno, String bankcode) {
        String output = "";
        try {
            Client client = Client.create();
            WebResource webResource = client
                    .resource("https://api.paystack.co/bank/resolve?account_number="+accountno+"&bank_code="+bankcode);
            ClientResponse response = webResource.type("application/json")
                    .get(ClientResponse.class);
           System.out.println("Output from Server .... \n");
            output = response.getEntity(String.class);
            System.out.println(response.getStatus()+": Output from paystack "+"\n");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
    
    public  String getAccountName(String accountno, String bankcode){
        String res=callService_paystack(accountno,bankcode);
        JSONObject data = new JSONObject();
        String output="";
        try{
            JSONObject res_json = new JSONObject(res);
            data = res_json.getJSONObject("data");
            data.put("responsecode", "00");
            output=data.toString();
        }catch(Exception e){
            output="\"responsecode\":\"11\"";
        }
        
    return data.toString();}
    
    public static void main(String args[]){
        //System.out.println(getAccountName("0009781760", "044"));
    }
}
