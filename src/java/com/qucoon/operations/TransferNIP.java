/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.qucoon.dbo.Niptransactions;
import com.qucoon.dbo.Octopuscompletetransactions;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.Tmlogs;
import com.qucoon.hbng.HbngInterface;
import com.qucoon.slacklogger.SlackLogMessage;
import java.math.BigDecimal;
import org.json.JSONObject;

/**
 *
 * @author neptune
 */
public class TransferNIP {
    SlackLogMessage log = new SlackLogMessage();
    String appname="NIP";
    String classname="TransferNIP";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public String doTransfer(String source,String transid,String sessid,String amount,String bankcode,String craccountno,String benelastname,String beneothername, String senderphone,String senderemail,String senderothername,String senderlastname, String unqref, String narration, String senderaccount, String debittype, String bankname, String senderbank){
      Niptransactions nip = new Niptransactions();
      HbngInterface hbng =  new HbngInterface();
      Mailer mail = new Mailer();
      Tmlogs tmlog = new Tmlogs();
      OctopusOperations op = new OctopusOperations();
      String rtn = "";
      BigDecimal amtset = new BigDecimal(amount);
      BigDecimal amt = amtset.divide(new BigDecimal("100"),2, BigDecimal.ROUND_HALF_UP);
      nip.setAmount(amt.doubleValue());
      nip.setBankcode(bankcode);
      nip.setCreditaccount(craccountno);
      //nip.setFee(Double.NaN);
      nip.setCreditname(benelastname+" "+beneothername);
      nip.setPaymentreference(senderphone);
      nip.setPhonenumber(senderphone);
      nip.setSource(source);
      nip.setPaymentreference(unqref);
      nip.setRequestdate(Utilities.fixTime());
      nip.setTransid(transid);
      nip.setSendername(senderlastname+" "+senderothername);
      nip.setSenderaccount(senderaccount);
      nip.setNerequestdate(Utilities.fixTime());
      nip.setTransactionstatus("CheckNameEnquiry");
      log.info(appname, classname, "NIPProcess Initialize", "Transaction ID:"+transid+" Customer:"+senderphone+" Beneficiary Account:"+craccountno+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+bankcode+" Amount"+amount);
                                     
      try{
      PersistenceAgent.logTransactions(nip);
      String benename="";
      String benebvn="";
      //do name enquiry
      String neobject = hbng.banknameenquiry(source, sessid, transid, "", bankcode, craccountno, "8");
      nip.setNerespondedate(Utilities.fixTime());
      if(Utilities.isJSONValid(neobject)){
          JSONObject ne = new JSONObject(neobject);
          if(ne.optString("responsecode").equals("00")){
               benename = ne.optString("accountname");
               benebvn = ne.optString("bankverificationvumber");
              nip.setCreditname(benename);
              
              //do FT Transactton
              String ftobject = hbng.hbngniptransfer(source, craccountno, bankcode, benename, narration, amt.toString(), unqref, senderaccount, senderlastname+" "+senderothername);
              nip.setResponsedate(Utilities.fixTime());
              if(Utilities.isJSONValid(ftobject)){
                  JSONObject ft = new JSONObject(ftobject);
                  //if not successful requery nibss for status.
                  
                        if(ft.optString("responsecode").equals("00")){
                            nip.setSessionid(ft.optString("sessionID"));
                            nip.setResponsecode("00");
                            nip.setTransactionstatus("TransactionSuccessful");
                            rtn="00";
                            
                            //log transaction as successful
                            Octopuscompletetransactions comtrans = new Octopuscompletetransactions();
                            //String billdetails="Payment of N"+amt.toString()+" for "+destinationfirstname+" "+destinationmiddlename+" "+destinationlastname+" with account:"+creditaccount+".";
                                comtrans.setBvn(benebvn);
                                comtrans.setAmount(amount);
                                comtrans.setDestinationaccount(craccountno);
                                comtrans.setDestinationbank(bankcode);
                                comtrans.setNarration(narration);
                                comtrans.setPhonenumber(senderphone);
                                comtrans.setPreferredmedium("NIP");
                                comtrans.setSessionid(ft.optString("sessionID"));
                                comtrans.setSource(source);
                                comtrans.setSourcecard(debittype);
                                comtrans.setSourcebank("card");
                                comtrans.setTransactionid(transid);
                                comtrans.setTranscode("IPG01");
                                comtrans.setTransdate(Utilities.fixTime().toString());
                                comtrans.setTransreference(unqref);
                                comtrans.setTranstype("ft");
                                comtrans.setDestinationfirstname("");
                                comtrans.setDestinationlastname(benename);
                                comtrans.setDestinationmiddlename("");
                                comtrans.setSourcefirstname(senderothername);
                                comtrans.setSourcelastname(senderlastname);
                                comtrans.setSourcemiddlename("");
                                comtrans.setCustomerid("");
                                comtrans.setPaymentcode("");
                                PersistenceAgent.logTransactions(comtrans);
                                mail.ftpaymentNotification(senderphone, senderemail, senderothername+" "+senderlastname, Utilities.fixTime().toString(), "Funds Transfer", senderphone, benename, craccountno, bankname, narration, amt.toString(), transid, debittype, senderphone, senderbank);
                                op.addBeneficiary(source, transid, sessid, senderphone, "", craccountno, benename, bankname, bankcode, "", "", "ft");   
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(senderphone);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("funds transfer success");
                                tmlog.setActiondetails("funds transfer success: "+unqref+" ");
                                tmlog.setActiontime(Utilities.fixTime().toString());
                                PersistenceAgent.logTransactions(tmlog);
                        }else{
                            nip.setResponsecode(ft.optString("responsecode"));
                            nip.setTransactionstatus("Transactionfailed");
                            rtn=ft.optString("responsecode");
                        }
              }else{
                  //ft failed
                  nip.setTransactionstatus("Transactionfailed");
                  rtn="96";
              }
              
          }else{
              rtn=ne.optString("responsecode");
              nip.setTransactionstatus("NameValidationFailed");
          }
         
          
      }else{
          nip.setTransactionstatus("NameValidationFailed");
          nip.setResponsecode("07");
          rtn="07";
      }
      PersistenceAgent.logTransactions(nip);
      }catch(Exception s){
          s.printStackTrace();
          rtn="96";
      }
      if(rtn.equals("00")){
          log.info(appname, classname, "NIPProcess Successful", "Transaction ID:"+transid+" Customer:"+senderphone+" Beneficiary Account:"+craccountno+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+bankcode+" Amount"+amount);
       
      }else{
          log.error(appname, classname, "NIPProcess Failed", "Transaction ID:"+transid+" Customer:"+senderphone+" Beneficiary Account:"+craccountno+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+bankcode+" Amount"+amount+ " Responsecode="+rtn);
       
      }
      
    return rtn;
    
}


}