/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.qucoon.operations;
import com.sendgrid.*;
import java.io.IOException;
/**
 *
 * @author User001
 */
public class Mailer {
    
    public  String sendmail(String toaddr,String subj,String msg){
        Email from = new Email("digital@octopuszone.com");
        String subject = subj;
        Email to = new Email(toaddr);
        Content content = new Content("text/plain", msg);
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
          request.setMethod(Method.POST);
          request.setEndpoint("mail/send");
          request.setBody(mail.build());
          Response response = sg.api(request);
          System.out.println(response.getStatusCode());
          System.out.println(response.getBody());
          System.out.println(response.getHeaders());
        } catch (Exception e) {
          e.printStackTrace();
        }
    return "";}
    
    
    public String sendmail_html(String toaddr,String subj,String msg){
        Email from = new Email("digital@octopuszone.com");
        String subject = subj;
        Email to = new Email(toaddr);
        Content content = new Content("text/html", msg);
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
          request.setMethod(Method.POST);
          request.setEndpoint("mail/send");
          request.setBody(mail.build());
          Response response = sg.api(request);
          System.out.println(response.getStatusCode());
          System.out.println(response.getBody());
          System.out.println(response.getHeaders());
        } catch (Exception e) {
          e.printStackTrace();
        }
    return "";}
    
    
    public String loginNotification(String source,String emailaddress,String fullname,String date_time,String logonstat){
        String subject ="Octopus Digital Bank Logon Notification";
        String msg="";
        String loginsummary="";
        String realsource="";
        Mailer mail = new Mailer();
        
        if(logonstat.equals("true")){
            if(source.equalsIgnoreCase("mobile")||source.equalsIgnoreCase("web")){
                loginsummary="You just logged on to OCTOPUS "+source.toUpperCase()+" at "+date_time;
            }else{
                if(source.equalsIgnoreCase("TELEBOT")){
                    realsource="TELEGRAM";
                }else{
                    realsource="FACEBOOK";
                }
                loginsummary="Octopus10 Bot just authorized you on "+realsource.toUpperCase()+" at "+date_time;
            }
            msg = loginnotification;
            msg = msg.replace("{full_name}", fullname);
            msg = msg.replace("{login_summary}", loginsummary);
            mail.sendmail_html(emailaddress,subject,msg);
        }else if(logonstat.equals("false")){
            if(source.equalsIgnoreCase("mobile")||source.equalsIgnoreCase("web")){
                loginsummary="You just made a failed logon attempt on OCTOPUS "+source.toUpperCase()+" at "+date_time;
            }else{
                if(source.equalsIgnoreCase("TELEBOT")){
                    realsource="TELEGRAM";
                }else{
                    realsource="FACEBOOK";
                }
                loginsummary="Octopus10 Bot just failed at authorizing you on "+realsource.toUpperCase()+" at "+date_time;
            }
            msg = loginnotification;
            msg = msg.replace("{full_name}", fullname);
            msg = msg.replace("{login_summary}", loginsummary);
            mail.sendmail_html(emailaddress,subject,msg);
        }else if(logonstat.equals("blocked")){
            if(source.equalsIgnoreCase("mobile")||source.equalsIgnoreCase("web")){
                loginsummary="Due to several failed logon attempts, your OCTOPUS profile has been blocked. "+" at "+date_time;
            }else{
                if(source.equalsIgnoreCase("TELEBOT")){
                    realsource="TELEGRAM";
                }else{
                    realsource="FACEBOOK";
                }
                loginsummary="Due to several failed logon attempts, your OCTOPUS profile has been blocked. "+" at "+date_time;
            }
            msg = loginnotification;
            msg = msg.replace("{full_name}", fullname);
            msg = msg.replace("{date_time}", date_time);
            mail.sendmail(emailaddress,subject,msg);
        }
    return "";}
    
    public String billpaymentNotification(String phonenumber,String emailaddress,String fullname,String transdate,String transtype,String cardno,String billername,String billdetails,String narration,String amount,String transref,String instrumentname,String instrumentno,String bank,String mscdata,String rechargepin)
    {
        String subject ="Octopus Digital Bank Transaction Notification";
        String msg="";
        Mailer mail = new Mailer();
        msg = bptransaction;
        msg = msg.replace("{phone_number}", phonenumber);
        msg = msg.replace("{full_name}", fullname);
        msg = msg.replace("{instrument_name}", instrumentname);
        msg = msg.replace("{instrument_no}", instrumentno);
        msg = msg.replace("{bank}", bank);
        msg = msg.replace("{biller_name}", billername);
        msg = msg.replace("{bill_details}", billdetails);
        msg = msg.replace("{amount}", amount);
        msg = msg.replace("{trans_type}", transtype);
        msg = msg.replace("{narration}", narration);
        msg = msg.replace("{trans_date}", transdate);
        msg = msg.replace("{trans_ref}", transref);
        msg = msg.replace("{msc_data}", mscdata);
        msg = msg.replace("{recharge_pin}", rechargepin);
        mail.sendmail_html(emailaddress,subject,msg);
        
    return "";}
    
    public String ftpaymentNotification(String phonenumber,String emailaddress,String fullname,String transdate,String transtype,String cardno,String benename,String beneaccount,String benebank,String narration,String amount,String transref,String instrumentname,String instrumentno,String bank)
    {
        String subject ="Octopus Digital Bank Transaction Notification";
        String msg="";
        Mailer mail = new Mailer();
        msg = fttransaction;
        msg = msg.replace("{phone_number}", phonenumber);
        msg = msg.replace("{full_name}", fullname);
        msg = msg.replace("{instrument_name}", instrumentname);
        msg = msg.replace("{instrument_no}", instrumentno);
        msg = msg.replace("{bank}", bank);
        msg = msg.replace("{bene_name}", benename);
        msg = msg.replace("{bene_account}", beneaccount);
        msg = msg.replace("{bene_bank}", benebank);
        msg = msg.replace("{amount}", amount);
        msg = msg.replace("{trans_type}", transtype);
        msg = msg.replace("{narration}", narration);
        msg = msg.replace("{trans_date}", transdate);
        msg = msg.replace("{trans_ref}", transref);
        mail.sendmail_html(emailaddress,subject,msg);
        
    return "";}
    
    public String addCardNotification(String phonenumber,String emailaddress,String fullname,String bankname,String cardno)
    {
        String subject ="Octopus Digital Bank Add Card Notification";
        String msg="";
        Mailer mail = new Mailer();
        msg = addcardsuccess;
        msg = msg.replace("{full_name}", fullname);
        msg = msg.replace("{card_no}", cardno);
        msg = msg.replace("{bank_name}", bankname);
        mail.sendmail_html(emailaddress,subject,msg);
        
    return "";}
    
    public String addAccountNotification(String phonenumber,String emailaddress,String fullname,String bankname,String accountno)
    {
        String subject ="Octopus Digital Bank Add Account Notification";
        String msg="";
        Mailer mail = new Mailer();
        msg = addaccountsuccess;
        msg = msg.replace("{full_name}", fullname);
        msg = msg.replace("{account_no}", accountno);
        msg = msg.replace("{bank_name}", bankname);
        mail.sendmail_html(emailaddress,subject,msg);
        
    return "";}
    
    public String joinCommunityNotification(String phonenumber,String emailaddress,String fullname,String commname)
    {
        String subject ="Octopus Digital Bank Join Community Notification";
        String msg="";
        Mailer mail = new Mailer();
        msg = joincommunitysuccess;
        msg = msg.replace("{full_name}", fullname);
        msg = msg.replace("{comm_name}", commname);
        mail.sendmail_html(emailaddress,subject,msg);
        
    return "";}
    
    
    public static void main(String args[]){
        Mailer mail = new Mailer();
        mail.sendmail_html("tayo.alimi@qucoon.com","Test",bptransaction);
                
    }
    
    static String loginnotification=PropsReader.getProperty("loginnotification");
    static String fttransaction=PropsReader.getProperty("fundstransfersuccess");
    static String bptransaction=PropsReader.getProperty("billpaymentsuccess");
    static String addcardsuccess=PropsReader.getProperty("addcardsuccess");
    static String addaccountsuccess=PropsReader.getProperty("addaccountsuccess");
    static String joincommunitysuccess=PropsReader.getProperty("joincommunitysuccess");
}
