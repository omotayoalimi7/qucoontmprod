/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import java.util.Map;
import javax.servlet.ServletRequest;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author neptune
 */
public class validateJSON {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JSONException {
        // TODO code application logic here
        JSONObject data = new JSONObject();
        data.put("user", "tope");
        data.put("password", "232");
        data.put("amount", 1);
        //data.put("lastname", "1");
        String checkfieldnames = "user,password,amount,lastname";
        validateJSON c = new validateJSON();
        System.out.println(c.isJsonParamSet(data,checkfieldnames));
    }
    
    public  String isJsonParamSet(JSONObject json, String values){
        String isvalid = "";
        //System.out.println("json="+json.toString());
        //System.out.println("values="+values);
        values = (values != null) ? values : "";
        String[] fieldnames = values.split(",");
        for(int j = 0; j< fieldnames.length; j++){
            if(!json.has(fieldnames[j])){
                isvalid = isvalid + fieldnames[j]+",";
            }
            
//            if(json.has(fieldnames[j]) && json.optString(fieldnames[j]).length() < 1){ //emtpy params
//                isvalid = isvalid + fieldnames[j]+",";
//            }
                 
            
        }
        if(isvalid.length() >  0){
            isvalid = isvalid.substring(0,isvalid.length()-1);
            isvalid = "The following fields ("+ isvalid + ") are required.";
        }
        
        return isvalid;
    }
    
      public JSONObject requestToJSON(ServletRequest req) throws JSONException {
        JSONObject jsonObj = new JSONObject();
        Map<String,String[]> params = req.getParameterMap();
        System.out.println("params=="+params);
        for (Map.Entry<String,String[]> entry : params.entrySet()) {
            String v[] = entry.getValue();
            Object o = (v.length == 1) ? v[0] : v;
            jsonObj.put(entry.getKey(), o);
        }
        return jsonObj;
}
    
}
