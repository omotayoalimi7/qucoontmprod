/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.qucoon.dbo.Debitlogs;
import com.qucoon.dbo.Niptransactions;
import com.qucoon.dbo.Octopuscompletetransactions;
import com.qucoon.dbo.Octopusdisputetransactions;
import com.qucoon.dbo.Octopustransactions;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.Tmlogs;
import com.qucoon.hbng.HbngInterface;
import com.qucoon.isw.QtServices;
import static com.qucoon.operations.OctopusTransactions.logger;
import com.qucoon.slacklogger.SlackLogMessage;
import java.math.BigDecimal;
import org.json.JSONObject;

/**
 *
 * @author neptune
 */
public class TransferProcessor {
private static String QUCOON_NIP_ACCOUNT = PropsReader.getProperty("QUCOON_NIP_ACCOUNT");
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public static JSONObject qt(String source,String transid,String sessid,String amount,String creditbank,String creditaccount,String destinationlastname,String destinationfirstname, String phonenumber,String sourceemailadress,String sourcefirstname,String sourcelastname, String unqref, String narration, String senderaccount, String debittype, String bankname, String senderbank, String cid, String cardno){
        Niptransactions nip = new Niptransactions();
      HbngInterface hbng =  new HbngInterface();
      Mailer mail = new Mailer();
      Tmlogs tmlog = new Tmlogs();
      SlackLogMessage log = new SlackLogMessage();
      JSONObject tmresponse = new JSONObject();
      QtServices qts = new QtServices();
      OctopusTransactions ot = new OctopusTransactions();
      Octopustransactions logot = new Octopustransactions();
      Octopuscompletetransactions comtrans = new Octopuscompletetransactions();
      Octopusdisputetransactions distrans = new Octopusdisputetransactions();
      Utilities utils = new Utilities();
      Debitlogs dbl = new Debitlogs();
      OctopusOperations op = new OctopusOperations();
      String rtn = "";
      String resmsg="";
        String billdetails="";
        String paymentcoderesponse_str="";
        String paymentcoderesponsecode="";
        String MscData="";
        String rechargePIN="";
        String billername="";
      String appname="QucconTM";
    String classname="OctopusTransactions";
    try{
    String transref = "1528"+transid.substring(10,transid.length());
                            String qtresponsestr = qts.doTransfer(source,transid,sessid,amount, creditbank, creditaccount, destinationlastname, destinationfirstname,phonenumber,sourceemailadress,sourcefirstname,sourcelastname,transref);
                            JSONObject qtresponse = new JSONObject();
                            if(Utilities.isJSONValid(qtresponsestr)){
                                try{
                                qtresponse = new JSONObject(qtresponsestr);
                                }catch(Exception s){
                                    s.printStackTrace();
                                }
                            }
                            String qtresponsecode=qtresponse.optString("responsecode");
                            String qttransref="";
                            if(qtresponsecode.equals("00")){
                                 billdetails="Payment of N"+amount+" for "+destinationfirstname+" "+destinationlastname+" with account:"+creditaccount+".";
                                comtrans.setAmount(amount);
                                comtrans.setDestinationaccount(creditaccount);
                                comtrans.setDestinationbank(creditbank);
                                comtrans.setNarration(""+transid);
                                comtrans.setPhonenumber(phonenumber);
                                comtrans.setPreferredmedium("ipg");
                                comtrans.setSessionid(sessid);
                                comtrans.setSource(source);
                                comtrans.setSourcecard(cid);
                                comtrans.setSourcebank("card");
                                comtrans.setTransactionid(transid);
                                comtrans.setTranscode("IPG01");
                                comtrans.setTransdate(Utilities.fixTime().toString());
                                comtrans.setTransreference(transref);
                                comtrans.setTranstype("ft");
                                comtrans.setDestinationfirstname(destinationfirstname);
                                comtrans.setDestinationlastname(destinationlastname);
                                comtrans.setDestinationmiddlename("");
                                comtrans.setSourcefirstname(sourcefirstname);
                                comtrans.setSourcelastname(sourcelastname);
                                comtrans.setSourcemiddlename("");
                                comtrans.setCustomerid("");
                                comtrans.setPaymentcode("");
                                PersistenceAgent.logTransactions(comtrans);
                                if(qtresponse.has("transactionRef")){
                                    resmsg="validated";
                                    qttransref=qtresponse.optString("transactionRef");
                                    tmresponse.put("qttransref", qttransref);

                                }
                                if(qtresponse.has("MscData")){
                                    MscData=qtresponse.getString("MscData");
                                    tmresponse.put("MscData", MscData);
                                    billdetails=billdetails+" MscData:"+MscData+".";
                                }
                                if(qtresponse.has("rechargePIN")){
                                    rechargePIN=qtresponse.getString("rechargePIN");
                                    tmresponse.put("rechargePIN", rechargePIN);
                                    billdetails=billdetails+" rechargePIN:"+rechargePIN+".";
                                }
                                tmresponse.put("responsecode", "00");
                                tmresponse.put("responsemessage", "funds transfer success-"+resmsg);
                                tmresponse.put("transref",transid); 
                                logot.setAmount(amount);
                                logot.setDestinationaccount(creditaccount);
                                logot.setDestinationbank(creditbank);
                                logot.setNarration("Billpayment"+transid);
                                logot.setPhonenumber(phonenumber);
                                logot.setPreferredmedium("ipg");
                                logot.setSessionid(sessid);
                                logot.setSource(source);
                                logot.setSourcecard(cid);
                                logot.setSourcebank("card");
                                logot.setTransactionid(transid);
                                logot.setTranscode("IPG01");
                                logot.setTransdate(Utilities.fixTime().toString());
                                logot.setTransreference(qttransref);
                                logot.setTranstype("C");
                                logot.setDestinationfirstname(destinationfirstname);
                                logot.setDestinationlastname(destinationlastname);
                                logot.setDestinationmiddlename("");
                                logot.setSourcefirstname(sourcefirstname);
                                logot.setSourcelastname(sourcelastname);
                                logot.setSourcemiddlename("");
                                PersistenceAgent.logTransactions(logot);
                                logot = new Octopustransactions();
                                dbl.setActiondetails("funds transfer success: "+qttransref+" ");
                                dbl.setActionperformed("funds transfer success");
                                dbl.setActiontime(Utilities.fixTime().toString());
                                dbl.setAmount(amount);
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("FT"+transid);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                                logger.info(phonenumber+"funds transfer success: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("funds transfer success");
                                tmlog.setActiondetails("funds transfer success: "+qttransref+" ");
                                tmlog.setActiontime(Utilities.fixTime().toString());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                BigDecimal amtset = new BigDecimal(amount);
                                BigDecimal amt = amtset.divide(new BigDecimal("100"),2, BigDecimal.ROUND_HALF_UP);
                                mail.ftpaymentNotification(phonenumber, sourceemailadress, sourcefirstname+" "+sourcelastname, utils.getDate(), "Funds Transfer", cardno, destinationfirstname+" "+destinationlastname, creditaccount, bankname, narration, amt.toString(), transid, "Card", cardno, senderbank);
                                log.info(appname, classname, "Transaction Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Transaction ID "+transid);
                                logger.info(phonenumber+"Mail sent. Funds transfer success: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("Mail sent. Funds transfer success");
                                tmlog.setActiondetails("Mail sent. Funds transfer success: "+qttransref+" ");
                                tmlog.setActiontime(Utilities.fixTime().toString());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                op.addBeneficiary(source, transid, sessid, phonenumber, "", creditaccount, destinationfirstname+" "+destinationlastname, bankname, creditbank, "", "", "ft");
                                logger.info(phonenumber+"Beneficiary Saved. Funds transfer success: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("Beneficiary Saved. Funds transfer success");
                                tmlog.setActiondetails("Beneficiary Saved. Funds transfer success: "+qttransref+" ");
                                tmlog.setActiontime(Utilities.fixTime().toString());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                            }else{
                                //try NIP then log as dispute.
                                TransferNIP nipx = new TransferNIP();
                                //senderaccount, debittype
                                 senderaccount=QUCOON_NIP_ACCOUNT;
                                 debittype="Card";
                                String nipresp = nipx.doTransfer(source, transid, sessid, amount, creditbank, creditaccount, destinationlastname, destinationfirstname, phonenumber, sourceemailadress, sourcefirstname, sourcelastname, transref, narration, senderaccount, debittype,bankname,senderbank);
                                if(nipresp.equals("00")){
                                    tmresponse.put("responsecode", "00");
                                    tmresponse.put("responsemessage", "funds transfer success-"+resmsg);
                                    tmresponse.put("transref",transid); 
                                }else{
                                resmsg="error";
                                log.dispute(appname, classname, "Dispute", "Transaction ID:"+transid+" Customer:"+phonenumber+" Beneficiary Account:"+creditaccount+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+creditbank+" Amount"+amount);
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "Transaction has been submitted successfully and is being processed. You shall be notified on completion.");
                                distrans.setAmount(amount);
                                distrans.setDestinationaccount(creditaccount);
                                distrans.setDestinationbank(creditbank);
                                distrans.setNarration(""+transid);
                                distrans.setPhonenumber(phonenumber);
                                distrans.setPreferredmedium("ipg");
                                distrans.setSessionid(sessid);
                                distrans.setSource(source);
                                distrans.setSourcecard(cid);
                                distrans.setSourcebank("card");
                                distrans.setTransactionid(transid);
                                distrans.setTranscode("IPG01");
                                distrans.setTransdate(Utilities.fixTime().toString());
                                distrans.setTransreference(transref);
                                distrans.setTranstype("ft");
                                distrans.setDestinationfirstname(destinationfirstname);
                                distrans.setDestinationlastname(destinationlastname);
                                distrans.setDestinationmiddlename("");
                                distrans.setSourcefirstname(sourcefirstname);
                                distrans.setSourcelastname(sourcelastname);
                                distrans.setSourcemiddlename("");
                                distrans.setCustomerid("");
                                distrans.setPaymentcode("");
                                PersistenceAgent.logTransactions(distrans);
                                dbl.setActiondetails("funds transfer failed:reversal needed: ");
                                dbl.setActionperformed("funds transfer failed:reversal needed");
                                dbl.setActiontime(Utilities.fixTime().toString());
                                dbl.setAmount(amount);
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("FT"+transid);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                                logger.info(phonenumber+"funds transfer failed: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("funds transfer failed");
                                tmlog.setActiondetails("funds transfer failed: "+qttransref+" ");
                                tmlog.setActiontime(utils.getDate());
                                PersistenceAgent.logTransactions(tmlog);
                                
                                }
                            }
                        
    
    }
    catch(Exception s){
    s.printStackTrace();
}
    return tmresponse;
}
    
    
}