/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.qucoon.dbo.DataObjects;
import com.qucoon.dbo.Facebook_users;
import com.qucoon.dbo.State_manager_facebook;
import com.qucoon.dbo.State_manager_telegram;
import com.qucoon.dbo.Telegram_users;

/**
 *
 * @author User001
 */
public class Operations {
    public static Telegram_users getCustomer_tele(String wid){
        Telegram_users tm_user=null;
        tm_user= DataObjects.getTelegram_user(wid);
        String chatid = (tm_user.getChatid() != null)?tm_user.getChatid().trim():"";
        if(chatid==""){
            tm_user=null;
        }
    return tm_user;
    }
    
    public static State_manager_telegram getCurrentCustomer_tele(String wid){
        State_manager_telegram tm_user=null;
        tm_user= DataObjects.getCurrentTelegram_user(wid);
        String chatid = (tm_user.getChatid() != null)?tm_user.getChatid().trim():"";
        String status = (tm_user.getStatus()!=null)?tm_user.getStatus().trim():"";
        String nextaction = (tm_user.getNextaction()!=null)?tm_user.getNextaction().trim():"";
        if(chatid==""){
            tm_user=null;
        }
        if(chatid!=""&&status.toLowerCase().equals("inactive")){
           tm_user=null; 
        }
        if(chatid!=""&&!nextaction.toLowerCase().equals("collectpasscodelink")&&!nextaction.toLowerCase().equals("collectaddcardlink")){
           tm_user=null; 
        }
        
    return tm_user;
    }
    
    public static Facebook_users getCustomer_face(String wid){
        Facebook_users fb_user=null;
        fb_user= DataObjects.getFacebook_user(wid);
        String chatid = (fb_user.getChatid() != null)?fb_user.getChatid().trim():"";
        if(chatid==""){
            fb_user=null;
        }
    return fb_user;
    }
    
    public static State_manager_facebook getCurrentCustomer_face(String wid)
    {
        State_manager_facebook fb_user=null;
        fb_user= DataObjects.getCurrentFacebook_user(wid);
        String chatid = (fb_user.getChatid() != null)?fb_user.getChatid().trim():"";
        String status = (fb_user.getStatus()!=null)?fb_user.getStatus().trim():"";
        String nextaction = (fb_user.getNextaction()!=null)?fb_user.getNextaction().trim():"";
        if(chatid==""){
            fb_user=null;
        }
        if(chatid!=""&&status.toLowerCase().equals("inactive")){
           fb_user=null; 
        }
        if(chatid!=""&&!nextaction.toLowerCase().equals("collectpasscodelink")&&!nextaction.toLowerCase().equals("collectaddcardlink")){
           fb_user=null; 
        }
    return fb_user;
    }
}
