/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.qucoon.dbo.Bankcodes;
import com.qucoon.dbo.DataObjects;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.Tmlogs;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class BankNamefromAcc {
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BankNamefromAcc.class);
    public static void main(String args[]){
        BankNamefromAcc bna = new BankNamefromAcc();
        System.out.println(bna.getBanks("{\"source\":\""+"USSD"+"\",\"accountnumber\":\"1961018984\"}"));
    }
    public String getBanks(String req){
        String response="";
        BankNamefromAcc bna = new BankNamefromAcc();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String code="";
        String bankname="";
        String accountnumber="";JSONObject finalres = new JSONObject();
        JSONArray gatherbanks = new JSONArray();
        ArrayList <String> validbanks = new ArrayList<String>();
        List<Bankcodes> obj;
        obj = DataObjects.getBankcodes();
        String source="";
        try{
            JSONObject request = new JSONObject(req);
            accountnumber= request.getString("accountnumber");
            validbanks=bna.calc(accountnumber);
            source= request.getString("source");
            logger.info(source+"  : request for bank with ac no tm response: "+ request.toString().replace("\"", ":"));
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setActionperformed("request for bank with ac no tm response");
            tmlog.setActiondetails("request for bank with ac no tm response "+request.toString().replace("\"", ":"));
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            for(int j=0; j<validbanks.size();j++){
                for(int i=0;i<obj.size();i++){
                    Bankcodes b=obj.get(i);
                    if(b.getCode().equals(validbanks.get(j))){
                        JSONObject buildjson = new JSONObject();
                        buildjson.put("bankname", b.getBankname());
                        buildjson.put("bankcode", b.getBankcode());
                        gatherbanks.put(buildjson);
                    }
                }
            }
            finalres.put("result", gatherbanks);
            finalres.put("responsedate",utils.getDate());
            finalres.put("responsetime", utils.getTime());
            finalres.put("responsecode","00");
            finalres.put("responsemessage", "success");
            logger.info(source+"  : response to request for bank with account number: "+ finalres.toString().replace("\"", ":"));
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setActionperformed("response to request for bank with account number:");
            tmlog.setActiondetails("response to request for bank with account number: "+finalres.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            response = finalres.toString();
            }
        catch(Exception e){
            tmlog = new Tmlogs();
            response="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"99\",\"responsemessage\":\"Error parsing json-expects source and accountnumber\"}"; 
            logger.info(source+"  : error parsing json for request for bank with account number: "+ response.toString().replace("\"", ":"));
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setActionperformed("error parsing json for request for bank with account number");
            tmlog.setActiondetails("error parsing json for request for bank with account number "+response.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return response;}
    
    public ArrayList <String> calc(String acc){
        ArrayList <String> validbanks = new ArrayList<String>();
         int controldigit;
         int i,j,Z,sum1,sum2,sum3,sum;
        if(acc.length()!=10){
            
        }else{
            if(Integer.parseInt(acc.substring(9,10))==0){
             controldigit   =Integer.parseInt(acc.substring(9,10));
            }else{
        controldigit = 10 - Integer.parseInt(acc.substring(9,10));
            }
        sum1 = 3*Integer.parseInt(acc.substring(0,1)) + 7*Integer.parseInt(acc.substring(1,2)) + 3 * Integer.parseInt(acc.substring(2,3));
        sum2 = 3 *Integer.parseInt(acc.substring(3,4)) + 7 *Integer.parseInt(acc.substring(4,5)) + 3 * Integer.parseInt(acc.substring(5,6));
        sum3 = 3*Integer.parseInt(acc.substring(6,7)) + 7*Integer.parseInt(acc.substring(7,8)) + 3 * Integer.parseInt(acc.substring(8,9));
        sum = sum1 + sum2 + sum3;
        
        Z = 10;
        i = 1;
        j = 0;
        while(i < 50){
        Z = 10 * i + controldigit;
        if(Z > sum ){
            int zs =Z - sum;
            validbanks.add(zs+"");
            j = j + 1;
        }
        i = i + 1;
        }
        }
    return validbanks;}
    
}
