/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.qucoon.dbo.Bankcodes;
import com.qucoon.dbo.DataObjects;
import com.qucoon.dbo.Debitlogs;
import com.qucoon.dbo.Octopuscompletetransactions;
import com.qucoon.dbo.Octopuscustomersaccounts;
import com.qucoon.dbo.Octopuscustomerscards;
import com.qucoon.dbo.Octopuscustomerstm;
import com.qucoon.dbo.Octopusdisputetransactions;
import com.qucoon.dbo.Octopusinboxtransactions;
import com.qucoon.dbo.Octopuspndtrans;
import com.qucoon.dbo.Octopustransactions;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.kernelapi.BankITOperations;
import com.qucoon.dbo.Tmlogs;
import com.qucoon.hbng.HbngInterface;
import com.qucoon.isw.QtServices;
import com.qucoon.kernelapi.Authorization;
import com.qucoon.kernelapi.IpgOperations;
import com.qucoon.paystack.NameEnquiry;
import com.qucoon.slacklogger.SlackLogMessage;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class OctopusTransactions {
    
   
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(OctopusTransactions.class);
    private static String QUCOON_NIP_ACCOUNT = PropsReader.getProperty("QUCOON_NIP_ACCOUNT");
    SlackLogMessage log = new SlackLogMessage();
    String appname="QucconTM";
    String classname="OctopusTransactions";
    public String fundsTransfer(String source,String sessid,String transid,String phonenumber,String accountstat,String debitaccount,String debitbank,String creditaccount,String creditbank,String destinationfirstname,String destinationmiddlename,String destinationlastname,String cardstat,String cid,String amount,String narration,String inputpasscode, JSONObject req)
    {
        OctopusTransactions ot = new OctopusTransactions();
        Octopustransactions logot = new Octopustransactions();
        Octopuscompletetransactions comtrans = new Octopuscompletetransactions();
        Octopusdisputetransactions distrans = new Octopusdisputetransactions();
        Debitlogs dbl = new Debitlogs();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        boolean debitaccountstat=false;
        Octopuscustomersaccounts ocustacc;
        Octopuscustomerscards ocustcards;
        Octopuscustomerstm ocusttm;
        String accountid="";
        String dbpasscode="";
        String output="";
        boolean passcodestatus;
        String auth=Authorization.getAuth();
        JSONObject tmresponse = new JSONObject();
        OctopusOperations op = new OctopusOperations();
        Mailer mail = new Mailer();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        String debitcardstat_str;
        JSONObject debitcardstat;
        String debitcardrescode;
        String resmsg;
        String billdetails="";
        String paymentcoderesponse_str="";
        String paymentcoderesponsecode="";
        String MscData="";
        String rechargePIN="";
        String billername="";
        String transdate=utils.getDate()+" "+utils.getTime();
        JSONObject paymentcoderesponse;
        Bankcodes bankcode = DataObjects.getBankcodes_code(creditbank);
        String bankname=(bankcode.getBankname()!=null)?bankcode.getBankname():"";
        phonenumber=utils.formatPhone(phonenumber);
        ocusttm=DataObjects.getOctopuscustomerstm_rec(phonenumber);
        String firstname = (ocusttm.getFirstname()!=null)?ocusttm.getFirstname():"";
        String lastname = (ocusttm.getLastname()!=null)?ocusttm.getLastname():"";
        String emailaddress = (ocusttm.getEmailaddress()!=null)?ocusttm.getEmailaddress():"";
        NumberFormat formatter = new DecimalFormat("####.00");
        amount = formatter.format(new BigDecimal(amount)).toString();
        phonenumber=utils.formatPhone(phonenumber);
        transid=utils.generateTransid();
        //debit source is account
        try{
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, inputpasscode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a user
                tmresponse.put("responsecode", "44");
                tmresponse.put("responsemessage", "not a user");
            }else if(loginrescode.equals("22")){
                //wrong passcode
                tmresponse.put("responsecode", "55");
                tmresponse.put("responsemessage", "wrong passcode");
            }else if(loginrescode.equals("33")){
                //wrong passcode
                tmresponse.put("responsecode", "66");
                tmresponse.put("responsemessage", "Account blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to reset account");
            }else if(loginrescode.equals("00")){
                tmresponse.put("responsedate", utils.getDate());
                tmresponse.put("responsetime", utils.getTime());
                String customerstatstr = op.customerStatus(source, transid, sessid, phonenumber, "NG");
                JSONObject customerstat = new JSONObject(customerstatstr);
                String sourcefirstname=customerstat.getString("firstname");
                String sourcelastname=customerstat.getString("lastname");
                String sourceemailadress=customerstat.getString("emailaddress");
                String sourcemiddlename="";
                if(accountstat.toLowerCase().equals("true")){
                    logger.info(phonenumber+"new debit customer request using account" +sessid+" transid: "+transid);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setRequesttime(transdate);
                    tmlog.setActionperformed("new debit customer request using account");
                    tmlog.setActiondetails("new debit customer request using account: "+sessid+" transid: "+transid);
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    dbl.setActiondetails("new debit customer request using account "+sessid+" transid: "+transid);
                    dbl.setActionperformed("new debit customer request using account");
                    dbl.setAmount(amount);
                    dbl.setBankitstatus("");
                    dbl.setDestinationaccount(creditaccount);
                    dbl.setDestinationbank(creditbank);
                    dbl.setNarration(narration);
                    dbl.setPhonenumber(phonenumber);
                    dbl.setPreferredmedium("heritage");
                    dbl.setRequesttime(transdate);
                    dbl.setSessionid(sessid);
                    dbl.setSource(source);
                    dbl.setSourceaccount(debitaccount);
                    dbl.setSourcebank(debitbank);
                    dbl.setTransactionid(transid);
                    PersistenceAgent.logTransactions(dbl);
                    dbl = new Debitlogs();
                    ocustacc= DataObjects.getOctopuscustomersaccounts_acc(debitaccount);
                    String phno = ocustacc.getPhonenumber() != null?ocustacc.getPhonenumber():"";
                    //if phonenumber sending request is same with phonenumber that registered account
                    if(phno.equals(phonenumber)){
                        logger.info(phonenumber+"request phone is account phone" +sessid+" transid: "+transid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("request phone is account phone");
                        tmlog.setActiondetails("request phone is account phone: "+sessid+" transid: "+transid);
                        tmlog.setActiontime(transdate);
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        dbl.setActiondetails("request phone is account phone "+sessid+" transid: "+transid);
                        dbl.setActionperformed("request phone is account phone");
                        dbl.setActiontime(transdate);
                        dbl.setAmount(amount);
                        dbl.setBankitstatus("");
                        dbl.setDestinationaccount(creditaccount);
                        dbl.setDestinationbank(creditbank);
                        dbl.setNarration(narration);
                        dbl.setPhonenumber(phonenumber);
                        dbl.setPreferredmedium("heritage");
                        dbl.setSessionid(sessid);
                        dbl.setSource(source);
                        dbl.setSourceaccount(debitaccount);
                        dbl.setSourcebank(debitbank);
                        dbl.setTransactionid(transid);
                        PersistenceAgent.logTransactions(dbl);
                        dbl = new Debitlogs();
                        //get accountid
                        accountid=ocustacc.getBankitid();
                        //debit account
                        if(debitbank.equals("030")){
                            tmresponse.put("debitmedium", "heritage"); 
                            if(creditbank.equals("030")){
                                tmresponse.put("creditmedium", "heritage");
                                //debit heritage accnt credit heritage accnt
                                debitaccountstat=ot.debitAccountHbng(source, sessid, transid, phonenumber, auth, debitaccount,debitbank, creditaccount,creditbank, amount, narration,"ft");
                                if(debitaccountstat==true){
                                    //success
                                    tmresponse.put("responsecode", "00");
                                    tmresponse.put("responsemessage", "funds transfer success");
                                    logger.info(phonenumber+"debit success" +sessid+" transid: "+transid);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("debit success");
                                    tmlog.setActiondetails("debit success "+sessid+" transid: "+transid);
                                    tmlog.setActiontime(transdate);
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    dbl.setActiondetails("debit success "+sessid+" transid: "+transid);
                                    dbl.setActionperformed("debit success");
                                    dbl.setActiontime(transdate);
                                    dbl.setAmount(amount);
                                    dbl.setBankitstatus("");
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration(narration);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("heritage");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourceaccount(debitaccount);
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                }else{
                                    //debitfailed
                                    tmresponse.put("responsecode", "11");
                                    tmresponse.put("responsemessage", "funds transfer failed");
                                    logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("debit failed");
                                    tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                    tmlog.setActiontime(transdate);
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    dbl.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                    dbl.setActionperformed("debit failed");
                                    dbl.setActiontime(transdate);
                                    dbl.setAmount(amount);
                                    dbl.setBankitstatus("");
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration(narration);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("heritage");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourceaccount(debitaccount);
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                }
                                
                            }else{ 
                                tmresponse.put("creditmedium", "nip/qt");
                                //debit heritage accnt credit heritage ledger
                                //issue a direct NIP transfer straigt.
                                  TransferNIP nip = new TransferNIP();
                                    //senderaccount, debittype
                                    String senderaccount=debitaccount;
                                    String debittype="Account";
                                    String nipresp = nip.doTransfer(source, transid, sessid, amount, creditbank, creditaccount, destinationlastname, destinationfirstname, phonenumber, emailaddress, firstname, lastname, transid, narration, senderaccount, debittype,bankname,debitbank);
                                    
                                            
                                
                                //debitaccountstat=ot.debitAccountHbng(source, sessid, transid, phonenumber, auth, debitaccount,debitbank, "ledger","030", amount, narration,"ft");
                                //debitaccountstat=true;
                                    if(nipresp.equals("00")){
                                    //success
                                    logger.info(phonenumber+"debit success" +sessid+" transid: "+transid);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("debit success");
                                    tmlog.setActiondetails("debit success "+sessid+" transid: "+transid);
                                    tmlog.setActiontime(transdate);
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    dbl.setActiondetails("debit success "+sessid+" transid: "+transid);
                                    dbl.setActionperformed("debit success");
                                    dbl.setActiontime(transdate);
                                    dbl.setAmount(amount);
                                    dbl.setBankitstatus("");
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration(narration);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("heritage");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourceaccount(debitaccount);
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                    tmresponse.put("responsecode", "00");
                                    tmresponse.put("responsemessage", "funds transfer success");
                                    //credit account with nip
                                    
                                            
                                    //...
                                    //failed credit reverse
                                }else{
                                    //debitfailed
                                    tmresponse.put("responsecode", "11");
                                    tmresponse.put("responsemessage", "funds transfer failed");
                                    logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("debit failed");
                                    tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                    tmlog.setActiontime(transdate);
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    dbl.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                    dbl.setActionperformed("debit failed");
                                    dbl.setActiontime(transdate);
                                    dbl.setAmount(amount);
                                    dbl.setBankitstatus("");
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration(narration);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("heritage");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourceaccount(debitaccount);
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                }
                                
                            }
                        }else{
                            tmresponse.put("debitmedium", "bankit");
                            //debit using bankit
                            String debitaccountresponse_str=ot.debitAccountBankit(source, sessid, transid, phonenumber, debitaccount, accountid, debitbank, creditaccount, creditbank, amount, narration, inputpasscode);
                            JSONObject debitaccountresponse = new JSONObject(debitaccountresponse_str);
                            String debitaccountresponsecode=debitaccountresponse.getString("responsecode");
                            String debitaccountresponsemessage=debitaccountresponse.getString("responsemessage");
                            String debitviabankit="99";
                            
                            if(debitaccountresponsecode.equals("00")){
                            if(creditbank.equals("030")){
                                //debit qucoon credit heritage customer
                                debitaccountstat=ot.debitAccountHbng(source, sessid, transid, phonenumber, auth, QUCOON_NIP_ACCOUNT,debitbank, creditaccount,creditbank, amount, narration,"ft");
                                if(debitaccountstat==true){
                                    //success
                                    tmresponse.put("responsecode", "00");
                                    tmresponse.put("responsemessage", "funds transfer success");
                                    logger.info(phonenumber+"debit success" +sessid+" transid: "+transid);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("debit success");
                                    tmlog.setActiondetails("debit success "+sessid+" transid: "+transid);
                                    tmlog.setActiontime(transdate);
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    dbl.setActiondetails("debit success "+sessid+" transid: "+transid);
                                    dbl.setActionperformed("debit success");
                                    dbl.setActiontime(transdate);
                                    dbl.setAmount(amount);
                                    dbl.setBankitstatus("");
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration(narration);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("heritage");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourceaccount(debitaccount);
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                }else{
                                    //debitfailed
                                    tmresponse.put("responsecode", "11");
                                    tmresponse.put("responsemessage", "funds transfer failed");
                                    logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("debit failed");
                                    tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                    tmlog.setActiontime(transdate);
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    dbl.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                    dbl.setActionperformed("debit failed");
                                    dbl.setActiontime(transdate);
                                    dbl.setAmount(amount);
                                    dbl.setBankitstatus("");
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration(narration);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("heritage");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourceaccount(debitaccount);
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                }
                                tmresponse.put("creditmedium", "heritage");
//                                tmresponse.put("responsecode", "00");
//                                tmresponse.put("responsemessage", "funds transfer success");
                                //ddebit bankit
                                //debit ledger credit account
                            }else{
                                TransferNIP nip = new TransferNIP();
                                String senderaccount=QUCOON_NIP_ACCOUNT;
                                String debittype="bankit";
                                String nipresp = nip.doTransfer(source, transid, sessid, amount, creditbank, creditaccount, destinationlastname, destinationfirstname, phonenumber, emailaddress, firstname, lastname, transid, narration, senderaccount, debittype,bankname,debitbank);
                                        if(nipresp.equals("00")){
                                            tmresponse.put("creditmedium", "nip/qt");
                                            tmresponse.put("responsecode", "00");
                                            tmresponse.put("responsemessage", "funds transfer success");
                                            tmresponse.put("transref",transid); 
                                        }else{

                                        resmsg="error";
                                        log.dispute(appname, classname, "Dispute", "Transaction ID:"+transid+" Customer:"+phonenumber+" Beneficiary Account:"+creditaccount+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+creditbank+" Amount"+amount);
                                        tmresponse.put("responsecode", "11");
                                        tmresponse.put("responsemessage", "Transaction has been submitted successfully and is being processed. You shall be notified on completion.");
                                        distrans.setAmount(amount);
                                        distrans.setDestinationaccount(creditaccount);
                                        distrans.setDestinationbank(creditbank);
                                        distrans.setNarration(""+transid);
                                        distrans.setPhonenumber(phonenumber);
                                        distrans.setPreferredmedium("ipg");
                                        distrans.setSessionid(sessid);
                                        distrans.setSource(source);
                                        distrans.setSourcecard(cid);
                                        distrans.setSourcebank("card");
                                        distrans.setTransactionid(transid);
                                        distrans.setTranscode("bankid");
                                        distrans.setTransdate(transdate);
                                        distrans.setTransreference(transid);
                                        distrans.setTranstype("ft");
                                        distrans.setDestinationfirstname(destinationfirstname);
                                        distrans.setDestinationlastname(destinationlastname);
                                        distrans.setDestinationmiddlename(destinationmiddlename);
                                        distrans.setSourcefirstname(sourcefirstname);
                                        distrans.setSourcelastname(sourcelastname);
                                        distrans.setSourcemiddlename(sourcemiddlename);
                                        distrans.setCustomerid("");
                                        distrans.setPaymentcode("");
                                        PersistenceAgent.logTransactions(distrans);
                                        dbl.setActiondetails("funds transfer failed:reversal needed: ");
                                        dbl.setActionperformed("funds transfer failed:reversal needed");
                                        dbl.setActiontime(transdate);
                                        dbl.setAmount(amount);
                                        dbl.setDestinationaccount(creditaccount);
                                        dbl.setDestinationbank(creditbank);
                                        dbl.setNarration("FT"+transid);
                                        dbl.setPhonenumber(phonenumber);
                                        dbl.setPreferredmedium("ipg");
                                        dbl.setSessionid(sessid);
                                        dbl.setSource(source);
                                        dbl.setSourcecard(cid);
                                        dbl.setSourcebank("card");
                                        dbl.setTransactionid(transid);
                                        PersistenceAgent.logTransactions(dbl);
                                        dbl = new Debitlogs();
                                        logger.info(phonenumber+"funds transfer failed: "+transid+" ");
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("funds transfer failed");
                                        tmlog.setActiondetails("funds transfer failed: "+transid+" ");
                                        tmlog.setActiontime(transdate);
                                        PersistenceAgent.logTransactions(tmlog);

                                        }
                                
                                }
                                //credit nip
                            
                        }else{
                               //debitbankviabankit failed
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "funds transfer failed");
                                logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid);
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("debit failed");
                                tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                dbl.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                dbl.setActionperformed("debit failed");
                                dbl.setActiontime(transdate);
                                dbl.setAmount(amount);
                                dbl.setBankitstatus("");
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("DB"+transid+" "+creditaccount+" "+creditbank);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                
                                }
                        }
                    }else{
                        //potential fraud
                        tmresponse.put("responsecode", "22");
                        tmresponse.put("responsemessage", "potential fraud");
                        logger.info(phonenumber+"potential fraud:request phone not account phone" +sessid+" transid: "+transid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("potential fraud:request phone not account phone");
                        tmlog.setActiondetails("potential fraud:request phone not account phone "+sessid+" transid: "+transid);
                        tmlog.setActiontime(transdate);
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        dbl.setActiondetails("potential fraud:request phone not account phone "+sessid+" transid: "+transid);
                        dbl.setActionperformed("potential fraud:request phone not account phone");
                        dbl.setActiontime(transdate);
                        dbl.setAmount(amount);
                        dbl.setBankitstatus("");
                        dbl.setDestinationaccount(creditaccount);
                        dbl.setDestinationbank(creditbank);
                        dbl.setNarration(narration);
                        dbl.setPhonenumber(phonenumber);
                        dbl.setPreferredmedium("bankit");
                        dbl.setSessionid(sessid);
                        dbl.setSource(source);
                        dbl.setSourceaccount(debitaccount);
                        dbl.setSourcebank(debitbank);
                        dbl.setTransactionid(transid);
                        PersistenceAgent.logTransactions(dbl);
                        dbl = new Debitlogs();
                        System.out.println(output);
                    }
                }
                if(cardstat.toLowerCase().equals("true")){
                    ocustcards= DataObjects.getOctopuscustomerscards_cid(cid);
                    String phno = ocustcards.getPhonenumber() != null?ocustcards.getPhonenumber():"";
                    String cardname = ocustcards.getCardname() != null?ocustcards.getCardname():"";
                    String cardno=(ocustcards.getCardno()!=null)?ocustcards.getCardno():"";
                    String cardbank=(ocustcards.getCardbank()!=null)?ocustcards.getCardbank():"";
                    System.out.println("cardname "+cardname);
                    if(phno.equals(phonenumber)){
                        //debit card
                        debitcardstat_str=ot.debitCard(source, sessid, transid, phonenumber, amount,sourcefirstname,sourcemiddlename,sourcelastname, cid, cardname,destinationfirstname,destinationmiddlename,destinationlastname, "NG", "NGN", creditaccount, creditbank,"ft","Ipg02",narration);
                        debitcardstat= new JSONObject(debitcardstat_str);
                        debitcardrescode=debitcardstat.getString("responsecode");
                        resmsg=debitcardstat.getString("responsemessage");
                        if(debitcardrescode.equals("00")){
                            String transref=debitcardstat.getString("transref");
                            logger.info(phonenumber+"debit success" +sessid+" transid: "+transid);
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("debit success");
                            tmlog.setActiondetails("debit success "+sessid+" transid: "+transid);
                            tmlog.setActiontime(transdate);
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            dbl.setActiondetails("debit success "+sessid+" transid: "+transid);
                            dbl.setActionperformed("debit success");
                            dbl.setActiontime(transdate);
                            dbl.setAmount(amount);
                            dbl.setDestinationaccount(creditaccount);
                            dbl.setDestinationbank(creditbank);
                            dbl.setNarration("DB"+transid+" "+creditaccount+" "+creditbank);
                            dbl.setPhonenumber(phonenumber);
                            dbl.setPreferredmedium("ipg");
                            dbl.setSessionid(sessid);
                            dbl.setSource(source);
                            dbl.setSourcecard(cid);
                            dbl.setSourcebank("card");
                            dbl.setTransactionid(transid);
                            PersistenceAgent.logTransactions(dbl);
                            dbl = new Debitlogs();
                            logot.setAmount(amount);
                            logot.setDestinationaccount(creditaccount);
                            logot.setDestinationbank(creditbank);
                            logot.setNarration("DB"+transid);
                            logot.setPhonenumber(phonenumber);
                            logot.setPreferredmedium("ipg");
                            logot.setSessionid(sessid);
                            logot.setSource(source);
                            logot.setSourcecard(cid);
                            logot.setSourcebank("card");
                            logot.setTransactionid(transid);
                            logot.setTranscode("IPG01");
                            logot.setTransdate(transdate);
                            logot.setTransreference(transid);
                            logot.setTranstype("D");
                            logot.setDestinationfirstname(destinationfirstname);
                            logot.setDestinationlastname(destinationlastname);
                            logot.setDestinationmiddlename(destinationmiddlename);
                            logot.setSourcefirstname(sourcefirstname);
                            logot.setSourcelastname(sourcelastname);
                            logot.setSourcemiddlename(sourcemiddlename);
                            PersistenceAgent.logTransactions(logot);
                            logot = new Octopustransactions();
                            QtServices qts = new QtServices();
                            String notificationamount=amount;
                            amount = amount.replace(".", "");
                            
                            /////do transfer via hbng NIP
                            String transfermethod="qt";
                            //do name enquiry
                            //do FT
                            
                            
                            
                            
                            
                            //QT Transfer Option
                            if(transfermethod.equalsIgnoreCase("qt")){
                            String qtresponsestr = qts.doTransfer(source,transid,sessid,amount, creditbank, creditaccount, destinationlastname, destinationfirstname+" "+destinationmiddlename,phonenumber,sourceemailadress,sourcefirstname+" "+sourcemiddlename,sourcelastname,"1528"+transid.substring(10,transid.length()));
                            JSONObject qtresponse = new JSONObject(qtresponsestr);
                            String qtresponsecode=qtresponse.getString("responsecode");
                            String qttransref="";
                            if(qtresponsecode.equals("00")){
                                billdetails="Payment of N"+amount+" for "+destinationfirstname+" "+destinationmiddlename+" "+destinationlastname+" with account:"+creditaccount+".";
                                comtrans.setAmount(amount);
                                comtrans.setDestinationaccount(creditaccount);
                                comtrans.setDestinationbank(creditbank);
                                comtrans.setNarration(""+transid);
                                comtrans.setPhonenumber(phonenumber);
                                comtrans.setPreferredmedium("ipg");
                                comtrans.setSessionid(sessid);
                                comtrans.setSource(source);
                                comtrans.setSourcecard(cid);
                                comtrans.setSourcebank("card");
                                comtrans.setTransactionid(transid);
                                comtrans.setTranscode("IPG01");
                                comtrans.setTransdate(transdate);
                                comtrans.setTransreference(transref);
                                comtrans.setTranstype("ft");
                                comtrans.setDestinationfirstname(destinationfirstname);
                                comtrans.setDestinationlastname(destinationlastname);
                                comtrans.setDestinationmiddlename(destinationmiddlename);
                                comtrans.setSourcefirstname(sourcefirstname);
                                comtrans.setSourcelastname(sourcelastname);
                                comtrans.setSourcemiddlename(sourcemiddlename);
                                comtrans.setCustomerid("");
                                comtrans.setPaymentcode("");
                                PersistenceAgent.logTransactions(comtrans);
                                if(qtresponse.has("transactionRef")){
                                    resmsg="validated";
                                    qttransref=qtresponse.getString("transactionRef");
                                    tmresponse.put("qttransref", qttransref);

                                }
                                if(qtresponse.has("MscData")){
                                    MscData=qtresponse.getString("MscData");
                                    tmresponse.put("MscData", MscData);
                                    billdetails=billdetails+" MscData:"+MscData+".";
                                }
                                if(qtresponse.has("rechargePIN")){
                                    rechargePIN=qtresponse.getString("rechargePIN");
                                    tmresponse.put("rechargePIN", rechargePIN);
                                    billdetails=billdetails+" rechargePIN:"+rechargePIN+".";
                                }
                                tmresponse.put("responsecode", "00");
                                tmresponse.put("responsemessage", "funds transfer success-"+resmsg);
                                tmresponse.put("transref",transid); 
                                logot.setAmount(amount);
                                logot.setDestinationaccount(creditaccount);
                                logot.setDestinationbank(creditbank);
                                logot.setNarration("Billpayment"+transid);
                                logot.setPhonenumber(phonenumber);
                                logot.setPreferredmedium("ipg");
                                logot.setSessionid(sessid);
                                logot.setSource(source);
                                logot.setSourcecard(cid);
                                logot.setSourcebank("card");
                                logot.setTransactionid(transid);
                                logot.setTranscode("IPG01");
                                logot.setTransdate(transdate);
                                logot.setTransreference(qttransref);
                                logot.setTranstype("C");
                                logot.setDestinationfirstname(destinationfirstname);
                                logot.setDestinationlastname(destinationlastname);
                                logot.setDestinationmiddlename(destinationmiddlename);
                                logot.setSourcefirstname(sourcefirstname);
                                logot.setSourcelastname(sourcelastname);
                                logot.setSourcemiddlename(sourcemiddlename);
                                PersistenceAgent.logTransactions(logot);
                                logot = new Octopustransactions();
                                dbl.setActiondetails("funds transfer success: "+qttransref+" ");
                                dbl.setActionperformed("funds transfer success");
                                dbl.setActiontime(transdate);
                                dbl.setAmount(amount);
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("FT"+transid);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                                logger.info(phonenumber+"funds transfer success: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("funds transfer success");
                                tmlog.setActiondetails("funds transfer success: "+qttransref+" ");
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                mail.ftpaymentNotification(phonenumber, emailaddress, firstname+" "+lastname, transdate, "Funds Transfer", cardno, destinationfirstname+" "+destinationmiddlename+" "+destinationlastname, creditaccount, bankname, narration, notificationamount, transid, "Card", cardno, cardbank);
                                log.info(appname, classname, "Transaction Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Transaction ID "+transid);
                                logger.info(phonenumber+"Mail sent. Funds transfer success: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("Mail sent. Funds transfer success");
                                tmlog.setActiondetails("Mail sent. Funds transfer success: "+qttransref+" ");
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                op.addBeneficiary(source, transid, sessid, phonenumber, "", creditaccount, destinationfirstname+" "+destinationmiddlename+" "+destinationlastname, bankname, creditbank, "", "", "ft");
                                logger.info(phonenumber+"Beneficiary Saved. Funds transfer success: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("Beneficiary Saved. Funds transfer success");
                                tmlog.setActiondetails("Beneficiary Saved. Funds transfer success: "+qttransref+" ");
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                            }else{
                                //try NIP then log as dispute.
                                TransferNIP nip = new TransferNIP();
                                //senderaccount, debittype
                                String senderaccount=QUCOON_NIP_ACCOUNT;
                                String debittype="Card";
                                String nipresp = nip.doTransfer(source, transid, sessid, amount, creditbank, creditaccount, destinationlastname, destinationfirstname, phonenumber, emailaddress, firstname, lastname, transref, narration, senderaccount, debittype,bankname,cardbank);
                                if(nipresp.equals("00")){
                                    tmresponse.put("responsecode", "00");
                                    tmresponse.put("responsemessage", "funds transfer success-"+resmsg);
                                    tmresponse.put("transref",transid); 
                                }else{
                                resmsg="error";
                                log.dispute(appname, classname, "Dispute", "Transaction ID:"+transid+" Customer:"+phonenumber+" Beneficiary Account:"+creditaccount+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+creditbank+" Amount"+amount);
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "Transaction has been submitted successfully and is being processed. You shall be notified on completion.");
                                distrans.setAmount(amount);
                                distrans.setDestinationaccount(creditaccount);
                                distrans.setDestinationbank(creditbank);
                                distrans.setNarration(""+transid);
                                distrans.setPhonenumber(phonenumber);
                                distrans.setPreferredmedium("ipg");
                                distrans.setSessionid(sessid);
                                distrans.setSource(source);
                                distrans.setSourcecard(cid);
                                distrans.setSourcebank("card");
                                distrans.setTransactionid(transid);
                                distrans.setTranscode("IPG01");
                                distrans.setTransdate(transdate);
                                distrans.setTransreference(transref);
                                distrans.setTranstype("ft");
                                distrans.setDestinationfirstname(destinationfirstname);
                                distrans.setDestinationlastname(destinationlastname);
                                distrans.setDestinationmiddlename(destinationmiddlename);
                                distrans.setSourcefirstname(sourcefirstname);
                                distrans.setSourcelastname(sourcelastname);
                                distrans.setSourcemiddlename(sourcemiddlename);
                                distrans.setCustomerid("");
                                distrans.setPaymentcode("");
                                PersistenceAgent.logTransactions(distrans);
                                dbl.setActiondetails("funds transfer failed:reversal needed: ");
                                dbl.setActionperformed("funds transfer failed:reversal needed");
                                dbl.setActiontime(transdate);
                                dbl.setAmount(amount);
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("FT"+transid);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                                logger.info(phonenumber+"funds transfer failed: "+qttransref+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("funds transfer failed");
                                tmlog.setActiondetails("funds transfer failed: "+qttransref+" ");
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                
                                }
                            }
                        }
                            
                            
                            
                            
                            
                            
                            
                        }else if(debitcardrescode.equals("33")){
                            String transref=debitcardstat.getString("transref");
                            String paymentid =debitcardstat.getString("paymentid");
                            //String otp=debitcardstat.getString("otp");
                            logger.info(phonenumber+"otp sent-debit pending" +sessid+" transid: "+transid);
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("opt sent-debit pending");
                            tmlog.setActiondetails("opt sent-debit pending "+sessid+" transid: "+transid);
                            tmlog.setActiontime(transdate);
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            dbl.setActiondetails("opt sent-debit pending "+sessid+" transid: "+transid);
                            dbl.setActionperformed("opt sent-debit pending");
                            dbl.setActiontime(transdate);
                            dbl.setAmount(amount);
                            dbl.setDestinationaccount(creditaccount);
                            dbl.setDestinationbank(creditbank);
                            dbl.setNarration("DB"+transid+" "+creditaccount+" "+creditbank);
                            dbl.setPhonenumber(phonenumber);
                            dbl.setPreferredmedium("ipg");
                            dbl.setSessionid(sessid);
                            dbl.setSource(source);
                            dbl.setSourcecard(cid);
                            dbl.setSourcebank("card");
                            dbl.setTransactionid(transid);
                            PersistenceAgent.logTransactions(dbl);
                            dbl = new Debitlogs();
                            tmresponse.put("responsecode", "33");
                            tmresponse.put("responsemessage", resmsg);
                            tmresponse.put("transref",transref);
                            tmresponse.put("paymentid", paymentid);
                            //tmresponse.put("otp",otp);
                            tmresponse.put("source", source);
                            tmresponse.put("transid", transid);
                            tmresponse.put("sessid", sessid);
                            tmresponse.put("phonenumber", phonenumber);
                            tmresponse.put("cardstat", "true");
                            tmresponse.put("cid", cid);
                            tmresponse.put("amount", amount);
                            tmresponse.put("creditaccount", creditaccount);
                            tmresponse.put("creditbank", creditbank);
                            //transaction validate
                        }else{
                            //debitfailed
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "funds transfer failed-"+resmsg);
                                logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid);
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("debit failed");
                                tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                dbl.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                dbl.setActionperformed("debit failed");
                                dbl.setActiontime(transdate);
                                dbl.setAmount(amount);
                                dbl.setBankitstatus("");
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("DB"+transid+" "+creditaccount+" "+creditbank);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                        }
                    }else{
                        //potential fraud
                        tmresponse.put("responsecode", "22");
                        tmresponse.put("responsemessage", "potential fraud");
                        logger.info(phonenumber+"potential fraud:request phone not card phone" +sessid+" transid: "+transid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("potential fraud:request phone not card phone");
                        tmlog.setActiondetails("potential fraud:request phone not card phone "+sessid+" transid: "+transid);
                        tmlog.setActiontime(transdate);
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        dbl.setActiondetails("potential fraud:request phone not card phone "+sessid+" transid: "+transid);
                        dbl.setActionperformed("potential fraud:request phone not card phone");
                        dbl.setActiontime(transdate);
                        dbl.setAmount(amount);
                        dbl.setBankitstatus("");
                        dbl.setIpgstatus("true");
                        dbl.setDestinationaccount(creditaccount);
                        dbl.setDestinationbank(creditbank);
                        dbl.setNarration(narration);
                        dbl.setPhonenumber(phonenumber);
                        dbl.setPreferredmedium("ipg");
                        dbl.setSessionid(sessid);
                        dbl.setSource(source);
                        dbl.setSourcecard(cid);
                        dbl.setSourcebank("card");
                        dbl.setTransactionid(transid);
                        PersistenceAgent.logTransactions(dbl);
                        dbl = new Debitlogs();
                        System.out.println(output);
                    }
                    
                }
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("parse error ");
            tmlog.setActiondetails("parse error "+output);
            tmlog.setActiontime(transdate);
            PersistenceAgent.logTransactions(tmlog);
            dbl.setActiondetails("parse error "+output);
            dbl.setActionperformed("parse error");
            dbl.setActiontime(transdate);
            dbl.setAmount(amount);
            dbl.setBankitstatus(output+"");
            dbl.setDestinationaccount(creditaccount);
            dbl.setDestinationbank(creditbank);
            dbl.setNarration(narration);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("bankit");
            dbl.setRequesttime(transdate);
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourceaccount(debitaccount);
            dbl.setSourcebank(debitbank);
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
        }
    return output;}
    
    public String debitAccountBankit(String source,String sessid,String transid,String phonenumber,String debitaccount,String accountid,String debitbank,String creditaccount,String creditbank,String amount,String narration,String passcode)
    {
        boolean status=false;
        Debitlogs dbl = new Debitlogs();
        Tmlogs tmlog = new Tmlogs();
        Octopustransactions ocdb = new Octopustransactions();
        BankITOperations bio = new BankITOperations();
        Utilities utils = new Utilities();
        String bankitresponse="";
        String bankitresponsecode="";
        String bankitresponsemessage="";
        String output="";
        JSONObject bankitjson;
        JSONObject tmresponse = new JSONObject();
        phonenumber=utils.formatPhone(phonenumber);
        logger.info(phonenumber+"trying to debit account with bankit sessid: "+sessid+" transid: "+transid);
        tmlog.setSource(source);
        tmlog.setPhonenumber(phonenumber);
        tmlog.setSessionid(sessid);
        tmlog.setTransactionid(transid);
        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
        tmlog.setActionperformed("trying to debit account with bankit");
        tmlog.setActiondetails("trying to debit account with bankit sessid: "+sessid+" transid: "+transid);
        PersistenceAgent.logTransactions(tmlog);
        tmlog = new Tmlogs();
        dbl.setActiondetails("trying to debit account with bankit sessid: "+sessid+" transid: "+transid);
        dbl.setActionperformed("trying to debit account with bankit");
        dbl.setAmount(amount);
        dbl.setBankitstatus("");
        dbl.setDestinationaccount(creditaccount);
        dbl.setDestinationbank(creditbank);
        dbl.setNarration(narration);
        dbl.setPhonenumber(phonenumber);
        dbl.setPreferredmedium("bankit");
        dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
        dbl.setSessionid(sessid);
        dbl.setSource(source);
        dbl.setSourceaccount(debitaccount);
        dbl.setSourcebank(debitbank);
        dbl.setTransactionid(transid);
        PersistenceAgent.logTransactions(dbl);
        dbl = new Debitlogs();
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            bankitresponse=bio.sendDebit(source, transid, sessid, accountid, phonenumber, passcode,debitaccount,amount,narration);
            bankitjson = new JSONObject(bankitresponse);
            bankitresponsecode=bankitjson.getString("responsecode");
            bankitresponsemessage=bankitjson.getString("responsemessage");
            logger.info(phonenumber+"bankit response: "+bankitjson.toString().replace("\"", ":"));
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("bankit response");
            tmlog.setActiondetails("bankit response: "+bankitjson.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            if(bankitresponsecode.equals("00")){
                status=true;
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"bankit response debit success: "+bankitresponsecode);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("bankit response debit success");
                tmlog.setActiondetails("bankit response debit success: "+bankitresponsecode);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                dbl.setActiondetails("bankit response debit success: "+bankitresponsecode);
                dbl.setActionperformed("bankit response debit success");
                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                dbl.setAmount(amount);
                dbl.setBankitstatus(status+"");
                dbl.setDestinationaccount(creditaccount);
                dbl.setDestinationbank(creditbank);
                dbl.setNarration(narration);
                dbl.setPhonenumber(phonenumber);
                dbl.setPreferredmedium("bankit");
                dbl.setSessionid(sessid);
                dbl.setSource(source);
                dbl.setSourceaccount(debitaccount);
                dbl.setSourcebank(debitbank);
                dbl.setTransactionid(transid);
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
            }else{
                status=false;
                dbl.setActiondetails("bankit response debit failed: "+bankitresponsecode);
                dbl.setActionperformed("bankit response debit failed");
                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                dbl.setAmount(amount);
                dbl.setBankitstatus(status+"");
                dbl.setDestinationaccount(creditaccount);
                dbl.setDestinationbank(creditbank);
                dbl.setNarration(narration);
                dbl.setPhonenumber(phonenumber);
                dbl.setPreferredmedium("bankit");
                dbl.setSessionid(sessid);
                dbl.setSource(source);
                dbl.setSourceaccount(debitaccount);
                dbl.setSourcebank(debitbank);
                dbl.setTransactionid(transid);
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
                if(bankitresponsecode.equals("11")){
                    //cannot debit account
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", bankitresponsemessage);
                    logger.info(phonenumber+" cannot debit account with bankit"+bankitresponsecode+" "+bankitresponsemessage);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("cannot debit account with bankit");
                    tmlog.setActiondetails("cannot debit account with bankit: "+bankitresponsecode+" "+bankitresponsemessage);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else if(bankitresponsecode.equals("22")){
                    //cannot receive otp 
                    tmresponse.put("responsecode", "22");
                    tmresponse.put("responsemessage", bankitresponsemessage);
                    logger.info(phonenumber+"cannot receive otp with bankit"+bankitresponsecode+" "+bankitresponsemessage);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("cannot receive otp with bankit");
                    tmlog.setActiondetails("cannot receive otp with bankit: "+bankitresponsecode+" "+bankitresponsemessage);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else if(bankitresponsecode.equals("33")){
                    //cannot send account 
                    tmresponse.put("responsecode", "33");
                    tmresponse.put("responsemessage", bankitresponsemessage);
                    logger.info(phonenumber+"cannot send account with bankit"+bankitresponsecode+" "+bankitresponsemessage);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("cannot send account with bankit");
                    tmlog.setActiondetails("cannot send account with bankit: "+bankitresponsecode+" "+bankitresponsemessage);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", bankitresponsecode);
                    tmresponse.put("responsemessage", bankitresponsemessage);
                    logger.info(phonenumber+"cannot send account with bankit"+bankitresponsecode);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("cannot send account with bankit");
                    tmlog.setActiondetails("cannot send account with bankit: "+bankitresponsecode);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
                output=tmresponse.toString();
            }
        }catch(Exception e){
            status=false;
            tmlog = new Tmlogs();
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ status);
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("parse error ");
            tmlog.setActiondetails("parse error "+status);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            dbl.setActiondetails("parse error "+status);
            dbl.setActionperformed("parse error");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amount);
            dbl.setBankitstatus(status+"");
            dbl.setDestinationaccount(creditaccount);
            dbl.setDestinationbank(creditbank);
            dbl.setNarration(narration);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("bankit");
            dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourceaccount(debitaccount);
            dbl.setSourcebank(debitbank);
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
        }


    return output;}
    
    public boolean debitAccountHbng(String source,String sessid,String transid,String phonenumber,String auth,String debitaccount,String debitbank,String creditaccount,String creditbank,String amount,String narration,String product)
    {
        boolean status=false;
        Octopustransactions logot= new Octopustransactions();
        Debitlogs dbl = new Debitlogs();
        Tmlogs tmlog = new Tmlogs();
        HbngInterface hbng = new HbngInterface();
        Utilities utils = new Utilities();
        String heritageresponse="";
        String heritageresponsecode="";
        String heritageresponsemsg="";
        String tstamp="";
        String stan="";
        String transref="";
        String rvsref="";
        JSONObject heritagejson;
        phonenumber=utils.formatPhone(phonenumber);
        logger.info(phonenumber+"trying to debit account with heritage sessid: "+sessid+" transid: "+transid);
        tmlog.setSource(source);
        tmlog.setPhonenumber(phonenumber);
        tmlog.setSessionid(sessid);
        tmlog.setTransactionid(transid);
        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
        tmlog.setActionperformed("trying to debit account with heritage");
        tmlog.setActiondetails("trying to debit account with heritage sessid: "+sessid+" transid: "+transid);
        PersistenceAgent.logTransactions(tmlog);
        tmlog = new Tmlogs();
        dbl.setActiondetails("trying to debit account with heritage sessid: "+sessid+" transid: "+transid);
        dbl.setActionperformed("trying to debit account with heritage");
        dbl.setAmount(amount);
        dbl.setBankitstatus("");
        dbl.setDestinationaccount(creditaccount);
        dbl.setDestinationbank(creditbank);
        dbl.setNarration(narration);
        dbl.setPhonenumber(phonenumber);
        dbl.setPreferredmedium("heritage");
        dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
        dbl.setSessionid(sessid);
        dbl.setSource(source);
        dbl.setSourceaccount(debitaccount);
        dbl.setSourcebank(debitbank);
        dbl.setTransactionid(transid);
        PersistenceAgent.logTransactions(dbl);
        dbl = new Debitlogs();
        try{
            heritageresponse=hbng.hbnginternaltrf(source, sessid, transid, auth, debitaccount, creditaccount, amount, narration);
            heritagejson = new JSONObject(heritageresponse);
            heritageresponsecode=heritagejson.getString("responsecode");
            heritageresponsemsg=heritagejson.getString("responsemessage");
            logger.info(phonenumber+"heritage response: "+heritagejson.toString().replace("\"", ":"));
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("heritage response");
            tmlog.setActiondetails("heritage response: "+heritagejson.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            if(heritageresponsecode.equals("00")){
                tstamp=heritagejson.getString("tstamp");
                stan=heritagejson.getString("stan");
                rvsref=heritagejson.getString("retrievalreferenceno");
                transref=heritagejson.getString("reference");
                status=true;
                logot.setAmount(amount);
                logot.setDestinationaccount(creditaccount);
                logot.setDestinationbank(creditbank);
                logot.setNarration(narration);
                logot.setPhonenumber(phonenumber);
                logot.setPreferredmedium("heritage");
                logot.setSessionid(sessid);
                logot.setSource(source);
                logot.setSourceaccount(debitaccount);
                logot.setSourcebank(debitbank);
                logot.setTransactionid(transid);
                logot.setTranscode("HTG01");
                logot.setTransdate(utils.getDate()+" "+utils.getTime());
                logot.setTransreference(transref);
                logot.setTransrvsreference(rvsref);
                logot.setStan(stan);
                logot.setTstamp(tstamp);
                logot.setTranstype("D");
                logot.setProduct(product);
                PersistenceAgent.logTransactions(logot);
                logot = new Octopustransactions();
                logot.setAmount(amount);
                logot.setDestinationaccount(creditaccount);
                logot.setDestinationbank(creditbank);
                logot.setNarration(narration);
                logot.setPhonenumber(phonenumber);
                logot.setPreferredmedium("heritage");
                logot.setSessionid(sessid);
                logot.setSource(source);
                logot.setSourceaccount(debitaccount);
                logot.setSourcebank(debitbank);
                logot.setTransactionid(transid);
                logot.setTranscode("HTG01");
                logot.setTransdate(utils.getDate()+" "+utils.getTime());
                logot.setTransreference(transref);
                logot.setStan(stan);
                logot.setTransreference(transref);
                logot.setTransrvsreference(rvsref);
                logot.setTstamp(tstamp);
                logot.setTranstype("C");
                logot.setProduct(product);
                PersistenceAgent.logTransactions(logot);
                logot = new Octopustransactions();
                logger.info(phonenumber+"heritage response debit success: "+heritageresponsecode+" "+heritageresponsemsg);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("heritage response debit success");
                tmlog.setActiondetails("heritage response debit success: "+heritageresponsecode+" "+heritageresponsemsg);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                dbl.setActiondetails("heritage response debit success: "+heritageresponsecode+" "+heritageresponsemsg);
                dbl.setActionperformed("heritage response debit success");
                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                dbl.setAmount(amount);
                dbl.setBankitstatus("");
                dbl.setDestinationaccount(creditaccount);
                dbl.setDestinationbank(creditbank);
                dbl.setNarration(narration);
                dbl.setPhonenumber(phonenumber);
                dbl.setPreferredmedium("heritage");
                dbl.setSessionid(sessid);
                dbl.setSource(source);
                dbl.setSourceaccount(debitaccount);
                dbl.setSourcebank(debitbank);
                dbl.setTransactionid(transid);
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
            }else{
                status=false;
                logger.info(phonenumber+"heritage response debit failed: "+heritageresponsecode+" "+heritageresponsemsg);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("heritage response debit failed");
                tmlog.setActiondetails("heritage response debit failed: "+heritageresponsecode+" "+heritageresponsemsg);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                dbl.setActiondetails("heritage response debit failed: "+heritageresponsecode+" "+heritageresponsemsg);
                dbl.setActionperformed("heritage response debit failed");
                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                dbl.setAmount(amount);
                dbl.setBankitstatus("");
                dbl.setDestinationaccount(creditaccount);
                dbl.setDestinationbank(creditbank);
                dbl.setNarration(narration);
                dbl.setPhonenumber(phonenumber);
                dbl.setPreferredmedium("heritage");
                dbl.setSessionid(sessid);
                dbl.setSource(source);
                dbl.setSourceaccount(debitaccount);
                dbl.setSourcebank(debitbank);
                dbl.setTransactionid(transid);
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
            }
        }catch(Exception e){
            status=false;
            tmlog = new Tmlogs();
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ status);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("parse error ");
            tmlog.setActiondetails("parse error "+status);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            dbl.setActiondetails("parse error "+status);
            dbl.setActionperformed("parse error");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amount);
            dbl.setBankitstatus("");
            dbl.setDestinationaccount(creditaccount);
            dbl.setDestinationbank(creditbank);
            dbl.setNarration(narration);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("heritage");
            dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourceaccount(debitaccount);
            dbl.setSourcebank(debitbank);
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
        }


    return status;}
    
    public String debitCard(String source,String sessid,String transid,String phonenumber,String amount,String sourcefirstname,String sourcemiddlename,String sourcelastname,String cid,String cardname,String destinationfirstname,String destinationmiddlename,String destinationlastname,String country,String currency,String creditaccount,String creditbank,String transtype,String transcode,String narration)
    {
        String status="false";
        JSONObject tmresponse = new JSONObject();
        Octopuspndtrans octpnd = new Octopuspndtrans();
        Octopustransactions logot= new Octopustransactions();
        Debitlogs dbl = new Debitlogs();
        Tmlogs tmlog = new Tmlogs();
        IpgOperations ipg = new IpgOperations();
        Utilities utils = new Utilities();
        String ipgresponse="";
        String ipgresponsecode="";
        String ipgresponsemsg="";
        JSONObject ipgjson;
        String transactionRef="";
        String otp;
        String output="";
        String shortnarration;
        phonenumber=utils.formatPhone(phonenumber);
        System.out.println("logged trying to debit card "+utils.getDate()+" "+utils.getTime());
        logger.info(phonenumber+"trying to debit with card sessid: "+sessid+" transid: "+transid);
        tmlog.setSource(source);
        tmlog.setPhonenumber(phonenumber);
        tmlog.setSessionid(sessid);
        tmlog.setTransactionid(transid);
        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
        tmlog.setActionperformed("trying to debit with card");
        tmlog.setActiondetails("trying to debit with card sessid: "+sessid+" transid: "+transid);
        PersistenceAgent.logTransactions(tmlog);
        tmlog = new Tmlogs();
        dbl.setActiondetails("trying to debit with card sessid: "+sessid+" transid: "+transid);
        dbl.setActionperformed("trying to debit with card");
        dbl.setAmount(amount);
        dbl.setBankitstatus("");
        dbl.setDestinationaccount(creditaccount);
        dbl.setDestinationbank(creditbank);
        dbl.setNarration("DB"+transid);
        dbl.setPhonenumber(phonenumber);
        dbl.setPreferredmedium("");
        dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
        dbl.setSessionid(sessid);
        dbl.setSource(source);
        dbl.setSourcecard(cid);
        dbl.setSourcebank("card");
        dbl.setTransactionid(transid);
        PersistenceAgent.logTransactions(dbl);
        dbl = new Debitlogs();
        try{
            System.out.println("actual card debit card "+utils.getDate()+" "+utils.getTime());
            shortnarration=narration.replace(" ", "");
            shortnarration=narration.replace(".", "");
            shortnarration=narration.replace(",", "");
            shortnarration=narration.toUpperCase();
            if(narration.length()>=5){
                shortnarration=narration.substring(0,5);
            }
            ipgresponse=ipg.debitcard(source, "QUC|"+transtype.toUpperCase()+"|"+shortnarration+"|"+transid, sessid, phonenumber, amount, cid, cardname, country, currency);
            System.out.println("debit card responded "+utils.getDate()+" "+utils.getTime());
            System.out.println("ipg response "+ipgresponse);
            ipgjson = new JSONObject(ipgresponse);
            ipgresponsecode=ipgjson.getString("responsecode");
            ipgresponsemsg=ipgjson.getString("responsemessage");
            logger.info(phonenumber+"ipg response: "+ipgjson.toString().replace("\"", ":"));
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("ipg response");
            tmlog.setActiondetails("ipg response: "+ipgjson.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            if(ipgresponsecode.equals("00")){
                transactionRef=ipgjson.getString("inteswitchreference");
                status="true";
                tmresponse.put("responsemessage", "success");
                tmresponse.put("responsecode", "00");
                tmresponse.put("transref", transactionRef);
                logot.setAmount(amount);
                logot.setDestinationaccount(creditaccount);
                logot.setDestinationbank(creditbank);
                logot.setNarration("DB"+transid);
                logot.setPhonenumber(phonenumber);
                logot.setPreferredmedium("ipg");
                logot.setSessionid(sessid);
                logot.setSource(source);
                logot.setSourcecard(cid);
                logot.setSourcebank("card");
                logot.setTransactionid(transid);
                logot.setTranscode("IPG01");
                logot.setTransdate(utils.getDate()+" "+utils.getTime());
                logot.setTransreference(transactionRef);
                logot.setTranstype("D");
                logot.setDestinationfirstname(destinationfirstname);
                logot.setDestinationlastname(destinationlastname);
                logot.setDestinationmiddlename(destinationmiddlename);
                logot.setSourcefirstname(sourcefirstname);
                logot.setSourcelastname(sourcelastname);
                logot.setSourcemiddlename(sourcemiddlename);
                PersistenceAgent.logTransactions(logot);
                logot = new Octopustransactions();
                logger.info(phonenumber+"ipg response debit success: "+ipgresponsecode+" "+ipgresponsemsg);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("ipg response debit success");
                tmlog.setActiondetails("ipg response debit success: "+ipgresponsecode+" "+ipgresponsemsg);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                dbl.setActiondetails("ipg response debit success: "+ipgresponsecode+" "+ipgresponsemsg);
                dbl.setActionperformed("ipg response debit success");
                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                dbl.setAmount(amount);
                dbl.setDestinationaccount(creditaccount);
                dbl.setDestinationbank(creditbank);
                dbl.setNarration("DB"+transid);
                dbl.setPhonenumber(phonenumber);
                dbl.setPreferredmedium("ipg");
                dbl.setSessionid(sessid);
                dbl.setSource(source);
                dbl.setSourcecard(cid);
                dbl.setSourcebank("card");
                dbl.setTransactionid(transid);
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
            }else if (ipgresponsecode.equals("T0")){
                status="otp";
                transactionRef=ipgjson.getString("transactionid");
                String paymentid=ipgjson.getString("paymentId");
                ipgresponsemsg=ipgjson.getString("responsemessage");
                //String encryptedotp = utils.encryptPass(otp);
                tmresponse.put("responsemessage", ipgresponsemsg);
                tmresponse.put("responsecode", "33");
                tmresponse.put("transref", transactionRef);
                tmresponse.put("paymentid", paymentid);
                //tmresponse.put("otp", otp);
                octpnd.setAmount(amount);
                octpnd.setDestinationaccount(creditaccount);
                octpnd.setDestinationbank(creditbank);
                octpnd.setPhonenumber(phonenumber);
                octpnd.setSessionid(sessid);
                octpnd.setSource(source);
                octpnd.setSourcebank("card");
                octpnd.setSourcecard(cid);
                //octpnd.setToken(encryptedotp);
                octpnd.setTokenstat("pending");
                octpnd.setTransactionid(transid);
                octpnd.setTransactionref(transactionRef);
                octpnd.setTranscode(transcode);
                octpnd.setTranstype(transtype);
                octpnd.setDestinationfirstname(destinationfirstname);
                octpnd.setDestinationlastname(destinationlastname);
                octpnd.setDestinationmiddlename(destinationmiddlename);
                octpnd.setSourcefirstname(sourcefirstname);
                octpnd.setSourcelastname(sourcelastname);
                octpnd.setSourcemiddlename(sourcemiddlename);
                octpnd.setTransdate(utils.getDate()+" "+utils.getTime());
                octpnd.setNarration(narration);
                PersistenceAgent.logTransactions(octpnd);
                logger.info(phonenumber+"ipg response otp sent: "+ipgresponsecode+" "+ipgresponsemsg);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("ipg response otp sent");
                tmlog.setActiondetails("ipg response otp sent: "+ipgresponsecode+" "+ipgresponsemsg);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                dbl.setActiondetails("ipg response otp sent: "+ipgresponsecode+" "+ipgresponsemsg);
                dbl.setActionperformed("ipg response otp sent");
                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                dbl.setAmount(amount);
                dbl.setDestinationaccount(creditaccount);
                dbl.setDestinationbank(creditbank);
                dbl.setNarration("DB"+transid);
                dbl.setPhonenumber(phonenumber);
                dbl.setPreferredmedium("ipg");
                dbl.setSessionid(sessid);
                dbl.setSource(source);
                dbl.setSourcecard(cid);
                dbl.setSourcebank("card");
                dbl.setTransactionid(transid);
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
            }else{
                status="failed";
                ipgresponsemsg=ipgjson.getString("responsemessage");
                tmresponse.put("responsemessage", ipgresponsemsg);
                tmresponse.put("responsecode", "11");
                logger.info(phonenumber+"ipg response debit failed: "+ipgresponsecode+" "+ipgresponsemsg);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("ipg response debit failed");
                tmlog.setActiondetails("ipg response debit failed: "+ipgresponsecode+" "+ipgresponsemsg);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                dbl.setActiondetails("ipg response debit failed: "+ipgresponsecode+" "+ipgresponsemsg);
                dbl.setActionperformed("ipg response debit failed");
                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                dbl.setAmount(amount);
                dbl.setBankitstatus(status+"");
                dbl.setDestinationaccount(creditaccount);
                dbl.setDestinationbank(creditbank);
                dbl.setNarration("DB"+transid);
                dbl.setPhonenumber(phonenumber);
                dbl.setPreferredmedium("ipg");
                dbl.setSessionid(sessid);
                dbl.setSource(source);
                dbl.setSourcecard(cid);
                dbl.setSourcebank("card");
                dbl.setTransactionid(transid);
                PersistenceAgent.logTransactions(dbl);
                dbl = new Debitlogs();
            }
            output = tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            tmlog = new Tmlogs();
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ status);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("parse error ");
            tmlog.setActiondetails("parse error "+status);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            dbl.setActiondetails("parse error "+status);
            dbl.setActionperformed("parse error");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amount);
            dbl.setDestinationaccount(creditaccount);
            dbl.setDestinationbank(creditbank);
            dbl.setNarration("DB"+transid);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("ipg");
            dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourcecard(cid);
            dbl.setSourcebank("card");
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
        }


    return output;}
    
    public String billPayment(String source,String sessid,String transid,String phonenumber,String accountstat,String debitaccount,String debitbank,String customerid,String paymentcode,String cardstat,String cid,String amount,String narration,String inputpasscode, JSONObject req)
    {   
        OctopusTransactions ot = new OctopusTransactions();
        Octopuscustomerstm ocusttm = new Octopuscustomerstm();
        Octopustransactions logot = new Octopustransactions();
        Octopuscompletetransactions comtrans = new Octopuscompletetransactions();
        Octopusdisputetransactions distrans = new Octopusdisputetransactions();
        Debitlogs dbl = new Debitlogs();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        boolean debitaccountstat=false;
        String debitcardstat_str="";
        JSONObject debitcardstat;
        String debitcardrescode;
        String debitcardresmsg;
        Octopuscustomersaccounts ocustacc;
        Octopuscustomerscards ocustcards;
        String accountid="";
        String dbpasscode="";
        String output="";
        boolean passcodestatus;
        Mailer mail=new Mailer();
        String auth=Authorization.getAuth();
        JSONObject tmresponse = new JSONObject();
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        String creditaccount="5100274866";
        String creditbank="heritage";
        String billdetails="";
        String paymentcoderesponse_str="";
        String paymentcoderesponsecode="";
        String MscData="";
        String rechargePIN="";
        String billername="";
        String transdate=utils.getDate()+" "+utils.getTime();
        JSONObject paymentcoderesponse;
        Bankcodes bankcode = DataObjects.getBankcodes_code(creditbank);
        String bankname=(bankcode.getBankname()!=null)?bankcode.getBankname():"";
        phonenumber=utils.formatPhone(phonenumber);
        ocusttm=DataObjects.getOctopuscustomerstm_rec(phonenumber);
        String firstname = (ocusttm.getFirstname()!=null)?ocusttm.getFirstname():"";
        String lastname = (ocusttm.getLastname()!=null)?ocusttm.getLastname():"";
        String emailaddress = (ocusttm.getEmailaddress()!=null)?ocusttm.getEmailaddress():"";
        NumberFormat formatter = new DecimalFormat("####.00");
        phonenumber=utils.formatPhone(phonenumber);
        transid=utils.generateTransid();
        amount = formatter.format(new BigDecimal(amount)).toString();
        //debit source is account
        System.out.println("class entered "+utils.getDate()+" "+utils.getTime());
        try{
            System.out.println("trying to login "+utils.getDate()+" "+utils.getTime());
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, inputpasscode,"");
            System.out.println("login successful "+utils.getDate()+" "+utils.getTime());
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a user
                tmresponse.put("responsecode", "44");
                tmresponse.put("responsemessage", "not a user");
            }else if(loginrescode.equals("22")){
                //wrong passcode
                tmresponse.put("responsecode", "55");
                tmresponse.put("responsemessage", "wrong passcode");
            }else if(loginrescode.equals("33")){
                //wrong passcode
                tmresponse.put("responsecode", "66");
                tmresponse.put("responsemessage", "Account blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to reset account");
            }else if(loginrescode.equals("00")){
                tmresponse.put("responsedate", utils.getDate());
                tmresponse.put("responsetime", utils.getTime());
                String customerstatstr = op.customerStatus(source, transid, sessid, phonenumber, "NG");
                JSONObject customerstat = new JSONObject(customerstatstr);
                String sourcefirstname=customerstat.getString("firstname");
                String sourcelastname=customerstat.getString("lastname");
                String sourceemailadress=customerstat.getString("emailaddress");
                String sourcemiddlename="";
                if(accountstat.toLowerCase().equals("true")){
                    logger.info(phonenumber+"new debit customer request using account" +sessid+" transid: "+transid);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    tmlog.setActionperformed("new debit customer request using account");
                    tmlog.setActiondetails("new debit customer request using account: "+sessid+" transid: "+transid);
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    dbl.setActiondetails("new debit customer request using account "+sessid+" transid: "+transid);
                    dbl.setActionperformed("new debit customer request using account");
                    dbl.setAmount(amount);
                    dbl.setBankitstatus("");
                    dbl.setDestinationaccount(creditaccount);
                    dbl.setDestinationbank(creditbank);
                    dbl.setNarration(narration);
                    dbl.setPhonenumber(phonenumber);
                    dbl.setPreferredmedium("");
                    dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
                    dbl.setSessionid(sessid);
                    dbl.setSource(source);
                    dbl.setSourceaccount(debitaccount);
                    dbl.setSourcebank(debitbank);
                    dbl.setTransactionid(transid);
                    PersistenceAgent.logTransactions(dbl);
                    dbl = new Debitlogs();
                    ocustacc= DataObjects.getOctopuscustomersaccounts_acc(debitaccount);
                    String phno = ocustacc.getPhonenumber() != null?ocustacc.getPhonenumber():"";
                    //if phonenumber sending request is same with phonenumber that registered account
                    if(phno.equals(phonenumber)){
                        logger.info(phonenumber+"request phone is account phone" +sessid+" transid: "+transid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("request phone is account phone");
                        tmlog.setActiondetails("request phone is account phone: "+sessid+" transid: "+transid);
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        dbl.setActiondetails("request phone is account phone "+sessid+" transid: "+transid);
                        dbl.setActionperformed("request phone is account phone");
                        dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                        dbl.setAmount(amount);
                        dbl.setBankitstatus("");
                        dbl.setDestinationaccount(creditaccount);
                        dbl.setDestinationbank(creditbank);
                        dbl.setNarration(narration);
                        dbl.setPhonenumber(phonenumber);
                        dbl.setPreferredmedium("");
                        dbl.setSessionid(sessid);
                        dbl.setSource(source);
                        dbl.setSourceaccount(debitaccount);
                        dbl.setSourcebank(debitbank);
                        dbl.setTransactionid(transid);
                        PersistenceAgent.logTransactions(dbl);
                        dbl = new Debitlogs();
                        //get accountid
                        accountid=ocustacc.getBankitid();
                        //debit account
                        if(debitbank.equals("030")){
                            tmresponse.put("debitmedium", "heritage"); 
                            tmresponse.put("creditmedium", "heritage");
                            //debit heritage accnt credit ledger
                            debitaccountstat=ot.debitAccountHbng(source, sessid, transid, phonenumber, auth, debitaccount,debitbank, creditaccount,"030", amount, narration,"bp");
                            if(debitaccountstat==true){
                                //success
                                logger.info(phonenumber+"debit success" +sessid+" transid: "+transid);
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("debit success");
                                tmlog.setActiondetails("debit success "+sessid+" transid: "+transid);
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                dbl.setActiondetails("debit success "+sessid+" transid: "+transid);
                                dbl.setActionperformed("debit success");
                                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                                dbl.setAmount(amount);
                                dbl.setBankitstatus("");
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration(narration);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("heritage");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourceaccount(debitaccount);
                                dbl.setSourcebank(debitbank);
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                                tmresponse.put("responsecode", "00");
                                tmresponse.put("responsemessage", "bill payment success");
                                //sendbill advice
                            }else{
                                //debitfailed
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "bill payment failed");
                                logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid);
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("debit failed");
                                tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                dbl.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                dbl.setActionperformed("debit failed");
                                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                                dbl.setAmount(amount);
                                dbl.setBankitstatus("");
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration(narration);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("heritage");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourceaccount(debitaccount);
                                dbl.setSourcebank(debitbank);
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                            }
                        }else{
                            tmresponse.put("debitmedium", "bankit");
                            tmresponse.put("responsecode", "00");
                            tmresponse.put("responsemessage", "bill payment success");
                            String debitaccountresponse_str=ot.debitAccountBankit(source, sessid, transid, phonenumber, debitaccount, accountid, debitbank, creditaccount, creditbank, amount, narration, inputpasscode);
                            JSONObject debitaccountresponse = new JSONObject(debitaccountresponse_str);
                            String debitaccountresponsecode=debitaccountresponse.getString("responsecode");
                            String debitaccountresponsemessage=debitaccountresponse.getString("responsemessage");
                            if(debitaccountresponsecode.equals("00")){
                                tmresponse.put("responsecode","00");
                                tmresponse.put("responsemessage","success");
                                logot.setAmount(amount);
                                logot.setDestinationaccount(creditaccount);
                                logot.setDestinationbank(creditbank);
                                logot.setNarration("DB"+transid);
                                logot.setPhonenumber(phonenumber);
                                logot.setPreferredmedium("bankit");
                                logot.setSessionid(sessid);
                                logot.setSource(source);
                                logot.setSourcecard("");
                                logot.setSourcebank(debitbank);
                                logot.setTransactionid(transid);
                                logot.setTranscode("BKT01");
                                logot.setTransdate(utils.getDate()+" "+utils.getTime());
                                logot.setTransreference(transid);
                                logot.setTranstype("D");
                                logot.setDestinationfirstname("");
                                logot.setDestinationlastname("");
                                logot.setDestinationmiddlename("");
                                logot.setSourcefirstname(sourcefirstname);
                                logot.setSourcelastname(sourcelastname);
                                logot.setSourcemiddlename(sourcemiddlename);
                                PersistenceAgent.logTransactions(logot);
                                logot = new Octopustransactions();
                                QtServices qts = new QtServices();
                                //send bill advice
                                String resmsg="validated";
                                amount = amount.replace(".", "");
                                System.out.println("amount to qt"+amount);
                                String qtresponsestr = qts.payBill(source,transid,sessid,paymentcode, customerid, phonenumber, amount, "1528"+transid.substring(10,transid.length()));
                                JSONObject qtresponse = new JSONObject(qtresponsestr);
                                String qtresponsecode=qtresponse.getString("responsecode");
                                String qttransref="";
                                if(qtresponsecode.equals("00")){
                                    comtrans.setAmount(amount);
                                    comtrans.setDestinationaccount(creditaccount);
                                    comtrans.setDestinationbank(creditbank);
                                    comtrans.setNarration(""+transid);
                                    comtrans.setPhonenumber(phonenumber);
                                    comtrans.setPreferredmedium("bankit");
                                    comtrans.setSessionid(sessid);
                                    comtrans.setSource(source);
                                    comtrans.setSourcecard("");
                                    comtrans.setSourcebank(debitbank);
                                    comtrans.setTransactionid(transid);
                                    comtrans.setTranscode("BKT01");
                                    comtrans.setTransdate(utils.getDate()+" "+utils.getTime());
                                    comtrans.setTransreference(transid);
                                    comtrans.setTranstype("bp");
                                    comtrans.setDestinationfirstname("");
                                    comtrans.setDestinationlastname("");
                                    comtrans.setDestinationmiddlename("");
                                    comtrans.setSourcefirstname(sourcefirstname);
                                    comtrans.setSourcelastname(sourcelastname);
                                    comtrans.setSourcemiddlename(sourcemiddlename);
                                    comtrans.setCustomerid(customerid);
                                    comtrans.setPaymentcode(paymentcode);
                                    PersistenceAgent.logTransactions(comtrans);
                                    if(qtresponse.has("transactionRef")){
                                    qttransref=qtresponse.getString("transactionRef");
                                    tmresponse.put("qttransref", qttransref);
                                    }
                                    tmresponse.put("responsecode", "00");
                                    tmresponse.put("responsemessage", "bill payment success");
                                    tmresponse.put("transref",transid); 
                                    logot.setAmount(amount);
                                    logot.setDestinationaccount(creditaccount);
                                    logot.setDestinationbank(creditbank);
                                    logot.setNarration("Billpayment"+transid);
                                    logot.setPhonenumber(phonenumber);
                                    logot.setPreferredmedium("qt");
                                    logot.setSessionid(sessid);
                                    logot.setSource(source);
                                    logot.setSourcecard(cid);
                                    logot.setSourcebank(debitbank);
                                    logot.setTransactionid(transid);
                                    logot.setTranscode("BKT01");
                                    logot.setTransdate(utils.getDate()+" "+utils.getTime());
                                    logot.setTransreference(qttransref);
                                    logot.setTranstype("C");
                                    logot.setDestinationfirstname("");
                                    logot.setDestinationlastname("");
                                    logot.setDestinationmiddlename("");
                                    logot.setSourcefirstname(sourcefirstname);
                                    logot.setSourcelastname(sourcelastname);
                                    logot.setSourcemiddlename(sourcemiddlename);
                                    PersistenceAgent.logTransactions(logot);
                                    logot = new Octopustransactions();
                                    dbl.setActiondetails("bill payment success: "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                    dbl.setActionperformed("bill payment success");
                                    dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                                    dbl.setAmount(amount);
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration("DB"+transid);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("bankit");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourcecard("");
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                    logger.info(phonenumber+"bill payment success: "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("bill payment success");
                                    tmlog.setActiondetails("bill payment success: "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                }else{
                                    tmresponse.put("responsecode", "11");
                                    tmresponse.put("responsemessage", "bill payment failed:reversal needed");
                                    dbl.setActiondetails("bill payment failed:reversal needed: ");
                                    dbl.setActionperformed("bill payment failed:reversal needed");
                                    dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                                    dbl.setAmount(amount);
                                    dbl.setDestinationaccount(creditaccount);
                                    dbl.setDestinationbank(creditbank);
                                    dbl.setNarration("DB"+transid);
                                    dbl.setPhonenumber(phonenumber);
                                    dbl.setPreferredmedium("bankit");
                                    dbl.setSessionid(sessid);
                                    dbl.setSource(source);
                                    dbl.setSourcecard(cid);
                                    dbl.setSourcebank(debitbank);
                                    dbl.setTransactionid(transid);
                                    PersistenceAgent.logTransactions(dbl);
                                    dbl = new Debitlogs();
                                    logger.info(phonenumber+"bill payment failed: "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("bill payment failed");
                                    tmlog.setActiondetails("bill payment failed: "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                }
                            }else{
                                //debitfailed
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "bill payment failed-"+" "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid+" "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("debit failed");
                                tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid+" "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                dbl.setActiondetails("debit failed "+sessid+" transid: "+transid+" "+debitaccountresponsecode+" "+debitaccountresponsemessage);
                                dbl.setActionperformed("debit failed");
                                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                                dbl.setAmount(amount);
                                dbl.setBankitstatus("");
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("DB"+transid+" "+customerid+" "+paymentcode);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                            }
                        }
                        
                    }else{
                        //potential fraud
                        tmresponse.put("responsecode", "22");
                        tmresponse.put("responsemessage", "potential fraud");
                        logger.info(phonenumber+"potential fraud:request phone not account phone" +sessid+" transid: "+transid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("potential fraud:request phone not account phone");
                        tmlog.setActiondetails("potential fraud:request phone not account phone "+sessid+" transid: "+transid);
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        dbl.setActiondetails("potential fraud:request phone not account phone "+sessid+" transid: "+transid);
                        dbl.setActionperformed("potential fraud:request phone not account phone");
                        dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                        dbl.setAmount(amount);
                        dbl.setBankitstatus("");
                        dbl.setDestinationaccount(creditaccount);
                        dbl.setDestinationbank(creditbank);
                        dbl.setNarration(narration);
                        dbl.setPhonenumber(phonenumber);
                        dbl.setPreferredmedium("bankit");
                        dbl.setSessionid(sessid);
                        dbl.setSource(source);
                        dbl.setSourceaccount(debitaccount);
                        dbl.setSourcebank(debitbank);
                        dbl.setTransactionid(transid);
                        PersistenceAgent.logTransactions(dbl);
                        dbl = new Debitlogs();
                        System.out.println(output);
                    }
                }
                if(cardstat.toLowerCase().equals("true")){
                    System.out.println("card stat here "+utils.getDate()+" "+utils.getTime());
                    ocustcards= DataObjects.getOctopuscustomerscards_cid(cid);
                    System.out.println("checked db for cid "+utils.getDate()+" "+utils.getTime());
                    String phno = ocustcards.getPhonenumber() != null?ocustcards.getPhonenumber():"";
                    String cardname = ocustcards.getCardname() != null?ocustcards.getCardname():"";
                    String cardno=(ocustcards.getCardno()!=null)?ocustcards.getCardno():"";
                    String cardbank=(ocustcards.getCardbank()!=null)?ocustcards.getCardbank():"";
                    System.out.println("cardname "+cardname);
                    if(phno.equals(phonenumber)){
                        //debit card
                        System.out.println("calling debit card "+utils.getDate()+" "+utils.getTime());
                        debitcardstat_str=ot.debitCard(source, sessid, transid, phonenumber, amount,sourcefirstname,sourcemiddlename,sourcelastname, cid, cardname,"","","", "NG", "NGN", creditaccount, creditbank,"bp","Ipg01",narration);
                        debitcardstat= new JSONObject(debitcardstat_str);
                        debitcardrescode=debitcardstat.getString("responsecode");
                        String resmsg=debitcardstat.getString("responsemessage");
                        if(debitcardrescode.equals("00")){
                            String transref=debitcardstat.getString("inteswitchreference");
                            logger.info(phonenumber+"debit success" +sessid+" transid: "+transid);
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("debit success");
                            tmlog.setActiondetails("debit success "+sessid+" transid: "+transid);
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            dbl.setActiondetails("debit success "+sessid+" transid: "+transid);
                            dbl.setActionperformed("debit success");
                            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                            dbl.setAmount(amount);
                            dbl.setDestinationaccount(creditaccount);
                            dbl.setDestinationbank(creditbank);
                            dbl.setNarration("DB"+transid+" "+creditaccount+" "+creditbank);
                            dbl.setPhonenumber(phonenumber);
                            dbl.setPreferredmedium("ipg");
                            dbl.setSessionid(sessid);
                            dbl.setSource(source);
                            dbl.setSourcecard(cid);
                            dbl.setSourcebank("card");
                            dbl.setTransactionid(transid);
                            PersistenceAgent.logTransactions(dbl);
                            dbl = new Debitlogs();
                            tmresponse.put("responsecode", "00");
                            tmresponse.put("responsemessage", "bill payment success-"+resmsg);
                            tmresponse.put("transref",transref);
                            logot.setAmount(amount);
                            logot.setDestinationaccount(creditaccount);
                            logot.setDestinationbank(creditbank);
                            logot.setNarration("DB"+transid);
                            logot.setPhonenumber(phonenumber);
                            logot.setPreferredmedium("ipg");
                            logot.setSessionid(sessid);
                            logot.setSource(source);
                            logot.setSourcecard(cid);
                            logot.setSourcebank("card");
                            logot.setTransactionid(transid);
                            logot.setTranscode("IPG01");
                            logot.setTransdate(utils.getDate()+" "+utils.getTime());
                            logot.setTransreference(transref);
                            logot.setTranstype("D");
                            logot.setDestinationfirstname("");
                            logot.setDestinationlastname("");
                            logot.setDestinationmiddlename("");
                            logot.setSourcefirstname(sourcefirstname);
                            logot.setSourcelastname(sourcelastname);
                            logot.setSourcemiddlename(sourcemiddlename);
                            PersistenceAgent.logTransactions(logot);
                            //send bill advice
                            QtServices qts = new QtServices();
                            String notificationamount=amount;
                            amount = amount.replace(".", "");
                            String qtresponsestr = qts.payBill(source,transid,sessid,paymentcode, customerid, phonenumber, amount, "1528"+transid.substring(10,transid.length()));
                            JSONObject qtresponse = new JSONObject(qtresponsestr);
                            String qtresponsecode=qtresponse.getString("responsecode");
                            if(qtresponsecode.equals("00")){
                                billdetails="Payment of N"+amount+" for "+customerid+".";
                                comtrans.setAmount(amount);
                                comtrans.setDestinationaccount(creditaccount);
                                comtrans.setDestinationbank(creditbank);
                                comtrans.setNarration(""+transid);
                                comtrans.setPhonenumber(phonenumber);
                                comtrans.setPreferredmedium("ipg");
                                comtrans.setSessionid(sessid);
                                comtrans.setSource(source);
                                comtrans.setSourcecard(cid);
                                comtrans.setSourcebank("card");
                                comtrans.setTransactionid(transid);
                                comtrans.setTranscode("IPG01");
                                comtrans.setTransdate(utils.getDate()+" "+utils.getTime());
                                comtrans.setTransreference(transref);
                                comtrans.setTranstype("bp");
                                comtrans.setDestinationfirstname("");
                                comtrans.setDestinationlastname("");
                                comtrans.setDestinationmiddlename("");
                                comtrans.setSourcefirstname(sourcefirstname);
                                comtrans.setSourcelastname(sourcelastname);
                                comtrans.setSourcemiddlename(sourcemiddlename);
                                comtrans.setCustomerid(customerid);
                                comtrans.setPaymentcode(paymentcode);
                                PersistenceAgent.logTransactions(comtrans);
                                if(qtresponse.has("transactionRef")){
                                    String qttransref=qtresponse.getString("transactionRef");
                                    tmresponse.put("qttransref", qttransref);
                                }
                                if(qtresponse.has("MscData")){
                                    MscData=qtresponse.getString("MscData");
                                    tmresponse.put("MscData", MscData);
                                    MscData="MscData: "+MscData;
                                    billdetails=billdetails+" MscData:"+MscData+".";
                                }
                                if(qtresponse.has("rechargePIN")){
                                    rechargePIN=qtresponse.getString("rechargePIN");
                                    tmresponse.put("rechargePIN", rechargePIN);
                                    rechargePIN="RechargePIN: "+rechargePIN;
                                    billdetails=billdetails+" rechargePIN:"+rechargePIN+".";
                                }
                                tmresponse.put("responsecode", "00");
                                tmresponse.put("responsemessage", "bill payment success-"+resmsg);
                                tmresponse.put("transref",transref); 
                                mail.billpaymentNotification(phonenumber, emailaddress, firstname+" "+lastname,transdate , "Bills Payment", cardno, billername, billdetails,narration,notificationamount , transid, "Card", cardno, cardbank,MscData,rechargePIN);
                                log.info(appname, classname, "Transaction Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Transaction ID "+transid);
                                logger.info(phonenumber+"Bill payment success: "+resmsg+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("Mail sent. Bill payment success");
                                tmlog.setActiondetails("Mail sent. Bill payment success: "+resmsg+" ");
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                op.addBeneficiary(source, transid, sessid, phonenumber, "", "", "", "", "", customerid, paymentcode, "bp");
                                logger.info(phonenumber+"Beneficiary Saved. Bill payment success: "+resmsg+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("Beneficiary Saved. Bill payment success");
                                tmlog.setActiondetails("Beneficiary Saved. bill payment success: "+resmsg+" ");
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                            }else{
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "Transaction has been submitted successfully and is being processed. You shall be notified on completion.");
                                log.dispute(appname, classname, "Dispute", "Transaction ID:"+transid+" Customer:"+phonenumber+" Customerid:"+customerid+" Paymentcode:"+paymentcode+" Biller:"+billername+" Amount"+amount);
                                distrans.setAmount(amount);
                                distrans.setDestinationaccount(creditaccount);
                                distrans.setDestinationbank(creditbank);
                                distrans.setNarration(""+transid);
                                distrans.setPhonenumber(phonenumber);
                                distrans.setPreferredmedium("ipg");
                                distrans.setSessionid(sessid);
                                distrans.setSource(source);
                                distrans.setSourcecard(cid);
                                distrans.setSourcebank("card");
                                distrans.setTransactionid(transid);
                                distrans.setTranscode("IPG01");
                                distrans.setTransdate(utils.getDate()+" "+utils.getTime());
                                distrans.setTransreference(transref);
                                distrans.setTranstype("bp");
                                distrans.setDestinationfirstname("");
                                distrans.setDestinationlastname("");
                                distrans.setDestinationmiddlename("");
                                distrans.setSourcefirstname(sourcefirstname);
                                distrans.setSourcelastname(sourcelastname);
                                distrans.setSourcemiddlename(sourcemiddlename);
                                distrans.setCustomerid(customerid);
                                distrans.setPaymentcode(paymentcode);
                                PersistenceAgent.logTransactions(distrans);
                                dbl.setActiondetails("bill payment failed:reversal needed: ");
                                dbl.setActionperformed("bill payment failed:reversal needed");
                                dbl.setActiontime(transdate);
                                dbl.setAmount(amount);
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("DB"+transid+" "+narration);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                                logger.info(phonenumber+"bill payment failed: "+resmsg+" ");
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("bill payment failed");
                                tmlog.setActiondetails("bill payment failed: "+resmsg+" ");
                                tmlog.setActiontime(transdate);
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                            }
                        }else if(debitcardrescode.equals("33")){
                            String transref=debitcardstat.getString("transref");
                            String paymentid=debitcardstat.getString("paymentid");
                            //String otp=debitcardstat.getString("otp");
                            logger.info(phonenumber+"opt sent-debit pending" +sessid+" transid: "+transid);
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("opt sent-debit pending");
                            tmlog.setActiondetails("opt sent-debit pending "+sessid+" transid: "+transid);
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            dbl.setActiondetails("opt sent-debit pending "+sessid+" transid: "+transid);
                            dbl.setActionperformed("opt sent-debit pending");
                            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                            dbl.setAmount(amount);
                            dbl.setDestinationaccount(creditaccount);
                            dbl.setDestinationbank(creditbank);
                            dbl.setNarration("DB"+transid+" "+customerid+" "+paymentcode);
                            dbl.setPhonenumber(phonenumber);
                            dbl.setPreferredmedium("ipg");
                            dbl.setSessionid(sessid);
                            dbl.setSource(source);
                            dbl.setSourcecard(cid);
                            dbl.setSourcebank("card");
                            dbl.setTransactionid(transid);
                            PersistenceAgent.logTransactions(dbl);
                            dbl = new Debitlogs();
                            tmresponse.put("responsecode", "33");
                            tmresponse.put("responsemessage", resmsg);
                            tmresponse.put("transref",transref);
                            tmresponse.put("paymentid",paymentid);
                            //tmresponse.put("otp",otp);
                            tmresponse.put("source", source);
                            tmresponse.put("transid", transid);
                            tmresponse.put("sessid", sessid);
                            tmresponse.put("phonenumber", phonenumber);
                            tmresponse.put("cardstat", "true");
                            tmresponse.put("cid", cid);
                            tmresponse.put("amount", amount);
                            tmresponse.put("customerid", customerid);
                            tmresponse.put("paymentcode", paymentcode);
                            //billpayment validate
                        }else{
                            //debitfailed
                                tmresponse.put("responsecode", "11");
                                tmresponse.put("responsemessage", "bill payment failed-"+resmsg);
                                logger.info(phonenumber+"debit failed" +sessid+" transid: "+transid);
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("debit failed");
                                tmlog.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                                dbl.setActiondetails("debit failed "+sessid+" transid: "+transid);
                                dbl.setActionperformed("debit failed");
                                dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                                dbl.setAmount(amount);
                                dbl.setBankitstatus("");
                                dbl.setDestinationaccount(creditaccount);
                                dbl.setDestinationbank(creditbank);
                                dbl.setNarration("DB"+transid+" "+customerid+" "+paymentcode);
                                dbl.setPhonenumber(phonenumber);
                                dbl.setPreferredmedium("ipg");
                                dbl.setSessionid(sessid);
                                dbl.setSource(source);
                                dbl.setSourcecard(cid);
                                dbl.setSourcebank("card");
                                dbl.setTransactionid(transid);
                                PersistenceAgent.logTransactions(dbl);
                                dbl = new Debitlogs();
                        }
                    }else{
                        //potential fraud
                        tmresponse.put("responsecode", "22");
                        tmresponse.put("responsemessage", "potential fraud");
                        logger.info(phonenumber+"potential fraud:request phone not card phone" +sessid+" transid: "+transid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("potential fraud:request phone not card phone");
                        tmlog.setActiondetails("potential fraud:request phone not card phone "+sessid+" transid: "+transid);
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        dbl.setActiondetails("potential fraud:request phone not card phone "+sessid+" transid: "+transid);
                        dbl.setActionperformed("potential fraud:request phone not card phone");
                        dbl.setActiontime(utils.getDate()+" "+utils.getTime());
                        dbl.setAmount(amount);
                        dbl.setBankitstatus("");
                        dbl.setIpgstatus("true");
                        dbl.setDestinationaccount(creditaccount);
                        dbl.setDestinationbank(creditbank);
                        dbl.setNarration(narration);
                        dbl.setPhonenumber(phonenumber);
                        dbl.setPreferredmedium("ipg");
                        dbl.setSessionid(sessid);
                        dbl.setSource(source);
                        dbl.setSourcecard(cid);
                        dbl.setSourcebank("card");
                        dbl.setTransactionid(transid);
                        PersistenceAgent.logTransactions(dbl);
                        dbl = new Debitlogs();
                        System.out.println(output);
                    }
                    
                }
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("parse error ");
            tmlog.setActiondetails("parse error "+output);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            dbl.setActiondetails("parse error "+output);
            dbl.setActionperformed("parse error");
            dbl.setActiontime(utils.getDate()+" "+utils.getTime());
            dbl.setAmount(amount);
            dbl.setBankitstatus(output+"");
            dbl.setDestinationaccount(creditaccount);
            dbl.setDestinationbank(creditbank);
            dbl.setNarration(narration);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("bankit");
            dbl.setRequesttime(utils.getDate()+" "+utils.getTime());
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourceaccount(debitaccount);
            dbl.setSourcebank(debitbank);
            dbl.setTransactionid(transid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
        }
    return output;}
    
    public String transactionValidate(String source,String sessid,String transid,String phonenumber,String customerid,String paymentcode,String creditaccount,String creditbank,String transtype,String cardstat,String cid,String amount,String transactionref,String cardname,String otp,String paymentid, JSONObject req){
       Octopuspndtrans octpnd ;
       Octopuscustomerscards ocustcards;
       Octopuscustomerstm ocusttm;
       OctopusOperations op = new OctopusOperations();
       Octopusdisputetransactions distrans = new Octopusdisputetransactions();
       Mailer mail = new Mailer();
       octpnd=DataObjects.getOctopuspndtrans(transactionref);
       String dbphone=(octpnd.getPhonenumber()!=null)?octpnd.getPhonenumber():"";
       String ttransid=(octpnd.getTransactionid()!=null)?octpnd.getTransactionid():"";
       String destinationfirstname=(octpnd.getDestinationfirstname()!=null)?octpnd.getDestinationfirstname():"";
       String destinationlastname=(octpnd.getDestinationlastname()!=null)?octpnd.getDestinationlastname():"";
       String destinationmiddlename=(octpnd.getDestinationmiddlename()!=null)?octpnd.getDestinationmiddlename():"";
       String sourcefirstname=(octpnd.getSourcefirstname()!=null)?octpnd.getSourcefirstname():"";
       String sourcelastname=(octpnd.getSourcelastname()!=null)?octpnd.getSourcelastname():"";
       String sourcemiddlename=(octpnd.getSourcemiddlename()!=null)?octpnd.getSourcemiddlename():"";
       String loggedamount=(octpnd.getAmount()!=null)?octpnd.getAmount():"";
       String dbnarration=(octpnd.getNarration()!=null)?octpnd.getNarration():"";
       System.out.println(loggedamount);
       NumberFormat formatter = new DecimalFormat("####.00");
       amount = formatter.format(new BigDecimal(loggedamount)).toString();
       IpgOperations ipg = new IpgOperations();
       String ipgresponse_str="";
       JSONObject ipgresponse;
       JSONObject tmresponse = new JSONObject();
       String ipgresponsecode;
       String ipgresponsemessage;
       Utilities utils = new Utilities();
       String transdate=utils.getDate()+" "+utils.getTime();
       Octopustransactions logot = new Octopustransactions();
       Octopuscompletetransactions comtrans = new Octopuscompletetransactions();
       Bankcodes bankcode = DataObjects.getBankcodes_code(creditbank);
       String bankname=(bankcode.getBankname()!=null)?bankcode.getBankname():"";
       Tmlogs tmlog = new Tmlogs();
       Debitlogs dbl = new Debitlogs();
       String output="";
       String billdetails="";
       String paymentcoderesponse_str="";
       String paymentcoderesponsecode="";
       String MscData="";
       String rechargePIN="";
       String billername="";
       JSONObject paymentcoderesponse;
       String loginresponse_str="";
       JSONObject loginresponse;
       String loginrescode="";
       phonenumber=utils.formatPhone(phonenumber);
       try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                ocusttm=DataObjects.getOctopuscustomerstm_rec(phonenumber);
                ocustcards= DataObjects.getOctopuscustomerscards_cid(cid);
                String cardno=(ocustcards.getCardno()!=null)?ocustcards.getCardno():"";
                String cardbank=(ocustcards.getCardbank()!=null)?ocustcards.getCardbank():"";
                String firstname = (ocusttm.getFirstname()!=null)?ocusttm.getFirstname():"";
                String lastname = (ocusttm.getLastname()!=null)?ocusttm.getLastname():"";
                String emailaddress = (ocusttm.getEmailaddress()!=null)?ocusttm.getEmailaddress():"";
                if (transtype.toLowerCase().equals("bp")){
                    creditaccount="";
                    creditbank="heritage";
                }
                
                if(dbphone!=""){
                     if(dbphone.equals(phonenumber)){
                         ipgresponse_str=ipg.debitcardvalidate(source, transid, sessid, phonenumber, transactionref, amount, cid, cardname, "NG", "NGN", otp,paymentid);
                         ipgresponse= new JSONObject(ipgresponse_str);
                         ipgresponsecode=ipgresponse.getString("responsecode");
                         ipgresponsemessage=ipgresponse.getString("responsemessage");
                         if(ipgresponsecode.equals("00")){
                             tmresponse.put("responsecode","00");
                             tmresponse.put("responsemessage","success");
                             logot.setAmount(amount);
                             logot.setDestinationaccount(creditaccount);
                             logot.setDestinationbank(creditbank);
                             logot.setNarration("DB"+ttransid);
                             logot.setPhonenumber(phonenumber);
                             logot.setPreferredmedium("ipg");
                             logot.setSessionid(sessid);
                             logot.setSource(source);
                             logot.setSourcecard(cid);
                             logot.setSourcebank("card");
                             logot.setTransactionid(ttransid);
                             logot.setTranscode("IPG01");
                             logot.setTransdate(transdate);
                             logot.setTransreference(transactionref);
                             logot.setTranstype("D");
                             logot.setDestinationfirstname(destinationfirstname);
                             logot.setDestinationlastname(destinationlastname);
                             logot.setDestinationmiddlename(destinationmiddlename);
                             logot.setSourcefirstname(sourcefirstname);
                             logot.setSourcelastname(sourcelastname);
                             logot.setSourcemiddlename(sourcemiddlename);
                             PersistenceAgent.logTransactions(logot);
                             logot = new Octopustransactions();
                             octpnd.setTokenstat("validated");
                             PersistenceAgent.logTransactions(octpnd);
                             dbl.setActiondetails("ipg response debit validate success: "+ipgresponsecode+" "+ipgresponsemessage);
                             dbl.setActionperformed("ipg response debit validate success");
                             dbl.setActiontime(transdate);
                             dbl.setAmount(amount);
                             dbl.setDestinationaccount(creditaccount);
                             dbl.setDestinationbank(creditbank);
                             dbl.setNarration("DB"+ttransid);
                             dbl.setPhonenumber(phonenumber);
                             dbl.setPreferredmedium("ipg");
                             dbl.setSessionid(sessid);
                             dbl.setSource(source);
                             dbl.setSourcecard(cid);
                             dbl.setSourcebank("card");
                             dbl.setTransactionid(ttransid);
                             PersistenceAgent.logTransactions(dbl);
                             dbl = new Debitlogs();
                             logger.info(phonenumber+"ipg response debit validate success: "+ipgresponsecode+" "+ipgresponsemessage);
                             tmlog.setSource(source);
                             tmlog.setPhonenumber(phonenumber);
                             tmlog.setSessionid(sessid);
                             tmlog.setTransactionid(ttransid);
                             tmlog.setActionperformed("ipg response debit validate success");
                             tmlog.setActiondetails("ipg response debit validate success: "+ipgresponsecode+" "+ipgresponsemessage);
                             tmlog.setActiontime(transdate);
                             PersistenceAgent.logTransactions(tmlog);
                             tmlog = new Tmlogs();
                             QtServices qts = new QtServices();
                             if (transtype.toLowerCase().equals("bp")){
                                 //send bill advice
                                 billdetails="Payment of N"+amount+" for "+customerid+".";
                                 paymentcoderesponse_str = op.iswPaymentitems_paymentcode(source, transid, sessid, phonenumber, paymentcode);
                                 paymentcoderesponse= new JSONObject(paymentcoderesponse_str);
                                 paymentcoderesponsecode=paymentcoderesponse.getString("responsecode");
                                 if(paymentcoderesponsecode.equals("00")){
                                     billername=paymentcoderesponse.getString("billername");
                                 }
                                 String resmsg="validated";
                                 String notificationamount=amount;
                                 amount = amount.replace(".", "");
                                 System.out.println("amount to qt"+amount);
                                 String qtresponsestr = qts.payBill(source,transid,sessid,paymentcode, customerid, phonenumber, amount, "1528"+ttransid.substring(10,ttransid.length()));
                                 JSONObject qtresponse = new JSONObject(qtresponsestr);
                                 String qtresponsecode=qtresponse.getString("responsecode");
                                 String qttransref="";
                                 if(qtresponsecode.equals("00")){
                                     comtrans.setAmount(amount);
                                     comtrans.setDestinationaccount(creditaccount);
                                     comtrans.setDestinationbank(creditbank);
                                     comtrans.setNarration("BP"+dbnarration);
                                     comtrans.setPhonenumber(phonenumber);
                                     comtrans.setPreferredmedium("ipg");
                                     comtrans.setSessionid(sessid);
                                     comtrans.setSource(source);
                                     comtrans.setSourcecard(cid);
                                     comtrans.setSourcebank("card");
                                     comtrans.setTransactionid(ttransid);
                                     comtrans.setTranscode("IPG01");
                                     comtrans.setTransdate(transdate);
                                     comtrans.setTransreference(transactionref);
                                     comtrans.setTranstype("bp");
                                     comtrans.setDestinationfirstname("");
                                     comtrans.setDestinationlastname("");
                                     comtrans.setDestinationmiddlename("");
                                     comtrans.setSourcefirstname(sourcefirstname);
                                     comtrans.setSourcelastname(sourcelastname);
                                     comtrans.setSourcemiddlename(sourcemiddlename);
                                     comtrans.setCustomerid(customerid);
                                     comtrans.setPaymentcode(paymentcode);
                                     PersistenceAgent.logTransactions(comtrans);
                                     if(qtresponse.has("transactionRef")){
                                         qttransref=qtresponse.getString("transactionRef");
                                         tmresponse.put("qttransref", qttransref);
                                     }
                                     if(qtresponse.has("MscData")){
                                         MscData=qtresponse.getString("MscData");
                                         tmresponse.put("MscData", MscData);
                                         MscData="MscData: "+MscData;
                                         billdetails=billdetails+" MscData:"+MscData+".";
                                     }
                                     if(qtresponse.has("rechargePIN")){
                                         rechargePIN=qtresponse.getString("rechargePIN");
                                         tmresponse.put("rechargePIN", rechargePIN);
                                         rechargePIN="RechargePIN: "+rechargePIN;
                                         billdetails=billdetails+" rechargePIN:"+rechargePIN+".";
                                     }
                                     tmresponse.put("responsecode", "00");
                                     tmresponse.put("responsemessage", "bill payment success");
                                     tmresponse.put("transref",transactionref); 
                                     logot.setAmount(amount);
                                     logot.setDestinationaccount(creditaccount);
                                     logot.setDestinationbank(creditbank);
                                     logot.setNarration("Billpayment"+ttransid+" "+dbnarration);
                                     logot.setPhonenumber(phonenumber);
                                     logot.setPreferredmedium("ipg");
                                     logot.setSessionid(sessid);
                                     logot.setSource(source);
                                     logot.setSourcecard(cid);
                                     logot.setSourcebank("card");
                                     logot.setTransactionid(ttransid);
                                     logot.setTranscode("IPG01");
                                     logot.setTransdate(transdate);
                                     logot.setTransreference(qttransref);
                                     logot.setTranstype("C");
                                     logot.setDestinationfirstname(destinationfirstname);
                                     logot.setDestinationlastname(destinationlastname);
                                     logot.setDestinationmiddlename(destinationmiddlename);
                                     logot.setSourcefirstname(sourcefirstname);
                                     logot.setSourcelastname(sourcelastname);
                                     logot.setSourcemiddlename(sourcemiddlename);
                                     PersistenceAgent.logTransactions(logot);
                                     logot = new Octopustransactions();
                                     dbl.setActiondetails("bill payment success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     dbl.setActionperformed("bill payment success");
                                     dbl.setActiontime(transdate);
                                     dbl.setAmount(amount);
                                     dbl.setDestinationaccount(creditaccount);
                                     dbl.setDestinationbank(creditbank);
                                     dbl.setNarration("DB"+ttransid+" "+dbnarration);
                                     dbl.setPhonenumber(phonenumber);
                                     dbl.setPreferredmedium("ipg");
                                     dbl.setSessionid(sessid);
                                     dbl.setSource(source);
                                     dbl.setSourcecard(cid);
                                     dbl.setSourcebank("card");
                                     dbl.setTransactionid(ttransid);
                                     PersistenceAgent.logTransactions(dbl);
                                     dbl = new Debitlogs();
                                     logger.info(phonenumber+"bill payment success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(ttransid);
                                     tmlog.setActionperformed("bill payment success");
                                     tmlog.setActiondetails("bill payment success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                                     mail.billpaymentNotification(phonenumber, emailaddress, firstname+" "+lastname,transdate , "Bills Payment", cardno, billername, billdetails,dbnarration,notificationamount , ttransid, "Card", cardno, cardbank,MscData,rechargePIN);
                                     logger.info(phonenumber+"Mail sent. Bill payment success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     log.info(appname, classname, "Transaction Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Transaction ID "+ttransid);
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(ttransid);
                                     tmlog.setActionperformed("Mail sent. Bill payment success");
                                     tmlog.setActiondetails("Mail sent. Bill payment success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                                     op.addBeneficiary(source, transid, sessid, phonenumber, "", "", "", "", "", customerid, paymentcode, "bp");
                                     logger.info(phonenumber+"Beneficiary Saved. Bill payment success: "+resmsg+" ");
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(transid);
                                     tmlog.setActionperformed("Beneficiary Saved. Bill payment success");
                                     tmlog.setActiondetails("Beneficiary Saved. bill payment success: "+resmsg+" ");
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                                 }else{
                                     tmresponse.put("responsecode", "11");
                                     tmresponse.put("responsemessage", "Transaction has been submitted successfully and is being processed. You shall be notified on completion.");
                                     log.dispute(appname, classname, "Dispute", "Transaction ID:"+ttransid+" Customer:"+phonenumber+" Customerid:"+customerid+" Paymentcode:"+paymentcode+" Biller:"+billername+" Amount"+amount);
                                     distrans.setAmount(amount);
                                     distrans.setDestinationaccount(creditaccount);
                                     distrans.setDestinationbank(creditbank);
                                     distrans.setNarration("BP"+dbnarration);
                                     distrans.setPhonenumber(phonenumber);
                                     distrans.setPreferredmedium("ipg");
                                     distrans.setSessionid(sessid);
                                     distrans.setSource(source);
                                     distrans.setSourcecard(cid);
                                     distrans.setSourcebank("card");
                                     distrans.setTransactionid(ttransid);
                                     distrans.setTranscode("IPG01");
                                     distrans.setTransdate(transdate);
                                     distrans.setTransreference(transactionref);
                                     distrans.setTranstype("bp");
                                     distrans.setDestinationfirstname("");
                                     distrans.setDestinationlastname("");
                                     distrans.setDestinationmiddlename("");
                                     distrans.setSourcefirstname(sourcefirstname);
                                     distrans.setSourcelastname(sourcelastname);
                                     distrans.setSourcemiddlename(sourcemiddlename);
                                     distrans.setCustomerid(customerid);
                                     distrans.setPaymentcode(paymentcode);
                                     PersistenceAgent.logTransactions(distrans);
                                     dbl.setActiondetails("bill payment failed:reversal needed: ");
                                     dbl.setActionperformed("bill payment failed:reversal needed");
                                     dbl.setActiontime(transdate);
                                     dbl.setAmount(amount);
                                     dbl.setDestinationaccount(creditaccount);
                                     dbl.setDestinationbank(creditbank);
                                     dbl.setNarration("DB"+ttransid+" "+dbnarration);
                                     dbl.setPhonenumber(phonenumber);
                                     dbl.setPreferredmedium("ipg");
                                     dbl.setSessionid(sessid);
                                     dbl.setSource(source);
                                     dbl.setSourcecard(cid);
                                     dbl.setSourcebank("card");
                                     dbl.setTransactionid(ttransid);
                                     PersistenceAgent.logTransactions(dbl);
                                     dbl = new Debitlogs();
                                     logger.info(phonenumber+"bill payment failed: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(ttransid);
                                     tmlog.setActionperformed("bill payment failed");
                                     tmlog.setActiondetails("bill payment failed: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                                 }
                             }else{
                                 //do transfer
                                 billdetails="Payment of N"+amount+" for "+destinationfirstname+" "+destinationmiddlename+" "+destinationlastname+" with account:"+creditaccount+".";
                                 String notificationamount=amount;
                                 amount = amount.replace(".", "");
                                 String qtresponsestr = qts.doTransfer(source,transid,sessid,amount, creditbank, creditaccount, destinationlastname, destinationfirstname+" "+destinationmiddlename,phonenumber,"",sourcefirstname+" "+sourcemiddlename,sourcelastname,"1528"+ttransid.substring(10,transid.length()));
                                 JSONObject qtresponse = new JSONObject(qtresponsestr);
                                 String qtresponsecode=qtresponse.getString("responsecode");
                                 String resmsg="";
                                 String qttransref="";
                                 if(qtresponsecode.equals("00")){
                                     comtrans.setAmount(amount);
                                     comtrans.setDestinationaccount(creditaccount);
                                     comtrans.setDestinationbank(creditbank);
                                     comtrans.setNarration("FT"+dbnarration);
                                     comtrans.setPhonenumber(phonenumber);
                                     comtrans.setPreferredmedium("ipg");
                                     comtrans.setSessionid(sessid);
                                     comtrans.setSource(source);
                                     comtrans.setSourcecard(cid);
                                     comtrans.setSourcebank("card");
                                     comtrans.setTransactionid(ttransid);
                                     comtrans.setTranscode("IPG01");
                                     comtrans.setTransdate(transdate);
                                     comtrans.setTransreference(transactionref);
                                     comtrans.setTranstype("ft");
                                     comtrans.setDestinationfirstname(destinationfirstname);
                                     comtrans.setDestinationlastname(destinationlastname);
                                     comtrans.setDestinationmiddlename(destinationmiddlename);
                                     comtrans.setSourcefirstname(sourcefirstname);
                                     comtrans.setSourcelastname(sourcelastname);
                                     comtrans.setSourcemiddlename(sourcemiddlename);
                                     comtrans.setCustomerid("");
                                     comtrans.setPaymentcode("");
                                     PersistenceAgent.logTransactions(comtrans);
                                     if(qtresponse.has("transactionRef")){
                                         resmsg="validated";
                                         qttransref=qtresponse.getString("transactionRef");
                                         tmresponse.put("qttransref", qttransref);

                                     }
                                     if(qtresponse.has("MscData")){
                                         MscData=qtresponse.getString("MscData");
                                         tmresponse.put("MscData", MscData);
                                         billdetails=billdetails+" MscData:"+MscData+".";
                                     }
                                     if(qtresponse.has("rechargePIN")){
                                         rechargePIN=qtresponse.getString("rechargePIN");
                                         tmresponse.put("rechargePIN", rechargePIN);
                                         billdetails=billdetails+" rechargePIN:"+rechargePIN+".";
                                     }
                                     tmresponse.put("responsecode", "00");
                                     tmresponse.put("responsemessage", "funds transfer success-"+resmsg);
                                     tmresponse.put("transref",transactionref); 
                                     logot.setAmount(amount);
                                     logot.setDestinationaccount(creditaccount);
                                     logot.setDestinationbank(creditbank);
                                     logot.setNarration("FT"+ttransid+" "+dbnarration);
                                     logot.setPhonenumber(phonenumber);
                                     logot.setPreferredmedium("ipg");
                                     logot.setSessionid(sessid);
                                     logot.setSource(source);
                                     logot.setSourcecard(cid);
                                     logot.setSourcebank("card");
                                     logot.setTransactionid(ttransid);
                                     logot.setTranscode("IPG01");
                                     logot.setTransdate(transdate);
                                     logot.setTransreference(qttransref);
                                     logot.setTranstype("C");
                                     logot.setDestinationfirstname(destinationfirstname);
                                     logot.setDestinationlastname(destinationlastname);
                                     logot.setDestinationmiddlename(destinationmiddlename);
                                     logot.setSourcefirstname(sourcefirstname);
                                     logot.setSourcelastname(sourcelastname);
                                     logot.setSourcemiddlename(sourcemiddlename);
                                     PersistenceAgent.logTransactions(logot);
                                     logot = new Octopustransactions();
                                     dbl.setActiondetails("funds transfer success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     dbl.setActionperformed("funds transfer success");
                                     dbl.setActiontime(transdate);
                                     dbl.setAmount(amount);
                                     dbl.setDestinationaccount(creditaccount);
                                     dbl.setDestinationbank(creditbank);
                                     dbl.setNarration("FT"+ttransid+" "+dbnarration);
                                     dbl.setPhonenumber(phonenumber);
                                     dbl.setPreferredmedium("ipg");
                                     dbl.setSessionid(sessid);
                                     dbl.setSource(source);
                                     dbl.setSourcecard(cid);
                                     dbl.setSourcebank("card");
                                     dbl.setTransactionid(ttransid);
                                     PersistenceAgent.logTransactions(dbl);
                                     dbl = new Debitlogs();
                                     logger.info(phonenumber+"funds transfer success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(ttransid);
                                     tmlog.setActionperformed("funds transfer success");
                                     tmlog.setActiondetails("funds transfer success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                                     mail.ftpaymentNotification(phonenumber, emailaddress, firstname+" "+lastname, transdate, "Funds Transfer", cardno, destinationfirstname+" "+destinationmiddlename+" "+destinationlastname, creditaccount, bankname, dbnarration, notificationamount, ttransid, "Card", cardno, cardbank);
                                     log.info(appname, classname, "Transaction Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Transaction ID "+ttransid);
                                     logger.info(phonenumber+"funds transfer success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(ttransid);
                                     tmlog.setActionperformed("Mail sent. Funds transfer success");
                                     tmlog.setActiondetails("Mail sent. Funds transfer success: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                                     op.addBeneficiary(source, transid, sessid, phonenumber, "", creditaccount, destinationfirstname+" "+destinationmiddlename+" "+destinationlastname, bankname, creditbank, "", "", "ft");
                                     logger.info(phonenumber+"Beneficiary Saved. Funds transfer success: "+qttransref+" ");
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(transid);
                                     tmlog.setActionperformed("Beneficiary Saved. Funds transfer success");
                                     tmlog.setActiondetails("Beneficiary Saved. Funds transfer success: "+qttransref+" ");
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                             }else{
                                     TransferNIP nip = new TransferNIP();
                                     billdetails="Payment of N"+amount+" from "+firstname+" "+lastname;
                                 
                                //senderaccount, debittype
                                String senderaccount=QUCOON_NIP_ACCOUNT;
                                String debittype="Card";
                                String nipresp = nip.doTransfer(source, transid, sessid, amount, creditbank, creditaccount, destinationlastname, destinationfirstname, phonenumber, emailaddress, firstname, lastname, transid, billdetails, senderaccount, debittype,bankname,cardbank);
                                if(nipresp.equals("00")){
                                    tmresponse.put("responsecode", "00");
                                    tmresponse.put("responsemessage", "funds transfer success");
                                    tmresponse.put("transref",transid); 
                                }else{
                                     resmsg="error";
                                     log.dispute(appname, classname, "Dispute", "Transaction ID:"+ttransid+" Customer:"+phonenumber+" Beneficiary Account:"+creditaccount+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+creditbank+" Amount"+amount);
                                     distrans.setDestinationaccount(creditaccount);
                                     distrans.setDestinationbank(creditbank);
                                     distrans.setNarration("FT"+dbnarration);
                                     distrans.setPhonenumber(phonenumber);
                                     distrans.setPreferredmedium("ipg");
                                     distrans.setSessionid(sessid);
                                     distrans.setSource(source);
                                     distrans.setSourcecard(cid);
                                     distrans.setSourcebank("card");
                                     distrans.setTransactionid(ttransid);
                                     distrans.setTranscode("IPG01");
                                     distrans.setTransdate(transdate);
                                     distrans.setTransreference(transactionref);
                                     distrans.setTranstype("ft");
                                     distrans.setDestinationfirstname(destinationfirstname);
                                     distrans.setDestinationlastname(destinationlastname);
                                     distrans.setDestinationmiddlename(destinationmiddlename);
                                     distrans.setSourcefirstname(sourcefirstname);
                                     distrans.setSourcelastname(sourcelastname);
                                     distrans.setSourcemiddlename(sourcemiddlename);
                                     distrans.setCustomerid("");
                                     distrans.setPaymentcode("");
                                     PersistenceAgent.logTransactions(distrans);
                                     tmresponse.put("responsecode", "11");
                                     tmresponse.put("responsemessage", "Transaction has been submitted successfully and is being processed. You shall be notified on completion.");
                                     dbl.setActiondetails("funds transfer failed:reversal needed: ");
                                     dbl.setActionperformed("funds transfer failed:reversal needed");
                                     dbl.setActiontime(transdate);
                                     dbl.setAmount(amount);
                                     dbl.setDestinationaccount(creditaccount);
                                     dbl.setDestinationbank(creditbank);
                                     dbl.setNarration("FT"+ttransid+" "+dbnarration);
                                     dbl.setPhonenumber(phonenumber);
                                     dbl.setPreferredmedium("ipg");
                                     dbl.setSessionid(sessid);
                                     dbl.setSource(source);
                                     dbl.setSourcecard(cid);
                                     dbl.setSourcebank("card");
                                     dbl.setTransactionid(ttransid);
                                     PersistenceAgent.logTransactions(dbl);
                                     dbl = new Debitlogs();
                                     logger.info(phonenumber+"funds transfer failed: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setSource(source);
                                     tmlog.setPhonenumber(phonenumber);
                                     tmlog.setSessionid(sessid);
                                     tmlog.setTransactionid(ttransid);
                                     tmlog.setActionperformed("funds transfer failed");
                                     tmlog.setActiondetails("funds transfer failed: "+ipgresponsecode+" "+ipgresponsemessage);
                                     tmlog.setActiontime(transdate);
                                     PersistenceAgent.logTransactions(tmlog);
                                     tmlog = new Tmlogs();
                                }
                                 }
                             }
                         }else if(ipgresponsecode.equals("T1")){
                             tmresponse.put("responsecode", "33");
                             tmresponse.put("responsemessage", ipgresponsemessage);
                             logger.info(phonenumber+"ipg response debit validate failed: "+ipgresponsecode+" "+ipgresponsemessage);
                             tmlog.setSource(source);
                             tmlog.setPhonenumber(phonenumber);
                             tmlog.setSessionid(sessid);
                             tmlog.setTransactionid(ttransid);
                             tmlog.setActionperformed("ipg response debit validate failed");
                             tmlog.setActiondetails("ipg response debit validate failed: "+ipgresponsecode+" "+ipgresponsemessage);
                             tmlog.setActiontime(transdate);
                             PersistenceAgent.logTransactions(tmlog);
                             tmlog = new Tmlogs();
                             dbl.setActiondetails("ipg response debit validate failed: "+ipgresponsecode+" "+ipgresponsemessage);
                             dbl.setActionperformed("ipg response debit validate failed");
                             dbl.setActiontime(transdate);
                             dbl.setAmount(amount);
                             dbl.setDestinationaccount(creditaccount);
                             dbl.setDestinationbank(creditbank);
                             dbl.setNarration("DB"+ttransid+" "+dbnarration);
                             dbl.setPhonenumber(phonenumber);
                             dbl.setPreferredmedium("ipg");
                             dbl.setSessionid(sessid);
                             dbl.setSource(source);
                             dbl.setSourcecard(cid);
                             dbl.setSourcebank("card");
                             dbl.setTransactionid(ttransid);
                             PersistenceAgent.logTransactions(dbl);
                             dbl = new Debitlogs();

                         }else{
                             tmresponse.put("responsecode", "11");
                             tmresponse.put("responsemessage", ipgresponsemessage);
                             logger.info(phonenumber+"ipg response debit validate failed: "+ipgresponsecode+" "+ipgresponsemessage);
                             tmlog.setSource(source);
                             tmlog.setPhonenumber(phonenumber);
                             tmlog.setSessionid(sessid);
                             tmlog.setTransactionid(ttransid);
                             tmlog.setActionperformed("ipg response debit validate failed");
                             tmlog.setActiondetails("ipg response debit validate failed: "+ipgresponsecode+" "+ipgresponsemessage);
                             tmlog.setActiontime(transdate);
                             PersistenceAgent.logTransactions(tmlog);
                             tmlog = new Tmlogs();
                             dbl.setActiondetails("ipg response debit validate failed: "+ipgresponsecode+" "+ipgresponsemessage);
                             dbl.setActionperformed("ipg response debit validate failed");
                             dbl.setActiontime(transdate);
                             dbl.setAmount(amount);
                             dbl.setDestinationaccount(creditaccount);
                             dbl.setDestinationbank(creditbank);
                             dbl.setNarration("DB"+ttransid+" "+dbnarration);
                             dbl.setPhonenumber(phonenumber);
                             dbl.setPreferredmedium("ipg");
                             dbl.setSessionid(sessid);
                             dbl.setSource(source);
                             dbl.setSourcecard(cid);
                             dbl.setSourcebank("card");
                             dbl.setTransactionid(ttransid);
                             PersistenceAgent.logTransactions(dbl);
                             dbl = new Debitlogs();

                         }
                     }else{
                         //unrecognised trans  
                         tmresponse.put("responsecode", "22");
                         tmresponse.put("responsemessage", "unrecognised transaction");
                         logger.info(phonenumber+"ipg response debit validate failed: "+"44"+" "+"unrecognised transaction");
                         tmlog.setSource(source);
                         tmlog.setPhonenumber(phonenumber);
                         tmlog.setSessionid(sessid);
                         tmlog.setTransactionid(ttransid);
                         tmlog.setActionperformed("ipg response debit validate failed");
                         tmlog.setActiondetails("ipg response debit validate failed: "+"44"+" "+"unrecognised transaction");
                         tmlog.setActiontime(transdate);
                         PersistenceAgent.logTransactions(tmlog);
                     }
                 }else{
                     //not found
                     tmresponse.put("responsecode", "44");
                     tmresponse.put("responsemessage", "Invalid Request");
                     logger.info(phonenumber+"ipg response debit validate failed: "+"55"+" "+"not found");
                     tmlog.setSource(source);
                     tmlog.setPhonenumber(phonenumber);
                     tmlog.setSessionid(sessid);
                     tmlog.setTransactionid(ttransid);
                     tmlog.setActionperformed("ipg response debit validate failed");
                     tmlog.setActiondetails("ipg response debit validate failed: "+"55"+" "+"not found");
                     tmlog.setActiontime(transdate);
                     PersistenceAgent.logTransactions(tmlog);
                 }
            }
       output = tmresponse.toString();
       }catch(Exception e){
           output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            tmlog = new Tmlogs();
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ ipgresponse_str);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(ttransid);
            tmlog.setActionperformed("parse error "+ipgresponse_str);
            tmlog.setActiondetails("parse error "+ipgresponse_str);
            tmlog.setActiontime(transdate);
            PersistenceAgent.logTransactions(tmlog);
            dbl.setActiondetails("parse error ");
            dbl.setActionperformed("parse error");
            dbl.setActiontime(transdate);
            dbl.setAmount(amount);
            dbl.setDestinationaccount("");
            dbl.setDestinationbank("");
            dbl.setNarration("DB"+ttransid+" "+dbnarration);
            dbl.setPhonenumber(phonenumber);
            dbl.setPreferredmedium("ipg");
            dbl.setRequesttime(transdate);
            dbl.setSessionid(sessid);
            dbl.setSource(source);
            dbl.setSourcecard(cid);
            dbl.setSourcebank("card");
            dbl.setTransactionid(ttransid);
            PersistenceAgent.logTransactions(dbl);
            dbl = new Debitlogs();
       }
    return output;}
    
    public String getBalance(String source,String sessid,String transid,String phonenumber,String passcode){
        OctopusOperations op = new OctopusOperations();
        Utilities utils = new Utilities();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        JSONObject customerstatus = new JSONObject();
        JSONObject tmresponse = new JSONObject();
        JSONArray accounts;
        String accountnumber="";
        String bankcode="";
        String customerstatusstr="";
        phonenumber=utils.formatPhone(phonenumber);
        try{
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a user
            }else if(loginrescode.equals("22")){
                //wrong passcode
            }else if(loginrescode.equals("00")){
                tmresponse.put("responsedate", utils.getDate());
                tmresponse.put("responsetime", utils.getTime());
                customerstatusstr=op.customerStatus(source, transid, sessid, phonenumber, "NG");
                customerstatus= new JSONObject(customerstatusstr);
                accounts=customerstatus.getJSONArray("accounts");
                for(int i=0;i<accounts.length();i++){
                    JSONObject account=accounts.getJSONObject(i);
                    JSONObject balance = new JSONObject();
                    accountnumber=account.getString("accountnumber");
                    bankcode=account.getString("bankcode");
                    if(bankcode.equals("030")){
                        //grt bal
                    }
                }
        
            }
            
        }
        catch(Exception e){
            
        }
    return "";}
    
    public String getAccountBalance(String source,String sessid,String transid,String phonenumber,String passcode){
        OctopusOperations op = new OctopusOperations();
        OctopusTransactions ot = new OctopusTransactions();
        IpgOperations ipg = new IpgOperations();
        Utilities utils = new Utilities();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        JSONObject customerstatus = new JSONObject();
        JSONObject tmresponse = new JSONObject();
        JSONArray accounts;
        JSONArray cards;
        JSONArray outbalancearr = new JSONArray();
        Tmlogs tmlog = new Tmlogs();
        String customerstatrescode="";
        String bankcode="";
        String customerstatusstr="";
        String balancestr="";
        String accountnumber="";
        String accountno="";
        String accounttype="";
        String currency="";
        String ledgerbalance="";
        String accountstatus="";
        String availbalance="";
        String cardtype="";
        String cardname="";
        String bank="";
        String last4="";
        String cardbalance="";
        String cardcategory="";
        String cid="";
        String output="";
        phonenumber=utils.formatPhone(phonenumber);
        try{
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a user
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a user");
            }else if(loginrescode.equals("22")){
                //wrong passcode
                tmresponse.put("responsecode", "33");
                tmresponse.put("responsemessage", "wrong passcode");
            }else if(loginrescode.equals("33")){
                //wrong passcode
                tmresponse.put("responsecode", "66");
                tmresponse.put("responsemessage", "Account blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to reset account");
            }else if(loginrescode.equals("00")){
                tmresponse.put("responsedate", utils.getDate());
                tmresponse.put("responsetime", utils.getTime());
                logger.info(phonenumber+"new get balance request using account" +sessid+" transid: "+transid);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                tmlog.setActionperformed("new get balance request using account");
                tmlog.setActiondetails("new get balance request using account: "+sessid+" transid: "+transid);
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                customerstatusstr=op.customerStatus(source, transid, sessid, phonenumber, "NG");
                customerstatus= new JSONObject(customerstatusstr);
                customerstatrescode=customerstatus.getString("responsecode");
                if(customerstatrescode.equals("00")){
                    logger.info(phonenumber+"new get balance request using account success" +sessid+" transid: "+transid);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    tmlog.setActionperformed("new get balance request using account success");
                    tmlog.setActiondetails("new get balance  request using account success: "+sessid+" transid: "+transid);
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    if(customerstatus.has("accounts")){
                        accounts=customerstatus.getJSONArray("accounts");
                        System.out.println("yes");
                        for(int i=0;i<accounts.length();i++){
                            JSONObject account=accounts.getJSONObject(i);
                            JSONObject balance;
                            JSONObject outbalance = new JSONObject();
                            accountnumber=account.getString("accountnumber");
                            bankcode=account.getString("bankcode");
                            if(bankcode.equals("030")){
                                //grt bal
                                balancestr=ot.accountGetBalance(source, transid, sessid, phonenumber, passcode, accountnumber);
                                balance = new JSONObject(balancestr);
                                accountno=balance.getString("accountno");
                                accounttype=balance.getString("accounttype");
                                currency=balance.getString("currency");
                                ledgerbalance=balance.getString("ledgerbalance");
                                availbalance=balance.getString("availbalance");
                                accountstatus=balance.getString("accountstatus");
                                outbalance.put("accountno", accountno);
                                outbalance.put("accounttype", accounttype);
                                outbalance.put("currency", currency);
                                outbalance.put("ledgerbalance", ledgerbalance);
                                outbalance.put("availbalance", availbalance);
                                outbalance.put("accountstatus", accountstatus);
                                outbalancearr.put(outbalance);
                            }
                        }
                        tmresponse.put("accounts", outbalancearr);
                }
                    
                if(customerstatus.has("cards")){
                        outbalancearr  = new JSONArray();
                        cards=customerstatus.getJSONArray("cards");
                        System.out.println("yes");
                        for(int i=0;i<cards.length();i++){
                            JSONObject card=cards.getJSONObject(i);
                            JSONObject balance;
                            JSONObject outbalance = new JSONObject();
                            cid=card.getString("cid");
                            //grt bal
                            balancestr=ipg.balance(source, transid, sessid, phonenumber, cid);
                            balance = new JSONObject(balancestr);
                            cardtype=balance.getString("cardtype");
                            cardname=balance.getString("cardname");
                            cardbalance=balance.getString("balance");
                            bank=balance.getString("bank");
                            cardcategory=balance.getString("cardcategory");
                            last4=balance.getString("last4");
                            outbalance.put("cardtype", cardtype);
                            outbalance.put("cardname", cardname);
                            outbalance.put("availbalance", cardbalance);
                            outbalance.put("bank", bank);
                            outbalance.put("cardcategory", cardcategory);
                            outbalance.put("last4", last4);
                            outbalancearr.put(outbalance);

                        }
                        tmresponse.put("cards", outbalancearr);
                }
                }else{
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "no account/card");
                     logger.info(phonenumber+"new get balance request using account no account" +sessid+" transid: "+transid);
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    tmlog.setActionperformed("new get balance request using account no account");
                    tmlog.setActiondetails("new debit customer request using account no account: "+sessid+" transid: "+transid);
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
            }
            output=tmresponse.toString();
            
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String approveInboxTransaction(String source,String transid,String sessid,String senderid,String sno)
    {
        Octopusinboxtransactions ocinbox =DataObjects.getOctopusinboxtransactions_rec(sno);
        Utilities utils = new Utilities();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        String responsecode="";
        String responsemessage="";  
        String output="";
        senderid=utils.formatPhone(senderid);
        int sno_int = Integer.parseInt(sno);
        
        logger.info(senderid+" source"+source+" "+sessid+":approve inbox transation: "+ source+" "+senderid );
        tmlog.setSource(source);
        tmlog.setPhonenumber(senderid);
        tmlog.setSessionid(sessid);
        tmlog.setTransactionid(transid);
        tmlog.setActionperformed("new approve inbox request");
        tmlog.setActiondetails("request details: "+senderid+"  "+sessid+":approve inbox transation: "+ source+" "+senderid );
        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
        PersistenceAgent.logTransactions(tmlog);
        tmlog = new Tmlogs();
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            ocinbox.setStatus("approved");
            ocinbox.setUpdatedat(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(ocinbox);
            tmresponse.put("responsecode", "00");
            tmresponse.put("responsemessage", "success");
            logger.info(senderid+" source"+source+" "+sessid+":approve inbox transaction successful source: "+ source+" "+senderid+" " +tmresponse.toString().replace("\"", ":"));
            tmlog.setSource(source);
            tmlog.setPhonenumber(senderid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("approve inbox successful");
            tmlog.setActiondetails("request details: "+senderid+"  "+sessid+":approve inbox transaction source: "+ source+" "+senderid+" " +tmresponse.toString().replace("\"", ":") );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(senderid+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(senderid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
   return output; }
    
    public String deleteInboxTransaction(String source,String transid,String sessid,String senderid,String sno)
    {
        Octopusinboxtransactions ocinbox =DataObjects.getOctopusinboxtransactions_rec(sno);
        Utilities utils = new Utilities();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        String responsecode="";
        String responsemessage="";  
        String output="";
        senderid=utils.formatPhone(senderid);
        int sno_int = Integer.parseInt(sno);
        
        logger.info(senderid+" source"+source+" "+sessid+":delete inbox transation: "+ source+" "+senderid );
        tmlog.setSource(source);
        tmlog.setPhonenumber(senderid);
        tmlog.setSessionid(sessid);
        tmlog.setTransactionid(transid);
        tmlog.setActionperformed("new delete inbox request");
        tmlog.setActiondetails("request details: "+senderid+"  "+sessid+":delete inbox transation: "+ source+" "+senderid );
        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
        PersistenceAgent.logTransactions(tmlog);
        tmlog = new Tmlogs();
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            ocinbox.setStatus("deleted");
            ocinbox.setUpdatedat(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(ocinbox);
            tmresponse.put("responsecode", "00");
            tmresponse.put("responsemessage", "success");
            logger.info(senderid+" source"+source+" "+sessid+":delete inbox transaction successful source: "+ source+" "+senderid+" " +tmresponse.toString().replace("\"", ":"));
            tmlog.setSource(source);
            tmlog.setPhonenumber(senderid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("approve inbox successful");
            tmlog.setActiondetails("request details: "+senderid+"  "+sessid+":delete inbox transaction source: "+ source+" "+senderid+" " +tmresponse.toString().replace("\"", ":") );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(senderid+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(senderid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
   return output; }
    
    public String cardBalance(String source,String transid,String sessid,String phonenumber,String passcode)
    {
        OctopusOperations op = new OctopusOperations();
        Utilities utils = new Utilities();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try{
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a user
            }else if(loginrescode.equals("22")){
                //wrong passcode
            }else if(loginrescode.equals("00")){
                //login success
                //get cid
                //call ipg
            }
        }
        catch(Exception e){
        
        }
    return "";
    }
    
    public String cardBalanceValidate(String source,String transid,String sessid,String phonenumber,String passcode,String otp){
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        Utilities utils = new Utilities();
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try{
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("22")){
                //not a user
            }else if(loginrescode.equals("11")){
                //wrong passcode
            }else if(loginrescode.equals("00")){
                //login success
                //get cid
                //call ipg
            }
        }
        catch(Exception e){
        
        }
    return "";}
    
    public String getNamewthAccount(String source,String transid,String sessid,String phonenumber,String accountno,String bankcode,String bankname)
    {
        JSONObject tmresponse = new JSONObject();
        JSONObject hbaccount ;
        Tmlogs tmlog = new Tmlogs();
        JSONObject hbresponse;
        String hbresponstr="";
        boolean hbstatus=true;
        HbngInterface hbng = new HbngInterface();
        Utilities utils = new Utilities();
        String output="";
        String accountname="";
        String hbresponse_str="";
        String hbresponsecode="";
        String authid = Authorization.getAuth();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+" source"+source+" "+sessid+":new get account name with account no request: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new get account name with account no request");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new get account name with account no request: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            if(bankcode.toLowerCase().equals("030")){
                
                hbresponse_str=hbng.accountgetname(source, sessid, transid, authid, accountno);
                logger.info(phonenumber+" source"+source+" "+sessid+" hbresponse"+ hbresponse_str );
                hbresponse = new JSONObject(hbresponse_str);
                hbresponsecode=hbresponse.getString("responsecode");
                if(hbresponsecode.equals("00")){
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    hbresponstr=hbresponse.getString("account");
                    hbaccount = new JSONObject(hbresponstr);
                    accountname=hbaccount.getString("accountname");
                    tmresponse.put("accountname", accountname);
                    hbstatus=true;
                    logger.info(phonenumber+" source"+source+" "+sessid+":account name success hbng: "+ source+" "+phonenumber+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("account name success hbng");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":account name success hnbg : "+ source+" "+phonenumber );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    output=tmresponse.toString();
                }
            }
            if(hbstatus){
                hbresponse_str=hbng.banknameenquiry(source, sessid, transid, authid, bankcode,accountno,"8");
                logger.info(phonenumber+" source"+source+" "+sessid+" hbresponse"+ hbresponse_str );
                hbresponse = new JSONObject(hbresponse_str);
                hbresponsecode=hbresponse.getString("responsecode");
                if(hbresponsecode.equals("00")){
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    hbresponstr=hbresponse.getString("account");
                    hbaccount = new JSONObject(hbresponstr);
                    accountname=hbaccount.getString("accountname");
                    tmresponse.put("accountname", accountname);
                    hbstatus=true;
                    logger.info(phonenumber+" source"+source+" "+sessid+":account name success: "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("account name success ");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":account name success : "+ source+" "+phonenumber );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "service unavailable");
                    logger.info(phonenumber+" source"+source+" "+sessid+":service unavailable: "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("service unavailable");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":service unavailable : "+ source+" "+phonenumber );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
                output=tmresponse.toString();
            }
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage()+hbresponstr);
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog); 
        }
    return output;}
    
    public String getNamewthAccountpaystack(String source,String transid,String sessid,String phonenumber,String accountno,String bankcode,String bankname)
    {
        NameEnquiry  ne = new NameEnquiry();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        String hbresponstr="";
        Utilities utils = new Utilities();
        String output="";
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+" source"+source+" "+sessid+":new get account name with account no request: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new get account name with account no request");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new get account name with account no request: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            log.info(appname, classname, "Get Account name"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+" "+accountno+" ");
            String nameenquiryresponse_str= ne.getAccountName(accountno, bankcode);
            JSONObject nameenquiryresponse= new JSONObject(nameenquiryresponse_str);
            String nameenquiryresponsecode = nameenquiryresponse.getString("responsecode");
            if(nameenquiryresponsecode.equals("00")){
                String paystackname =nameenquiryresponse.getString("account_name");
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                tmresponse.put("accountname", paystackname);
                logger.info(phonenumber+" source"+source+" "+sessid+":account name success: "+ source+" "+phonenumber );
                log.info(appname, classname, "Get Account name response success"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+" "+accountno+" "+paystackname);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("account name success ");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":account name success : "+ source+" "+phonenumber );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "service unavailable");
                    logger.info(phonenumber+" source"+source+" "+sessid+":service unavailable: "+ source+" "+phonenumber );
                    log.info(appname, classname, "Get Account name response service unavailable"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+" "+accountno+" ");
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("service unavailable");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":service unavailable : "+ source+" "+phonenumber );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
            }
                output=tmresponse.toString();
            
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage()+hbresponstr);
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog); 
        }
    return output;}
    
    public String bvnEnquiry(String source,String transid,String sessid,String bvn)
    {
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        JSONObject hbresponse;
        String hbresponstr="";
        HbngInterface hbng = new HbngInterface();
        Utilities utils = new Utilities();
        String output="";
        String hbresponse_str="";
        String hbresponsecode="";
        String firstname="";
        String lastname="";
        String middlename="";
        String dateofbirth="";
        String emailaddress="";
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(" source"+source+" "+sessid+":new bvn enquiry request: "+ source+" "+"" );
            tmlog.setSource(source);
            tmlog.setPhonenumber("");
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new bvn enquiry request");
            tmlog.setActiondetails("request details: "+""+"  "+sessid+":new bvn enquiry request: "+ source+" "+"" );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            hbresponse_str=hbng.bvnenquiry(source, sessid, transid, bvn);
            logger.info(""+" source"+source+" "+sessid+" hbresponse"+ hbresponse_str );
            hbresponse = new JSONObject(hbresponse_str);
            hbresponsecode=hbresponse.getString("responsecode");
            if(hbresponsecode.equals("00")){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                firstname=hbresponse.getString("firstname");
                lastname=hbresponse.getString("lastname");
                middlename=hbresponse.getString("middlename");
                dateofbirth=hbresponse.getString("dateofbirth");
                emailaddress=hbresponse.getString("emailaddress");
                tmresponse.put("firstname", firstname);
                tmresponse.put("lastname", lastname);
                tmresponse.put("middlename", middlename);
                tmresponse.put("dateofbirth", dateofbirth);
                tmresponse.put("emailaddress", emailaddress);
                logger.info(""+" source"+source+" "+sessid+":new bvn enquiry success hbng: "+ source+" "+""+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber("");
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new bvn enquiry");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":new bvn enquirysuccess hnbg : "+ source+" "+"" );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                output=tmresponse.toString();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "something went wrong");
                logger.info(""+" source"+source+" "+sessid+":something went wrong hbng: "+ source+" "+""+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber("");
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("something went wrong");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":something went wrong hnbg : "+ source+" "+"" );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
            
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage()+hbresponstr);
            logger.info(""+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber("");
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog); 
        }
    return output;}
    
    public String accountGetBalance(String source,String transid,String sessid,String phonenumber,String passcode,String accountno)
    {
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        JSONObject hbresponse;
        JSONObject account;
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        String auth = Authorization.getAuth();
        Utilities utils = new Utilities();
        HbngInterface hbng = new HbngInterface();
        String hbresponse_str="";
        String hbresponsecode="";
        String accountnumber="";
        String accountype="";
        String currency="";
        String ledgerbalance="";
        String availbalance="";
        String accountstatus="";
        String output="";
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
                logger.info(""+" source"+source+" "+sessid+":not a customer hbng: "+ source+" "+""+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber("");
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("not a customer");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":not a customer hnbg : "+ source+" "+"" );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else if(loginrescode.equals("22")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "wrong passcode");
                logger.info(""+" source"+source+" "+sessid+":wrong passcode hbng: "+ source+" "+""+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber("");
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("wrong passcode");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":wrong passcode hnbg : "+ source+" "+"" );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else if(loginrescode.equals("33")){
                //wrong passcode
                tmresponse.put("responsecode", "66");
                tmresponse.put("responsemessage", "Account blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to reset account");
            }else{
                logger.info(" source"+source+" "+sessid+":new account get balance request: "+ source+" "+"" );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new account get balance request");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":new account get balance request: "+ source+" "+"" );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                hbresponse_str=hbng.accountgetbalance(source, sessid, transid, auth,phonenumber,accountno);
                logger.info(""+" source"+source+" "+sessid+" hbresponse"+ hbresponse_str );
                hbresponse = new JSONObject(hbresponse_str);
                hbresponsecode=hbresponse.getString("responsecode");
                if(hbresponsecode.equals("00")){
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    account = hbresponse.getJSONObject("account");
                    accountnumber=account.getString("accountno");
                    accountype=account.getString("accounttype");
                    currency=account.getString("currency");
                    ledgerbalance=account.getString("ledgerbalance");
                    availbalance=account.getString("availbalance");
                    accountstatus=account.getString("accountstatus");
                    tmresponse.put("accountno", accountnumber);
                    tmresponse.put("accounttype", accountype);
                    tmresponse.put("currency", currency);
                    tmresponse.put("ledgerbalance", ledgerbalance);
                    tmresponse.put("availbalance", availbalance);
                    tmresponse.put("accountstatus", accountstatus);
                    logger.info(""+" source"+source+" "+sessid+":new account get balance success hbng: "+ source+" "+""+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("new account get balance");
                    tmlog.setActiondetails("request details: "+""+"  "+sessid+":new account get balance success hnbg : "+ source+" "+"" );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "something went wrong");
                    logger.info(""+" source"+source+" "+sessid+":something went wrong hbng: "+ source+" "+""+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("something went wrong");
                    tmlog.setActiondetails("request details: "+""+"  "+sessid+":something went wrong hnbg : "+ source+" "+"" );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }

            }
            output=tmresponse.toString();
        }
    
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage()+hbresponse_str);
            logger.info(""+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog); 
        }
    return output;
    }
    
    public String phoneGetBalance(String source,String transid,String sessid,String phonenumber,String passcode)
    {
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        JSONObject hbresponse;
        JSONArray accounts;
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        String auth = Authorization.getAuth();
        Utilities utils = new Utilities();
        HbngInterface hbng = new HbngInterface();
        String hbresponse_str="";
        String hbresponsecode="";
        String output="";
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(source.equals("USSD")){
                //loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
                loginrescode="00";
            }
            else{
                loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, passcode,"");
                loginresponse = new JSONObject(loginresponse_str);
                loginrescode=loginresponse.getString("responsecode");
            }
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
                logger.info(""+" source"+source+" "+sessid+":not a customer hbng: "+ source+" "+""+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber("");
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("not a customer");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":not a customer hnbg : "+ source+" "+"" );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else if(loginrescode.equals("22")){
                //wrong passcode
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "wrong passcode");
                logger.info(""+" source"+source+" "+sessid+":wrong passcode hbng: "+ source+" "+""+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber("");
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("wrong passcode");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":wrong passcode hnbg : "+ source+" "+"" );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                logger.info(" source"+source+" "+sessid+":new phoneno get balance request: "+ source+" "+"" );
                tmlog.setSource(source);
                tmlog.setPhonenumber("");
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new phoneno get balance");
                tmlog.setActiondetails("request details: "+""+"  "+sessid+":new phoneno get balance request: "+ source+" "+"" );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                hbresponse_str=hbng.phonegetbalance(source, sessid, transid, auth,phonenumber);
                logger.info(""+" source"+source+" "+sessid+" hbresponse"+ hbresponse_str );
                hbresponse = new JSONObject(hbresponse_str);
                hbresponsecode=hbresponse.getString("responsecode");
                if(hbresponsecode.equals("00")){
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    accounts = hbresponse.getJSONArray("accounts");
                    tmresponse.put("accounts", accounts);
                    logger.info(""+" source"+source+" "+sessid+":new account get balance success hbng: "+ source+" "+""+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber("");
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("new account get balance");
                    tmlog.setActiondetails("request details: "+""+"  "+sessid+":new account get balance success hnbg : "+ source+" "+"" );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "something went wrong");
                    logger.info(""+" source"+source+" "+sessid+":something went wrong hbng: "+ source+" "+""+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber("");
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("something went wrong");
                    tmlog.setActiondetails("request details: "+""+"  "+sessid+":something went wrong hnbg : "+ source+" "+"" );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }

            }
            output=tmresponse.toString();
        }
    
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage()+hbresponse_str);
            logger.info(""+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber("");
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog); 
        }
    return output;
    }
    
    public String getCompleteTransactions(String source,String transid,String sessid,String phonenumber,String inputpasscode){
        Octopuscompletetransactions comtrans = new Octopuscompletetransactions();
        List<Octopuscompletetransactions> allcomtrans;
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output="";
        JSONObject tmresponse = new JSONObject();
        JSONArray objarray = new JSONArray();
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        NumberFormat formatter = new DecimalFormat("####.00");
        phonenumber=utils.formatPhone(phonenumber);
        transid=utils.generateTransid();
        //debit source is account
        System.out.println("class entered "+utils.getDate()+" "+utils.getTime());
        try{
            System.out.println("trying to login "+utils.getDate()+" "+utils.getTime());
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber, inputpasscode,"");
            System.out.println("login successful "+utils.getDate()+" "+utils.getTime());
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a user
                tmresponse.put("responsecode", "44");
                tmresponse.put("responsemessage", "not a user");
            }else if(loginrescode.equals("22")){
                //wrong passcode
                tmresponse.put("responsecode", "55");
                tmresponse.put("responsemessage", "wrong passcode");
            }else if(loginrescode.equals("33")){
                //wrong passcode
                tmresponse.put("responsecode", "66");
                tmresponse.put("responsemessage", "Account blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to reset account");
            }else if(loginrescode.equals("00")){
                logger.info(phonenumber+" source"+source+" "+sessid+":new get complete transactions request: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new get complete transactions request");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new get complete transactions request: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                tmresponse.put("responsedate", utils.getDate());
                tmresponse.put("responsetime", utils.getTime());
                allcomtrans=DataObjects.getOctopuscompletetransactions(phonenumber);
                if(allcomtrans==null){
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "No transaction found");
                    logger.info(phonenumber+" source"+source+" "+sessid+":no complete transaction found: "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("no complete transaction found");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no complete transaction found: "+ source+" "+phonenumber );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs(); 
                }else{
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "Success");
                    logger.info(phonenumber+" source"+source+" "+sessid+":transactions found: "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("transactions found");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":transactions found: "+ source+" "+phonenumber );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs(); 
                    for(int i=0;i<allcomtrans.size();i++){
                        JSONObject eachobject = new JSONObject();
                        comtrans = allcomtrans.get(i);
                        String dbsource =(comtrans.getSource()!=null)?comtrans.getSource():"";
                        String dbphonenumber =(comtrans.getPhonenumber()!=null)?comtrans.getPhonenumber():"";
                        String dbtransactionid =(comtrans.getTransactionid()!=null)?comtrans.getTransactionid():"";
                        String dbsessionid =(comtrans.getSessionid()!=null)?comtrans.getSessionid():"";
                        String dbamount =(comtrans.getAmount()!=null)?comtrans.getAmount():"";
                        String dbsourceaccount =(comtrans.getSourceaccount()!=null)?comtrans.getSourceaccount():"";
                        String dbsourcebank =(comtrans.getSourcebank()!=null)?comtrans.getSourcebank():"";
                        String dbsourcefirstname =(comtrans.getSourcefirstname()!=null)?comtrans.getSourcefirstname():"";
                        String dbsourcemiddlename =(comtrans.getSourcemiddlename()!=null)?comtrans.getSourcemiddlename():"";
                        String dbsourcelastname =(comtrans.getSourcelastname()!=null)?comtrans.getSourcelastname():"";
                        String dbsourcecard =(comtrans.getSourcecard()!=null)?comtrans.getSourcecard():"";
                        String dbtranstype =(comtrans.getTranstype()!=null)?comtrans.getTranstype():"";
                        String dbtranscode =(comtrans.getTranscode()!=null)?comtrans.getTranscode():"";
                        String dbdestinationaccount =(comtrans.getDestinationaccount()!=null)?comtrans.getDestinationaccount():"";
                        String dbdestinationbank =(comtrans.getDestinationbank()!=null)?comtrans.getDestinationbank():"";
                        String dbdestinationfirstname =(comtrans.getDestinationfirstname()!=null)?comtrans.getDestinationfirstname():"";
                        String dbdestinationmiddlename =(comtrans.getDestinationmiddlename()!=null)?comtrans.getDestinationmiddlename():"";
                        String dbdestinationlastname =(comtrans.getDestinationlastname()!=null)?comtrans.getDestinationlastname():"";
                        String dbcustomerid =(comtrans.getCustomerid()!=null)?comtrans.getCustomerid():"";
                        String dbpaymentcode =(comtrans.getPaymentcode()!=null)?comtrans.getPaymentcode():"";
                        String dbnarration =(comtrans.getNarration()!=null)?comtrans.getNarration():"";
                        String dbpreferredmedium =(comtrans.getPreferredmedium()!=null)?comtrans.getPreferredmedium():"";
                        String dbtransdate =(comtrans.getTransdate()!=null)?comtrans.getTransdate():"";
                        String dbtransrvsreference =(comtrans.getTransrvsreference()!=null)?comtrans.getTransrvsreference():"";
                        String dbtransreference =(comtrans.getTransreference()!=null)?comtrans.getTransreference():"";
                        String dbproduct =(comtrans.getProduct()!=null)?comtrans.getProduct():"";
                        String dbstan =(comtrans.getStan()!=null)?comtrans.getStan():"";
                        String dbtstamp =(comtrans.getTstamp()!=null)?comtrans.getTstamp():"";
                        eachobject.put("source",dbsource);
                        eachobject.put("phonenumber",dbphonenumber);
                        eachobject.put("transactionid",dbtransactionid);
                        eachobject.put("sessionid",dbsessionid);
                        eachobject.put("amount",dbamount);
                        eachobject.put("sourceaccount",dbsourceaccount);
                        eachobject.put("sourcebank",dbsourcebank);
                        eachobject.put("sourcefirstname",dbsourcefirstname);
                        eachobject.put("sourcemiddlename",dbsourcemiddlename);
                        eachobject.put("sourcelastname",dbsourcelastname);
                        eachobject.put("sourcecard",dbsourcecard);
                        eachobject.put("transtype",dbtranstype);
                        eachobject.put("transcode",dbtranscode);
                        eachobject.put("destinationaccount",dbdestinationaccount);
                        eachobject.put("destinationbank",dbdestinationbank);
                        eachobject.put("destinationfirstname",dbdestinationfirstname);
                        eachobject.put("destinationmiddlename",dbdestinationmiddlename);
                        eachobject.put("destinationlastname",dbdestinationlastname);
                        eachobject.put("customerid",dbcustomerid);
                        eachobject.put("paymentcode",dbpaymentcode);
                        eachobject.put("narration",dbnarration);
                        eachobject.put("preferredmedium",dbpreferredmedium);
                        eachobject.put("transdate",dbtransdate);
                        eachobject.put("transrvsreference",dbtransrvsreference);
                        eachobject.put("transreference",dbtransreference);
                        eachobject.put("product",dbproduct);
                        eachobject.put("stan",dbstan);
                        eachobject.put("tstamp",dbtstamp);
                        objarray.put(eachobject);
                        if(i==4){
                            i=allcomtrans.size()+1;
                        }
                    }
                    tmresponse.put("result", objarray);
                }
                output = tmresponse.toString();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    return output;}
    
    
    public static void main(String args[]){
        OctopusTransactions ot = new OctopusTransactions();
        JSONObject jsn = new JSONObject();
       System.out.println(ot.fundsTransfer("USSD", "000094747366625242", "000094747366625242", "07038901111", "false", "", "", "0250562151", "058","Omotayo","Ishola","Alimi","true","40600c21-9118-4f13-a2fc-24ad6c4e87cd", "50", "trf to me", "1111",jsn));
        //System.out.println(ot.getAccountBalance("USSD", "801999999189989976", "801999999189989976", "07038901111", "1111"));
        //System.out.println(ot.billPayment("USSD", "801999999189989976", "801999999189989976", "07038901111", "false", "", "", "123", "122", "true", "4b119d1d-235d-4851-8113-5694e9bbe78d", "200", "", "1111"));
        //System.out.println(ot.approveInboxTransaction("USSD", "801999999189989976", "801999999189989976", "07038901111", "3"));
        //System.out.println(ot.getCompleteTransactions("USSD", "801999999189989976", "801999999189989976", "07038901111", "1111"));
        //System.out.println(ot.getNamewthAccount("USSD", "801999999189989976", "801999999189989976", "07038901111", "0023113070","051","ecobank"));
        //System.out.println(ot.transactionValidate("USSD", "000094747366625242", "000094747366625242", "07038901111", "", "",  "0250562151","058", "ft","true","40600c21-9118-4f13-a2fc-24ad6c4e87cd", "50", "QUC|FT|TRFTO|20170925061521234092", "Alimi Omotayo","748672","20374490"));
          //System.out.println(ot.getNamewthAccountpaystack("USSD", "000094747366625242", "000094747366625242", "07038901111", "0009781760", "044", "Access"));//System.out.println(ot.debitAccountBankit("USSD", "756473535474839893", "0000000000987", "08182120030", "0009781760", "10148", "044", "0023113070", "050", "10", "test", "3007"));
//        NumberFormat formatter = new DecimalFormat("####.00");
//        formatter.format(new BigDecimal("100")).toString().replace(".", "");
//        
//        System.out.println(formatter.format(new BigDecimal("105")).toString().replace(".", ""));
    }
    

}
