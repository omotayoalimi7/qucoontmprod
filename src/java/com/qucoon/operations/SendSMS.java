/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class SendSMS {
    public String sendSms_old(String telno,String text){
        String output="";
        JSONObject request = new JSONObject();
        String smsstatus="false";
        try{
            request.put("from", "Octopus");
            request.put("to", telno);
            request.put("text",text );
            String authstring = "octopus10:Live1234";
            String authorization = new String(Base64.encodeBase64(authstring.getBytes()));
            Client client = Client.create();
            WebResource webResource = client
               .resource("https://api.infobip.com/sms/1/text/single");
            ClientResponse response = webResource
                    .header("CONTENT-TYPE","application/json")
                    .header("Authorization","Basic "+authorization)
                    .post(ClientResponse.class,request.toString());

            String status=response.getStatus()+"";
            System.out.println("Response Code \n"+response.getStatus()+"\n");
            if(status.equals("200")){
                System.out.println("Message sent");
                output = response.getEntity(String.class);
                smsstatus="true";
            }else{
                System.out.println("error: Message not sent");
                smsstatus="false";
            }
        
        }   
        catch(Exception e){  
            e.printStackTrace();
        }

    return smsstatus;
    }
    
    public  String sendSms(String telno,String text){
        String responsemsg = "";
        String smsstatus="true";
         Utilities utils = new Utilities();
         JSONObject requestt = new JSONObject();
         String url="https://api.infobip.com/sms/1/text/single";
        try{
            requestt.put("from", "Octopus");
            requestt.put("to", telno);
            requestt.put("text",text );
            String authstring = "octopus10:Live1234";
            String authorization = new String(Base64.encodeBase64(authstring.getBytes()));
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, requestt.toString());
            Request request = new Request.Builder()
              .url(url)
              .addHeader("Authorization","Basic "+authorization)
              .post(body)
              .build();

            //Response response = client.newCall(request).execute();
            
            client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    
                // do something wih the result
               System.out.println (response.body().string().toString()+ "---"+utils.getDate()+" "+utils.getTime());
            }
        }
            });
            
            
        }catch(Exception s){
            s.printStackTrace();
        }
        
        
        
        //return responsemsg;
    return smsstatus;}
    public static void main(String args[]){
            //process("2347038901111","test");
        }
    
}
