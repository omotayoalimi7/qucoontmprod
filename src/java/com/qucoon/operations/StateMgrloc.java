/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author User001
 */
public class StateMgrloc {
    public Connection conn(){
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
        String DB_URL = "jdbc:mysql://localhost/swift";

        String USER = "root";
        String PASS = "";
        Connection conn = null; 
        try{
             Class.forName("com.mysql.jdbc.Driver");
             conn = DriverManager.getConnection(DB_URL, USER, PASS);
            }
       catch(Exception e){
           e.printStackTrace();
            }
        return conn;
    }
    
    public void openConnection(String refno, String nextaction){
        boolean status=false;
        String created_at=null;
        String updated_at=null;
        Date date = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        created_at = formater.format(date);
        updated_at=created_at;
        try{
        System.out.println("connecting");
        Statement stmt = conn().createStatement();
        System.out.println("connected");
        String sql = "insert into state_manager(reference,presentaction,nextaction,created_at,updated_at) values('"+ refno +"','openconnection','customertype','"+ created_at+"','"+ updated_at+"')";
        System.out.println(sql);
        stmt.executeUpdate(sql);
        status= true;
        System.out.println("added successfully");
        } catch(Exception e){
            e.printStackTrace();
            status=false;
        }
    }
    
    public void setnextAction(String refno, String presentaction, String nextaction){
        boolean status=false;
        String created_at=null;
        String updated_at=null;
        Date date = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        created_at = formater.format(date);
        updated_at=created_at;
        try{
        System.out.println("connecting");
        Statement stmt = conn().createStatement();
        System.out.println("connected");
        String sql = "update state_manager set presentaction='"+presentaction+"', nextaction='"+nextaction+"',updated_at='"+updated_at+"' where reference='"+refno+"'";
        System.out.println(sql);
        stmt.executeUpdate(sql);
        status= true;
        System.out.println("added successfully");
        } catch(Exception e){
            e.printStackTrace();
            status=false;
        }
    }
    
    public String getnextAction(String refno){
        String nextaction="none";
        try{
        System.out.println("connecting");
        Statement stmt = conn().createStatement();
        System.out.println("connected");
        String sql = "select nextaction from state_manager where reference='"+refno+"'";
        System.out.println(sql);
        ResultSet rs =stmt.executeQuery(sql);
        while(rs.next()){
            nextaction=rs.getString("nextaction");
        }
        System.out.println("added successfully");
        } catch(Exception e){
            e.printStackTrace();
            nextaction="none";
        }
    return nextaction;}
    
    public void killAction(String refno){
        boolean status=false;
        String created_at=null;
        String updated_at=null;
        Date date = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        created_at = formater.format(date);
        updated_at=created_at;
        try{
        System.out.println("connecting");
        Statement stmt = conn().createStatement();
        System.out.println("connected");
        String sql = "delete from state_manager where reference='"+refno+"'";
        System.out.println(sql);
        stmt.executeUpdate(sql);
        status= true;
        System.out.println("added successfully");
        } catch(Exception e){
            e.printStackTrace();
            status=false;
        }
    }
    
}
