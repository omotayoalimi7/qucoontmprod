/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;
import com.qucoonnlp.processor.connector;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
/**
 *
 * @author User001
 */
@Path("/operations")
public class OctopusServices {
    
    @POST
    @Path("/langprocessor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response langprocessor(String text)
    {
        connector connct = new connector();
        String output="";
        try{
            output=connct.langprocessor(text);
        }
        catch(Exception e){

        }
    return Response.ok(output)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/fetchmoviescity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchmoviescity(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            response = ex.fetchMoviesCity(source, transid, sessid,phonenumber);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    } 
    
    @POST
    @Path("/fetchmoviescinema")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchmoviescinema(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String cityid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,cityid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            cityid = request.getString("cityid");
            response = ex.fetchMoviesCinema(source, transid, sessid,phonenumber,cityid);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    } 
    
    @POST
    @Path("/fetchmovies")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchmovies(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String cinemaid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,cinemaid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            cinemaid = request.getString("cinemaid");
            response = ex.fetchMovies(source, transid, sessid,phonenumber,cinemaid);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    } 
    
    @POST
    @Path("/fetchmovierec")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchmovies_rec(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String movieid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,movieid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            movieid = request.getString("movieid");
            response = ex.fetchMovies_rec(source, transid, sessid,phonenumber,movieid);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    } 
    
    @POST
    @Path("/fetchmoviewithviewid")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchmoviewithviewid(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String viewid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,viewid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            viewid = request.getString("viewid");
            response = ex.fetchMoviesWithViewid(source, transid, sessid,phonenumber,viewid);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    } 
    
    @POST
    @Path("/getbankwithaccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getbankwithac(String req)
    {
        String response="";
        BankNamefromAcc bna = new BankNamefromAcc();
        response=bna.getBanks(req);
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/fundsrequest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fundsrequest(String req)
    {
        String response="";
        Utilities utils = new Utilities();
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String receiverid;
        String receivername;
        String receiveraccount;
        String receiverbank;
        String receiverbankcode;
        String customerid;
        String paymentcode;
        String billername;
        String billerid;
        String categoryname;
        String categoryid;
        String senderid;
        String sendername;
        String senderaccount;
        String senderbank;
        String senderbankcode;
        String amount;
        String transtype;
        String transcategory;
        String narration;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,receiverid,receivername,receiveraccount,receiverbank,receiverbankcode,customerid,paymentcode,billername,billerid,categoryname,categoryid,senderid,sendername,senderaccount,senderbank,senderbankcode,amount,narration,transtype,transcategory";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            receiverid = request.getString("receiverid");
            receivername = request.getString("receivername");
            receiveraccount = request.getString("receiveraccount");
            receiverbank = request.getString("receiverbank");
            receiverbankcode = request.getString("receiverbankcode");
            customerid = request.getString("customerid");
            paymentcode = request.getString("paymentcode");
            billername = request.getString("billername");
            billerid = request.getString("billerid");
            categoryname = request.getString("categoryname");
            categoryid = request.getString("categoryid");
            senderid = request.getString("senderid");
            sendername=request.getString("sendername");
            senderaccount = request.getString("senderaccount");
            senderbank=request.getString("senderbank");
            senderbankcode=request.getString("senderbankcode");
            amount = request.getString("amount");
            transtype = request.getString("transtype");
            transcategory = request.getString("transcategory");
            narration = request.getString("narration");
            response = ot.fundsRequest(source, transid, sessid, receiverid, receivername, receiveraccount, receiverbank, receiverbankcode, customerid, paymentcode, billername, billerid, categoryname, categoryid, senderid, sendername, senderaccount, senderbank, senderbankcode, amount, narration, transtype, transcategory);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    
    @POST
    @Path("/approveinboxtransaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response approveinboxtransaction(String req)
    {
        String response="";
        Utilities utils = new Utilities();
        OctopusTransactions ot = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String senderid;
        String sno;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,senderid,sno";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            senderid = request.getString("senderid");
            sno =request.getString("sno");
            response = ot.approveInboxTransaction(source, transid, sessid, senderid,sno);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/deleteinboxtransaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteinboxtransaction(String req)
    {
        String response="";
        Utilities utils = new Utilities();
        OctopusTransactions ot = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String senderid;
        String sno;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,senderid,sno";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            senderid = request.getString("senderid");
            sno =request.getString("sno");
            response = ot.deleteInboxTransaction(source, transid, sessid, senderid,sno);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/getinboxtransactions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getinboxtransactions(String req)
    {
        String response="";
        Utilities utils = new Utilities();
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
          response = ot.getInboxTransactions(source, sessid, transid, phonenumber);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/debitcustomer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response debitcustomer(String req)
    {
        String response="";
        Utilities utils = new Utilities();
        OctopusTransactions ot = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String accountstat;
        String debitaccount;
        String sourcebank;
        String creditaccount;
        String destbank;
        String amount;
        String narration;
        String passcode;
        String error="";
        try{
            JSONObject request = new JSONObject(req);
//            source = request.getString("source");
//            transid = request.getString("transid");
//            sessid = request.getString("sessid");
//            phonenumber = request.getString("phonenumber");
//            accountstat = request.getString("accountstat");
//            debitaccount = request.getString("debitaccount");
//            sourcebank = request.getString("sourcebank");
//            creditaccount = request.getString("creditaccount");
//            destbank = request.getString("destbank");
//            amount = request.getString("amount");
//            narration = request.getString("narration");
//            passcode = request.getString("passcode");
            //res = ot.debitCustomer(source, sessid, transid, phonenumber, accountstat, debitaccount, sourcebank, creditaccount, destbank, amount, narration, passcode);
            response="{\"responsecode\":\"00\",\"responsemessage\":\"success\"}";
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/creditcustomer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response creditcustomer(String req)
    {
        String res="";
        try{
        JSONObject response= new JSONObject(req);
        response.put("responsecode", "11");
        response.put("responsemessage", "failed");
        res = response.toString();
        }catch(Exception e ){
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/fundstransfer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fundstransfer(String req)
    {
        String response="";
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String accountstat;
        String debitaccount;
        String debitbank;
        String creditaccount;
        String destinationfirstname;
        String destinationmiddlename;
        String destinationlastname;
        String creditbank;
        String cardstat;
        String cid;
        String amount;
        String narration;
        String passcode;
        String error="";
        OctopusTransactions ot = new OctopusTransactions();
        try{
            String checkfieldnames = "source,sessid,transid,phonenumber,accountstat,debitaccount,debitbank,creditaccount,destinationfirstname,destinationmiddlename,destinationlastname,creditbank,cardstat,cid,amount,narration,passcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            accountstat = request.getString("accountstat");
            debitaccount = request.getString("debitaccount");
            debitbank = request.getString("debitbank");
            destinationfirstname = request.getString("destinationfirstname");
            destinationmiddlename = request.getString("destinationmiddlename");
            destinationlastname = request.getString("destinationlastname");
            creditaccount = request.getString("creditaccount");
            creditbank = request.getString("creditbank");
            cardstat = request.getString("cardstat");
            cid = request.getString("cid");
            amount = request.getString("amount");
            narration = request.getString("narration");
            passcode = request.getString("passcode");
            response=ot.fundsTransfer(source, sessid, transid, phonenumber, accountstat, debitaccount, debitbank, creditaccount,creditbank,destinationfirstname,destinationmiddlename,destinationlastname, cardstat, cid, amount, narration, passcode,request);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/recharge")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response recharge(String req)
    {
        String response="";
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String accountstat;
        String debitaccount;
        String debitbank;
        String customerid;
        String paymentcode;
        String cardstat;
        String cid;
        String amount;
        String narration;
        String passcode;
        String error="";
        OctopusTransactions ot = new OctopusTransactions();
        try{
            String checkfieldnames = "source,sessid,transid,phonenumber,accountstat,debitaccount,debitbank,customerid,paymentcode,cardstat,cid,amount,narration,passcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            accountstat = request.getString("accountstat");
            debitaccount = request.getString("debitaccount");
            debitbank = request.getString("debitbank");
            customerid = request.getString("customerid");
            paymentcode = request.getString("paymentcode");
            cardstat = request.getString("cardstat");
            cid = request.getString("cid");
            amount = request.getString("amount");
            narration = request.getString("narration");
            passcode = request.getString("passcode");
            response=ot.billPayment(source, sessid, transid, phonenumber, accountstat, debitaccount, debitbank, customerid, paymentcode, cardstat, cid, amount, narration, passcode,request);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/sendbilladvice")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response sendbilladvice(String req)
    {
        String response="";
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String accountstat;
        String debitaccount;
        String debitbank;
        String customerid;
        String paymentcode;
        String cardstat;
        String cid;
        String amount;
        String narration;
        String passcode;
        String error="";
        OctopusTransactions ot = new OctopusTransactions();
        try{
            System.out.println("heeereee");
            String checkfieldnames = "source,sessid,transid,phonenumber,accountstat,debitaccount,debitbank,customerid,paymentcode,cardstat,cid,amount,narration,passcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            accountstat = request.getString("accountstat");
            debitaccount = request.getString("debitaccount");
            debitbank = request.getString("debitbank");
            customerid = request.getString("customerid");
            paymentcode = request.getString("paymentcode");
            cardstat = request.getString("cardstat");
            cid = request.getString("cid");
            amount = request.getString("amount");
            narration = request.getString("narration");
            passcode = request.getString("passcode");
            System.out.println("herrr");
            response=ot.billPayment(source, sessid, transid, phonenumber, accountstat, debitaccount, debitbank, customerid, paymentcode, cardstat, cid, amount, narration, passcode,request);
            System.out.println("herew");
        }catch(Exception e ){
            e.printStackTrace();
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/transactionvalidate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response transactionvalidate(String req)
    {
        String response="";
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String otp;
        String customerid;
        String paymentcode;
        String cardstat;
        String cid;
        String amount;
        String transactionref;
        String cardname;
        String creditaccount;
        String creditbank;
        String transtype;
        String paymentid;
        String error="";
        OctopusTransactions ot = new OctopusTransactions();
        try{
            String checkfieldnames = "source,sessid,transid,phonenumber,customerid,paymentcode,cardstat,cid,amount,transref,cardname,otp,creditaccount,creditbank,transtype,paymentid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            customerid = request.getString("customerid");
            paymentcode = request.getString("paymentcode");
            cardstat = request.getString("cardstat");
            cid = request.getString("cid");
            amount = request.getString("amount");
            transactionref = request.getString("transref");
            cardname = request.getString("cardname");
            otp = request.getString("otp");
            creditaccount = request.getString("creditaccount");
            creditbank = request.getString("creditbank");
            transtype = request.getString("transtype");
            paymentid = request.getString("paymentid");
            response=ot.transactionValidate(source, sessid, transid, phonenumber, customerid, paymentcode,creditaccount,creditbank,transtype,cardstat, cid, amount, transactionref, cardname,otp,paymentid,request);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/customervalidation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response customervalidation(String req)
    {
        String response="";
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String customerid;
        String paymentcode;
        String error="";
        OctopusOperations ot = new OctopusOperations();
        try{
            String checkfieldnames = "source,sessid,transid,phonenumber,customerid,paymentcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            customerid = request.getString("customerid");
            paymentcode= request.getString("paymentcode");
            System.out.println("cust id: "+customerid+" paymentcode "+paymentcode);
            response=ot.validateReference(source, transid, sessid, phonenumber,customerid, paymentcode);
            //response=ot.billPayment(source, sessid, transid, phonenumber, accountstat, debitaccount, debitbank, customerid, paymentcode, cardstat, cid, amount, narration, passcode);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/getaccountbalance")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getaccountbalance(String req)
    {
        String response="";
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String error="";
        OctopusTransactions ot = new OctopusTransactions();
        try{
            String checkfieldnames = "source,sessid,transid,phonenumber,passcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            response=ot.getAccountBalance(source, sessid, transid, phonenumber,passcode);
        }catch(Exception e ){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
        }
        
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
//    @POST
//    @Path("/login")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.TEXT_PLAIN)
//    public Response logincustomer(String req)
//    {
//        String response="";
//        OctopusOperations octop = new OctopusOperations();
//        String source;
//        String transid;
//        String sessid;
//        String phonenumber;
//        String passcode;
//        String error="";
//        try{
//            String checkfieldnames = "source,transid,sessid,phonenumber, passcode";
//            validateJSON c = new validateJSON();
//            JSONObject request = new JSONObject(req);
//            error=c.isJsonParamSet(request,checkfieldnames);
//            source = request.getString("source");
//            transid = request.getString("transid");
//            sessid = request.getString("sessid");
//            phonenumber = request.getString("phonenumber");
//            passcode = request.getString("passcode");
//            response = octop.loginCustomer(source, transid, sessid,phonenumber, passcode,"true");
//
//
//        }catch(Exception e){
//            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
//            //e.printStackTrace();
//        }
//
//    return Response.ok(response)
//      .header("Access-Control-Allow-Origin", "*")
//      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
//      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
//    } 
    
    
    
    @POST
    @Path("/registeroctopus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response registeroctopus(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String firstname;
        String lastname;
        String gender;
        String dob;
        String emailaddress;
        String country;
        String passcode;
        String accountstat;
        String bvn;
        String acno;
        String bank;
        String bankcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber, firstname, lastname, gender, dob, emailaddress, country, passcode, bvn,accountstat, acno, bank,bankcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            firstname = request.getString("firstname");
            lastname = request.getString("lastname");
            gender = request.getString("gender");
            dob = request.getString("dob");
            emailaddress = request.getString("emailaddress");
            country = request.getString("country");
            passcode = request.getString("passcode");
            accountstat = request.getString("accountstat");
            bvn = request.getString("bvn");
            acno = request.getString("acno");
            bank = request.getString("bank");
            bankcode = request.getString("bankcode");
            response = octop.registerOctopus(source, transid, sessid,phonenumber, firstname, lastname, gender, dob, emailaddress, country, passcode, bvn,accountstat, acno, bank,bankcode);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/registeroctopusvalidate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response registeroctopusvalidate(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String token;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumberfirstname,lastname,gender,dob,emailaddress,country,passcode,bvn,accountstat,acno,bank,bankcode,cardstat,cardno,cardname,cardexpiry,cardcvv,cardpin,cardcurrency";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            token = request.getString("token");
            response = octop.validateRegisterOctopus(source, transid, sessid,phonenumber, token);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    
    
    
    @POST
    @Path("/createcommunity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createcommunity(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String commownerid;
        String commname;
        String commdescription;
        String commentry;
        String commtype;
        String commimage;
        String commownername;
        String commwelcomemsg;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,commownerid,commownername, commname, commdescription,commwelcomemsg, commtype, commentry, commimage";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            commownerid = request.getString("commownerid");
            commownername = request.getString("commownername");
            commname = request.getString("commname");
            commdescription = request.getString("commdescription");
            commentry = request.getString("commentry");
            commtype = request.getString("commtype");
            commimage = request.getString("commimage");
            commwelcomemsg = request.getString("commwelcomemsg");
            response = ex.createCommunity(source, transid, sessid, commownerid,commownername, commname, commdescription,commwelcomemsg, commtype, commentry, commimage);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/deletecommunity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deletecommunity(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String commid;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber,commid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            commid = request.getString("commid");
            phonenumber = request.getString("phonenumber");
            response = ex.deleteCommunity(source, sessid,transid,phonenumber, commid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/deleteparty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteparty(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String partyid;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber,partyid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            partyid = request.getString("partyid");
            phonenumber = request.getString("phonenumber");
            response = ex.deleteParty(source, sessid,transid,phonenumber, partyid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    
    @POST
    @Path("/getblacklist")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getblacklist(String req)
    {
        String response="";
        OctopusOperations ex = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            response = ex.getBlacklist(source, sessid,transid,phonenumber);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/blacklistcustomer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response blacklistcustomer(String req)
    {
        String response="";
        OctopusOperations ex = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String blacklistcustomer;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber,blacklistcustomer";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            blacklistcustomer = request.getString("blacklistcustomer");
            phonenumber = request.getString("phonenumber");
            response = ex.blacklistCustomer(source, sessid,transid,phonenumber,blacklistcustomer);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/unblacklistcustomer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response unblacklistcustomer(String req)
    {
        String response="";
        OctopusOperations ex = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String blacklistcustomer;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber,blacklistcustomer";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            blacklistcustomer = request.getString("blacklistcustomer");
            phonenumber = request.getString("phonenumber");
            response = ex.unBlacklistCustomer(source, sessid,transid,phonenumber, blacklistcustomer);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/joincommunity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response joincommunity(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String commid;
        String commownerid;
        String memberid;
        String notification;
        String membername;
        String commname;
        String commtype;
        String commentry;
        String commownername;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,commid, commownerid,commownername, memberid,membername, notification,commname,commtype,commentry";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            commid = request.getString("commid");
            commownerid = request.getString("commownerid");
            memberid = request.getString("memberid");
            notification = request.getString("notification");
            membername = request.getString("membername");
            commname = request.getString("commname");
            commtype = request.getString("commtype");
            commentry = request.getString("commentry");
            commownername = request.getString("commownername");
            response = ex.joinCommunity(source, sessid,transid, commid, commownerid,commownername, memberid,membername, notification,commname,commtype,commentry);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/searchcommunity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response searchcommunity(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String comm;
        String phonenumber;
        String searchwith;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, comm";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            comm = request.getString("comm");
            searchwith = request.getString("searchwith");
            phonenumber = request.getString("phonenumber");
             if(searchwith.equals("id")){
                response = ex.searchCommunity_id(source, sessid,transid,phonenumber, comm);
             }else{
                response = ex.searchCommunity_name(source, sessid,transid,phonenumber, comm); 
             }
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/fetchcommunitymembers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchcommunitymembers(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String commid;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, commid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            commid = request.getString("commid");
            response = ex.fetchCommunityMembers(source, sessid,transid,phonenumber , commid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    
    @POST
    @Path("/fetchjoinedcommunities")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchjoinedcommunities(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String memberid;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, memberid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            memberid = request.getString("memberid");
            response = ex.fetchJoinedCommunities(source, sessid,transid,phonenumber , memberid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/fetchcreatedcommunities")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchcreatedcommunities(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String commownerid;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, commownerid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            commownerid = request.getString("commownerid");
            response = ex.fetchCreatedCommunities(source, sessid,transid,phonenumber , commownerid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    
    @POST
    @Path("/createparty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createparty(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String partyownerid;
        String partyname;
        String partydescription;
        String partyvenue;
        String partyfee;
        String partyimage;
        String partyownername;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, partyownerid,partyownername, partyname, partydescription, partyfee, partyvenue, partyimage";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            partyownerid = request.getString("partyownerid");
            partyownername = request.getString("partyownername");
            partyname = request.getString("partyname");
            partydescription = request.getString("partydescription");
            partyvenue = request.getString("partyvenue");
            partyfee = request.getString("partyfee");
            partyimage = request.getString("partyimage");
            response = ex.createParty(source, transid, sessid, partyownerid,partyownername, partyname, partydescription, partyfee, partyvenue, partyimage);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    
    @POST
    @Path("/fetchpartymembers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchpartymembers(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String partyid;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, partyid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            partyid = request.getString("partyid");
            response = ex.fetchPartyMembers(source, sessid,transid,phonenumber , partyid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/fetchjoinedparties")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchjoinedparties(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String memberid;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, memberid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            memberid = request.getString("memberid");
            response = ex.fetchJoinedParties(source, sessid,transid,phonenumber , memberid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/fetchcreatedparties")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchcreatedparties(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String partyownerid;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber, partyownerid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            partyownerid = request.getString("partyownerid");
            response = ex.fetchCreatedParties(source, sessid,transid,phonenumber , partyownerid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/joinparty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response joinparty(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String partyid;
        String partyownerid;
        String memberid;
        String taggingalong;
        String membername;
        String partyname;
        String partyvenue;
        String partyfee;
        String partyownername;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,partyid, partyownerid,partyownername, memberid,membername, taggingalong,partyname,partyvenue,partyfee";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            partyid = request.getString("partyid");
            partyownerid = request.getString("partyownerid");
            memberid = request.getString("memberid");
            taggingalong = request.getString("taggingalong");
            membername = request.getString("membername");
            partyname = request.getString("partyname");
            partyvenue = request.getString("partyvenue");
            partyfee = request.getString("partyfee");
            partyownername = request.getString("partyownername");
            response = ex.joinParty(source, sessid,transid, partyid, partyownerid,partyownername, memberid,membername, taggingalong,partyname,partyvenue,partyfee);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/searchparty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response searchparty(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String partyid;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber,partyid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            partyid = request.getString("partyid");
            phonenumber = request.getString("phonenumber");
            response = ex.searchParty(source, sessid,transid,phonenumber, partyid);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/fetchexchangerates")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchexchangerates(String req)
    {
        String response="";
        Extras ex = new Extras();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            response = ex.fetchExchangeRates(source, transid, sessid,phonenumber);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/gettransactions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response gettransactions(String req)
    {
        String response="";
        OctopusTransactions octop = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,passcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            response = octop.getCompleteTransactions(source, transid, sessid,phonenumber, passcode);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/addaccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addaccount(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String bvn;
        String acno;
        String bank;
        String bankcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,passcode,bvn,acno,bank,bankcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            bvn = request.getString("bvn");
            acno = request.getString("acno");
            bank = request.getString("bank");
            bankcode = request.getString("bankcode");
            response = octop.addAccount(source, transid, sessid,phonenumber, passcode, bvn, acno, bank, bankcode);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/deleteaccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteaccount(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String acno;
        String bank;
        String bankcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,passcode,acno,bank,bankcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            acno = request.getString("acno");
            bank = request.getString("bank");
            bankcode = request.getString("bankcode");
            response = octop.deleteAccount(source, transid, sessid,phonenumber, passcode, acno, bank, bankcode);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/addcard")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addcard(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String cardno;
        String cardexpiry;
        String cardpin;
        String cardcvv;
        String cardname;
        String cardcountry;
        String cardcurrency;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,passcode,cardno,cardpin,cardexpiry,cardcvv,cardname,cardcountry,cardcurrency";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            cardno = request.getString("cardno");
            cardexpiry = request.getString("cardexpiry");
            cardpin = request.getString("cardpin");
            cardcvv = request.getString("cardcvv");
            cardname = request.getString("cardname");
            cardcountry = request.getString("cardcountry");
            cardcurrency = request.getString("cardcurrency");
            response = octop.addCard(source, transid, sessid,phonenumber, passcode, cardno, cardpin, cardexpiry, cardcvv,cardname,cardcountry,"NGN");


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/addcardvalidate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addcardvalidate(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String cid;
        String otp;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,cid,otp";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            cid = request.getString("cid");
            otp = request.getString("otp");
            response = octop.addCardValidate(source, transid, sessid,phonenumber, cid, otp);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/deletecard")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deletecard(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String cid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,passcode,cid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            cid = request.getString("cid");
            response = octop.deleteCard(source, transid, sessid,phonenumber, passcode, cid);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/sourceaccounts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response sourceaccounts(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
             source = request.getString("source");
             transid = request.getString("transid");
             sessid = request.getString("sessid");
             phonenumber = request.getString("phonenumber");
             response = octop.sourceAccounts(source, transid, sessid,phonenumber);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
   
    @POST
    @Path("/savebeneficiary")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response savebeneficiary(String req){
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String benephone;
        String beneaccount;
        String benename;
        String benebank;
        String benebankcode;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber,benephone,beneaccount,benename,benebank,benebankcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            benephone = request.getString("benephone");
            beneaccount = request.getString("beneaccount");
            benename = request.getString("benename");
            benebank = request.getString("benebank");
            benebankcode = request.getString("benebankcode");
            //response = octop.addBeneficiary(source, transid, sessid,phonenumber, benephone,beneaccount, benename,  benebank,benebankcode);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured"+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
   
    @POST
    @Path("/fetchbeneficiary")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response fetchbeneficiary(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
         try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            response = octop.fetchBeneficiary(source, transid, sessid,phonenumber);
         }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured"+error+"\"}";
            //e.printStackTrace();
         }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
   
    @POST
    @Path("/customerstatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response customerstatus(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String country;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,country";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            country = request.getString("country");
            response = octop.customerStatus(source, transid, sessid, phonenumber, country);
         }catch(Exception e){
             e.printStackTrace();
             response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

  }
    
    @POST
    @Path("/getnamewithaccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getnamewthaccount(String req)
    {
        String res="";
        Utilities utils = new Utilities();
        OctopusTransactions ot = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String accountno;
        String bankcode;
        String bankname;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,accountno,bankcode,bankname";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            accountno =request.getString("accountno");
            bankcode = request.getString("bankcode");
            bankname =request.getString("bankname");
            res = ot.getNamewthAccountpaystack(source, transid, sessid, phonenumber,accountno,bankcode,bankname);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/phonegetbalance")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response phonegetbalance(String req)
    {
        String res="";
        Utilities utils = new Utilities();
        OctopusTransactions ot = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            res = ot.phoneGetBalance(source, transid, sessid, phonenumber,passcode);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/bvnenquiry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response bvnenquiry(String req)
    {
        String res="";
        OctopusTransactions ot = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String bvn;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,bvn";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            bvn = request.getString("bvn");
            res = ot.bvnEnquiry(source, transid, sessid, bvn);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/billercategorys")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response billercategorys(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            res = ot.iswCategorys(source, transid, sessid, phonenumber);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/billerlist")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response billerlist(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            res = ot.iswBillers(source, transid, sessid, phonenumber);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/billerlistwithcategoryid")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response billerlistwithcategoryid(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String categoryid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,categoryid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            categoryid = request.getString("categoryid");
            res = ot.iswBillers_catid(source, transid, sessid, phonenumber,categoryid);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/paymentitems")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response paymentitems(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            res = ot.iswPaymentitems(source, transid, sessid, phonenumber);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/paymentitemswithbillerid")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response paymentitemswithbillerid(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String billerid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,billerid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            billerid = request.getString("billerid");
            res = ot.iswPaymentitems_billerid(source, transid, sessid, phonenumber,billerid);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/rechargebillers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response rechargebillers(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            res = ot.iswMobileRecharge(source, transid, sessid, phonenumber);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/validatereference")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response validatereference(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String customerid;
        String paymentcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,customerid,customerid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            customerid = request.getString("customerid");
            paymentcode = request.getString("paymentcode");
            res = ot.validateReference(source,transid,sessid,phonenumber, customerid, paymentcode);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
   
  
  
    
  }
