/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;
import com.qucoon.operations.BankNamefromAcc;
import com.qucoon.operations.OctopusOperations;
import com.qucoon.operations.OctopusTransactions;
import com.qucoon.operations.Utilities;
import com.qucoon.operations.validateJSON;
import com.qucoonnlp.processor.connector;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
/**
 *
 * @author User001
 */
@Path("/otheroperations")
public class OtherServices {
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response logincustomer(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String passcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber, passcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            passcode = request.getString("passcode");
            response = octop.loginCustomer(source, transid, sessid,phonenumber, passcode,"true");


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }

    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    } 
    
    @POST
    @Path("/getnamewithaccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getnamewthaccount(String req)
    {
        String res="";
        Utilities utils = new Utilities();
        OctopusTransactions ot = new OctopusTransactions();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String accountno;
        String bankcode;
        String bankname;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,accountno,bankcode,bankname";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            accountno =request.getString("accountno");
            bankcode = request.getString("bankcode");
            bankname =request.getString("bankname");
            res = ot.getNamewthAccount(source, transid, sessid, phonenumber,accountno,bankcode,bankname);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    @POST
    @Path("/getbankwithaccount")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response getbankwithac(String req)
    {
        String response="";
        BankNamefromAcc bna = new BankNamefromAcc();
        response=bna.getBanks(req);
        
    return Response.ok(response)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/billercategorys")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response billercategorys(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            res = ot.iswCategorys(source, transid, sessid, phonenumber);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    @POST
    @Path("/billerlistwithcategoryid")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response billerlistwithcategoryid(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String categoryid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,categoryid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            categoryid = request.getString("categoryid");
            res = ot.iswBillers_catid(source, transid, sessid, phonenumber,categoryid);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    @POST
    @Path("/paymentitemswithbillerid")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response paymentitemswithbillerid(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String billerid;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,billerid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            billerid = request.getString("billerid");
            res = ot.iswPaymentitems_billerid(source, transid, sessid, phonenumber,billerid);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/customerstatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response customerstatus(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String country;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,country";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            country = request.getString("country");
            response = octop.customerStatus_simple(source, transid, sessid, phonenumber, country);
         }catch(Exception e){
             e.printStackTrace();
             response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

  }
    @POST
    @Path("/registeroctopus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response registeroctopus(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String firstname;
        String lastname;
        String gender;
        String dob;
        String emailaddress;
        String country;
        String passcode;
        String accountstat;
        String bvn;
        String acno;
        String bank;
        String bankcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber, firstname, lastname, gender, dob, emailaddress, country, passcode, bvn,accountstat, acno, bank,bankcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            firstname = request.getString("firstname");
            lastname = request.getString("lastname");
            gender = request.getString("gender");
            dob = request.getString("dob");
            emailaddress = request.getString("emailaddress");
            country = request.getString("country");
            passcode = request.getString("passcode");
            accountstat = request.getString("accountstat");
            bvn = request.getString("bvn");
            acno = request.getString("acno");
            bank = request.getString("bank");
            bankcode = request.getString("bankcode");
            response = octop.registerOctopus(source, transid, sessid,phonenumber, firstname, lastname, gender, dob, emailaddress, country, passcode, bvn,accountstat, acno, bank,bankcode);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/registeroctopusvalidate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response registeroctopusvalidate(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String token;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumberfirstname,lastname,gender,dob,emailaddress,country,passcode,bvn,accountstat,acno,bank,bankcode,cardstat,cardno,cardname,cardexpiry,cardcvv,cardpin,cardcurrency";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            token = request.getString("token");
            response = octop.validateRegisterOctopus(source, transid, sessid,phonenumber, token);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    @POST
    @Path("/langprocessor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response langprocessor(String text)
    {
        connector connct = new connector();
        String output="";
        try{
            output=connct.langprocessor(text);
        }
        catch(Exception e){

        }
    return Response.ok(output)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    @POST
    @Path("/validatereference")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response validatereference(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String customerid;
        String paymentcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,customerid,customerid";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            customerid = request.getString("customerid");
            paymentcode = request.getString("paymentcode");
            res = ot.validateReference(source,transid,sessid,phonenumber, customerid, paymentcode);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    @POST
    @Path("/rechargebillers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response rechargebillers(String req)
    {
        String res="";
        OctopusOperations ot = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            res = ot.iswMobileRecharge(source, transid, sessid, phonenumber);
        }catch(Exception e ){
            res="{\"responsecode\":\"999\",\"responsemessage\":\"An error Occured: "+error+"\"}";
            
        }
        
        
    return Response.ok(res)
      .header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
      .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    
    @POST
    @Path("/unblockcustomer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response unblockcustomer(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String token;
        String passcode;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber,token,passcode";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            token = request.getString("token");
            passcode = request.getString("passcode");
            response = octop.unBlockCustomer(source, transid, sessid,phonenumber,token, passcode);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
    
    @POST
    @Path("/resetpasscode")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response resetpasscode(String req)
    {
        String response="";
        OctopusOperations octop = new OctopusOperations();
        String source;
        String transid;
        String sessid;
        String phonenumber;
        String error="";
        try{
            String checkfieldnames = "source,transid,sessid,phonenumber";
            validateJSON c = new validateJSON();
            JSONObject request = new JSONObject(req);
            error=c.isJsonParamSet(request,checkfieldnames);
            source = request.getString("source");
            transid = request.getString("transid");
            sessid = request.getString("sessid");
            phonenumber = request.getString("phonenumber");
            response = octop.resetPasscode(source, transid, sessid,phonenumber);


        }catch(Exception e){
            response="{\"responsecode\":\"999\",\"responsemessage\":\"An Error Occured: "+error+"\"}";
            //e.printStackTrace();
        }
    return Response.ok(response)
       .header("Access-Control-Allow-Origin", "*")
       .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
       .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();

   }
}
