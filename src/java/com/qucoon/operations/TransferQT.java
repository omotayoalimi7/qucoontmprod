///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.qucoon.operations;
//
//import com.qucoon.dbo.Debitlogs;
//import com.qucoon.dbo.Octopuscompletetransactions;
//import com.qucoon.dbo.Octopuscustomersaccounts;
//import com.qucoon.dbo.Octopuscustomerscards;
//import com.qucoon.dbo.Octopuscustomerstm;
//import com.qucoon.dbo.Octopusdisputetransactions;
//import com.qucoon.dbo.Octopustransactions;
//import com.qucoon.dbo.PersistenceAgent;
//import com.qucoon.dbo.Tmlogs;
//import com.qucoon.isw.QtServices;
//import static com.qucoon.operations.OctopusTransactions.logger;
//import org.json.JSONObject;
//
///**
// *
// * @author neptune
// */
//public class TransferQT {
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        // TODO code application logic here
//    }
//    
//    public JSONObject doTransfer(String source,String transid,String sessid,String amount,String creditbank,String creditaccount,String destinationlastname,String beneothername, String phonenumber,String sourceemailadress,String senderothername,String sourcelastname, String unqref){
//        String iswresponse="";
//        String billdetails="";
//        Octopustransactions logot = new Octopustransactions();
//        QtServices qts = new QtServices();
//        OctopusTransactions ot = new OctopusTransactions();
//
//        Octopuscompletetransactions comtrans = new Octopuscompletetransactions();
//        Octopusdisputetransactions distrans = new Octopusdisputetransactions();
//        Debitlogs dbl = new Debitlogs();
//        Tmlogs tmlog = new Tmlogs();
//        Utilities utils = new Utilities();
//        boolean debitaccountstat=false;
//        Octopuscustomersaccounts ocustacc;
//        Octopuscustomerscards ocustcards;
//        Octopuscustomerstm ocusttm;
//        
//                        String qtresponsestr = qts.doTransfer(source,transid,sessid,amount, creditbank, creditaccount, destinationlastname, destinationfirstname+" "+destinationmiddlename,phonenumber,sourceemailadress,sourcefirstname+" "+sourcemiddlename,sourcelastname,"1528"+transid.substring(10,transid.length()));
//                            JSONObject qtresponse = new JSONObject(qtresponsestr);
//                            String qtresponsecode=qtresponse.getString("responsecode");
//                            String qttransref="";
//                            if(qtresponsecode.equals("00")){
//                                billdetails="Payment of N"+amount+" for "+destinationfirstname+" "+destinationmiddlename+" "+destinationlastname+" with account:"+creditaccount+".";
//                                comtrans.setAmount(amount);
//                                comtrans.setDestinationaccount(creditaccount);
//                                comtrans.setDestinationbank(creditbank);
//                                comtrans.setNarration(""+transid);
//                                comtrans.setPhonenumber(phonenumber);
//                                comtrans.setPreferredmedium("ipg");
//                                comtrans.setSessionid(sessid);
//                                comtrans.setSource(source);
//                                comtrans.setSourcecard(cid);
//                                comtrans.setSourcebank("card");
//                                comtrans.setTransactionid(transid);
//                                comtrans.setTranscode("IPG01");
//                                comtrans.setTransdate(transdate);
//                                comtrans.setTransreference(transref);
//                                comtrans.setTranstype("ft");
//                                comtrans.setDestinationfirstname(destinationfirstname);
//                                comtrans.setDestinationlastname(destinationlastname);
//                                comtrans.setDestinationmiddlename(destinationmiddlename);
//                                comtrans.setSourcefirstname(sourcefirstname);
//                                comtrans.setSourcelastname(sourcelastname);
//                                comtrans.setSourcemiddlename(sourcemiddlename);
//                                comtrans.setCustomerid("");
//                                comtrans.setPaymentcode("");
//                                PersistenceAgent.logTransactions(comtrans);
//                                if(qtresponse.has("transactionRef")){
//                                    resmsg="validated";
//                                    qttransref=qtresponse.getString("transactionRef");
//                                    tmresponse.put("qttransref", qttransref);
//
//                                }
//                                if(qtresponse.has("MscData")){
//                                    MscData=qtresponse.getString("MscData");
//                                    tmresponse.put("MscData", MscData);
//                                    billdetails=billdetails+" MscData:"+MscData+".";
//                                }
//                                if(qtresponse.has("rechargePIN")){
//                                    rechargePIN=qtresponse.getString("rechargePIN");
//                                    tmresponse.put("rechargePIN", rechargePIN);
//                                    billdetails=billdetails+" rechargePIN:"+rechargePIN+".";
//                                }
//                                tmresponse.put("responsecode", "00");
//                                tmresponse.put("responsemessage", "funds transfer success-"+resmsg);
//                                tmresponse.put("transref",transid); 
//                                logot.setAmount(amount);
//                                logot.setDestinationaccount(creditaccount);
//                                logot.setDestinationbank(creditbank);
//                                logot.setNarration("Billpayment"+transid);
//                                logot.setPhonenumber(phonenumber);
//                                logot.setPreferredmedium("ipg");
//                                logot.setSessionid(sessid);
//                                logot.setSource(source);
//                                logot.setSourcecard(cid);
//                                logot.setSourcebank("card");
//                                logot.setTransactionid(transid);
//                                logot.setTranscode("IPG01");
//                                logot.setTransdate(transdate);
//                                logot.setTransreference(qttransref);
//                                logot.setTranstype("C");
//                                logot.setDestinationfirstname(destinationfirstname);
//                                logot.setDestinationlastname(destinationlastname);
//                                logot.setDestinationmiddlename(destinationmiddlename);
//                                logot.setSourcefirstname(sourcefirstname);
//                                logot.setSourcelastname(sourcelastname);
//                                logot.setSourcemiddlename(sourcemiddlename);
//                                PersistenceAgent.logTransactions(logot);
//                                logot = new Octopustransactions();
//                                dbl.setActiondetails("funds transfer success: "+qttransref+" ");
//                                dbl.setActionperformed("funds transfer success");
//                                dbl.setActiontime(transdate);
//                                dbl.setAmount(amount);
//                                dbl.setDestinationaccount(creditaccount);
//                                dbl.setDestinationbank(creditbank);
//                                dbl.setNarration("FT"+transid);
//                                dbl.setPhonenumber(phonenumber);
//                                dbl.setPreferredmedium("ipg");
//                                dbl.setSessionid(sessid);
//                                dbl.setSource(source);
//                                dbl.setSourcecard(cid);
//                                dbl.setSourcebank("card");
//                                dbl.setTransactionid(transid);
//                                PersistenceAgent.logTransactions(dbl);
//                                dbl = new Debitlogs();
//                                logger.info(phonenumber+"funds transfer success: "+qttransref+" ");
//                                tmlog.setSource(source);
//                                tmlog.setPhonenumber(phonenumber);
//                                tmlog.setSessionid(sessid);
//                                tmlog.setTransactionid(transid);
//                                tmlog.setActionperformed("funds transfer success");
//                                tmlog.setActiondetails("funds transfer success: "+qttransref+" ");
//                                tmlog.setActiontime(transdate);
//                                PersistenceAgent.logTransactions(tmlog);
//                                tmlog = new Tmlogs();
//                                mail.ftpaymentNotification(phonenumber, emailaddress, firstname+" "+lastname, transdate, "Funds Transfer", cardno, destinationfirstname+" "+destinationmiddlename+" "+destinationlastname, creditaccount, bankname, narration, notificationamount, transid, "Card", cardno, cardbank);
//                                log.info(appname, classname, "Transaction Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Transaction ID "+transid);
//                                logger.info(phonenumber+"Mail sent. Funds transfer success: "+qttransref+" ");
//                                tmlog.setSource(source);
//                                tmlog.setPhonenumber(phonenumber);
//                                tmlog.setSessionid(sessid);
//                                tmlog.setTransactionid(transid);
//                                tmlog.setActionperformed("Mail sent. Funds transfer success");
//                                tmlog.setActiondetails("Mail sent. Funds transfer success: "+qttransref+" ");
//                                tmlog.setActiontime(transdate);
//                                PersistenceAgent.logTransactions(tmlog);
//                                tmlog = new Tmlogs();
//                                op.addBeneficiary(source, transid, sessid, phonenumber, "", creditaccount, destinationfirstname+" "+destinationmiddlename+" "+destinationlastname, bankname, creditbank, "", "", "ft");
//                                logger.info(phonenumber+"Beneficiary Saved. Funds transfer success: "+qttransref+" ");
//                                tmlog.setSource(source);
//                                tmlog.setPhonenumber(phonenumber);
//                                tmlog.setSessionid(sessid);
//                                tmlog.setTransactionid(transid);
//                                tmlog.setActionperformed("Beneficiary Saved. Funds transfer success");
//                                tmlog.setActiondetails("Beneficiary Saved. Funds transfer success: "+qttransref+" ");
//                                tmlog.setActiontime(transdate);
//                                PersistenceAgent.logTransactions(tmlog);
//                                tmlog = new Tmlogs();
//                            }else{
//                                resmsg="error";
//                                log.dispute(appname, classname, "Dispute", "Transaction ID:"+transid+" Customer:"+phonenumber+" Beneficiary Account:"+creditaccount+" Beneficiary bank:"+bankname+" Beneficiary bank code:"+creditbank+" Amount"+amount);
//                                tmresponse.put("responsecode", "11");
//                                tmresponse.put("responsemessage", "Transaction has been submitted successfully and is being processed. You shall be notified on completion.");
//                                distrans.setAmount(amount);
//                                distrans.setDestinationaccount(creditaccount);
//                                distrans.setDestinationbank(creditbank);
//                                distrans.setNarration(""+transid);
//                                distrans.setPhonenumber(phonenumber);
//                                distrans.setPreferredmedium("ipg");
//                                distrans.setSessionid(sessid);
//                                distrans.setSource(source);
//                                distrans.setSourcecard(cid);
//                                distrans.setSourcebank("card");
//                                distrans.setTransactionid(transid);
//                                distrans.setTranscode("IPG01");
//                                distrans.setTransdate(transdate);
//                                distrans.setTransreference(transref);
//                                distrans.setTranstype("ft");
//                                distrans.setDestinationfirstname(destinationfirstname);
//                                distrans.setDestinationlastname(destinationlastname);
//                                distrans.setDestinationmiddlename(destinationmiddlename);
//                                distrans.setSourcefirstname(sourcefirstname);
//                                distrans.setSourcelastname(sourcelastname);
//                                distrans.setSourcemiddlename(sourcemiddlename);
//                                distrans.setCustomerid("");
//                                distrans.setPaymentcode("");
//                                PersistenceAgent.logTransactions(distrans);
//                                dbl.setActiondetails("funds transfer failed:reversal needed: ");
//                                dbl.setActionperformed("funds transfer failed:reversal needed");
//                                dbl.setActiontime(transdate);
//                                dbl.setAmount(amount);
//                                dbl.setDestinationaccount(creditaccount);
//                                dbl.setDestinationbank(creditbank);
//                                dbl.setNarration("FT"+transid);
//                                dbl.setPhonenumber(phonenumber);
//                                dbl.setPreferredmedium("ipg");
//                                dbl.setSessionid(sessid);
//                                dbl.setSource(source);
//                                dbl.setSourcecard(cid);
//                                dbl.setSourcebank("card");
//                                dbl.setTransactionid(transid);
//                                PersistenceAgent.logTransactions(dbl);
//                                dbl = new Debitlogs();
//                                logger.info(phonenumber+"funds transfer failed: "+qttransref+" ");
//                                tmlog.setSource(source);
//                                tmlog.setPhonenumber(phonenumber);
//                                tmlog.setSessionid(sessid);
//                                tmlog.setTransactionid(transid);
//                                tmlog.setActionperformed("funds transfer failed");
//                                tmlog.setActiondetails("funds transfer failed: "+qttransref+" ");
//                                tmlog.setActiontime(transdate);
//                                PersistenceAgent.logTransactions(tmlog);
//                                tmlog = new Tmlogs();
//                            }
//        
//    }
//    
//}
