/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.qucoon.dbo.DataObjects;
import com.qucoon.dbo.Movies;
import com.qucoon.dbo.Movieslist;
import com.qucoon.dbo.Moviescinema;
import com.qucoon.dbo.Moviescity;
import com.qucoon.dbo.Octopuscommunities;
import com.qucoon.dbo.Octopuscommunitymembers;
import com.qucoon.dbo.Octopusexchangerates;
import com.qucoon.dbo.Octopusparties;
import com.qucoon.dbo.Octopuspartymembers;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.Tmlogs;
import static com.qucoon.operations.OctopusOperations.logger;
import com.qucoon.slacklogger.SlackLogMessage;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class Extras {
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Extras.class);
    SlackLogMessage log = new SlackLogMessage();
    String appname="QucconTM";
    String classname="Extras";
    public String fetchMovies(String source,String sessid,String transid,String phonenumber,String cinemaid)
    {
        List<Movies> allmovies;
        Movies movie ;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        String movieid;
        String moviename;
        String day;
        String price;
        String seats;
        String cinemaname;
        String cityid;
        String cityname;
        String url;
        String output;
        try{
            allmovies = DataObjects.getMovies(cinemaid);
            logger.info(phonenumber+"  "+sessid+":new allmovies request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new allmovies request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(allmovies.size()>0){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                for(int i=0;i<allmovies.size();i++){
                    JSONObject eachobject = new JSONObject();
                    movie=allmovies.get(i);
                    movieid=movie.getMovieid();
                    moviename = movie.getMoviename();
                    day = movie.getDay();
                    price = movie.getPrice();
                    seats = movie.getSeats();
                    cinemaname = movie.getCinemaname();
                    cityid = movie.getCityid();
                    cityname = movie.getCityname();
                    url=movie.getUrl();
                    eachobject.put("movieid", movieid);
                    eachobject.put("moviename", moviename);
                    eachobject.put("day", day);
                    eachobject.put("price", price);
                    eachobject.put("seats", seats);
                    eachobject.put("cinemaid", cinemaid);
                    eachobject.put("cinemaname", cinemaname);
                    eachobject.put("cityid", cityid);
                    eachobject.put("cityname", cityname);
                    eachobject.put("url", url);
                    arr.put(eachobject);
                }
                logger.info(phonenumber+"  "+sessid+":movies request success source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("movies request success");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                tmresponse.put("result", arr);
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no movie");
                logger.info(phonenumber+"  "+sessid+":movies request success no movie source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("movies request success no movie");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String fetchMoviesCinema(String source,String sessid,String transid,String phonenumber,String cityid)
    {
        List<Moviescinema> allmoviescinema;
        Moviescinema moviecinema ;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        String cinemaname;
        String cinemaid;
        String cityname;
        String output;
        try{
            allmoviescinema = DataObjects.getMoviescinema(cityid);
            logger.info(phonenumber+"  "+sessid+":new moviescinema request source: "+ source+" "+phonenumber+" "+cityid );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new moviescinema requet received");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(allmoviescinema.size()>0){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                for(int i=0;i<allmoviescinema.size();i++){
                    JSONObject eachobject = new JSONObject();
                    moviecinema=allmoviescinema.get(i);
                    cinemaname = moviecinema.getCinemaname();
                    cinemaid = moviecinema.getCinemaid();
                    cityname = moviecinema.getCityname();
                    eachobject.put("cinemaid", cinemaid);
                    eachobject.put("cinemaname", cinemaname);
                    eachobject.put("cityname", cityname);
                    eachobject.put("cityid", cityid);
                    arr.put(eachobject);
                }
                logger.info(phonenumber+"  "+sessid+":moviescinema request success source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("moviescinema request success");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                tmresponse.put("result", arr);
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no moviescinema");
                logger.info(phonenumber+"  "+sessid+":moviescinema request success no moviescinema source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("moviescinema request success no moviescinema");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String fetchMoviesCity(String source,String sessid,String transid,String phonenumber )
    {
        List<Moviescity> allmoviescity;
        Moviescity moviecity ;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        String cityid;
        String cityname;
        String output;
        try{
            allmoviescity = DataObjects.getMoviescity();
            logger.info(phonenumber+"  "+sessid+":new moviescity request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new moviescity requet received");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(allmoviescity.size()>0){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                for(int i=0;i<allmoviescity.size();i++){
                    JSONObject eachobject = new JSONObject();
                    moviecity=allmoviescity.get(i);
                    cityid = moviecity.getCityid();
                    cityname = moviecity.getCityname();
                    eachobject.put("cityname", cityname);
                    eachobject.put("cityid", cityid);
                    arr.put(eachobject);
                }
                logger.info(phonenumber+"  "+sessid+":moviescity request success source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("moviescity request success");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                tmresponse.put("result", arr);
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no moviescity");
                logger.info(phonenumber+"  "+sessid+":moviescity request success no moviescity source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("moviescity request success no moviescity");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String fetchMovies_rec(String source,String sessid,String transid,String phonenumber,String movieid)
    {
        List<Movieslist> allmovies;
        Movieslist movie ;
        JSONObject tmresponse = new JSONObject();
        JSONArray monday = new JSONArray();
        JSONArray tuesday = new JSONArray();
        JSONArray wednesday = new JSONArray();
        JSONArray thursday = new JSONArray();
        JSONArray friday = new JSONArray();
        JSONArray saturday = new JSONArray();
        JSONArray sunday = new JSONArray();
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        String moviename="";
        String viewid;
        String day;
        String time;
        String price;
        String seats;
        String cinemaname="";
        String cinemaid;
        String cityid;
        String cityname="";
        String url="";
        String output;
        try{
            logger.info(phonenumber+"  "+sessid+":new movie request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new movie request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            tmresponse.put("responsecode", "00");
            tmresponse.put("responsemessage", "success");
            allmovies=DataObjects.getMovies_rec(movieid);
            if(allmovies.size()>0){
                for(int i=0;i<allmovies.size();i++){
                    JSONObject eachobject = new JSONObject();
                    movie=allmovies.get(i);
                    viewid = movie.getViewid();
                    moviename = movie.getMoviename();
                    day = movie.getDay();
                    time = movie.getTime();
                    price = movie.getPrice();
                    seats = movie.getSeats();
                    cinemaname = movie.getCinemaname();
                    cinemaid = movie.getCinemaid();
                    cityid = movie.getCityid();
                    cityname = movie.getCityname();
                    url=movie.getUrl();
                    if(day.toLowerCase().trim().equals("monday")){
                        eachobject = new JSONObject();
                        eachobject.put("viewid", viewid);
                        eachobject.put("movieid", movieid);
                        eachobject.put("moviename", moviename);
                        eachobject.put("day", day);
                        eachobject.put("time", time);
                        eachobject.put("price", price);
                        eachobject.put("seats", seats);
                        eachobject.put("cinemaid", cinemaid);
                        eachobject.put("cinemaname", cinemaname);
                        eachobject.put("cityid", cityid);
                        eachobject.put("cityname", cityname);
                        eachobject.put("url", url);
                        monday.put(eachobject);
                    }
                    if(day.toLowerCase().trim().equals("tuesday")){
                        eachobject = new JSONObject();
                        eachobject.put("viewid", viewid);
                        eachobject.put("movieid", movieid);
                        eachobject.put("moviename", moviename);
                        eachobject.put("day", day);
                        eachobject.put("time", time);
                        eachobject.put("price", price);
                        eachobject.put("seats", seats);
                        eachobject.put("cinemaid", cinemaid);
                        eachobject.put("cinemaname", cinemaname);
                        eachobject.put("cityid", cityid);
                        eachobject.put("cityname", cityname);
                        eachobject.put("url", url);
                        tuesday.put(eachobject);
                    }
                    if(day.toLowerCase().trim().equals("wednesday")){
                        eachobject = new JSONObject();
                        eachobject.put("viewid", viewid);
                        eachobject.put("movieid", movieid);
                        eachobject.put("moviename", moviename);
                        eachobject.put("day", day);
                        eachobject.put("time", time);
                        eachobject.put("price", price);
                        eachobject.put("seats", seats);
                        eachobject.put("cinemaid", cinemaid);
                        eachobject.put("cinemaname", cinemaname);
                        eachobject.put("cityid", cityid);
                        eachobject.put("cityname", cityname);
                        eachobject.put("url", url);
                        wednesday.put(eachobject);
                    }
                    if(day.toLowerCase().trim().equals("thursday")){
                        eachobject = new JSONObject();
                        eachobject.put("viewid", viewid);
                        eachobject.put("movieid", movieid);
                        eachobject.put("moviename", moviename);
                        eachobject.put("day", day);
                        eachobject.put("time", time);
                        eachobject.put("price", price);
                        eachobject.put("seats", seats);
                        eachobject.put("cinemaid", cinemaid);
                        eachobject.put("cinemaname", cinemaname);
                        eachobject.put("cityid", cityid);
                        eachobject.put("cityname", cityname);
                        eachobject.put("url", url);
                        thursday.put(eachobject);
                    }
                    if(day.toLowerCase().trim().equals("friday")){
                        eachobject = new JSONObject();
                        eachobject.put("viewid", viewid);
                        eachobject.put("movieid", movieid);
                        eachobject.put("moviename", moviename);
                        eachobject.put("day", day);
                        eachobject.put("time", time);
                        eachobject.put("price", price);
                        eachobject.put("seats", seats);
                        eachobject.put("cinemaid", cinemaid);
                        eachobject.put("cinemaname", cinemaname);
                        eachobject.put("cityid", cityid);
                        eachobject.put("cityname", cityname);
                        eachobject.put("url", url);
                        friday.put(eachobject);
                    }
                    if(day.toLowerCase().trim().equals("saturday")){
                        eachobject = new JSONObject();
                        eachobject.put("viewid", viewid);
                        eachobject.put("movieid", movieid);
                        eachobject.put("moviename", moviename);
                        eachobject.put("day", day);
                        eachobject.put("time", time);
                        eachobject.put("price", price);
                        eachobject.put("seats", seats);
                        eachobject.put("cinemaid", cinemaid);
                        eachobject.put("cinemaname", cinemaname);
                        eachobject.put("cityid", cityid);
                        eachobject.put("cityname", cityname);
                        eachobject.put("url", url);
                        saturday.put(eachobject);
                    }
                    if(day.toLowerCase().trim().equals("sunday")){
                        eachobject = new JSONObject();
                        eachobject.put("viewid", viewid);
                        eachobject.put("movieid", movieid);
                        eachobject.put("moviename", moviename);
                        eachobject.put("day", day);
                        eachobject.put("time", time);
                        eachobject.put("price", price);
                        eachobject.put("seats", seats);
                        eachobject.put("cinemaid", cinemaid);
                        eachobject.put("cinemaname", cinemaname);
                        eachobject.put("cityid", cityid);
                        eachobject.put("cityname", cityname);
                        eachobject.put("url", url);
                        sunday.put(eachobject);
                    }
                    
                }
                if(monday.length()>0){
                tmresponse.put("monday", monday);
                }
                if(tuesday.length()>0){
                tmresponse.put("tuesday", tuesday);
                }
                if(wednesday.length()>0){
                tmresponse.put("wednesday", wednesday);
                }
                if(thursday.length()>0){
                tmresponse.put("thursday", thursday);
                }
                if(friday.length()>0){
                tmresponse.put("friday", friday);
                }
                if(saturday.length()>0){
                tmresponse.put("saturday", saturday);
                }
                if(sunday.length()>0){
                tmresponse.put("sunday", sunday);
                }
                tmresponse.put("moviename", moviename);
                tmresponse.put("url", url);
                tmresponse.put("cinemaname", cinemaname);
                tmresponse.put("cityname", cityname);
                logger.info(phonenumber+"  "+sessid+":movie request success source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("movie request success");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no movie");
                logger.info(phonenumber+"  "+sessid+":movie request success no movie source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("movie request success no movie");
                tmlog.setActiondetails("details: "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String fetchMoviesWithViewid(String source,String sessid,String transid,String phonenumber,String viewid)
    {
        Movieslist movielist;
        String movieid;
        String moviename;
        String day;
        String time;
        String seats;
        String price;
        String cinemaname;
        String cityname;
        JSONObject tmresponse = new JSONObject();
        JSONObject eachobject = new JSONObject();
        JSONArray arr = new JSONArray();
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        String output="";
        try{
            logger.info(phonenumber+"  "+sessid+":new view id movie request source: "+ source+" "+phonenumber+" "+viewid );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new view id movie request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" "+viewid);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            movielist=DataObjects.getMovieswithviewid(viewid);
            movieid=movielist.getMovieid();
            movieid = (movieid != null)?movieid.trim() :"";
            if(movieid.equals("")){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "view id not found");
                logger.info(phonenumber+"  "+sessid+":view id not found source: "+ source+" "+phonenumber+" "+viewid );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("view id not found");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" "+viewid);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                moviename=movielist.getMoviename();
                day=movielist.getDay();
                time=movielist.getTime();
                seats=movielist.getSeats();
                price=movielist.getPrice();
                cinemaname=movielist.getCinemaname();
                cityname=movielist.getCityname();
                eachobject.put("movieid", movieid);
                eachobject.put("moviename", moviename);
                eachobject.put("day", day);
                eachobject.put("time", time);
                eachobject.put("seats", seats);
                eachobject.put("price", price);
                eachobject.put("cinemaname", cinemaname);
                eachobject.put("cityname", cityname);
                eachobject.put("viewid", viewid);
                arr.put(eachobject);
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+": success view id found source: "+ source+" "+phonenumber+" "+viewid );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("success view id found ");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" "+viewid);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    
    public String fetchExchangeRates(String source,String sessid,String transid,String phonenumber)
    {
        Octopusexchangerates exg;
        List<Octopusexchangerates> exglist;
        JSONArray arr = new JSONArray();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String date;
        String usdbuysell;
        String gbpbuysell;
        String eurbuysell;
        String output;
        try{
            logger.info(phonenumber+"  "+sessid+":new exchange rate request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new exchange rate request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            exglist = DataObjects.getOctopusexchangerates();
            //int size = (exglist.size()!= null)? exglist.size() :"";
            if(exglist.size()>0){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                for(int i=0;i<exglist.size();i++){
                    JSONObject eachobject = new JSONObject();
                    exg=exglist.get(i);
                    date=exg.getDate();
                    usdbuysell=exg.getUsdbuysell();
                    gbpbuysell=exg.getGbpbuysell();
                    eurbuysell=exg.getEurbuysell();
                    eachobject.put("date", date);
                    eachobject.put("usdbuysell", usdbuysell);
                    eachobject.put("gbpbuysell", gbpbuysell);
                    eachobject.put("eurbuysell", eurbuysell);
                    arr.put(eachobject);
                }
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+":exchange rate request success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("exchange rate request success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "rates not available");
                logger.info(phonenumber+"  "+sessid+":rates not available: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("rates not available");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String createParty(String source,String sessid,String transid,String partyownerid,String partyownername,String partyname,String partydescription,String partyfee,String partyvenue,String partyimage)
    {
        List<Octopusparties> alloctp;
        Octopusparties octp_s;
        Octopusparties octp = new Octopusparties();
        Extras ex = new Extras();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        int previd=0;
        dte=utils.getDate()+" "+utils.getTime();
        partyownerid=utils.formatPhone(partyownerid);
        try{
            logger.info(partyownerid+"  "+sessid+":new create party request source: "+ source+" "+partyownerid+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(partyownerid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new create party request");
            tmlog.setActiondetails("request details: "+partyownerid+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            alloctp=DataObjects.getOctopusparties();
            int size=0;
            if(alloctp.isEmpty()){
                size=0;
                previd=size;
            }else{
                size=alloctp.size(); 
                octp_s=alloctp.get(size-1);
                previd=octp_s.getId();
            }
            String pprevid="";
           if(previd<10){
               pprevid="000"+previd;
           }else if(previd>=10 && previd<100){
               pprevid="00"+previd;
           }else if(previd>=100 && previd<1000){
               pprevid="0"+previd;
           }else{
               pprevid=""+previd;
           }
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            octp.setPartyid("E"+pprevid);
            octp.setPartyownerid(partyownerid);
            octp.setPartyownername(partyownername);
            octp.setPartyname(partyname);
            octp.setPartydescription(partydescription);
            octp.setPartyfee(partyfee);
            octp.setPartyimage(partyimage);
            octp.setPartyvenue(partyvenue);
            octp.setCreated_at(dte);
            octp.setUpdated_at(dte);
            octp.setStatus("active");
            PersistenceAgent.logTransactions(octp);
            tmresponse.put("responsecode", "00");
            tmresponse.put("responsemessage", "success");
            tmresponse.put("partyid", "E"+pprevid);
            logger.info(partyownerid+"  "+sessid+":create party request success source: "+ source+" "+partyownerid+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(partyownerid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("create party request success");
            tmlog.setActiondetails("request details: "+partyownerid+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            String jp=ex.joinParty(source, sessid, transid, "E"+pprevid, partyownerid,partyownername,partyownerid,partyownername,"",partyname,partyvenue,partyfee);
            
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(partyownerid+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(partyownerid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String joinParty(String source,String sessid,String transid,String partyid,String partyownerid,String partyownername,String memberid,String membername,String taggingalong,String partyname,String partyvenue,String partyfee)
    {
        Octopuspartymembers octpm = new Octopuspartymembers();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String exresponse_str;
        JSONObject exresponse;
        JSONArray exresponsearr;
        Extras ex = new Extras();
        boolean joined=false;
        dte=utils.getDate()+" "+utils.getTime();
        partyownerid=utils.formatPhone(partyownerid);
        memberid=utils.formatPhone(memberid);
        partyid=partyid.toUpperCase();
        try{
            logger.info(partyownerid+"  "+sessid+":new join party request source: "+ source+" "+partyownerid+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(partyownerid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new join party request");
            tmlog.setActiondetails("request details: "+partyownerid+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            tmresponse.put("responsecode", "00");
            tmresponse.put("responsemessage", "success");
            exresponse_str=ex.fetchJoinedParties(source, sessid, transid, memberid, memberid);
            exresponse= new JSONObject(exresponse_str);
            if(exresponse.has("result")){
            exresponsearr=exresponse.getJSONArray("result");
            //if(exresponsearr.length()>0){
             for(int i=0;i<exresponsearr.length();i++){
                 JSONObject eachobject =exresponsearr.getJSONObject(i);
                 String pcommid=eachobject.getString("partyid");
                 if(partyid.equalsIgnoreCase(pcommid)){
                   joined=true;  
                 }
             }
            //}
            }
            if(joined==false){
                tmresponse.put("requestdate", utils.getDate());
                tmresponse.put("requesttime", utils.getTime());
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                octpm.setPartyid(partyid);
                octpm.setPartyownerid(partyownerid);
                octpm.setMemberid(memberid);
                octpm.setMembername(membername);
                octpm.setTaggingalong(taggingalong);
                octpm.setPartyfee(partyfee);
                octpm.setPartyownername(partyownername);
                octpm.setPartyvenue(partyvenue);
                octpm.setPartyname(partyname);
                octpm.setStatus("active");
                octpm.setCreated_at(dte);
                octpm.setUpdated_at(dte);
                PersistenceAgent.logTransactions(octpm);
                logger.info(partyownerid+"  "+sessid+":join party request success source: "+ source+" "+partyownerid+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(partyownerid);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("join party request success");
                tmlog.setActiondetails("request details: "+partyownerid+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("requestdate", utils.getDate());
                tmresponse.put("requesttime", utils.getTime());
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "Already joined");
                logger.info(partyownerid+"  "+sessid+":user already joined party source: "+ source+" "+partyownerid+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(partyownerid);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("user already joined party");
                tmlog.setActiondetails("request details: "+partyownerid+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(partyownerid+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(partyownerid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String searchParty(String source,String sessid,String transid,String phonenumber,String partyid)
    {
        
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String partyname;
        String dbpartyownerid;
        String partydescription;
        String partyfee;
        String partyvenue;
        String partyimage;
        String partyownername;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        partyid=partyid.toUpperCase();
        Octopusparties octp = DataObjects.getOctopusparties_id(partyid);
        try{
            logger.info(phonenumber+"  "+sessid+":new search party request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new search party request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            partyname = octp.getPartyname();
            partydescription = octp.getPartydescription();
            partyfee = octp.getPartyfee();
            partyvenue = octp.getPartyvenue();
            partyimage=octp.getPartyimage();
            partyownername=octp.getPartyownername();
            dbpartyownerid=octp.getPartyownerid();
            dbpartyownerid=(dbpartyownerid!=null) ?dbpartyownerid.trim(): "";
            if(!dbpartyownerid.equals("")){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                tmresponse.put("partyownerid", dbpartyownerid);
                tmresponse.put("partyname", partyname);
                tmresponse.put("partydescription", partydescription);
                tmresponse.put("partyfee", partyfee);
                tmresponse.put("partyvenue", partyvenue);
                tmresponse.put("partyimage", partyimage);
                tmresponse.put("partyownername", partyownername);
                logger.info(phonenumber+"  "+sessid+":search party request success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("search party request success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "party not found");
                logger.info(phonenumber+"  "+sessid+":party not foundsource: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("party not found");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String fetchCreatedParties(String source,String sessid,String transid,String phonenumber,String partyownerid)
    {
        
        Octopusparties octp;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String partyid;
        String partyname;
        String dbpartyownerid;
        String partydescription;
        String partyfee;
        String partyvenue;
        String partyimage;
        String partyownername;
        String partystatus;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        partyownerid=utils.formatPhone(partyownerid);
        List<Octopusparties> octplist = DataObjects.getOctopusparties_owner(partyownerid);
        try{
            logger.info(phonenumber+"  "+sessid+":new fetch all parties by owner request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new fetch all parties by owner");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(octplist.isEmpty()){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no party found for owner");
                logger.info(phonenumber+"  "+sessid+":no party found for owner source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no party found for owner");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no party found for owner");
                for(int i=0;i<octplist.size();i++){
                    JSONObject eachobject = new JSONObject();
                    octp= octplist.get(i);
                    partyid = octp.getPartyid();
                    partyname = octp.getPartyname();
                    partyownername = octp.getPartyownername();
                    partydescription = octp.getPartydescription();
                    partyfee = octp.getPartyfee();
                    partyvenue = octp.getPartyvenue();
                    partyimage=octp.getPartyimage();
                    dbpartyownerid=octp.getPartyownerid();
                    partystatus=octp.getStatus();
                    if(partystatus.equals("active")){
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "success");
                        eachobject.put("partyid", partyid);
                        eachobject.put("partyownerid", dbpartyownerid);
                        eachobject.put("partyname", partyname);
                        eachobject.put("partyownername", partyownername);
                        eachobject.put("partydescription", partydescription);
                        eachobject.put("partyfee", partyfee);
                        eachobject.put("partyvenue", partyvenue);
                        eachobject.put("partyimage", partyimage);
                        arr.put(eachobject);
                    }
                }
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+":fetch all parties by owner success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("fetch all parties by owner");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String fetchJoinedParties(String source,String sessid,String transid,String phonenumber,String memberid)
    {
        
        Octopuspartymembers octp;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String partyid;
        String taggingalong;
        String dbpartyownerid;
        String status;
        String partyname;
        String partyvenue;
        String partyfee;
        String membername;
        String partyownername;
        String partystatus;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        memberid=utils.formatPhone(memberid);
        List<Octopuspartymembers> octplist = DataObjects.getOctopusparties_member(memberid);
        try{
            logger.info(phonenumber+"  "+sessid+":new fetch all parties by member request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new fetch all parties by member");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(octplist.isEmpty()){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no party found for owner");
                logger.info(phonenumber+"  "+sessid+":no party found for member source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no party found for member");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no party found for owner");
                for(int i=0;i<octplist.size();i++){
                    JSONObject eachobject = new JSONObject();
                    octp= octplist.get(i);
                    partyid = octp.getPartyid();
                    taggingalong = octp.getTaggingalong();
                    status = octp.getStatus();
                    partyname=octp.getPartyname();
                    partyvenue=octp.getPartyvenue();
                    partyfee=octp.getPartyfee();
                    membername=octp.getMembername();
                    partyownername=octp.getPartyownername();
                    dbpartyownerid=octp.getPartyownerid();
                    partystatus=octp.getStatus();
                    if(partystatus.equals("active")){
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "success");
                        eachobject.put("partyid", partyid);
                        eachobject.put("partyname", partyname);
                        eachobject.put("partyvenue", partyvenue);
                        eachobject.put("partyfee", partyfee);
                        eachobject.put("membername", membername);
                        eachobject.put("memberid", memberid);
                        eachobject.put("partyownerid", dbpartyownerid);
                        eachobject.put("partyownername", partyownername);
                        eachobject.put("taggingalong", taggingalong);
                        eachobject.put("status", status);
                        arr.put(eachobject);
                    }
                }
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+":fetch all parties by member success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("fetch all parties by member");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String fetchPartyMembers(String source,String sessid,String transid,String phonenumber,String partyid)
    {
        
        Octopuspartymembers octp;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String memberid;
        String taggingalong;
        String dbpartyownerid;
        String status;
        String partyname;
        String partyvenue;
        String partyfee;
        String membername;
        String partyownername;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        List<Octopuspartymembers> octplist = DataObjects.getOctopusparties_mymembers(partyid);
        try{
            logger.info(phonenumber+"  "+sessid+":new fetch my party member request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new fetch all parties by owner");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(octplist.isEmpty()){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no members found for partyid");
                logger.info(phonenumber+"  "+sessid+":no members found for partyid source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no members found for partyid member");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                for(int i=0;i<octplist.size();i++){
                    JSONObject eachobject = new JSONObject();
                    octp= octplist.get(i);
                    taggingalong = octp.getTaggingalong();
                    status = octp.getStatus();
                    memberid=octp.getMemberid();
                    partyname=octp.getPartyname();
                    partyvenue=octp.getPartyvenue();
                    partyfee=octp.getPartyfee();
                    membername=octp.getMembername();
                    partyownername=octp.getPartyownername();
                    dbpartyownerid=octp.getPartyownerid();
                    eachobject.put("partyid", partyid);
                    eachobject.put("partyname", partyname);
                    eachobject.put("partyvenue", partyvenue);
                    eachobject.put("partyfee", partyfee);
                    eachobject.put("membername", membername);
                    eachobject.put("memberid", memberid);
                    eachobject.put("partyownerid", dbpartyownerid);
                    eachobject.put("partyownername", partyownername);
                    eachobject.put("taggingalong", taggingalong);
                    eachobject.put("status", status);
                    arr.put(eachobject);
                }
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+":members found for partyid success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("members found for partyid success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String createCommunity(String source,String sessid,String transid,String commownerid,String commownername,String commname,String commdescription,String commwelcomemsg,String commtype,String commentry,String commimage)
   {
       List<Octopuscommunities> alloctp;
       Octopuscommunities octp_s;
       Octopuscommunities octp = new Octopuscommunities();
       Extras ex = new Extras();
       JSONObject tmresponse = new JSONObject();
       Tmlogs tmlog = new Tmlogs();
       Utilities utils = new Utilities();
       String output;
       String dte;
       int previd=0;
       dte=utils.getDate()+" "+utils.getTime();
       commownerid=utils.formatPhone(commownerid);
       try{
           logger.info(commownerid+"  "+sessid+":new create comm request source: "+ source+" "+commownerid+" " );
           tmlog.setSource(source);
           tmlog.setPhonenumber(commownerid);
           tmlog.setSessionid(sessid);
           tmlog.setTransactionid(transid);
           tmlog.setActionperformed("new create comm request");
           tmlog.setActiondetails("request details: "+commownerid+" "+sessid+" "+transid+" "+source+" ");
           tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
           PersistenceAgent.logTransactions(tmlog);
           tmlog = new Tmlogs();
           alloctp=DataObjects.getOctopuscommunities();
           int size=0;
           if(alloctp.isEmpty()){
               size=0;
               previd=size;
           }else{
               size=alloctp.size(); 
               octp_s=alloctp.get(size-1);
               previd=octp_s.getId();
           }
           String pprevid="";
           if(previd<10){
               pprevid="000"+previd;
           }else if(previd>=10 && previd<100){
               pprevid="00"+previd;
           }else if(previd>=100 && previd<1000){
               pprevid="0"+previd;
           }else{
               pprevid=""+previd;
           }

           tmresponse.put("requestdate", utils.getDate());
           tmresponse.put("requesttime", utils.getTime());
           octp.setCommid("C"+pprevid);
           octp.setCommownerid(commownerid);
           octp.setCommownername(commownername);
           octp.setCommname(commname);
           octp.setCommdescription(commdescription);
           octp.setCommwelcomemsg(commwelcomemsg);
           octp.setCommtype(commtype);
           octp.setCommimage(commimage);
           octp.setCommentry(commentry);
           octp.setCreated_at(dte);
           octp.setUpdated_at(dte);
           octp.setStatus("active");
           PersistenceAgent.logTransactions(octp);
           tmresponse.put("responsecode", "00");
           tmresponse.put("responsemessage", "success");
           tmresponse.put("commid", "C"+pprevid);
           logger.info(commownerid+"  "+sessid+":create comm request success source: "+ source+" "+commownerid+" " );
           tmlog.setSource(source);
           tmlog.setPhonenumber(commownerid);
           tmlog.setSessionid(sessid);
           tmlog.setTransactionid(transid);
           tmlog.setActionperformed("create comm request success");
           tmlog.setActiondetails("request details: "+commownerid+" "+sessid+" "+transid+" "+source+" ");
           tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
           PersistenceAgent.logTransactions(tmlog);
           tmlog = new Tmlogs();
           String jp=ex.joinCommunity(source, sessid, transid, "C"+pprevid, commownerid,commownername,commownerid,commownername,"true",commname,commtype,commentry);

           output=tmresponse.toString();
       }catch(Exception e){
           e.printStackTrace();
           tmlog = new Tmlogs();
           output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
           logger.info("error details: "+ e.getMessage());
           logger.info(commownerid+"  "+sessid+": tm response: "+ output);
           tmlog = new Tmlogs();
           tmlog.setSource(source);
           tmlog.setPhonenumber(commownerid);
           tmlog.setSessionid(sessid);
           tmlog.setTransactionid(transid);
           tmlog.setActionperformed("build tm response");
           tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
           tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
           PersistenceAgent.logTransactions(tmlog);
         }
   return output;}
     
    public String joinCommunity(String source,String sessid,String transid,String commid,String commownerid,String commownername,String memberid,String membername,String notification,String commname,String commtype,String commentry)
   {
       Octopuscommunitymembers octpm = new Octopuscommunitymembers();
       JSONObject tmresponse = new JSONObject();
       OctopusOperations op = new OctopusOperations();
       Tmlogs tmlog = new Tmlogs();
       Utilities utils = new Utilities();
       Mailer mail = new Mailer();
       String output;
       String dte;
       Extras ex = new Extras();
       String exresponse_str="";
       JSONObject exresponse;
       JSONArray exresponsearr;
       String firstname;
       String lastname;
       String emailaddress;
       String customerstatrescode;
       JSONObject customerstatresjson;
       dte=utils.getDate()+" "+utils.getTime();
       boolean joined = false;
       commownerid=utils.formatPhone(commownerid);
       memberid=utils.formatPhone(memberid);
       commid=commid.toUpperCase();
       try{
           logger.info(commownerid+"  "+sessid+":new join comm request source: "+ source+" "+commownerid+" " );
           tmlog.setSource(source);
           tmlog.setPhonenumber(commownerid);
           tmlog.setSessionid(sessid);
           tmlog.setTransactionid(transid);
           tmlog.setActionperformed("new join comm request");
           tmlog.setActiondetails("request details: "+commownerid+" "+sessid+" "+transid+" "+source+" ");
           tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
           PersistenceAgent.logTransactions(tmlog);
           tmlog = new Tmlogs();
           exresponse_str=ex.fetchJoinedCommunities(source, sessid, transid, memberid, memberid);
           exresponse= new JSONObject(exresponse_str);
           if(exresponse.has("result")){
           exresponsearr=exresponse.getJSONArray("result");
           //if(exresponsearr.length()>0){
            for(int i=0;i<exresponsearr.length();i++){
                JSONObject eachobject =exresponsearr.getJSONObject(i);
                String jcommid=eachobject.getString("commid");
                if(commid.equalsIgnoreCase(jcommid)){
                  joined=true;  
                }
            }
           //}
           }
           if(joined==false){
                tmresponse.put("requestdate", utils.getDate());
                tmresponse.put("requesttime", utils.getTime());
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                octpm.setCommid(commid);
                octpm.setCommownerid(commownerid);
                octpm.setMemberid(memberid);
                octpm.setMembername(membername);
                octpm.setNotification(notification);
                octpm.setCommentry(commentry);
                octpm.setCommownername(commownername);
                octpm.setCommtype(commtype);
                octpm.setCommname(commname);
                octpm.setStatus("active");
                octpm.setCreated_at(dte);
                octpm.setUpdated_at(dte);
                PersistenceAgent.logTransactions(octpm);
                logger.info(commownerid+"  "+sessid+":join comm request success source: "+ source+" "+commownerid+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(commownerid);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("join comm request success");
                tmlog.setActiondetails("request details: "+commownerid+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                String customerstatres = op.customerStatus(source, transid, sessid, memberid, "NG");
                customerstatresjson = new JSONObject(customerstatres);
                customerstatrescode=customerstatresjson.getString("responsecode");
                if(customerstatrescode.equals("00")){
                    firstname = customerstatresjson.getString("firstname");
                    lastname = customerstatresjson.getString("lastname");
                    emailaddress = customerstatresjson.getString("emailaddress");
                    mail.joinCommunityNotification(memberid, emailaddress, firstname+" "+lastname, commname);
                    log.info(appname, classname, "Join Community Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+memberid+", Community: "+commname+" "+commid);
                    logger.info(memberid+"  "+sessid+":Mail Sent. Community successfully joined "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(memberid);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("Mail Sent. Community successfully joined");
                    tmlog.setActiondetails("Mail Sent. Community successfully joined. kernel response "+memberid+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                }
           }else{
                tmresponse.put("requestdate", utils.getDate());
                tmresponse.put("requesttime", utils.getTime());
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "Already joined");
                logger.info(commownerid+"  "+sessid+":user already joined comm source: "+ source+" "+commownerid+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(commownerid);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("user already joined comm");
                tmlog.setActiondetails("request details: "+commownerid+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
           }
           output=tmresponse.toString();
       }catch(Exception e){
           e.printStackTrace();
           tmlog = new Tmlogs();
           output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
           logger.info("error details: "+ e.getMessage());
           logger.info(commownerid+"  "+sessid+": tm response: "+ output);
           tmlog = new Tmlogs();
           tmlog.setSource(source);
           tmlog.setPhonenumber(commownerid);
           tmlog.setSessionid(sessid);
           tmlog.setTransactionid(transid);
           tmlog.setActionperformed("build tm response");
           tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
           tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
           PersistenceAgent.logTransactions(tmlog);
         }
   return output;}
    
    public String searchCommunity_id(String source,String sessid,String transid,String phonenumber,String commid)
    {
        
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String commname;
        String dbcommownerid;
        String commdescription;
        String commentry;
        String commtype;
        String commimage;
        String commownername;
        String commstatus;
        String commwelcomemsg;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        commid=commid.toUpperCase();
        Octopuscommunities octp = DataObjects.getOctopuscommunities_id(commid);
        try{
            logger.info(phonenumber+"  "+sessid+":new search comm request id source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new search comm id request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            commname = octp.getCommname();
            commdescription = octp.getCommdescription();
            commentry = octp.getCommentry();
            commtype = octp.getCommtype();
            commimage=octp.getCommimage();
            commownername=octp.getCommownername();
            commwelcomemsg=octp.getCommwelcomemsg();
            commstatus=octp.getStatus();
            dbcommownerid=octp.getCommownerid();
            dbcommownerid=(dbcommownerid!=null) ?dbcommownerid.trim(): "";
            commstatus=(commstatus!=null) ?commstatus.trim(): "";
            if(!dbcommownerid.equals("")&&commstatus.equals("active")){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                tmresponse.put("commownerid", dbcommownerid);
                tmresponse.put("commname", commname);
                tmresponse.put("commdescription", commdescription);
                tmresponse.put("commentry", commentry);
                tmresponse.put("commtype", commtype);
                tmresponse.put("commid", commid);
                tmresponse.put("commimage", commimage);
                tmresponse.put("commownername", commownername);
                tmresponse.put("commwelcomemsg", commwelcomemsg);
                logger.info(phonenumber+"  "+sessid+":search comm id request success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("search comm id request success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "comm id not found");
                logger.info(phonenumber+"  "+sessid+":comm id not foundsource: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("comm id not found");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String searchCommunity_name(String source,String sessid,String transid,String phonenumber,String commname)
    {
        
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String commid;
        String dbcommownerid;
        String commdescription;
        String commentry;
        String commtype;
        String commimage;
        String commownername;
        String commstatus;
        String commwelcomemsg;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        Octopuscommunities octp = DataObjects.getOctopuscommunities_name(commname);
        try{
            logger.info(phonenumber+"  "+sessid+":new search comm name request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new search comm name request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            commid = octp.getCommid();
            commdescription = octp.getCommdescription();
            commentry = octp.getCommentry();
            commtype = octp.getCommtype();
            commimage=octp.getCommimage();
            commownername=octp.getCommownername();
            commwelcomemsg=octp.getCommwelcomemsg();
            commstatus=octp.getStatus();
            dbcommownerid=octp.getCommownerid();
            dbcommownerid=(dbcommownerid!=null) ?dbcommownerid.trim(): "";
            if(!dbcommownerid.equals("")&&commstatus.equals("active")){
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                tmresponse.put("commownerid", dbcommownerid);
                tmresponse.put("commid", commid);
                tmresponse.put("commname", commname);
                tmresponse.put("commdescription", commdescription);
                tmresponse.put("commentry", commentry);
                tmresponse.put("commtype", commtype);
                tmresponse.put("commimage", commimage);
                tmresponse.put("commownername", commownername);
                tmresponse.put("commwelcomemsg", commwelcomemsg);
                logger.info(phonenumber+"  "+sessid+":search comm name request success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("search comm name request success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "comm name not found");
                logger.info(phonenumber+"  "+sessid+":comm name not foundsource: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("comm name not found");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String deleteCommunity(String source,String sessid,String transid,String phonenumber,String commid)
    {
        
        Octopuscommunitymembers octm;
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String commname;
        String dbcommownerid;
        String commdescription;
        String commentry;
        String commtype;
        String commimage;
        String commownername;
        String commstatus;
        String commwelcomemsg;
        commid=commid.toUpperCase();
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        Octopuscommunities octp = DataObjects.getOctopuscommunities_id(commid);
        List<Octopuscommunitymembers> octmlist = DataObjects.getOctopuscommunitiesmembers_id(commid);
        try{
            logger.info(phonenumber+"  "+sessid+":new search comm request id source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new search comm id request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            commname = octp.getCommname();
            commdescription = octp.getCommdescription();
            commentry = octp.getCommentry();
            commtype = octp.getCommtype();
            commimage=octp.getCommimage();
            commownername=octp.getCommownername();
            commwelcomemsg=octp.getCommwelcomemsg();
            commstatus=octp.getStatus();
            dbcommownerid=octp.getCommownerid();
            dbcommownerid=(dbcommownerid!=null) ?dbcommownerid.trim(): "";
            commstatus=(commstatus!=null) ?commstatus.trim(): "";
            if(dbcommownerid.equals(phonenumber)&&commstatus.equals("active")){
                octp.setStatus("deleted");
                PersistenceAgent.logTransactions(octp);
                for(int i=0;i<octmlist.size();i++){
                   octm=octmlist.get(i);
                   octm.setStatus("deleted");
                   PersistenceAgent.logTransactions(octm);
                }
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "Community deleted");
                logger.info(phonenumber+"  "+sessid+":delete comm id request success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("delete comm id request success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "You either do not have permission or community does not exist");
                logger.info(phonenumber+"  "+sessid+":comm id not foundsource: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("comm id not found");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public String deleteParty(String source,String sessid,String transid,String phonenumber,String partyid)
    {
        
        Octopuspartymembers octm;
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String dbpartyownerid;
        String partystatus;
        partyid=partyid.toUpperCase();
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        Octopusparties octp = DataObjects.getOctopusparties_id(partyid);
        List<Octopuspartymembers> octmlist = DataObjects.getOctopuspartiesmembers_id(partyid);
        try{
            logger.info(phonenumber+"  "+sessid+":new search party request id source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new search party id request");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            dbpartyownerid=octp.getPartyownerid();
            partystatus=octp.getStatus();
            dbpartyownerid=(dbpartyownerid!=null) ?dbpartyownerid.trim(): "";
            partystatus=(partystatus!=null) ?partystatus.trim(): "";
            System.out.println(dbpartyownerid+" "+partystatus);
            if(dbpartyownerid.equals(phonenumber)&&partystatus.equals("active")){
                octp.setStatus("deleted");
                PersistenceAgent.logTransactions(octp);
                for(int i=0;i<octmlist.size();i++){
                   octm=octmlist.get(i);
                   octm.setStatus("deleted");
                   PersistenceAgent.logTransactions(octm);
                }
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "Event deleted");
                logger.info(phonenumber+"  "+sessid+":delete party id request success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("delete party id request success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "You either do not have permission or event does not exist");
                logger.info(phonenumber+"  "+sessid+":party id not foundsource: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("party id not found");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    
    public String fetchJoinedCommunities(String source,String sessid,String transid,String phonenumber,String memberid)
    {
        
        Octopuscommunitymembers octp;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String commid;
        String commnotification;
        String dbcommownerid;
        String status;
        String commname;
        String commtype;
        String commentry;
        String membername;
        String commownername;
        String commstatus;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        memberid=utils.formatPhone(memberid);
        List<Octopuscommunitymembers> octplist = DataObjects.getOctopuscommunitymembers_member(memberid);
        try{
            logger.info(phonenumber+"  "+sessid+":new fetch all comm by member request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new fetch all comm by member");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(octplist.isEmpty()){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no comm found for owner");
                logger.info(phonenumber+"  "+sessid+":no comm found for member source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no comm found for member");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no comm found for owner");
                for(int i=0;i<octplist.size();i++){
                    JSONObject eachobject = new JSONObject();
                    octp= octplist.get(i);
                    commid = octp.getCommid();
                    commnotification = octp.getNotification();
                    status = octp.getStatus();
                    commname=octp.getCommname();
                    commtype=octp.getCommtype();
                    commentry=octp.getCommentry();
                    membername=octp.getMembername();
                    commownername=octp.getCommownername();
                    dbcommownerid=octp.getCommownerid();
                    commstatus=octp.getStatus();
                    if(commstatus.equals("active")){
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "success");
                        eachobject.put("commid", commid);
                        eachobject.put("commname", commname);
                        eachobject.put("commtype", commtype);
                        eachobject.put("commentry", commentry);
                        eachobject.put("membername", membername);
                        eachobject.put("memberid", memberid);
                        eachobject.put("commownerid", dbcommownerid);
                        eachobject.put("commownername", commownername);
                        eachobject.put("commnotification", commnotification);
                        eachobject.put("status", status);
                        arr.put(eachobject);
                    }
                }
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+":fetch all comm by member success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("fetch all comm by member");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
     public String fetchCreatedCommunities(String source,String sessid,String transid,String phonenumber,String commownerid)
    {
        
        Octopuscommunities octp;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String commid;
        String commname;
        String dbcommownerid;
        String commdescription;
        String commentry;
        String commtype;
        String commimage;
        String commownername;
        String commwelcomemsg;
        String commstatus;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        commownerid=utils.formatPhone(commownerid);
        List<Octopuscommunities> octplist = DataObjects.getOctopuscommunities_owner(commownerid);
        try{
            logger.info(phonenumber+"  "+sessid+":new fetch all parties by owner request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new fetch all parties by owner");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(octplist.isEmpty()){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no comm found for owner");
                logger.info(phonenumber+"  "+sessid+":no comm found for owner source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no comm found for owner");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no comm found for owner");
                for(int i=0;i<octplist.size();i++){
                    JSONObject eachobject = new JSONObject();
                    octp= octplist.get(i);
                    commid = octp.getCommid();
                    commname = octp.getCommname();
                    commownername = octp.getCommownername();
                    commdescription = octp.getCommdescription();
                    commentry = octp.getCommentry();
                    commtype = octp.getCommtype();
                    commimage=octp.getCommimage();
                    commwelcomemsg=octp.getCommwelcomemsg();
                    dbcommownerid=octp.getCommownerid();
                    commstatus=octp.getStatus();
                    if(commstatus.equals("active")){
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "success");
                        eachobject.put("commid", commid);
                        eachobject.put("commownerid", dbcommownerid);
                        eachobject.put("commname", commname);
                        eachobject.put("commownername", commownername);
                        eachobject.put("commdescription", commdescription);
                        eachobject.put("commentry", commentry);
                        eachobject.put("commtype", commtype);
                        eachobject.put("commimage", commimage);
                        eachobject.put("commwelcomemsg", commwelcomemsg);
                        arr.put(eachobject);
                    }
                }
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+":fetch all parties by owner success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("fetch all parties by owner");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
     
     public String fetchCommunityMembers(String source,String sessid,String transid,String phonenumber,String commid)
    {
        
        Octopuscommunitymembers octp;
        JSONObject tmresponse = new JSONObject();
        JSONArray arr = new JSONArray();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String output;
        String dte;
        String memberid;
        String commnotification;
        String dbcommownerid;
        String status;
        String commname;
        String commtype;
        String commentry;
        String membername;
        String commownername;
        dte=utils.getDate()+" "+utils.getTime();
        phonenumber=utils.formatPhone(phonenumber);
        List<Octopuscommunitymembers> octplist = DataObjects.getOctopuscommunities_mymembers(commid);
        try{
            logger.info(phonenumber+"  "+sessid+":new fetch my comm member request source: "+ source+" "+phonenumber+" " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new fetch all parties by owner");
            tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            if(octplist.isEmpty()){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no members found for commid");
                logger.info(phonenumber+"  "+sessid+":no members found for commid source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no members found for commid member");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                for(int i=0;i<octplist.size();i++){
                    JSONObject eachobject = new JSONObject();
                    octp= octplist.get(i);
                    commnotification = octp.getNotification();
                    status = octp.getStatus();
                    memberid=octp.getMemberid();
                    commname=octp.getCommname();
                    commtype=octp.getCommtype();
                    commentry=octp.getCommentry();
                    membername=octp.getMembername();
                    commownername=octp.getCommownername();
                    dbcommownerid=octp.getCommownerid();
                    eachobject.put("commid", commid);
                    eachobject.put("commname", commname);
                    eachobject.put("commtype", commtype);
                    eachobject.put("commentry", commentry);
                    eachobject.put("membername", membername);
                    eachobject.put("memberid", memberid);
                    eachobject.put("commownerid", dbcommownerid);
                    eachobject.put("commownername", commownername);
                    eachobject.put("commnotification", commnotification);
                    eachobject.put("status", status);
                    arr.put(eachobject);
                }
                tmresponse.put("result", arr);
                logger.info(phonenumber+"  "+sessid+":members found for commid success source: "+ source+" "+phonenumber+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("members found for commid success");
                tmlog.setActiondetails("request details: "+phonenumber+" "+sessid+" "+transid+" "+source+" ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }catch(Exception e){
            e.printStackTrace();
            tmlog = new Tmlogs();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
          }
    return output;}
    
    public static void main(String args[]){
        Extras ex = new Extras();
        //System.out.println(ex.createCommunity("USSD","12345","12345","07038901111","omotayo","the party","nice party","welcome","2000","my house","123"));
        //System.out.println(ex.fetchJoinedCommunities("USSD","12345","12345","07037617123","07037617123"));
        //System.out.println(ex.deleteCommunity("USSD","12345","12345","07037617125","C0003"));
        //System.out.println(ex.joinCommunity("USSD","12345","12345","C0003","07037617123","Wumi","07037617123","Wumi","true","Wuumzy","Estate","By invitation"));
        //System.out.println(ex.fetchJoinedParties("USSD","12345","12345","07030293145","07030293145"));
        System.out.println(ex.deleteParty("USSD","12345","12345","07038901111","E0011"));
        //System.out.println(ex.joinParty("USSD","12345","12345","E0006","07030293145","Lovely","07030293146","Lovely","2","four","Edit name","0"));
        
    }
    
}
