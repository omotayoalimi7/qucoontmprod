/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class Utilities {
    private static final String ALPHA_NUM_RAND = "ABCDEFGHIJKLMNOPQRSTUV1234567890";
    public  String getRandAlphaNumeric(int len) {
StringBuffer sb = new StringBuffer(len);

    for (int i = 0; i < len; i++) {
        int ndx = (int) (Math.random() * ALPHA_NUM_RAND.length());
        sb.append(ALPHA_NUM_RAND.charAt(ndx));
    }
    return sb.toString();
  }
     public static boolean isJSONValid(String data) {
    try {
        new JSONObject(data);
    } catch (JSONException ex) {
        // edited, to include @Arthur's comment
        // e.g. in case JSONArray is valid as well...
        try {
            new JSONArray(data);
        } catch (JSONException ex1) {
            return false;
        }
    }
    return true;
}
   
    
    public static Timestamp fixTime(){
	    long time = new Date().getTime();
	    Timestamp stamp = new Timestamp(time);
	    //String tm = stamp.toString();
	    return stamp; //tm.substring(0,tm.indexOf("."))+".0";
	   }
    public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
}
    public String getDate(){
        Date date = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("dd-MMM-yyyy");
        String currdate = dt.format(date);
    return currdate;}
    
    public String getDDDate(){
        Date date = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("MMM dd, yyyy");
        String currdate = dt.format(date);
    return currdate;}
    
    public String getTime(){
        Date date = new Date();
        SimpleDateFormat tm = new SimpleDateFormat("hh:mm:ss.S");
        String currtime = tm.format(date);
    return currtime;}
    
    public String getTTTime(){
        Date date = new Date();
        SimpleDateFormat tm = new SimpleDateFormat("hh:mm:ss:S");
        String currtime = tm.format(date);
    return currtime;}
    
    public String encryptPass(String pass){
        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        String encryptedPassword = passwordEncryptor.encryptPassword(pass);
    return encryptedPassword;}
    
    public boolean decryptPass(String inputPassword,String encryptedPassword ){
        boolean status=false;
        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();   
        if (passwordEncryptor.checkPassword(inputPassword, encryptedPassword)) {
           status=true;
           } else {
           status = false;
         }
    return status;}
    
    public String generateToken(){
        Random rand = new Random();
        int  rdm = rand.nextInt((9999 - 1000) + 1) + 1000;
    return rdm+"";}
    
    public String sendTokentoPhone(String telno,String token){
        SendSMS sendsms = new SendSMS();
        String text="Please input "+token+" as your token to complete Octopus user registration";
        String sendstatus=sendsms.sendSms(telno, text);
    return sendstatus;}
    
    public String sendunBlockToken(String telno,String token){
        SendSMS sendsms = new SendSMS();
        String text="Please input "+token+" as your token on Octopus";
        String sendstatus=sendsms.sendSms(telno, text);
    return sendstatus;}
    
    public String sendInboxNotification(String telno,String receivername,String amount,String transtype,String narration){
        SendSMS sendsms = new SendSMS();
        String text="";
        if(transtype.toLowerCase().equals("ft")){
            text="You have a transaction waiting for approval. "+receivername+" has requested the sum of N"+amount+" from you- "+narration;
        }
        String sendstatus=sendsms.sendSms(telno, text);
    return sendstatus;}
    
    public String formatPhone(String phonenumber){
        phonenumber=phonenumber.replace("+", "");
        if(phonenumber.startsWith("0")){
            phonenumber="234"+phonenumber.substring(1);
        }
    return phonenumber;}
    public String formatPhoneZero(String phonenumber){
        phonenumber=phonenumber.replace("+", "");
        if(phonenumber.startsWith("0")){
            phonenumber=phonenumber;
        }else if(phonenumber.startsWith("2")&&phonenumber.length()==13){
            phonenumber="0"+phonenumber.substring(3);
        }
    return phonenumber;}
    
    public String generateTransid(){
        Date date = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmss");
        Random rand = new Random();
        int  frdm = rand.nextInt((999999 - 100000) + 1) + 100000;
        String lrmd= fmt.format(date);
        String rdm = lrmd+frdm;
    return rdm;}
    
    public static void main(String args[]){
        Utilities utils = new Utilities();
        //System.out.println(utils.encryptPass("2174"));
        System.out.println(utils.decryptPass("1121", "xylTmSz+zgSubv7KQykz06kK5iO1S2l36S0RPU4Q1VjiZbJTLUNwVvbS0Luh6heG"));
    }
    
    
}
