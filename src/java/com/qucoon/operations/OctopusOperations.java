/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.operations;

import com.qucoon.dbo.Beneficiaries;
import com.qucoon.dbo.DataObjects;
import com.qucoon.dbo.Octopusblacklist;
import com.qucoon.dbo.Octopuscustomersaccounts;
import com.qucoon.dbo.Octopuscustomerscards;
import com.qucoon.dbo.Octopuscustomerstm;
import com.qucoon.dbo.Octopuscustomerstmpending;
import com.qucoon.dbo.Octopusdelcustomersaccounts;
import com.qucoon.dbo.Octopusdelcustomerscards;
import com.qucoon.dbo.Octopusinboxtransactions;
import com.qucoon.dbo.Octopuspasswordreset;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.Tmlogs;
import com.qucoon.dbo.iswBillers;
import com.qucoon.dbo.iswCategorys;
import com.qucoon.dbo.iswPaymentitems;
import com.qucoon.hbng.HbngInterface;
import com.qucoon.isw.QtServices;
import com.qucoon.kernelapi.Authorization;
import com.qucoon.kernelapi.BankITOperations;
import com.qucoon.kernelapi.IpgOperations;
import com.qucoon.kernelapi.kernel;
import com.qucoon.slacklogger.SlackLogMessage;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import serv.jwt.AuthHelper;

/**
 *
 * @author User001
 */
public class OctopusOperations {
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(OctopusOperations.class);
    SlackLogMessage log = new SlackLogMessage();
    String appname="QucconTM";
    String classname="OctopusOperations";
    
    public String isCustomer(String source,String transid,String sessid,String phonenumber){
        String output="";
        List<Octopuscustomerstm> ocusttm_s;
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        JSONObject tmresponse = new JSONObject();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            ocusttm_s = DataObjects.getOctopuscustomerstm(phonenumber);
            if(ocusttm_s.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
                logger.info(phonenumber+"  "+sessid+":not a customer source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("not a customer");
                tmlog.setActiondetails("not a customer details: "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
               //not a customer 
            }else{
                //is a customer
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
        }
    return output; }
    
    public String isCorrectPasscode(String source,String transid,String sessid,String phonenumber,String passcode){
        Octopuscustomerstm ocusttm ;
        String output="";
        List<Octopuscustomerstm> ocusttm_s;
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        String kernelresponse_str="";
        String dbpasscode="";
        boolean passcodestatus;
        JSONObject tmresponse = new JSONObject();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            ocusttm_s = DataObjects.getOctopuscustomerstm(phonenumber);
            if(ocusttm_s.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
                logger.info(phonenumber+"  "+sessid+":not a customer source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("not a customer");
                tmlog.setActiondetails("not a customer details: "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
               //not a customer 
            }else{
                //is a customer
                ocusttm =ocusttm_s.get(0);
                dbpasscode=ocusttm.getPasscode();
                passcodestatus=utils.decryptPass(passcode, dbpasscode);
                if(passcodestatus==false){
                    //wrong passcode
                    tmresponse.put("responsecode", "22");
                    tmresponse.put("responsemessage", "wrong passcode");
                    logger.info(phonenumber+"  "+sessid+":wrong passcode source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("wrong passcode");
                    tmlog.setActiondetails("wrong passcode "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                }
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
        }
   return output; }
    
    public String customerStatus(String source,String transid,String sessid,String phonenumber,String country)
    {
        //initialize variable
        String output="";
        Utilities utils = new Utilities();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        JSONObject requestbody = new JSONObject();
        Octopuscustomerstm ocusttm;
        List<Octopuscustomersaccounts> objList;
        List<Octopuscustomerscards> objListcards;
        Octopuscustomersaccounts ocustac;
        Octopuscustomerscards ocustca;
        String auth =Authorization.getAuth();
        phonenumber=utils.formatPhone(phonenumber);
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("country", country);
            requestbody.put("authorization", Authorization.getAuth());
            logger.info(phonenumber+"  "+sessid+": new customer status request sessid: "+ source );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new requet received");
            tmlog.setActiondetails("new customer status requet received "+requestbody.toString().replace("\"", ":"));
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            //call kernel
            logger.info(phonenumber+"  "+sessid+":calling tm db " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("call tm db");
            tmlog.setActiondetails("calling tm db "+requestbody.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestor", source);
            tmresponse.put("transid", sessid);
            tmresponse.put("sessid", transid);
            tmresponse.put("phonenumber", phonenumber);
            tmresponse.put("country", country);
            tmresponse.put("responsedate", utils.getDate());
            tmresponse.put("responsetime", utils.getTime());
            tmresponse.put("authorization", auth);
            ocusttm=DataObjects.getOctopuscustomerstm_rec(phonenumber);
            objList=DataObjects.getOctopuscustomersaccounts_rec(phonenumber);
            objListcards=DataObjects.getOctopuscustomerscards_rec(phonenumber);
            System.out.println("cust: "+ocusttm.getLast10phonenumber());
            if(ocusttm.getLast10phonenumber()==null){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "require registraion"); 
                logger.info(phonenumber+"  "+sessid+": tm responsecode: "+ "11"+": tm responsemessage: "+ "require registration");
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("build tm response");
                tmlog.setActiondetails("tm responsecode: "+"11"+" responsemessage: "+"require registration"+" "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                String dbfirstname = ocusttm.getFirstname();
                String dblastname = ocusttm.getLastname();
                String dbgender = ocusttm.getGender();
                String dbdob = ocusttm.getDob();
                String dbemailaddress = ocusttm.getEmailaddress();
                String dbbvn = ocusttm.getBvn();
                String dbrecordstat = ocusttm.getRecordstatus();
                String dblastlogindate = ocusttm.getLastlogindate();
                String dblastloginsource = ocusttm.getLastloginsource();
                tmresponse.put("firstname", dbfirstname);
                tmresponse.put("lastname", dblastname);
                tmresponse.put("gender", dbgender);
                tmresponse.put("dob", dbdob);
                tmresponse.put("emailaddress", dbemailaddress);
                tmresponse.put("bvn", dbbvn);
                tmresponse.put("recordstat", dbrecordstat);
                tmresponse.put("lastlogindate", dblastlogindate);
                tmresponse.put("lastloginsource", dblastloginsource);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+": tm responsecode: "+ "00"+"responsemessage: success");
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("build tm response");
                tmlog.setActiondetails("tm responsecode: "+"00"+" responsemessage: "+"success"+" "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                
                System.out.println("cust1: "+objList.size());
                if(objList.size()<1){
                    tmresponse.put("accountstat", "false");
                    logger.info(phonenumber+"  "+sessid+": tm response: "+ "no account found");
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("no account found");
                    tmlog.setActiondetails("tm respons "+"no account found");
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("accountstat", "true");
                    JSONArray gatheraccount= new JSONArray();
                    for(int i=0; i<objList.size();i++){
                        JSONObject eachaccount = new JSONObject();
                        ocustac =objList.get(i);
                        String dbaccountnumber = ocustac.getAccountnumber();
                        String dbbankname = ocustac.getBankname();
                        String dbbankcode = ocustac.getBankcode();
                        String dbbankitstatus = ocustac.getBankitstatus();
                        String dbbankitid = ocustac.getBankitid();
                        String dbisbankitaccount =ocustac.getIsbankitaccount();
                        String dbbvnn =ocustac.getBvn();
                        eachaccount.put("accountnumber", dbaccountnumber);
                        eachaccount.put("bankcode", dbbankcode);
                        eachaccount.put("bankname", dbbankname);
                        eachaccount.put("bankitstatus", dbbankitstatus);
                        eachaccount.put("bankitid", dbbankitid);
                        eachaccount.put("isbankitaccount", dbisbankitaccount);
                        eachaccount.put("bvn", dbbvnn);
                        gatheraccount.put(eachaccount);
                    }
                    tmresponse.put("accounts", gatheraccount);
                    logger.info(phonenumber+"  "+sessid+": tm response: "+ "account found");
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("account found");
                    tmlog.setActiondetails("tm respons "+"account found");
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
                //
                System.out.println("cust2: "+objListcards.size());
                if(objListcards.size()<1){
                    tmresponse.put("cardstat", "false");
                    logger.info(phonenumber+"  "+sessid+": tm response: "+ " no card found");
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed(" no card found");
                    tmlog.setActiondetails(" no card found");
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("cardstat", "true");
                    JSONArray gathercards= new JSONArray();
                    for(int i=0; i<objListcards.size();i++){
                        JSONObject eachcard = new JSONObject();
                        ocustca =objListcards.get(i);
                        String dbacardcid = ocustca.getCardid();
                        String dbcardname = ocustca.getCardname();
                        String dbacardbank = ocustca.getCardbank();
                        String dbcardbrand = ocustca.getCardbrand();
                        String dbacardcategory = ocustca.getCardcategory();
                        String dbcardcountry = ocustca.getCardcountry();
                        String dbcardcountrycode = ocustca.getCardcountrycode();
                        String dbcardno = ocustca.getCardno();
                        String dbcardtype = ocustca.getCardtype();
                        eachcard.put("cid", dbacardcid);
                        eachcard.put("cardname", dbcardname);
                        eachcard.put("cardbank", dbacardbank);
                        eachcard.put("cardbrand", dbcardbrand);
                        eachcard.put("cardcategory", dbacardcategory);
                        eachcard.put("cardcountry", dbcardcountry);
                        eachcard.put("cardcountrycode", dbcardcountrycode);
                        eachcard.put("cardno", dbcardno);
                        eachcard.put("cardtype", dbcardtype);
                        gathercards.put(eachcard);
                    }
                    tmresponse.put("cards", gathercards);
                    logger.info(phonenumber+"  "+sessid+": tm response: "+ "card found");
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("card found");
                    tmlog.setActiondetails("card found");
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
                
            }
            
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
         }
        
    return output;
    }
    public String customerStatus_simple(String source,String transid,String sessid,String phonenumber,String country)
    {
        //initialize variable
        String output="";
        Utilities utils = new Utilities();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        JSONObject requestbody = new JSONObject();
        Octopuscustomerstm ocusttm;
        List<Octopuscustomersaccounts> objList;
        List<Octopuscustomerscards> objListcards;
        Octopuscustomersaccounts ocustac;
        Octopuscustomerscards ocustca;
        String auth =Authorization.getAuth();
        phonenumber=utils.formatPhone(phonenumber);
        try {
            requestbody.put("requestor", source);
            requestbody.put("transactionid", transid);
            requestbody.put("sessionid", sessid);
            requestbody.put("phonenumber", phonenumber);
            requestbody.put("country", country);
            requestbody.put("authorization", Authorization.getAuth());
            logger.info(phonenumber+"  "+sessid+": new customer status request sessid: "+ source );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new requet received");
            tmlog.setActiondetails("new customer status requet received "+requestbody.toString().replace("\"", ":"));
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            //call kernel
            logger.info(phonenumber+"  "+sessid+":calling tm db " );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("call tm db");
            tmlog.setActiondetails("calling tm db "+requestbody.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            tmresponse.put("requestor", source);
            tmresponse.put("transid", sessid);
            tmresponse.put("sessid", transid);
            tmresponse.put("phonenumber", phonenumber);
            tmresponse.put("country", country);
            tmresponse.put("responsedate", utils.getDate());
            tmresponse.put("responsetime", utils.getTime());
            tmresponse.put("authorization", auth);
            ocusttm=DataObjects.getOctopuscustomerstm_rec(phonenumber);
            //objList=DataObjects.getOctopuscustomersaccounts_rec(phonenumber);
            //objListcards=DataObjects.getOctopuscustomerscards_rec(phonenumber);
            System.out.println("cust: "+ocusttm.getLast10phonenumber());
            if(ocusttm.getLast10phonenumber()==null){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "require registraion"); 
                logger.info(phonenumber+"  "+sessid+": tm responsecode: "+ "11"+": tm responsemessage: "+ "require registration");
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("build tm response");
                tmlog.setActiondetails("tm responsecode: "+"11"+" responsemessage: "+"require registration"+" "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                String dbfirstname = ocusttm.getFirstname();
                String dblastname = ocusttm.getLastname();
                String dbgender = ocusttm.getGender();
                String dbdob = ocusttm.getDob();
                String dbemailaddress = ocusttm.getEmailaddress();
                String dbbvn = ocusttm.getBvn();
                String dbrecordstat = ocusttm.getRecordstatus();
                String dblastlogindate = ocusttm.getLastlogindate();
                String dblastloginsource = ocusttm.getLastloginsource();
                tmresponse.put("firstname", dbfirstname);
                tmresponse.put("lastname", dblastname);
                tmresponse.put("gender", dbgender);
                tmresponse.put("dob", dbdob);
                tmresponse.put("emailaddress", dbemailaddress);
                tmresponse.put("bvn", dbbvn);
                tmresponse.put("recordstat", dbrecordstat);
                tmresponse.put("lastlogindate", dblastlogindate);
                tmresponse.put("lastloginsource", dblastloginsource);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+": tm responsecode: "+ "00"+"responsemessage: success");
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("build tm response");
                tmlog.setActiondetails("tm responsecode: "+"00"+" responsemessage: "+"success"+" "+tmresponse.toString().replace("\"", ":"));
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                
                //System.out.println("cust1: "+objList.size());
                
                //
                //System.out.println("cust2: "+objListcards.size());
                
                
            }
            
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
         }
        
    return output;
    }
    
//    public String forgotPassword(String source,String transid,String sessid,String phonenumber)
//    {
//        //initialize variable
//        String output="";
//        String tmresponsecode="";
//        String tmresponsemessage="";
//        String bankaddedstat = "";
//        String cardaddedstat = "";
//        String customeraddedstat = "";
//        String output_f="";
//        String token="";
//        String encrypttoken="";
//        String sendsms="";
//        String encrytpass="";
//        String dttme="";
//        String auth=Authorization.getAuth();
//        Utilities utils = new Utilities();
//        JSONObject tmresponse = new JSONObject();
//        Tmlogs tmlog = new Tmlogs();
//        Octopuscustomerstmpending ocusttmpnd = new Octopuscustomerstmpending();
//        List<Octopuscustomerstm>ocusttm_email ;
//        List<Octopuscustomerstm> ocusttm_s ;
//        phonenumber=utils.formatPhone(phonenumber);
//        try{
//            logger.info(phonenumber+"  "+sessid+": new register octopus request sessid: "+ sessid );
//            logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
//            tmlog.setSource(source);
//            tmlog.setPhonenumber(phonenumber);
//            tmlog.setSessionid(sessid);
//            tmlog.setTransactionid(transid);
//            tmlog.setActionperformed("new register requet received");
//            tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
//            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
//            PersistenceAgent.logTransactions(tmlog);
//            tmlog = new Tmlogs();
//            ocusttm_s=DataObjects.getOctopuscustomerstm(phonenumber);
//            
//            log.info(appname, classname, "New Register Request at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponse.toString());
//            ocusttm_email = DataObjects.getOctopuscustomerstm_email(emailaddress);
//            if(ocusttm_s.size()>0){
//                tmresponsecode="22";
//                tmresponsemessage="phone number already registered";
//                customeraddedstat="phone number already registered";
//                tmresponse.put("responsecode", tmresponsecode);
//                tmresponse.put("responsemessage", tmresponsemessage);
//                log.info(appname, classname, "Register Request. Phone number already registered at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponse.toString());
//                logger.info(phonenumber+"  "+sessid+":phone number already registered: " );
//                tmlog.setSource(source);
//                tmlog.setPhonenumber(phonenumber);
//                tmlog.setSessionid(sessid);
//                tmlog.setTransactionid(transid);
//                tmlog.setActionperformed("phone number already registered");
//                tmlog.setActiondetails("request details: phone number already registered ");
//                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
//                PersistenceAgent.logTransactions(tmlog);
//                tmlog = new Tmlogs();
//            }else if(ocusttm_email.size()>0){
//                tmresponsecode="33";
//                tmresponsemessage="email address already registered";
//                customeraddedstat="email address already registered";
//                logger.info(phonenumber+"  "+sessid+":email address already registered: "+ tmresponsemessage );
//                log.info(appname, classname, "Register Request. Email address already registered at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
//                tmlog.setSource(source);
//                tmlog.setPhonenumber(phonenumber);
//                tmlog.setSessionid(sessid);
//                tmlog.setTransactionid(transid);
//                tmlog.setActionperformed("email address already registered");
//                tmlog.setActiondetails("request details: email address already registered "+tmresponsemessage);
//                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
//                PersistenceAgent.logTransactions(tmlog);
//                tmlog = new Tmlogs(); 
//            }else{
//                ocusttmpnd=DataObjects.getOctopuscustomerstmpending_rec(phonenumber);
//                String pndphone;
//                pndphone = (ocusttmpnd.getphonenumber()!= null)?ocusttmpnd.getphonenumber().trim() :"";
//                if(!pndphone.equals("")){
//                    token =utils.generateToken();
//                    sendsms=utils.sendTokentoPhone(phonenumber, token);
//                    if(sendsms.equals("false")){
//                        tmresponsecode="11"; 
//                        tmresponsemessage="cannot send token";
//                        customeraddedstat="cannot send token";
//                        tmresponse.put("responsecode", tmresponsecode);
//                        tmresponse.put("responsemessage", tmresponsemessage);
//                        log.info(appname, classname, "Register Request. Cannot send token at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
//                    }else{
//                        encrypttoken = utils.encryptPass(token);
//                        encrytpass=utils.encryptPass(passcode);
//                        ocusttmpnd.setphonenumber(phonenumber);
//                        ocusttmpnd.setLast10phonenumber(phonenumber.substring(3));
//                        ocusttmpnd.setFirstname(firstname);
//                        ocusttmpnd.setLastname(lastname);
//                        ocusttmpnd.setEmailaddress(emailaddress);
//                        ocusttmpnd.setGender(gender);
//                        ocusttmpnd.setDob(dob);
//                        ocusttmpnd.setPasscode(encrytpass);
//                        ocusttmpnd.setCountry("NG");
//                        ocusttmpnd.setRecordstatus("pending");
//                        ocusttmpnd.setToken(encrypttoken);
//                        dttme = utils.getDate()+" "+utils.getTime();
//                        ocusttmpnd.setCreatedat(dttme);
//                        ocusttmpnd.setUpdatedat(dttme);
//                        ocusttmpnd.setBankcode(bankcode);
//                        ocusttmpnd.setAccountnumber(acno);
//                        ocusttmpnd.setBankname(bank);
//                        ocusttmpnd.setFirstname(firstname);
//                        ocusttmpnd.setPasscode(encrytpass);
//                        PersistenceAgent.logTransactions(ocusttmpnd);
//                        logger.info(phonenumber+"  "+sessid+":phone number on pending list new token sent: " );
//                        tmlog.setSource(source);
//                        tmlog.setPhonenumber(phonenumber);
//                        tmlog.setSessionid(sessid);
//                        tmlog.setTransactionid(transid);
//                        tmlog.setActionperformed("phone number on pending list new token sent");
//                        tmlog.setActiondetails("request details: phone number on pending list new token sent ");
//                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
//                        PersistenceAgent.logTransactions(tmlog);
//                        tmlog = new Tmlogs();
//                        tmresponsecode="00";
//                        tmresponsecode="00";
//                        customeraddedstat="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
//                        tmresponsemessage="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
//                        tmresponse.put("responsecode", tmresponsecode);
//                        tmresponse.put("responsemessage", tmresponsemessage);
//                        log.info(appname, classname, "Register Request. Customer exists but unverified. New token sent at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
//                    }
//                }else{
//                    ocusttmpnd = new Octopuscustomerstmpending();
//                    token =utils.generateToken();
//                    sendsms=utils.sendTokentoPhone(phonenumber, token);
//                    if(sendsms.equals("false")){
//                        tmresponsecode="11"; 
//                        tmresponsemessage="cannot send token";
//                        tmresponse.put("responsecode", tmresponsecode);
//                        tmresponse.put("responsemessage", tmresponsemessage);
//                        log.info(appname, classname, "Register Request. Cannot send token at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
//                    }else{
//                        encrypttoken = utils.encryptPass(token);
//                        encrytpass=utils.encryptPass(passcode);
//                        ocusttmpnd.setphonenumber(phonenumber);
//                        ocusttmpnd.setLast10phonenumber(phonenumber.substring(3));
//                        ocusttmpnd.setFirstname(firstname);
//                        ocusttmpnd.setLastname(lastname);
//                        ocusttmpnd.setEmailaddress(emailaddress);
//                        ocusttmpnd.setGender(gender);
//                        ocusttmpnd.setDob(dob);
//                        ocusttmpnd.setPasscode(encrytpass);
//                        ocusttmpnd.setCountry("NG");
//                        ocusttmpnd.setRecordstatus("pending");
//                        ocusttmpnd.setToken(encrypttoken);
//                        dttme = utils.getDate()+" "+utils.getTime();
//                        ocusttmpnd.setCreatedat(dttme);
//                        ocusttmpnd.setUpdatedat(dttme);
//                        ocusttmpnd.setBankcode(bankcode);
//                        ocusttmpnd.setAccountnumber(acno);
//                        ocusttmpnd.setBankname(bank);
//                        ocusttmpnd.setFirstname(firstname);
//                        ocusttmpnd.setPasscode(encrytpass);
//                        PersistenceAgent.logTransactions(ocusttmpnd);
//                        logger.info(phonenumber+"  "+sessid+":phone number added on pending list token sent: " );
//                        tmlog.setSource(source);
//                        tmlog.setPhonenumber(phonenumber);
//                        tmlog.setSessionid(sessid);
//                        tmlog.setTransactionid(transid);
//                        tmlog.setActionperformed("phone number added on pending list token sent");
//                        tmlog.setActiondetails("request details: phone number added on pending list token sent:  ");
//                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
//                        PersistenceAgent.logTransactions(tmlog);
//                        tmlog = new Tmlogs();
//                        tmresponsecode="00";
//                        customeraddedstat="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
//                        tmresponsemessage="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
//                        tmresponse.put("responsecode", tmresponsecode);
//                        tmresponse.put("responsemessage", tmresponsemessage);
//                        log.info(appname, classname, "Register Request. Customer created and unverified. Token sent at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
//                    }
//                }
//            }  
//            tmresponse.put("responsecode", tmresponsecode);
//            tmresponse.put("responsemessage", customeraddedstat+". "+bankaddedstat+". "+cardaddedstat);
//            output_f = tmresponse.toString();
//            output = output_f;
//            
//        }catch(Exception e){
//            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
//            logger.info("error details: "+ e.getMessage());
//            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
//            log.error(appname, classname, "Register Request -error parsing json at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+output+" "+e.getMessage());
//            tmlog = new Tmlogs();
//            tmlog.setSource(source);
//            tmlog.setPhonenumber(phonenumber);
//            tmlog.setSessionid(sessid);
//            tmlog.setTransactionid(transid);
//            tmlog.setActionperformed("build tm response");
//            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
//            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
//            PersistenceAgent.logTransactions(tmlog);
//        }
//    return output;}
    
    
    public String registerOctopus(String source,String transid,String sessid,String phonenumber,String firstname,String lastname,String gender,String dob,String emailaddress,String country,String passcode,String bvn,String accountstat,String acno,String bank,String bankcode)
    {
        //initialize variable
        String output="";
        String tmresponsecode="";
        String tmresponsemessage="";
        String bankaddedstat = "";
        String cardaddedstat = "";
        String customeraddedstat = "";
        String output_f="";
        String token="";
        String encrypttoken="";
        String sendsms="";
        String encrytpass="";
        String dttme="";
        String auth=Authorization.getAuth();
        Utilities utils = new Utilities();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        Octopuscustomerstmpending ocusttmpnd = new Octopuscustomerstmpending();
        List<Octopuscustomerstm>ocusttm_email ;
        List<Octopuscustomerstm> ocusttm_s ;
        phonenumber=utils.formatPhone(phonenumber);
        try{
            logger.info(phonenumber+"  "+sessid+": new register octopus request sessid: "+ sessid );
            logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new register requet received");
            tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            ocusttm_s=DataObjects.getOctopuscustomerstm(phonenumber);
            tmresponse.put("source", source);
            tmresponse.put("transid", sessid);
            tmresponse.put("sessid", transid);
            tmresponse.put("responsedate", utils.getDate());
            tmresponse.put("responsetime", utils.getTime());
            tmresponse.put("authorization",auth);
            tmresponse.put("phonenumber", phonenumber);
            tmresponse.put("country", country);
            tmresponse.put("gender", gender);
            tmresponse.put("description", "REGISTRATION");
            tmresponse.put("emailaddress", emailaddress);
            tmresponse.put("lastname", lastname);
            tmresponse.put("accountstat", accountstat);
            tmresponse.put("bvn", bvn);
            tmresponse.put("bank", bank);
            tmresponse.put("dob", dob);
            tmresponse.put("bankcode", bankcode);
            log.info(appname, classname, "New Register Request at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponse.toString());
            ocusttm_email = DataObjects.getOctopuscustomerstm_email(emailaddress);
            if(ocusttm_s.size()>0){
                tmresponsecode="22";
                tmresponsemessage="phone number already registered";
                customeraddedstat="phone number already registered";
                tmresponse.put("responsecode", tmresponsecode);
                tmresponse.put("responsemessage", tmresponsemessage);
                log.info(appname, classname, "Register Request. Phone number already registered at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponse.toString());
                logger.info(phonenumber+"  "+sessid+":phone number already registered: " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("phone number already registered");
                tmlog.setActiondetails("request details: phone number already registered ");
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else if(ocusttm_email.size()>0){
                tmresponsecode="33";
                tmresponsemessage="Email address already registered";
                customeraddedstat="Email address already registered";
                logger.info(phonenumber+"  "+sessid+":email address already registered: "+ tmresponsemessage );
                log.info(appname, classname, "Register Request. Email address already registered at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("email address already registered");
                tmlog.setActiondetails("request details: email address already registered "+tmresponsemessage);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs(); 
            }else{
                ocusttmpnd=DataObjects.getOctopuscustomerstmpending_rec(phonenumber);
                String pndphone;
                pndphone = (ocusttmpnd.getphonenumber()!= null)?ocusttmpnd.getphonenumber().trim() :"";
                if(!pndphone.equals("")){
                    token =utils.generateToken();
                    sendsms=utils.sendTokentoPhone(phonenumber, token);
                    if(sendsms.equals("false")){
                        tmresponsecode="11"; 
                        tmresponsemessage="cannot send token";
                        customeraddedstat="cannot send token";
                        tmresponse.put("responsecode", tmresponsecode);
                        tmresponse.put("responsemessage", tmresponsemessage);
                        log.info(appname, classname, "Register Request. Cannot send token at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                    }else{
                        encrypttoken = utils.encryptPass(token);
                        encrytpass=utils.encryptPass(passcode);
                        ocusttmpnd.setphonenumber(phonenumber);
                        ocusttmpnd.setLast10phonenumber(phonenumber.substring(3));
                        ocusttmpnd.setFirstname(firstname);
                        ocusttmpnd.setLastname(lastname);
                        ocusttmpnd.setEmailaddress(emailaddress);
                        ocusttmpnd.setGender(gender);
                        ocusttmpnd.setDob(dob);
                        ocusttmpnd.setPasscode(encrytpass);
                        ocusttmpnd.setCountry("NG");
                        ocusttmpnd.setRecordstatus("pending");
                        ocusttmpnd.setToken(encrypttoken);
                        dttme = utils.getDate()+" "+utils.getTime();
                        ocusttmpnd.setCreatedat(dttme);
                        ocusttmpnd.setUpdatedat(dttme);
                        ocusttmpnd.setBankcode(bankcode);
                        ocusttmpnd.setAccountnumber(acno);
                        ocusttmpnd.setBankname(bank);
                        ocusttmpnd.setFirstname(firstname);
                        ocusttmpnd.setPasscode(encrytpass);
                        PersistenceAgent.logTransactions(ocusttmpnd);
                        logger.info(phonenumber+"  "+sessid+":phone number on pending list new token sent: " );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("phone number on pending list new token sent");
                        tmlog.setActiondetails("request details: phone number on pending list new token sent ");
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        tmresponsecode="00";
                        tmresponsecode="00";
                        customeraddedstat="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
                        tmresponsemessage="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
                        tmresponse.put("responsecode", tmresponsecode);
                        tmresponse.put("responsemessage", tmresponsemessage);
                        log.info(appname, classname, "Register Request. Customer exists but unverified. New token sent at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                    }
                }else{
                    ocusttmpnd = new Octopuscustomerstmpending();
                    token =utils.generateToken();
                    sendsms=utils.sendTokentoPhone(phonenumber, token);
                    if(sendsms.equals("false")){
                        tmresponsecode="11"; 
                        tmresponsemessage="cannot send token";
                        tmresponse.put("responsecode", tmresponsecode);
                        tmresponse.put("responsemessage", tmresponsemessage);
                        log.info(appname, classname, "Register Request. Cannot send token at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                    }else{
                        encrypttoken = utils.encryptPass(token);
                        encrytpass=utils.encryptPass(passcode);
                        ocusttmpnd.setphonenumber(phonenumber);
                        ocusttmpnd.setLast10phonenumber(phonenumber.substring(3));
                        ocusttmpnd.setFirstname(firstname);
                        ocusttmpnd.setLastname(lastname);
                        ocusttmpnd.setEmailaddress(emailaddress);
                        ocusttmpnd.setGender(gender);
                        ocusttmpnd.setDob(dob);
                        ocusttmpnd.setPasscode(encrytpass);
                        ocusttmpnd.setCountry("NG");
                        ocusttmpnd.setRecordstatus("pending");
                        ocusttmpnd.setToken(encrypttoken);
                        dttme = utils.getDate()+" "+utils.getTime();
                        ocusttmpnd.setCreatedat(dttme);
                        ocusttmpnd.setUpdatedat(dttme);
                        ocusttmpnd.setBankcode(bankcode);
                        ocusttmpnd.setAccountnumber(acno);
                        ocusttmpnd.setBankname(bank);
                        ocusttmpnd.setFirstname(firstname);
                        ocusttmpnd.setPasscode(encrytpass);
                        PersistenceAgent.logTransactions(ocusttmpnd);
                        logger.info(phonenumber+"  "+sessid+":phone number added on pending list token sent: " );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("phone number added on pending list token sent");
                        tmlog.setActiondetails("request details: phone number added on pending list token sent:  ");
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        tmresponsecode="00";
                        customeraddedstat="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
                        tmresponsemessage="Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete registration";
                        tmresponse.put("responsecode", tmresponsecode);
                        tmresponse.put("responsemessage", tmresponsemessage);
                        log.info(appname, classname, "Register Request. Customer created and unverified. Token sent at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                    }
                }
            }  
            tmresponse.put("responsecode", tmresponsecode);
            tmresponse.put("responsemessage", customeraddedstat+". "+bankaddedstat+". "+cardaddedstat);
            output_f = tmresponse.toString();
            output = output_f;
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            log.error(appname, classname, "Register Request -error parsing json at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+output+" "+e.getMessage());
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String unBlockCustomer(String source,String transid,String sessid,String phonenumber,String token,String newpassword)
    {
        //initialize variable
        String recordstatus;
        boolean status=false;
        String output="";
        String tmresponsecode="";
        String tmresponsemessage="";
        String resetstatus="";
        String resettoken="";
        Utilities utils = new Utilities();
        kernel kn = new kernel();
        JSONObject kernelresponse = new JSONObject();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        JSONObject requestbody = new JSONObject();
        Octopuscustomerstm ocusttm = new Octopuscustomerstm();
        List<Octopuspasswordreset>occustresetlist ;
        Octopuspasswordreset occustreset;
        Octopuspasswordreset occustresetactual = new Octopuspasswordreset();
        String reqreset="false";
        Mailer mail = new Mailer();
        String dbtoken="";
        String firstname="";
        String lastname="";
        String emailaddress="";
        String loginresponse_str="";
        String encryptedpass="";
        JSONObject loginresponse;
        String loginrescode="";
        OctopusOperations op = new OctopusOperations();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
            }else{
                logger.info(phonenumber+"  "+sessid+": new unblock customer request sessid: "+ sessid );
                logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new unblock customer received");
                tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                occustresetlist=DataObjects.getOctopuspasswordreset(phonenumber);
                ocusttm = DataObjects.getOctopuscustomerstm_rec(phonenumber);
                if(!occustresetlist.isEmpty()){
                    for(int i=0; i<occustresetlist.size();i++){
                        occustreset=occustresetlist.get(i);
                        resetstatus=occustreset.getStatus();
                        if(resetstatus.equalsIgnoreCase("active")){
                            occustresetactual=occustreset;
                            reqreset="true";
                        }
                    }
                }
                if(reqreset.equals("false")){
                    logger.info(phonenumber+"  "+sessid+": Customer not blocked sessid: "+ sessid );
                    logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
                    tmresponse.put("responsecode", "44");
                    tmresponse.put("responsemessage", "Account not blocked");
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("Customer not blocked");
                    tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    dbtoken=occustresetactual.getToken();
                    status =utils.decryptPass(token, dbtoken);
                    if (status==false){
                        //wrong token
                        tmresponse.put("responsecode", "22");
                        tmresponse.put("responsemessage", "Wrong token");
                        logger.info(phonenumber+"  "+sessid+":wrong token: "+ tmresponsemessage );
                        log.info(appname, classname, "Unblock customer Request. Wrong Token at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("wrong token");
                        tmlog.setActiondetails("request details:wrong token "+tmresponsemessage);
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }else{
                        occustresetactual.setStatus("inactive");
                        occustresetactual.setUpdatedat(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(occustresetactual);
                        firstname=ocusttm.getFirstname();
                        lastname=ocusttm.getLastname();
                        emailaddress=ocusttm.getEmailaddress();
                        encryptedpass=utils.encryptPass(newpassword);
                        ocusttm.setRecordstatus("active");
                        ocusttm.setPasscode(encryptedpass);
                        ocusttm.setLogincount(0);
                        ocusttm.setUpdatedat(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(ocusttm);
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "Customer reset successful");
                        logger.info(phonenumber+"  "+sessid+": Customer unblocked successfully sessid: "+ sessid );
                        logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("Customer unblocked successfully ");
                        tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        mail.loginNotification(source,emailaddress, firstname+" "+lastname, utils.getDDDate()+" "+utils.getTTTime(), "true");
                    }
                }
            }
            output = tmresponse.toString();
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            log.error(appname, classname, "Unblock Customer Request -error parsing json at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+output+" "+e.getMessage());
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    
    public String validateRegisterOctopus(String source,String transid,String sessid,String phonenumber,String token)
    {
        //initialize variable
        String last10phonenumber;
        String country;
        String firstname;
        String lastname;
        String gender;
        String dob;
        String emailaddress;
        String uniqueid;
        String passcode;
        String accountnumber;
        String bankname;
        String bankcode;
        String bankitid;
        String bankitstatus;
        String bvn;
        String dbtoken;
        String recordstatus;
        boolean status=false;
        String output="";
        String kernelresponse_str="";
        String kernelresponsecode="";
        String kernelresponsemessage="";
        String tmphonenumber="";
        String tmresponsecode="";
        String tmresponsemessage="";
        String tmaccountnumber = "";
        String tmlastname = "";
        String tmbankitid = "";
        String bankaddedstat = "";
        String cardaddedstat = "";
        String customeraddedstat = "";
        String output_f="";
        String cid="";
        String accountstat="";
        String encrypttoken="";
        String sendsms="";
        String auth=Authorization.getAuth();
        Utilities utils = new Utilities();
        kernel kn = new kernel();
        JSONObject kernelresponse = new JSONObject();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        JSONObject requestbody = new JSONObject();
        Octopuscustomerstm ocusttm = new Octopuscustomerstm();
        List<Octopuscustomerstm>ocusttm_email ;
        List<Octopuscustomerstm> ocusttm_s ;
        Octopuscustomersaccounts ocustac = new Octopuscustomersaccounts();
        List<Octopuscustomersaccounts> ocustac_s ;
        String pndphone="";
        phonenumber=utils.formatPhone(phonenumber);
        Octopuscustomerstmpending ocusttmpnd = DataObjects.getOctopuscustomerstmpending_rec(phonenumber);
        try{
            logger.info(phonenumber+"  "+sessid+": new register octopus validate request sessid: "+ sessid );
            logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new register validate received");
            tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            pndphone = (ocusttmpnd.getphonenumber() != null)?ocusttmpnd.getphonenumber().trim() :"";
            log.info(appname, classname, "New Register Validate Request at "+utils.getDate()+" "+utils.getTime(), phonenumber+" ");
            if (pndphone.equals("")){
                customeraddedstat="wrong request";
                tmresponsecode="66";
                tmresponsemessage="wrong request";
                logger.info(phonenumber+"  "+sessid+":wrong request: "+ tmresponsemessage );
                log.info(appname, classname, "Register Validate Request at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("wrong request");
                tmlog.setActiondetails("request details:wrong request "+tmresponsemessage);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
            }else{
                last10phonenumber=(ocusttmpnd.getLast10phonenumber()!=null)?ocusttmpnd.getLast10phonenumber().trim():"";
                country=(ocusttmpnd.getCountry()!=null)?ocusttmpnd.getCountry().trim():"";
                firstname=(ocusttmpnd.getFirstname()!=null)?ocusttmpnd.getFirstname().trim():"";
                lastname=(ocusttmpnd.getLastname()!=null)?ocusttmpnd.getLastname().trim():"";
                gender=(ocusttmpnd.getGender()!=null)?ocusttmpnd.getGender().trim():"";
                dob=(ocusttmpnd.getDob()!=null)?ocusttmpnd.getDob().trim():"";
                emailaddress=(ocusttmpnd.getEmailaddress()!=null)?ocusttmpnd.getEmailaddress().trim():"";
                uniqueid=(ocusttmpnd.getUniqueid()!=null)?ocusttmpnd.getUniqueid().trim():"";
                passcode=(ocusttmpnd.getPasscode()!=null)?ocusttmpnd.getPasscode().trim():"";
                accountnumber=(ocusttmpnd.getAccountnumber()!=null)?ocusttmpnd.getAccountnumber().trim():"";
                if(!accountnumber.equals("")){
                    accountstat="false";
                }else{
                    accountstat="false";
                }
                bankname=(ocusttmpnd.getBankname()!=null)?ocusttmpnd.getBankname().trim():"";
                bankcode=(ocusttmpnd.getBankcode()!=null)?ocusttmpnd.getBankcode().trim():"";
                bvn=(ocusttmpnd.getBvn()!=null)?ocusttmpnd.getBvn().trim():"";
                dbtoken=(ocusttmpnd.getToken()!=null)?ocusttmpnd.getToken().trim():"";
                recordstatus=(ocusttmpnd.getRecordstatus()!=null)?ocusttmpnd.getRecordstatus().trim():"";
                if(recordstatus.equals("active")){
                    logger.info(phonenumber+"  "+sessid+": no unvalidated registration request sessid: "+ sessid );
                    logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("no unvalidated registration");
                    tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    customeraddedstat="wrong request";
                    tmresponsecode="66";
                    tmresponsemessage="wrong request";
                    logger.info(phonenumber+"  "+sessid+":wrong request: "+ tmresponsemessage );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("wrong request");
                    tmlog.setActiondetails("request details:wrong request "+tmresponsemessage);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    log.info(appname, classname, "Register Validate Request. No unvalidated registration at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                }else{
                    status =utils.decryptPass(token, dbtoken);
                    if (status==false){
                        //wrong token
                        customeraddedstat="wrong token";
                        tmresponsecode="22";
                        tmresponsemessage="wrong token";
                        logger.info(phonenumber+"  "+sessid+":wrong token: "+ tmresponsemessage );
                        log.info(appname, classname, "Register Validate Request. Wrong Token at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("wrong token");
                        tmlog.setActiondetails("request details:wrong token "+tmresponsemessage);
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }else{
                        ocusttmpnd.setRecordstatus("active");
                        ocusttmpnd.setUpdatedat(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(ocusttmpnd);
                        requestbody.put("requestor", source);
                        requestbody.put("transactionid", transid);
                        requestbody.put("sessionid", sessid);
                        requestbody.put("phonenumber", phonenumber);
                        requestbody.put("firstname", firstname);
                        requestbody.put("lastname", lastname);
                        requestbody.put("gender", gender);
                        requestbody.put("dob", dob);
                        requestbody.put("emailaddress", emailaddress);
                        requestbody.put("country", country);
                        requestbody.put("passcode", passcode);
                        requestbody.put("bvn", bvn);
                        requestbody.put("accountnumber", accountnumber);
                        requestbody.put("bank", bankname);
                        requestbody.put("authorization", auth);
                        ocusttm_email = DataObjects.getOctopuscustomerstm_email(emailaddress);
                        ocusttm_s=DataObjects.getOctopuscustomerstm(phonenumber);
                        //System.out.println("size: "+ocusttm_s.size());
                        tmresponse.put("requestor", source);
                        tmresponse.put("transid", sessid);
                        tmresponse.put("sessid", transid);
                        tmresponse.put("responsedate", utils.getDate());
                        tmresponse.put("responsetime", utils.getTime());
                        tmresponse.put("authorization",auth);
                        tmresponse.put("phonenumber", phonenumber);
                        tmresponse.put("country", country);
                        tmresponse.put("gender", gender);
                        tmresponse.put("description", "REGISTRATION");
                        tmresponse.put("emailaddress", emailaddress);
                        tmresponse.put("lastname", lastname);
                        tmresponse.put("accountstat", accountstat);
                        tmresponse.put("bvn", bvn);
                        tmresponse.put("bank", bankname);
                        tmresponse.put("dob", dob);
                        tmresponse.put("bankcode", bankcode);
                        if(ocusttm_s.size()>0){
                            tmresponsecode="33";
                            tmresponsemessage="phone number already registered";
                            customeraddedstat="phone number already registered";
                            logger.info(phonenumber+"  "+sessid+":phone number already registered: "+ tmresponsemessage );
                            log.info(appname, classname, "Register Validate Request. Phone number already registered at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("phone number already registered");
                            tmlog.setActiondetails("request details: phone number already registered "+tmresponsemessage);
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                        }else if(ocusttm_email.size()>0){
                            tmresponsecode="55";
                            tmresponsemessage="email address already registered";
                            customeraddedstat="email address already registered";
                            log.info(appname, classname, "Register Validate Request. Email already registered at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                            logger.info(phonenumber+"  "+sessid+":email address already registered: "+ tmresponsemessage );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("email address already registered");
                            tmlog.setActiondetails("request details: email address already registered "+tmresponsemessage);
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs(); 
                        }else{
                            ocusttm.setphonenumber(phonenumber);
                            ocusttm.setLast10phonenumber(phonenumber.substring(3));
                            ocusttm.setFirstname(firstname);
                            ocusttm.setLastname(lastname);
                            ocusttm.setEmailaddress(emailaddress);
                            ocusttm.setEmailvalidated("unvalidated");
                            ocusttm.setGender(gender);
                            ocusttm.setDob(dob);
                            ocusttm.setPasscode(passcode);
                            ocusttm.setCountry("NG");
                            ocusttm.setRecordstatus("active");
                            ocusttm.setLastloginsource(source);
                            String dttme = utils.getDate()+" "+utils.getTime();
                            ocusttm.setLastlogindate(dttme);
                            ocusttm.setCreatedat(dttme);
                            ocusttm.setUpdatedat(dttme);
                            PersistenceAgent.logTransactions(ocusttm);
                            tmresponsecode="00";
                            tmresponsemessage="Success: Octopus account created and verified";
                            customeraddedstat="Success: Octopus account created and verified";
                            logger.info(phonenumber+"  "+sessid+": success octopus customer created and verified"+ sessid );
                            logger.info(phonenumber+"  "+sessid+":success octopus customer created and verified: "+ source+" "+phonenumber );
                            log.info(appname, classname, "Register Validate Request Success. Customer created and active "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponsemessage+" "+tmresponsecode);
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("success octopus customer created ");
                            tmlog.setActiondetails("success octopus customer created");
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            if(accountstat.toLowerCase().equals("true")){
                                ocustac_s=DataObjects.getOctopuscustomersaccounts(accountnumber);
                                System.out.println("existing acct:" +ocustac_s.size());
                                if(ocustac_s.size()>0){
                                    tmresponsecode="44";
                                    tmresponsemessage="octopus customer created bank exists and cannot be added";
                                    bankaddedstat="The bank account details you supplied exists and cannot be added to you octopus account";
                                    logger.info(phonenumber+"  "+sessid+": success octopus customer created bank exists and cannot be added: "+ sessid );
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("new register requet received");
                                    tmlog.setActiondetails("request details: success octopus customer created bank exists and cannot be added "+phonenumber+" "+tmresponsemessage);
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                }else{            
                                    logger.info(phonenumber+"  "+sessid+":calling kernel to register bank " );
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("calling kernel to register bank");
                                    tmlog.setActiondetails("calling kernel ");
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    kernelresponse_str=kn.registeroctopus(requestbody.toString());
                                    requestbody.put("sessionid", sessid+"a");
                                    kernelresponse_str=kn.registeroctopus(requestbody.toString());
                                    System.out.println("kernel response \n"+kernelresponse_str+"\n");
                                    kernelresponse= new JSONObject(kernelresponse_str);   
                                    kernelresponsecode =kernelresponse.getString("responsecode");
                                    kernelresponsemessage =kernelresponse.getString("responsemessage");
                                    logger.info(phonenumber+"  "+sessid+":kernel responsecode: "+ kernelresponsecode);
                                    logger.info(phonenumber+"  "+sessid+":kernel responsemessage: "+ kernelresponsemessage);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("kernel response ");
                                    tmlog.setActiondetails("kernel responsecode: "+kernelresponsecode+" responsemessage: "+kernelresponsemessage+" "+kernelresponse.toString().replace("\"", ":"));
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    //build tm response
                                    if(kernelresponsecode.equals("00")){
                                        tmphonenumber = kernelresponse.getString("phonenumber");
                                        tmaccountnumber = kernelresponse.getString("accountnumber");;
                                        tmlastname = kernelresponse.getString("lastname");
                                        if(kernelresponse.has("bankitid")){
                                        tmbankitid = kernelresponse.getString("bankitid");
                                        }
                //                        if(kernelresponse.has("accounts")){
                //                        tmaccounts = kernelresponse.getJSONArray("accounts");
                //                        }
                                        ocustac.setBankcode(bankcode);
                                        ocustac.setAccountnumber(tmaccountnumber);
                                        ocustac.setBankitid(tmbankitid);
                                        ocustac.setBankitstatus("true");
                                        ocustac.setBankname(bankname);
                                        ocustac.setFirstname(firstname);
                                        ocustac.setLastname(tmlastname);
                                        ocustac.setPhonenumber(tmphonenumber);
                                        ocustac.setRecordstatus("active");
                                        ocustac.setPasscode(passcode);
                                        ocustac.setCreatedat(utils.getDate()+" "+utils.getTime());
                                        ocustac.setUpdatedat(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(ocustac);
                                        tmresponsecode="00";
                                        tmresponsemessage="octopus account created, bank added";
                                        bankaddedstat="The bank account details you supplied has been added to your account";
                                        logger.info(phonenumber+"  "+sessid+":register user and account success "+ source+" " +tmresponsemessage );
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("register user and account success ");
                                        tmlog.setActiondetails("register user and account success");
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs(); 
                                    }else if(kernelresponsecode.equals("ETZ_100")){
                                        tmresponsecode="11";
                                        tmresponsemessage = "octopus customer created bank not added";
                                        bankaddedstat="The bank account details you supplied cannot be added";
                                        logger.info(phonenumber+"  "+sessid+" "+source+": tm responsecode: "+ tmresponsemessage);
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("build tm response");
                                        tmlog.setActiondetails("kn responsecode: "+kernelresponsecode+" kn responsemessage: "+kernelresponsemessage+" "+tmresponse.toString().replace("\"", ":"));
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs();

                                    }else if(kernelresponsecode.equals("QO2")){
                                        tmresponsecode="11";
                                        tmresponsemessage = "octopus customer created bank not added";
                                        bankaddedstat="The bank account details you supplied cannot be added";
                                        logger.info(phonenumber+"  "+sessid+" "+source+": tm responsecode: "+ tmresponsemessage);
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("build tm response");
                                        tmlog.setActiondetails("kn responsecode: "+kernelresponsecode+"kn responsemessage: "+kernelresponsemessage+" "+tmresponse.toString().replace("\"", ":"));
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs();
                                    }else{
                                        tmresponsecode="11";
                                        tmresponsemessage = "octopus customer created bank not added";
                                        bankaddedstat="The bank account details you supplied cannot be added";
                                        logger.info(phonenumber+"  "+sessid+" "+source+": tm responsecode: "+ tmresponsemessage);
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("build tm response");
                                        tmlog.setActiondetails("kn responsecode: "+kernelresponsecode+"kn responsemessage: "+kernelresponsemessage+" "+tmresponse.toString().replace("\"", ":"));
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs();
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
            tmresponse.put("responsecode", tmresponsecode);
            tmresponse.put("responsemessage", customeraddedstat+". "+bankaddedstat+".");
            output_f = tmresponse.toString();
            output = output_f;
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            log.error(appname, classname, "Register Validate Request -error parsing json at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+output+" "+e.getMessage());
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String validateRegisterOctopus_old2(String source,String transid,String sessid,String phonenumber,String token)
    {
        //initialize variable
        String last10phonenumber;
        String country;
        String firstname;
        String lastname;
        String gender;
        String dob;
        String emailaddress;
        String uniqueid;
        String passcode;
        String accountnumber;
        String bankname;
        String bankcode;
        String bankitid;
        String bankitstatus;
        String bvn;
        String dbtoken;
        String recordstatus;
        boolean status=false;
        String output="";
        String kernelresponse_str="";
        String kernelresponsecode="";
        String kernelresponsemessage="";
        String tmphonenumber="";
        String tmresponsecode="";
        String tmresponsemessage="";
        String tmaccountnumber = "";
        String tmlastname = "";
        String tmbankitid = "";
        String bankaddedstat = "";
        String cardaddedstat = "";
        String customeraddedstat = "";
        String output_f="";
        String cid="";
        String accountstat="";
        String encrypttoken="";
        String sendsms="";
        String auth=Authorization.getAuth();
        Utilities utils = new Utilities();
        kernel kn = new kernel();
        JSONObject kernelresponse = new JSONObject();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs();
        JSONObject requestbody = new JSONObject();
        Octopuscustomerstm ocusttm = new Octopuscustomerstm();
        List<Octopuscustomerstm>ocusttm_email ;
        List<Octopuscustomerstm> ocusttm_s ;
        Octopuscustomersaccounts ocustac = new Octopuscustomersaccounts();
        List<Octopuscustomersaccounts> ocustac_s ;
        String pndphone="";
        phonenumber=utils.formatPhone(phonenumber);
        Octopuscustomerstmpending ocusttmpnd = DataObjects.getOctopuscustomerstmpending_rec(phonenumber);
        try{
            logger.info(phonenumber+"  "+sessid+": new register octopus validate request sessid: "+ sessid );
            logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("new register validate received");
            tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            pndphone = (ocusttmpnd.getphonenumber() != null)?ocusttmpnd.getphonenumber().trim() :"";
            if (pndphone.equals("")){
                customeraddedstat="wrong request";
                tmresponsecode="66";
                tmresponsemessage="wrong request";
                logger.info(phonenumber+"  "+sessid+":wrong request: "+ tmresponsemessage );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("wrong request");
                tmlog.setActiondetails("request details:wrong request "+tmresponsemessage);
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
            }else{
                last10phonenumber=(ocusttmpnd.getLast10phonenumber()!=null)?ocusttmpnd.getLast10phonenumber().trim():"";
                country=(ocusttmpnd.getCountry()!=null)?ocusttmpnd.getCountry().trim():"";
                firstname=(ocusttmpnd.getFirstname()!=null)?ocusttmpnd.getFirstname().trim():"";
                lastname=(ocusttmpnd.getLastname()!=null)?ocusttmpnd.getLastname().trim():"";
                gender=(ocusttmpnd.getGender()!=null)?ocusttmpnd.getGender().trim():"";
                dob=(ocusttmpnd.getDob()!=null)?ocusttmpnd.getDob().trim():"";
                emailaddress=(ocusttmpnd.getEmailaddress()!=null)?ocusttmpnd.getEmailaddress().trim():"";
                uniqueid=(ocusttmpnd.getUniqueid()!=null)?ocusttmpnd.getUniqueid().trim():"";
                passcode=(ocusttmpnd.getPasscode()!=null)?ocusttmpnd.getPasscode().trim():"";
                accountnumber=(ocusttmpnd.getAccountnumber()!=null)?ocusttmpnd.getAccountnumber().trim():"";
                if(!accountnumber.equals("")){
                    accountstat="true";
                }else{
                    accountstat="false";
                }
                bankname=(ocusttmpnd.getBankname()!=null)?ocusttmpnd.getBankname().trim():"";
                bankcode=(ocusttmpnd.getBankcode()!=null)?ocusttmpnd.getBankcode().trim():"";
                bvn=(ocusttmpnd.getBvn()!=null)?ocusttmpnd.getBvn().trim():"";
                dbtoken=(ocusttmpnd.getToken()!=null)?ocusttmpnd.getToken().trim():"";
                recordstatus=(ocusttmpnd.getRecordstatus()!=null)?ocusttmpnd.getRecordstatus().trim():"";
                if(recordstatus.equals("active")){
                    logger.info(phonenumber+"  "+sessid+": no unvalidated registration request sessid: "+ sessid );
                    logger.info(phonenumber+"  "+sessid+":request source: "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("no unvalidated registration");
                    tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    customeraddedstat="wrong request";
                    tmresponsecode="66";
                    tmresponsemessage="wrong request";
                    logger.info(phonenumber+"  "+sessid+":wrong request: "+ tmresponsemessage );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("wrong request");
                    tmlog.setActiondetails("request details:wrong request "+tmresponsemessage);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                }else{
                    status =utils.decryptPass(token, dbtoken);
                    if (status==false){
                        //wrong token
                        customeraddedstat="wrong token";
                        tmresponsecode="22";
                        tmresponsemessage="wrong token";
                        logger.info(phonenumber+"  "+sessid+":wrong token: "+ tmresponsemessage );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("wrong token");
                        tmlog.setActiondetails("request details:wrong token "+tmresponsemessage);
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }else{
                        ocusttmpnd.setRecordstatus("active");
                        ocusttmpnd.setUpdatedat(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(ocusttmpnd);
                        requestbody.put("requestor", source);
                        requestbody.put("transactionid", transid);
                        requestbody.put("sessionid", sessid);
                        requestbody.put("phonenumber", phonenumber);
                        requestbody.put("firstname", firstname);
                        requestbody.put("lastname", lastname);
                        requestbody.put("gender", gender);
                        requestbody.put("dob", dob);
                        requestbody.put("emailaddress", emailaddress);
                        requestbody.put("country", country);
                        requestbody.put("passcode", passcode);
                        requestbody.put("bvn", bvn);
                        requestbody.put("accountnumber", accountnumber);
                        requestbody.put("bank", bankname);
                        requestbody.put("authorization", auth);
                        ocusttm_email = DataObjects.getOctopuscustomerstm_email(emailaddress);
                        ocusttm_s=DataObjects.getOctopuscustomerstm(phonenumber);
                        //System.out.println("size: "+ocusttm_s.size());
                        tmresponse.put("requestor", source);
                        tmresponse.put("transid", sessid);
                        tmresponse.put("sessid", transid);
                        tmresponse.put("responsedate", utils.getDate());
                        tmresponse.put("responsetime", utils.getTime());
                        tmresponse.put("authorization",auth);
                        tmresponse.put("phonenumber", phonenumber);
                        tmresponse.put("country", country);
                        tmresponse.put("gender", gender);
                        tmresponse.put("description", "REGISTRATION");
                        tmresponse.put("emailaddress", emailaddress);
                        tmresponse.put("lastname", lastname);
                        tmresponse.put("accountstat", accountstat);
                        tmresponse.put("bvn", bvn);
                        tmresponse.put("bank", bankname);
                        tmresponse.put("dob", dob);
                        tmresponse.put("bankcode", bankcode);
                        if(ocusttm_s.size()>0){
                            tmresponsecode="33";
                            tmresponsemessage="phone number already registered";
                            customeraddedstat="phone number already registered";
                            logger.info(phonenumber+"  "+sessid+":phone number already registered: "+ tmresponsemessage );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("new register requet received");
                            tmlog.setActiondetails("request details: phone number already registered "+tmresponsemessage);
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                        }else if(ocusttm_email.size()>0){
                            tmresponsecode="55";
                            tmresponsemessage="email address already registered";
                            customeraddedstat="email address already registered";
                            logger.info(phonenumber+"  "+sessid+":email address already registered: "+ tmresponsemessage );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("new register requet received");
                            tmlog.setActiondetails("request details: email address already registered "+tmresponsemessage);
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs(); 
                        }else{
                            ocusttm.setphonenumber(phonenumber);
                            ocusttm.setLast10phonenumber(phonenumber.substring(3));
                            ocusttm.setFirstname(firstname);
                            ocusttm.setLastname(lastname);
                            ocusttm.setEmailaddress(emailaddress);
                            ocusttm.setGender(gender);
                            ocusttm.setDob(dob);
                            ocusttm.setPasscode(passcode);
                            ocusttm.setCountry("NG");
                            ocusttm.setRecordstatus("active");
                            String dttme = utils.getDate()+" "+utils.getTime();
                            ocusttm.setCreatedat(dttme);
                            ocusttm.setUpdatedat(dttme);
                            PersistenceAgent.logTransactions(ocusttm);
                            tmresponsecode="00";
                            tmresponsemessage="Success: Octopus account created and verified";
                            customeraddedstat="Success: Octopus account created and verified";
                            logger.info(phonenumber+"  "+sessid+": success octopus customer created and verified"+ sessid );
                            logger.info(phonenumber+"  "+sessid+":success octopus customer created and verified: "+ source+" "+phonenumber );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("success octopus customer created ");
                            tmlog.setActiondetails("success octopus customer created");
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            if(accountstat.toLowerCase().equals("true")){
                                ocustac_s=DataObjects.getOctopuscustomersaccounts(accountnumber);
                                System.out.println("existing acct:" +ocustac_s.size());
                                if(ocustac_s.size()>0){
                                    tmresponsecode="44";
                                    tmresponsemessage="octopus customer created bank exists and cannot be added";
                                    bankaddedstat="The bank account details you supplied exists and cannot be added to you octopus account";
                                    logger.info(phonenumber+"  "+sessid+": success octopus customer created bank exists and cannot be added: "+ sessid );
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("new register requet received");
                                    tmlog.setActiondetails("request details: success octopus customer created bank exists and cannot be added "+phonenumber+" "+tmresponsemessage);
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                }else{            
                                    logger.info(phonenumber+"  "+sessid+":calling kernel to register bank " );
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("calling kernel to register bank");
                                    tmlog.setActiondetails("calling kernel ");
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    kernelresponse_str=kn.registeroctopus(requestbody.toString());
                                    requestbody.put("sessionid", sessid+"a");
                                    kernelresponse_str=kn.registeroctopus(requestbody.toString());
                                    System.out.println("kernel response \n"+kernelresponse_str+"\n");
                                    kernelresponse= new JSONObject(kernelresponse_str);   
                                    kernelresponsecode =kernelresponse.getString("responsecode");
                                    kernelresponsemessage =kernelresponse.getString("responsemessage");
                                    logger.info(phonenumber+"  "+sessid+":kernel responsecode: "+ kernelresponsecode);
                                    logger.info(phonenumber+"  "+sessid+":kernel responsemessage: "+ kernelresponsemessage);
                                    tmlog.setSource(source);
                                    tmlog.setPhonenumber(phonenumber);
                                    tmlog.setSessionid(sessid);
                                    tmlog.setTransactionid(transid);
                                    tmlog.setActionperformed("kernel response ");
                                    tmlog.setActiondetails("kernel responsecode: "+kernelresponsecode+" responsemessage: "+kernelresponsemessage+" "+kernelresponse.toString().replace("\"", ":"));
                                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                    PersistenceAgent.logTransactions(tmlog);
                                    tmlog = new Tmlogs();
                                    //build tm response
                                    if(kernelresponsecode.equals("00")){
                                        tmphonenumber = kernelresponse.getString("phonenumber");
                                        tmaccountnumber = kernelresponse.getString("accountnumber");;
                                        tmlastname = kernelresponse.getString("lastname");
                                        if(kernelresponse.has("bankitid")){
                                        tmbankitid = kernelresponse.getString("bankitid");
                                        }
                //                        if(kernelresponse.has("accounts")){
                //                        tmaccounts = kernelresponse.getJSONArray("accounts");
                //                        }
                                        ocustac.setBankcode(bankcode);
                                        ocustac.setAccountnumber(tmaccountnumber);
                                        ocustac.setBankitid(tmbankitid);
                                        ocustac.setBankitstatus("true");
                                        ocustac.setBankname(bankname);
                                        ocustac.setFirstname(firstname);
                                        ocustac.setLastname(tmlastname);
                                        ocustac.setPhonenumber(tmphonenumber);
                                        ocustac.setRecordstatus("active");
                                        ocustac.setPasscode(passcode);
                                        ocustac.setCreatedat(utils.getDate()+" "+utils.getTime());
                                        ocustac.setUpdatedat(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(ocustac);
                                        tmresponsecode="00";
                                        tmresponsemessage="octopus account created, bank added";
                                        bankaddedstat="The bank account details you supplied has been added to your account";
                                        logger.info(phonenumber+"  "+sessid+":register user and account success "+ source+" " +tmresponsemessage );
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("register user and account success ");
                                        tmlog.setActiondetails("register user and account success");
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs(); 
                                    }else if(kernelresponsecode.equals("ETZ_100")){
                                        tmresponsecode="11";
                                        tmresponsemessage = "octopus customer created bank not added";
                                        bankaddedstat="The bank account details you supplied cannot be added";
                                        logger.info(phonenumber+"  "+sessid+" "+source+": tm responsecode: "+ tmresponsemessage);
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("build tm response");
                                        tmlog.setActiondetails("kn responsecode: "+kernelresponsecode+" kn responsemessage: "+kernelresponsemessage+" "+tmresponse.toString().replace("\"", ":"));
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs();

                                    }else if(kernelresponsecode.equals("QO2")){
                                        tmresponsecode="11";
                                        tmresponsemessage = "octopus customer created bank not added";
                                        bankaddedstat="The bank account details you supplied cannot be added";
                                        logger.info(phonenumber+"  "+sessid+" "+source+": tm responsecode: "+ tmresponsemessage);
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("build tm response");
                                        tmlog.setActiondetails("kn responsecode: "+kernelresponsecode+"kn responsemessage: "+kernelresponsemessage+" "+tmresponse.toString().replace("\"", ":"));
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs();
                                    }else{
                                        tmresponsecode="11";
                                        tmresponsemessage = "octopus customer created bank not added";
                                        bankaddedstat="The bank account details you supplied cannot be added";
                                        logger.info(phonenumber+"  "+sessid+" "+source+": tm responsecode: "+ tmresponsemessage);
                                        tmlog.setSource(source);
                                        tmlog.setPhonenumber(phonenumber);
                                        tmlog.setSessionid(sessid);
                                        tmlog.setTransactionid(transid);
                                        tmlog.setActionperformed("build tm response");
                                        tmlog.setActiondetails("kn responsecode: "+kernelresponsecode+"kn responsemessage: "+kernelresponsemessage+" "+tmresponse.toString().replace("\"", ":"));
                                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                        PersistenceAgent.logTransactions(tmlog);
                                        tmlog = new Tmlogs();
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
            tmresponse.put("responsecode", tmresponsecode);
            tmresponse.put("responsemessage", customeraddedstat+". "+bankaddedstat+".");
            output_f = tmresponse.toString();
            output = output_f;
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String validateRegisterOctopuss_old(String source,String transid,String sessid,String phonenumber,String token){
        Octopuscustomerstm occust;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String dbtoken="";
        boolean status;
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        OctopusOperations op = new OctopusOperations();
        Tmlogs tmlog = new Tmlogs();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a customer");
            }else{
                logger.info(phonenumber+"  "+sessid+":request validate register source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("validate register");
                tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                occust = DataObjects.getOctopuscustomerstm_rec(phonenumber);
                dbtoken=occust.getToken();
                status =utils.decryptPass(token, dbtoken);
                   if (status==true){
                        occust.setRecordstatus("active");
                        occust.setUpdatedat(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(occust);
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "Account verified");
                        logger.info(phonenumber+"  "+sessid+":account verifiedsource: "+ source+" "+phonenumber );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("account verified");
                        tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                   }else{
                        tmresponse.put("responsecode", "11");
                        tmresponse.put("responsemessage", "wrong token");
                        logger.info(phonenumber+"  "+sessid+":wrong token source: "+ source+" "+phonenumber );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("wrong token");
                        tmlog.setActiondetails("request details: "+" "+phonenumber+" "+source+" "+sessid);
                        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                   }
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info(phonenumber+"  "+sessid+" "+source+": error parsing json "+ " response "+output.toString().replace("\"", ":") );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed(source+": error parsing json ");
            tmlog.setActiondetails("error parsing json"+source+" "+sessid+" "+phonenumber+" response "+output.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs(); 
        }
    return output;}
    
    public String resetPasscode(String source,String transid,String sessid,String phonenumber){
        Octopuspasswordreset occust;
        List<Octopuspasswordreset> occustlist;
        Octopuspasswordreset occust2 = new Octopuspasswordreset();
        Octopuscustomerstm occustm = new Octopuscustomerstm();
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        String dbpass="";
        boolean status=false;
        String firstname="";
        String lastname="";
        String authorization="";
        String output="";
        String dbstatus="";
        String tmrecordstatus="";
        JSONObject tmresponse = new JSONObject();
        String loginresponse_str="";
        JSONObject loginresponse;
        String loginrescode="";
        OctopusOperations op = new OctopusOperations();
        Mailer mail = new Mailer();
        Octopuscustomerstmpending ocusttmpnd;
        phonenumber=utils.formatPhone(phonenumber);
        logger.info(phonenumber+"  "+sessid+" "+source+": new account login request "+ sessid );
        tmlog.setSource(source);
        tmlog.setPhonenumber(phonenumber);
        tmlog.setSessionid(sessid);
        tmlog.setTransactionid(transid);
        tmlog.setActionperformed(source+": new account login request");
        tmlog.setActiondetails("new account login request"+source+" "+sessid+" "+phonenumber);
        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
        PersistenceAgent.logTransactions(tmlog);
        tmlog = new Tmlogs();
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
            }else{
                String token =utils.generateToken();
                String encrypttoken = utils.encryptPass(token);
                String sendsms=utils.sendunBlockToken(phonenumber, token);
                if(sendsms.equals("false")){
                    tmresponse.put("responsecode", "44");
                    tmresponse.put("responsemessage", "Can not send token to +"+phonenumber);
                    log.info(appname, classname, "Login Request -Customer is blocked.  Cannot new token sent at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponse.toString());
                    logger.info(phonenumber+"  "+sessid+" "+source+":Cannot Send Token to reset account "+ " response "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed(source+":Cannot Send Token Sent to reset account");
                    tmlog.setActiondetails("Cannot Send Token Sent to reset account "+source+" "+sessid+" "+phonenumber+" response "+tmresponse.toString().replace("\"", ":"));
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    occustm=DataObjects.getOctopuscustomerstm_rec(phonenumber);
                    occustlist=DataObjects.getOctopuspasswordreset(phonenumber);
                    if(!occustlist.isEmpty()){
                        for(int i=0;i<occustlist.size();i++){
                            occust=occustlist.get(i);
                            dbstatus=(occust.getStatus()!=null)?occust.getStatus():"";
                            tmrecordstatus=(occustm.getRecordstatus()!=null)?occustm.getRecordstatus():"";
                            if(!dbstatus.equals("")){
                                occust.setPhonenumber(phonenumber);
                                occust.setStatus("inactive");
                                occust.setCreatedat(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(occust);
                            }
                        }
                    }
                    occust2.setSource(source);
                    occust2.setPhonenumber(phonenumber);
                    occust2.setToken(encrypttoken);
                    occust2.setStatus("active");
                    occust2.setCreatedat(utils.getDate()+" "+utils.getTime());
                    occust2.setUpdatedat(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(occust2);
                    occustm.setRecordstatus("blocked");
                    PersistenceAgent.logTransactions(occustm);
                    tmresponse.put("responsecode", "33");
                    tmresponse.put("responsemessage", "Please enter token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to complete reset");
                    log.info(appname, classname, "Login Request -Token Sent to reset account at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+tmresponse.toString());
                    logger.info(phonenumber+"  "+sessid+" "+source+": Token Sent to reset account "+ " response "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed(source+": Token Sent to reset account");
                    tmlog.setActiondetails("Token Sent to reset account "+source+" "+sessid+" "+phonenumber+" response "+tmresponse.toString().replace("\"", ":"));
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
            }
            output=tmresponse.toString();
            
        }catch(Exception e){
            
        }
    return output;}
    
    public String loginCustomer(String source,String transid,String sessid,String phonenumber,String passcode,String actuallogin)
    {
        Octopuscustomerstm occust;
        OctopusOperations op = new OctopusOperations();
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        String dbpass="";
        boolean status=false;
        String firstname="";
        String lastname="";
        String authorization="";
        String output="";
        JSONObject response = new JSONObject();
        Mailer mail = new Mailer();
        Octopuscustomerstmpending ocusttmpnd;
        phonenumber=utils.formatPhone(phonenumber);
        if(actuallogin.toLowerCase().equals("true")){
            log.info(appname, classname, "New Login Request at"+utils.getDate()+" "+utils.getTime(), phonenumber);
        }
        logger.info(phonenumber+"  "+sessid+" "+source+": new account login request "+ sessid );
        tmlog.setSource(source);
        tmlog.setPhonenumber(phonenumber);
        tmlog.setSessionid(sessid);
        tmlog.setTransactionid(transid);
        tmlog.setActionperformed(source+": new account login request");
        tmlog.setActiondetails("new account login request"+source+" "+sessid+" "+phonenumber);
        tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
        PersistenceAgent.logTransactions(tmlog);
        tmlog = new Tmlogs();
        try{
            response.put("responsedate", utils.getDate());
            response.put("responsetime", utils.getTime());
            response.put("sessionid", sessid);
            response.put("transactionid", transid);
            response.put("source", source);
            response.put("authorization", "");
            occust = DataObjects.getOctopuscustomerstm_rec(phonenumber);
            dbpass=(occust.getPasscode()!=null)?occust.getPasscode():"";
            String dbrecordstatus=(occust.getRecordstatus()!=null)?occust.getRecordstatus():"";
            String dbemailaddress=(occust.getEmailaddress()!=null)?occust.getEmailaddress():"";
            logger.info(phonenumber+"  "+sessid+" "+source+": db check "+ sessid );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed(source+": db check for login request");
            tmlog.setActiondetails("db check for login request"+source+" "+sessid+" "+phonenumber);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            if(!dbpass.equals("")){
                if(!dbpass.equals("")&&dbrecordstatus.equals("active")){
                    firstname=occust.getFirstname();
                    lastname=occust.getLastname();
                    logger.info(phonenumber+"  "+sessid+" "+source+": phonenumber exist "+ sessid );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed(source+": phonenumber exist");
                    tmlog.setActiondetails("phonenumber exist"+source+" "+sessid+" "+phonenumber);
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    status =utils.decryptPass(passcode, dbpass);
                    if(status==true){
                        if(actuallogin.toLowerCase().equals("true")){
                            occust.setLogincount(0);
                            PersistenceAgent.logTransactions(occust);
                            if(!dbemailaddress.equals("")){
                                mail.loginNotification(source,dbemailaddress, firstname+" "+lastname, utils.getDDDate()+" "+utils.getTTTime(), "true");
                            }
                            log.info(appname, classname, "Login Request success at"+utils.getDate()+" "+utils.getTime(), phonenumber);
                        }
                        response=new JSONObject(op.customerStatus(source, transid, sessid, phonenumber, "NG"));
                        logger.info(phonenumber+"  "+sessid+" "+source+": login true "+ " response "+response.toString().replace("\"", ":") );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed(source+": login true");
                        tmlog.setActiondetails("login true"+source+" "+sessid+" "+phonenumber+" response "+response.toString().replace("\"", ":"));
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                        if(actuallogin.equals("true")){
                            authorization=AuthHelper.createJWT(response,phonenumber,source);
                            response.put("authorization", authorization);
                            occust.setLastloginsource(source);
                            occust.setLastlogindate(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(occust);
                        }
                    }else{
                        if(actuallogin.toLowerCase().equals("true")){
                            if(!dbemailaddress.equals("")){
                                mail.loginNotification(source,dbemailaddress, firstname+" "+lastname, utils.getDDDate()+" "+utils.getTTTime(), "false");
                            }
                            log.info(appname, classname, "Login Request failed-wrong passscode at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+response.toString());
                            int logincount = (occust.getLogincount()!=null)?occust.getLogincount():0;
                            if(logincount>3){
                                occust.setRecordstatus("blocked");
                                PersistenceAgent.logTransactions(occust);
                                response=new JSONObject(op.resetPasscode(source, transid, sessid, phonenumber));
                                response.put("responsemessage", "Account Blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to verify your account");
                                log.info(appname, classname, "Login Request failed-More than 4 attempts made. Customer account blocked at "+utils.getDate()+" "+utils.getTime(), phonenumber+" "+response.toString());
                                if(!dbemailaddress.equals("")){
                                    mail.loginNotification(source,dbemailaddress, firstname+" "+lastname, utils.getDDDate()+" "+utils.getTTTime(), "blocked");
                                }
                                logger.info(phonenumber+"  "+sessid+" "+source+": Wrong Passcode Login Request failed-More than 4 attempts made. Customer account status blocked"+ " response "+response.toString().replace("\"", ":") );
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed(source+": Login Request failed-More than 4 attempts made. Customer account blocked");
                                tmlog.setActiondetails("Login Request failed-More than 4 attempts made. Customer account blocked"+source+" "+sessid+" "+phonenumber+" response "+response.toString().replace("\"", ":"));
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                            }else{
                                occust.setLogincount(logincount+1);
                                PersistenceAgent.logTransactions(occust);
                                response.put("responsecode", "22");
                                response.put("responsemessage", "Wrong Passcode");
                                logger.info(phonenumber+"  "+sessid+" "+source+": Wrong Passcode "+ " response "+response.toString().replace("\"", ":") );
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed(source+": Wrong Passcode");
                                tmlog.setActiondetails("Wrong Passcode"+source+" "+sessid+" "+phonenumber+" response "+response.toString().replace("\"", ":"));
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();
                            }
                        }else{
                            response.put("responsecode", "22");
                            response.put("responsemessage", "Wrong Passcode");
                            logger.info(phonenumber+"  "+sessid+" "+source+": Wrong Passcode "+ " response "+response.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed(source+": Wrong Passcode");
                            tmlog.setActiondetails("Wrong Passcode"+source+" "+sessid+" "+phonenumber+" response "+response.toString().replace("\"", ":"));
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                        }
                    }
                    }else if(!dbpass.equals("")&&dbrecordstatus.equals("blocked")){
                        response=new JSONObject(op.resetPasscode(source, transid, sessid, phonenumber));
                        response.put("responsemessage", "Account Blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to verify your account");
                        logger.info(phonenumber+"  "+sessid+" "+source+": Account blocked "+ " response "+response.toString().replace("\"", ":") );
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed(source+": Account blocked");
                        tmlog.setActiondetails("Account blocked "+source+" "+sessid+" "+phonenumber+" response "+response.toString().replace("\"", ":"));
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }
                }else{
                    response.put("responsecode", "11");
                    response.put("responsemessage", "Phone number not registered");
                    log.info(appname, classname, "Login Request -Customer is not registered at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+response.toString());
                    logger.info(phonenumber+"  "+sessid+" "+source+": Phone number not registered "+ " response "+response.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed(source+": Phone number not registered");
                    tmlog.setActiondetails("Phone number not registered"+source+" "+sessid+" "+phonenumber+" response "+response.toString().replace("\"", ":"));
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
            }
            output = response.toString();
        }catch(Exception e){
            e.printStackTrace();
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info(phonenumber+"  "+sessid+" "+source+": error parsing json "+ " response "+output.toString().replace("\"", ":") );
            log.error(appname, classname, "Login Request -error parsing json at"+utils.getDate()+" "+utils.getTime(), phonenumber+" "+output+" "+e.getMessage());
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed(source+": error parsing json ");
            tmlog.setActiondetails("error parsing json"+source+" "+sessid+" "+phonenumber+" response "+output.toString().replace("\"", ":"));
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();   
        }
        
    return output;}
    
    public String getInboxTransactions(String source,String transid,String sessid,String phonenumber)
    {
        List<Octopusinboxtransactions> ocinboxlist;
        Octopusinboxtransactions ocinbox;
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        int sno;
        String receiverid="";
        String receivername="";
        String receiveraccount="";
        String receiverbank="";
        String receiverbankcode="";
        String customerid="";
        String paymentcode="";
        String billername="";
        String billerid="";
        String categoryname="";
        String categoryid="";
        String senderid="";
        String sendername="";
        String senderaccount="";
        String senderbank="";
        String senderbankcode="";
        String amount="";
        String narration="";
        String transtype="";
        String transcategory="";
        String status="";
        String responsecode="";
        String responsemessage="";
        JSONArray arr = new JSONArray();
        JSONObject tmresponse = new JSONObject();
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        OctopusOperations op = new OctopusOperations();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a customer");
            }else{
                logger.info(phonenumber+"  "+sessid+": new checkinbox trans request received "+ sessid );
                logger.info(phonenumber+"  "+sessid+":new checkinbox trans request received: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new checkinbox trans request received");
                tmlog.setActiondetails("request details: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                ocinboxlist= DataObjects.getOctopusinboxtransactions(phonenumber);
                System.out.println("size:"+ ocinboxlist.size());
                if(ocinboxlist.size()<1){
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "no inbox transaction");
                    logger.info(phonenumber+"  "+sessid+"no inbox transaction "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("no inbox transaction");
                    tmlog.setActiondetails("no inbox transactionrequest details: "+ source+" "+phonenumber+" "+"no inbox transaction" );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                }else{
                    responsecode="11";
                    responsemessage="no unapproved transaction";
                    for(int i=0;i<ocinboxlist.size();i++){
                        ocinbox=ocinboxlist.get(i);
                        status=ocinbox.getStatus() != null ? ocinbox.getStatus(): "" ;
                        System.out.println("status: "+status);
                        if(status.equals("unapproved")){
                            JSONObject eachobject = new JSONObject();
                            responsecode="00";
                            responsemessage="success";
                            sno=ocinbox.getId();
                            receiverid=ocinbox.getReceiverid();
                            receivername= ocinbox.getReceivername();
                            receiveraccount = ocinbox.getReceiveraccount();
                            receiverbank = ocinbox.getReceiverbank();
                            receiverbankcode = ocinbox.getReceiverbankcode();
                            customerid = ocinbox.getCustomerid();
                            paymentcode = ocinbox.getPaymentcode();
                            billername = ocinbox.getBillername();
                            billerid = ocinbox.getBillerid();
                            categoryname = ocinbox.getCategoryname();
                            categoryid = ocinbox.getCategoryid();
                            senderid=ocinbox.getSenderid();
                            sendername=ocinbox.getSendername();
                            senderaccount=ocinbox.getSenderaccount();
                            senderbank=ocinbox.getSenderbank();
                            senderbankcode=ocinbox.getSenderbankcode();
                            amount=ocinbox.getAmount();
                            narration = ocinbox.getNarration();
                            transtype = ocinbox.getTranstype();
                            transcategory = ocinbox.getTranscategory();
                            eachobject.put("sno", sno);
                            eachobject.put("receiverid", receiverid);
                            eachobject.put("receivername", receivername);
                            eachobject.put("receiveraccount", receiveraccount);
                            eachobject.put("receiverbank", receiverbank);
                            eachobject.put("receiverbankcode", receiverbankcode);
                            eachobject.put("customerid", customerid);
                            eachobject.put("paymentcode", paymentcode);
                            eachobject.put("billername", billername);
                            eachobject.put("billerid", billerid);
                            eachobject.put("categoryname", categoryname);
                            eachobject.put("categoryid", categoryid);
                            eachobject.put("senderid", senderid);
                            eachobject.put("sendername", sendername);
                            eachobject.put("senderaccount", senderaccount);
                            eachobject.put("senderbank", senderbank);
                            eachobject.put("senderbankcode", senderbankcode);
                            eachobject.put("amount", amount);
                            eachobject.put("narration", narration);
                            eachobject.put("transtype", transtype);
                            eachobject.put("transcategory", transcategory);
                            arr.put(eachobject);
                        }
                    }
                    tmresponse.put("result", arr);
                    tmresponse.put("responsecode", responsecode);
                    tmresponse.put("responsemessage", responsemessage);
                    logger.info(phonenumber+"  "+sessid+" "+responsemessage+" "+ source+" "+phonenumber+" "+responsemessage );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed(responsemessage);
                    tmlog.setActiondetails(" "+responsemessage+" "+ source+" "+phonenumber+" "+responsemessage );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                }
            }
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("parse error ");
            tmlog.setActiondetails("parse error "+output);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
        
    return output;}
    
    public String getBlacklist(String source,String transid,String sessid,String phonenumber)
    {
        List<Octopusblacklist> ocblacklist;
        Octopusblacklist ocblist;
        Utilities utils = new Utilities();
        Tmlogs tmlog = new Tmlogs();
        int sno;
        String status="";
        String responsecode="";
        String responsemessage="";
        JSONArray arr = new JSONArray();
        JSONObject tmresponse = new JSONObject();
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        String bphonenumber="";
        JSONObject loginresponse;
        OctopusOperations op = new OctopusOperations();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a customer");
            }else{
                logger.info(phonenumber+"  "+sessid+": new get blacklist request received "+ sessid );
                logger.info(phonenumber+"  "+sessid+":new get blacklist request received: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new get blacklist request received");
                tmlog.setActiondetails("request details: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                ocblacklist= DataObjects.getOctopusblacklist_personal(phonenumber);
                System.out.println("size:"+ ocblacklist.size());
                if(ocblacklist.size()<1){
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "No blacklisted customer");
                    logger.info(phonenumber+"  "+sessid+"No blacklisted customer "+ source+" "+phonenumber );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("No blacklisted customer");
                    tmlog.setActiondetails("No blacklisted customerrequest details: "+ source+" "+phonenumber+" "+"no inbox transaction" );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                }else{
                    responsecode="11";
                    responsemessage="No blacklisted customer";
                    for(int i=0;i<ocblacklist.size();i++){
                        ocblist=ocblacklist.get(i);
                        status=ocblist.getStatus() != null ? ocblist.getStatus(): "" ;
                        System.out.println("status: "+status);
                        if(status.equals("active")){
                            JSONObject eachobject = new JSONObject();
                            responsecode="00";
                            responsemessage="Success";
                            bphonenumber = ocblist.getPhonenumber();
                            sno=ocblist.getId();
                            eachobject.put("sno", sno);
                            eachobject.put("phonenumber", bphonenumber);
                            arr.put(eachobject);
                        }
                    }
                    tmresponse.put("result", arr);
                    tmresponse.put("responsecode", responsecode);
                    tmresponse.put("responsemessage", responsemessage);
                    logger.info(phonenumber+"  "+sessid+" "+responsemessage+" "+ source+" "+phonenumber+" "+responsemessage );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed(responsemessage);
                    tmlog.setActiondetails(" "+responsemessage+" "+ source+" "+phonenumber+" "+responsemessage );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                }
            }
            output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": parse error "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("parse error ");
            tmlog.setActiondetails("parse error "+output);
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
        
    return output;}
    
    public String fundsRequest(String source,String transid,String sessid,String receiverid,String receivername,String receiveraccount,String receiverbank,String receiverbankcode,String customerid,String paymentcode,String billername,String billerid,String categoryname,String categoryid,String senderid,String sendername,String senderaccount,String senderbank,String senderbankcode,String amount,String narration,String transtype,String transcategory)
    {
        Octopusinboxtransactions ocinbox = new Octopusinboxtransactions();
        Utilities utils = new Utilities();
        JSONObject tmresponse = new JSONObject();
        Tmlogs tmlog = new Tmlogs(); 
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        List<Octopusblacklist> allblacklist;
        Octopusblacklist perblacklist;
        String existbene="false";
        OctopusOperations op = new OctopusOperations();
        senderid=utils.formatPhone(senderid);
        receiverid=utils.formatPhone(receiverid);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, receiverid);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                logger.info(receiverid+" source"+source+" "+sessid+":new fundsRequest request source: "+ source+" "+receiverid );
                tmlog.setSource(source);
                tmlog.setPhonenumber(receiverid);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new fundsRequest request");
                tmlog.setActiondetails("request details: "+receiverid+"  "+sessid+":new fundsRequest request source: "+ source+" "+receiverid );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                allblacklist = DataObjects.getOctopusblacklist(receiverid);
                if(allblacklist.isEmpty()==false){
                    for(int i=0;i<allblacklist.size();i++){
                        perblacklist=allblacklist.get(i);
                        if(perblacklist.getBlacklistedby().equals(senderid)&&perblacklist.getStatus().equals("active")){
                           existbene="true"; 
                        }
                    }
                }
                if(existbene.equals("false")){
                ocinbox.setAmount(amount);
                ocinbox.setNarration(narration);
                ocinbox.setReceiverid(receiverid);
                ocinbox.setReceivername(receivername);
                ocinbox.setReceiveraccount(receiveraccount);
                ocinbox.setReceiverbank(receiverbank);
                ocinbox.setReceiverbankcode(receiverbankcode);
                ocinbox.setCustomerid(customerid);
                ocinbox.setPaymentcode(paymentcode);
                ocinbox.setBillername(billername);
                ocinbox.setBillerid(billerid);
                ocinbox.setCategoryname(categoryname);
                ocinbox.setCategoryid(categoryid);
                ocinbox.setSenderid(senderid);
                ocinbox.setSendername(sendername);
                ocinbox.setSenderaccount(senderaccount);
                ocinbox.setSenderbank(senderbank);
                ocinbox.setSenderbankcode(senderbankcode);
                ocinbox.setStatus("unapproved");
                ocinbox.setTranstype(transtype);
                ocinbox.setTranscategory(transcategory);
                ocinbox.setCreatedat(utils.getDate()+" "+utils.getTime());
                ocinbox.setUpdatedat(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(ocinbox);
                if(transcategory.toLowerCase().equals("fundsrequest")){
                    utils.sendInboxNotification(senderid,receivername,amount,transtype,narration);
                }
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "Success");
                logger.info(receiverid+" source"+source+" "+sessid+":request successful source: "+ source+" "+receiverid+" " +tmresponse.toString().replace("\"", ":"));
                tmlog.setSource(source);
                tmlog.setPhonenumber(receiverid);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("request successful");
                tmlog.setActiondetails("request details: "+receiverid+"  "+sessid+":request successful source: "+ source+" "+receiverid+" " +tmresponse.toString().replace("\"", ":") );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "You can not request funds from this customer");
                    logger.info(receiverid+" source"+source+" "+sessid+":request successful source: "+ source+" "+receiverid+" " +tmresponse.toString().replace("\"", ":"));
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(receiverid);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("request successful");
                    tmlog.setActiondetails("request details: "+receiverid+"  "+sessid+":request successful source: "+ source+" "+receiverid+" " +tmresponse.toString().replace("\"", ":") );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
            }
                output=tmresponse.toString();
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(receiverid+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(receiverid);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
   return output; }
    
    public String addAccount(String source,String transid,String sessid,String phonenumber,String passcode,String bvn,String acno,String bank,String bankcode)
    {
        Octopuscustomerstm ocusttm ;
        List<Octopuscustomerstm> ocusttm_s ;
        Octopuscustomersaccounts ocustac;
        Octopuscustomersaccounts regocustac = new Octopuscustomersaccounts();
        kernel kn = new kernel();
        OctopusOperations op = new OctopusOperations();
        Tmlogs tmlog = new Tmlogs();
        List<Octopuscustomersaccounts> ocustac_s ;
        JSONObject requestbody = new JSONObject();
        Utilities utils = new Utilities();
        JSONObject customerstatresjson ;
        JSONObject kernelresponse ;
        String auth = Authorization.getAuth();
        String dbpasscode="";
        String kernelresponse_str="";
        boolean passcodestatus;
        String kernelresponsecode="";
        String tmphonenumber="";
        String tmcountry = "";
        String kernelresponsemessage="";
        String tmamount = "";
        String tmaccountnumber = "";
        String tmgender = "";
        String tmdescription = "";
        String tmemailaddress = "";
        String tmlastname = "";
        String tmbank = "";
        String tmdob = "";
        String tmbankitid = "";
        String tmbvn = "";
        JSONArray tmaccounts;
        JSONObject tmresponse = new JSONObject();
        Mailer mail = new Mailer();
        String gender="";
        String dob="";
        String emailaddress="";
        String bankitid="";
        String firstname="";
        String lastname="";
        String country="";
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber,passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
            }else if(loginrescode.equals("22")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "wrong passcode");
            }else{
                //is a customer
                //correct passcode
                logger.info(phonenumber+"  "+sessid+":new add account request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new add account request received");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new add account request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                ocustac_s=DataObjects.getOctopuscustomersaccounts(acno);
                boolean registeraccount=true;
                if(ocustac_s.isEmpty()==false){
                    //account already registered
                    for(int i=0;i<ocustac_s.size();i++){
                        ocustac= ocustac_s.get(i);
                        if(ocustac.getPhonenumber().equals(phonenumber)&&ocustac.getBankcode().equalsIgnoreCase(bankcode)){
                            //account already added by you
                            registeraccount=false;
                            tmresponse.put("responsecode", "33");
                            tmresponse.put("responsemessage", "Account number already registered by you");
                            logger.info(phonenumber+"  "+sessid+":account already registered by you source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("account already registered by you");
                            tmlog.setActiondetails("account already registered by you "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                        }else if(ocustac.getBankcode().equalsIgnoreCase(bankcode)){
                            //account added by another customer
                            registeraccount=false;
                            tmresponse.put("responsecode", "44");
                            tmresponse.put("responsemessage", "Account number already registered by another customer");
                            logger.info(phonenumber+"  "+sessid+":account already added by another customer source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("account already added by another customer");
                            tmlog.setActiondetails("account already added by another customer "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                        }
                    }
                    }
                    if(registeraccount==true){
                        //register account
                        String customerstatres = op.customerStatus(source, transid, sessid, phonenumber, "NG");
                        customerstatresjson = new JSONObject(customerstatres);
                        firstname = customerstatresjson.getString("firstname");
                        lastname = customerstatresjson.getString("lastname");
                        gender = customerstatresjson.getString("gender");
                        dob = customerstatresjson.getString("dob");
                        emailaddress = customerstatresjson.getString("emailaddress");
                        country = customerstatresjson.getString("country");
                        requestbody.put("requestor", source);
                        requestbody.put("transactionid", transid);
                        requestbody.put("sessionid", sessid);
                        requestbody.put("phonenumber", utils.formatPhoneZero(phonenumber));
                        requestbody.put("firstname", firstname);
                        requestbody.put("lastname", lastname);
                        requestbody.put("gender", gender);
                        requestbody.put("dob", dob);
                        requestbody.put("emailaddress", emailaddress);
                        requestbody.put("country", country);
                        requestbody.put("passcode", passcode);
                        requestbody.put("bvn", bvn);
                        requestbody.put("accountnumber", acno);
                        requestbody.put("bank", bankcode);
                        requestbody.put("authorization", auth);
                        if(!bankcode.equals("044")&&!bankcode.equals("011")&&!bankcode.equals("032")&&!bankcode.equals("215")&&!bankcode.equals("076")&&!bankcode.equals("033")&&!bankcode.equals("030")&&!bankcode.equals("082")&&!bankcode.equals("214")){
                            logger.info(phonenumber+"  "+sessid+": Registering account. Account not supported on bankit "+ source+" "+requestbody.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("Registering account. Account not supported on bankit");
                            tmlog.setActiondetails("Registering account. Account not supported on bankit "+phonenumber+" "+requestbody.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            regocustac.setAccountnumber(acno);
                            regocustac.setBankcode(bankcode);
                            regocustac.setBankitstatus("false");
                            regocustac.setBankname(bank);
                            regocustac.setBvn(bvn);
                            regocustac.setFirstname(firstname);
                            regocustac.setLastname(lastname);
                            regocustac.setPasscode(utils.encryptPass(passcode));
                            regocustac.setPhonenumber(phonenumber);
                            regocustac.setIsbankitaccount("false");
                            regocustac.setRecordstatus("active");
                            regocustac.setCreatedat(utils.getDate()+" "+utils.getTime());
                            regocustac.setUpdatedat(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(regocustac);
                            logger.info(phonenumber+"  "+sessid+": account successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("account successfully added");
                            tmlog.setActiondetails("account successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            tmresponse.put("responsecode", "00");
                            tmresponse.put("responsemessage", "success");
                            mail.addAccountNotification(phonenumber, emailaddress, firstname+" "+lastname, bank, acno);
                            log.info(appname, classname, "Add Account Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Account No "+acno);
                            logger.info(phonenumber+"  "+sessid+":Mail Sent. Account successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("Mail Sent. Account successfully added");
                            tmlog.setActiondetails("Mail Sent. Account successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        }else if (bankcode.equals("030")){
                            //heritage bank
                            //validate phonenumber and accountno match at heritage.
                            //then add account to profile and set bankit status to true.
                            HbngInterface hbng = new HbngInterface();
                            boolean isAccountMatchPhone = false;
                            String apiresp = hbng.phonereturnaccount(source, sessid, transid, auth, phonenumber);
                            if(Utilities.isJSONValid(apiresp)){
                                JSONObject jsn = new JSONObject(apiresp);
                                if(jsn.optString("responsecode").equals("00")){
                                    //iterate thru account list and check if it matches account presented
                                    JSONArray ArrAcc = new JSONArray(jsn.optString("accounts"));
                                        
                                        for(int j =0; j < ArrAcc.length(); j++){
                                            JSONObject data = ArrAcc.getJSONObject(j);
                                            if(data.optString("accountno").equalsIgnoreCase(acno)){
                                                isAccountMatchPhone=true;
                                            }
                                        }
                                    
                                }
                            
                            
                            if(isAccountMatchPhone){
                                 logger.info(phonenumber+"  "+sessid+": Registering account on Heritage "+ source+" "+requestbody.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("Registering account on Heritage");
                            tmlog.setActiondetails("Registering account on Heritage "+phonenumber+" "+requestbody.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            regocustac.setAccountnumber(acno);
                            regocustac.setBankcode(bankcode);
                            regocustac.setBankitstatus("true");
                            regocustac.setBankname(bank);
                            regocustac.setBvn(bvn);
                            regocustac.setFirstname(firstname);
                            regocustac.setLastname(lastname);
                            regocustac.setPasscode(utils.encryptPass(passcode));
                            regocustac.setPhonenumber(phonenumber);
                            regocustac.setIsbankitaccount("true");
                            regocustac.setRecordstatus("active");
                            regocustac.setCreatedat(utils.getDate()+" "+utils.getTime());
                            regocustac.setUpdatedat(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(regocustac);
                            logger.info(phonenumber+"  "+sessid+": account successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("account successfully added");
                            tmlog.setActiondetails("account successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            tmresponse.put("responsecode", "00");
                            tmresponse.put("responsemessage", "success");
                            mail.addAccountNotification(phonenumber, emailaddress, firstname+" "+lastname, bank, acno);
                            log.info(appname, classname, "Add Account Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Account No "+acno);
                            logger.info(phonenumber+"  "+sessid+":Mail Sent. Account successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("Mail Sent. Account successfully added");
                            tmlog.setActiondetails("Mail Sent. Account successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            }else{
                                kernelresponsemessage="Bank account number does not match mobile number presented";
                                tmresponse.put("responsecode", "55");
                                tmresponse.put("responsemessage", kernelresponsemessage);
                                logger.info(phonenumber+"  "+sessid+": "+kernelresponsemessage+" "+ source+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed(kernelresponsemessage);
                                tmlog.setActiondetails(kernelresponsemessage+" "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                            }
                            }else{
                                kernelresponsemessage="Unable to process request. Please try again";
                                tmresponse.put("responsecode", "56");
                                tmresponse.put("responsemessage", kernelresponsemessage);
                                logger.info(phonenumber+"  "+sessid+": "+kernelresponsemessage+" "+ source+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed(kernelresponsemessage);
                                tmlog.setActiondetails(kernelresponsemessage+" "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                            }
                            
                            
                        }else{
                            logger.info(phonenumber+"  "+sessid+": calling kernel to register account "+ source+" "+requestbody.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("calling kernel to register account");
                            tmlog.setActiondetails("calling kernel to register account "+phonenumber+" "+requestbody.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            kernelresponse_str=kn.registeroctopus(requestbody.toString());
                            requestbody.put("sessionid", sessid+"a");
                            kernelresponse_str=kn.registeroctopus(requestbody.toString());
                            System.out.println("kernel response \n"+kernelresponse_str+"\n");
                            kernelresponse= new JSONObject(kernelresponse_str);   
                            kernelresponsecode =kernelresponse.getString("responsecode");
                            kernelresponsemessage =kernelresponse.getString("responsemessage");
                            logger.info(phonenumber+"  "+sessid+": kernel response "+ source+" "+kernelresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("kernel response");
                            tmlog.setActiondetails("kernel response "+phonenumber+" "+kernelresponse.toString().replace("\"", ":") );
                            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                            if(kernelresponsecode.equals("00")){
                                //reg success
                                tmresponse.put("responsecode", "00");
                                tmresponse.put("responsemessage", "success");
                                tmphonenumber = kernelresponse.getString("phonenumber");
                                tmcountry = kernelresponse.getString("country");
                                tmamount = kernelresponse.getString("amount");
                                tmaccountnumber = kernelresponse.getString("accountnumber");
                                tmgender = kernelresponse.getString("gender");
                                tmdescription = kernelresponse.getString("description");
                                tmemailaddress = kernelresponse.getString("emailaddress");
                                tmlastname = kernelresponse.getString("lastname");
                                tmbank = kernelresponse.getString("bank");
                                tmdob = kernelresponse.getString("dob");
                                if(kernelresponse.has("bankitid")){
                                    tmbankitid = kernelresponse.getString("bankitid");
                                }
                                tmbvn = kernelresponse.getString("bvn");
                                if(kernelresponse.has("accounts")){
                                    tmaccounts = kernelresponse.getJSONArray("accounts");
                                }
                                regocustac.setAccountnumber(tmaccountnumber);
                                regocustac.setBankcode(bankcode);
                                regocustac.setIsbankitaccount("true");
                                regocustac.setBankitid(tmbankitid);
                                regocustac.setBankitstatus("true");
                                regocustac.setBankname(bank);
                                regocustac.setBvn(bvn);
                                regocustac.setFirstname(firstname);
                                regocustac.setLastname(tmlastname);
                                regocustac.setPasscode(utils.encryptPass(passcode));
                                regocustac.setPhonenumber(utils.formatPhone(tmphonenumber));
                                regocustac.setRecordstatus("active");
                                regocustac.setCreatedat(utils.getDate()+" "+utils.getTime());
                                regocustac.setUpdatedat(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(regocustac);
                                logger.info(phonenumber+"  "+sessid+": account successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed("account successfully added");
                                tmlog.setActiondetails("account successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();

                            }else{
                                //reg failed
                                tmresponse.put("responsecode", "55");
                                tmresponse.put("responsemessage", kernelresponsemessage);
                                logger.info(phonenumber+"  "+sessid+": "+kernelresponsemessage+" "+ source+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setSource(source);
                                tmlog.setPhonenumber(phonenumber);
                                tmlog.setSessionid(sessid);
                                tmlog.setTransactionid(transid);
                                tmlog.setActionperformed(kernelresponsemessage);
                                tmlog.setActiondetails(kernelresponsemessage+" "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                                PersistenceAgent.logTransactions(tmlog);
                                tmlog = new Tmlogs();

                            }
                        }

                    }
            }    
            
            output=tmresponse.toString();
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String deleteAccount(String source,String transid,String sessid,String phonenumber,String passcode,String acno,String bank,String bankcode)
    {
        Octopuscustomerstm ocusttm ;
        List<Octopuscustomerstm> ocusttm_s ;
        Octopuscustomersaccounts ocustac;
        Octopuscustomersaccounts regocustac = new Octopuscustomersaccounts();
        Octopusdelcustomersaccounts delaccount = new Octopusdelcustomersaccounts();
        kernel kn = new kernel();
        OctopusOperations op = new OctopusOperations();
        Tmlogs tmlog = new Tmlogs();
        List<Octopuscustomersaccounts> ocustac_s ;
        JSONObject requestbody = new JSONObject();
        Utilities utils = new Utilities();
        JSONObject customerstatresjson ;
        JSONObject kernelresponse ;
        String auth = Authorization.getAuth();
        String dbpasscode="";
        String kernelresponse_str="";
        boolean passcodestatus;
        String kernelresponsecode="";
        String tmphonenumber="";
        String tmcountry = "";
        String kernelresponsemessage="";
        String tmamount = "";
        String tmaccountnumber = "";
        String tmgender = "";
        String tmdescription = "";
        String tmemailaddress = "";
        String tmlastname = "";
        String tmbank = "";
        String tmdob = "";
        String tmbankitid = "";
        String tmbvn = "";
        JSONArray tmaccounts;
        JSONObject tmresponse = new JSONObject();
        Mailer mail = new Mailer();
        String gender="";
        String dob="";
        String emailaddress="";
        String bankitid="";
        String firstname="";
        String lastname="";
        String country="";
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber,passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
            }else if(loginrescode.equals("22")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "wrong passcode");
            }else{
                //is a customer
                //correct passcode
                logger.info(phonenumber+"  "+sessid+":new delete account request source: "+ source+" "+phonenumber );
                log.info(appname, classname, "New Delete Account Number Request at "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" Account Number: "+acno+" Bank Name: "+bank+" Bank Code: "+bankcode);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new delete account request received");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new delete account request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                boolean deleteaccount=false;
                ocustac_s=DataObjects.getOctopuscustomersaccounts(acno);
                if(ocustac_s.isEmpty()==false){
                    //account already registered
                    for(int i=0;i<ocustac_s.size();i++){
                    ocustac= ocustac_s.get(i);
                    if(ocustac.getPhonenumber().equals(phonenumber)&&ocustac.getBankcode().equalsIgnoreCase(bankcode)){
                        //account already added by you
                        deleteaccount=true;
                        delaccount.setAccountnumber(ocustac.getAccountnumber());
                        delaccount.setBankcode(ocustac.getBankcode());
                        delaccount.setBankitid(ocustac.getBankitid());
                        delaccount.setBankitstatus(ocustac.getBankitstatus());
                        delaccount.setBankname(ocustac.getBankname());
                        delaccount.setBvn(ocustac.getBvn());
                        delaccount.setCreatedat(utils.getDate()+" "+utils.getTime());
                        delaccount.setFirstname(ocustac.getFirstname());
                        delaccount.setIsbankitaccount(ocustac.getIsbankitaccount());
                        delaccount.setLastname(ocustac.getLastname());
                        delaccount.setPasscode(ocustac.getPasscode());
                        delaccount.setPhonenumber(ocustac.getPhonenumber());
                        delaccount.setRecordstatus("deleted");
                        delaccount.setUpdatedat(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(delaccount);
                        PersistenceAgent.deleteObject(ocustac);
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "Account number deleted successfully");
                        logger.info(phonenumber+"  "+sessid+":account deleted successfully source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                        log.info(appname, classname, "Account deleted successfully at "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" Account Number: "+acno+" Bank Name: "+bank+" Bank Code: "+bankcode);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("account deleted successfully");
                        tmlog.setActiondetails("account deleted successfully "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }else if(ocustac.getBankcode().equalsIgnoreCase(bankcode)){
                        //account added by another customer
                        tmresponse.put("responsecode", "33");
                        tmresponse.put("responsemessage", "Sorry, you do not have the permission to delete this account number.");
                        logger.info(phonenumber+"  "+sessid+":no permission to delete account source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                        log.info(appname, classname, "No permission to delete account number. Account number not matched with phonenumber "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" Account Number: "+acno+" Bank Name: "+bank+" Bank Code: "+bankcode);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("no permission to delete account. Account does not belong to phonenumber");
                        tmlog.setActiondetails("Account does not belong to phonenumber "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }else{
                        //account added by another customer
                        tmresponse.put("responsecode", "44");
                        tmresponse.put("responsemessage", "Account number not registered");
                        logger.info(phonenumber+"  "+sessid+":Account number not registered source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                        log.info(appname, classname, "Account number not registered at "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" Account Number: "+acno+" Bank Name: "+bank+" Bank Code: "+bankcode);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("Account number not registered");
                        tmlog.setActiondetails("Account number not registered "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }
                }
                    
                }else{
                        //account added by another customer
                        tmresponse.put("responsecode", "44");
                        tmresponse.put("responsemessage", "Account number not registered");
                        logger.info(phonenumber+"  "+sessid+":Account number not registered source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                        log.info(appname, classname, "Account number not registered at "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" Account Number: "+acno+" Bank Name: "+bank+" Bank Code: "+bankcode);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("Account number not registered");
                        tmlog.setActiondetails("Account number not registered "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }
            }
            
            output=tmresponse.toString();
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String deleteCard(String source,String transid,String sessid,String phonenumber,String passcode,String cid)
    {
        Octopuscustomerstm ocusttm ;
        List<Octopuscustomerstm> ocusttm_s ;
        Octopuscustomerscards ocustca;
        Octopuscustomersaccounts regocustac = new Octopuscustomersaccounts();
        Octopusdelcustomerscards delcard = new Octopusdelcustomerscards();
        kernel kn = new kernel();
        OctopusOperations op = new OctopusOperations();
        Tmlogs tmlog = new Tmlogs();
        List<Octopuscustomerscards> ocustca_s ;
        JSONObject requestbody = new JSONObject();
        Utilities utils = new Utilities();
        JSONObject customerstatresjson ;
        JSONObject kernelresponse ;
        String auth = Authorization.getAuth();
        String dbpasscode="";
        String kernelresponse_str="";
        boolean passcodestatus;
        String kernelresponsecode="";
        String tmphonenumber="";
        String tmcountry = "";
        String kernelresponsemessage="";
        String tmamount = "";
        String tmaccountnumber = "";
        String tmgender = "";
        String tmdescription = "";
        String tmemailaddress = "";
        String tmlastname = "";
        String tmbank = "";
        String tmdob = "";
        String tmbankitid = "";
        String tmbvn = "";
        JSONArray tmaccounts;
        JSONObject tmresponse = new JSONObject();
        Mailer mail = new Mailer();
        String gender="";
        String dob="";
        String emailaddress="";
        String bankitid="";
        String firstname="";
        String lastname="";
        String country="";
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        IpgOperations ipg = new IpgOperations();
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber,passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
            }else if(loginrescode.equals("22")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "wrong passcode");
            }else{
                //is a customer
                //correct passcode
                logger.info(phonenumber+"  "+sessid+":new delete card request source: "+ source+" "+phonenumber );
                log.info(appname, classname, "New Delete Card Request at "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" CID: "+cid);
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new delete card request received");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new delete card request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                boolean deleteaccount=false;
                ocustca=DataObjects.getOctopuscustomerscards_cid(cid);
                String dbphone = (ocustca.getPhonenumber()!=null)?ocustca.getPhonenumber():"";
                if(!dbphone.equals("")){
                    if(ocustca.getPhonenumber().equals(phonenumber)){
                        ipg.removecard(source, transid, sessid, phonenumber, cid);
                        deleteaccount=true;
                        delcard.setCardbank(ocustca.getCardbank());
                        delcard.setCardbrand(ocustca.getCardbrand());
                        delcard.setCardcategory(ocustca.getCardcategory());
                        delcard.setCardcountry(ocustca.getCardcountry());
                        delcard.setCardcountrycode(ocustca.getCardcountrycode());
                        delcard.setCardid(ocustca.getCardid());
                        delcard.setCardname(ocustca.getCardname());
                        delcard.setCardno(ocustca.getCardno());
                        delcard.setCardtype(ocustca.getCardtype());
                        delcard.setCreatedat(utils.getDate()+" "+utils.getTime());
                        delcard.setFirstname(ocustca.getFirstname());
                        delcard.setLastname(ocustca.getLastname());
                        delcard.setPhonenumber(ocustca.getPhonenumber());
                        delcard.setRecordstatus("deleted");
                        delcard.setUpdatedat(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(delcard);
                        PersistenceAgent.deleteObject(ocustca);
                        tmresponse.put("responsecode", "00");
                        tmresponse.put("responsemessage", "Card deleted successfully");
                        logger.info(phonenumber+"  "+sessid+":card deleted successfully source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                        log.info(appname, classname, "Card deleted successfully at "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" CID: "+cid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("card deleted successfully");
                        tmlog.setActiondetails("card deleted successfully "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }else {
                        //account added by another customer
                        tmresponse.put("responsecode", "33");
                        tmresponse.put("responsemessage", "Sorry, you do not have the permission to delete this card.");
                        logger.info(phonenumber+"  "+sessid+":no permission to delete card source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                        log.info(appname, classname, "No permission to delete card. Card not matched with phonenumber "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" CID: "+cid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("no permission to delete card. Card does not belong to phonenumber");
                        tmlog.setActiondetails("Card does not belong to phonenumber "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }
                
                    
                }else{
                        //account added by another customer
                        tmresponse.put("responsecode", "44");
                        tmresponse.put("responsemessage", "Card not registered");
                        logger.info(phonenumber+"  "+sessid+":Card not registered source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                        log.info(appname, classname, "Card not registered at "+utils.getDate()+" "+utils.getTime(), "Phonenumber: "+phonenumber+" CID: "+cid);
                        tmlog.setSource(source);
                        tmlog.setPhonenumber(phonenumber);
                        tmlog.setSessionid(sessid);
                        tmlog.setTransactionid(transid);
                        tmlog.setActionperformed("Card not registered");
                        tmlog.setActiondetails("Card not registered "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                        tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                        PersistenceAgent.logTransactions(tmlog);
                        tmlog = new Tmlogs();
                    }
            }
            
            output=tmresponse.toString();
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String addCard(String source, String transid, String sessid,String phonenumber,String passcode,String cardno,String pin,String expiry,String cvv,String cardname,String country,String currency)
     {
        Octopuscustomerscards regocustca = new Octopuscustomerscards();
        OctopusOperations op = new OctopusOperations();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        IpgOperations ipg= new IpgOperations();
        Mailer mail = new Mailer();
        JSONObject customerstatresjson ;
        JSONObject kernelresponse ;
        String kernelresponse_str="";
        String kernelresponsecode="";
        String kernelresponsemessage="";
        JSONObject tmresponse = new JSONObject();
        String firstname="";
        String lastname="";
        String emailaddress="";
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        System.out.println("class entered "+utils.getDate()+" "+utils.getTime());
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            System.out.println("trying to login "+utils.getDate()+" "+utils.getTime());
            loginresponse_str=op.loginCustomer(source, transid, sessid, phonenumber,passcode,"");
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
           }else if(loginrescode.equals("22")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "wrong passcode");
           }else if(loginrescode.equals("33")){
                //wrong passcode
                tmresponse.put("responsecode", "66");
                tmresponse.put("responsemessage", "Account blocked. Please input the 4 digit token sent to "+phonenumber.substring(0,6)+"xxxx"+phonenumber.substring(11,13)+" to reset account");
            }else{
                System.out.println("logged in "+utils.getDate()+" "+utils.getTime());
                logger.info(phonenumber+"  "+sessid+":new add card request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new add card request received");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new add card request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                //is a customer
                //register card
                System.out.println("customer stat request "+utils.getDate()+" "+utils.getTime());
                String customerstatres = op.customerStatus(source, transid, sessid, phonenumber, "NG");
                System.out.println("customer stat response "+utils.getDate()+" "+utils.getTime());
                customerstatresjson = new JSONObject(customerstatres);
                firstname = customerstatresjson.getString("firstname");
                lastname = customerstatresjson.getString("lastname");
                emailaddress = customerstatresjson.getString("emailaddress");
                logger.info(phonenumber+"  "+sessid+": calling kernel to register card "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("calling kernel to register card");
                tmlog.setActiondetails("calling kernel to register card "+phonenumber+" " );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                System.out.println("calling add card ipg "+utils.getDate()+" "+utils.getTime());
                kernelresponse_str=ipg.addcard(source, transid, sessid, phonenumber, cardno, pin, expiry, cvv, cardname, country, currency);
                System.out.println("add card response "+utils.getDate()+" "+utils.getTime());
                System.out.println("kernel response \n"+kernelresponse_str+"\n");
                kernelresponse= new JSONObject(kernelresponse_str);   
                kernelresponsecode =kernelresponse.getString("responsecode");
                logger.info(phonenumber+"  "+sessid+": kernel response "+ source+" "+kernelresponse.toString().replace("\"", ":") );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("kernel response");
                tmlog.setActiondetails("kernel response "+phonenumber+" "+kernelresponse.toString().replace("\"", ":") );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                if(kernelresponsecode.equals("00")){
                    //reg success
                    String cid =kernelresponse.getString("cid");
                    String last4 =kernelresponse.getString("last4");
                    String cardbank =kernelresponse.getString("bank");
                    String kncardname =kernelresponse.getString("cardname");
                    String cardcategory =kernelresponse.getString("cardcategory");
                    String cardbrand =kernelresponse.getString("brand");
                    String cardcountry =kernelresponse.getString("cardcountry");
                    String cardcountrycode =kernelresponse.getString("cardcountrycode");
                    String cardtype =kernelresponse.getString("cardtype");
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    tmresponse.put("cid", cid);
                    tmresponse.put("cardno", cardno);
                    regocustca.setCardid(cid);
                    regocustca.setCardname(kncardname);
                    regocustca.setCardno("xxxx-xxxx-xxxx-"+last4);
                    regocustca.setCardtype(cardtype);
                    regocustca.setCardbank(cardbank);
                    regocustca.setCardbrand(cardbrand);
                    regocustca.setCardcategory(cardcategory);
                    regocustca.setCardcountry(cardcountry);
                    regocustca.setCardcountrycode(cardcountrycode);
                    regocustca.setFirstname(firstname);
                    regocustca.setLastname(lastname);
                    regocustca.setPhonenumber(phonenumber);
                    regocustca.setRecordstatus("active");
                    regocustca.setCreatedat(utils.getDate()+" "+utils.getTime());
                    regocustca.setUpdatedat(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(regocustca);
                    logger.info(phonenumber+"  "+sessid+": card successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("card successfully added");
                    tmlog.setActiondetails("card successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    mail.addCardNotification(phonenumber, emailaddress, firstname+" "+lastname, cardbank, "xxxx-xxxx-xxxx-"+last4);
                    log.info(appname, classname, "Add Card Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Card No "+"xxxx-xxxx-xxxx-"+last4);
                    logger.info(phonenumber+"  "+sessid+":Mail sent: Card successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("Mail sent. Card successfully added");
                    tmlog.setActiondetails("Mail sent. Card successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                }else if(kernelresponsecode.equals("T0")){
                    //otp sent
                    String cid =kernelresponse.getString("cid");
                    kernelresponsecode =kernelresponse.getString("responsemessage");
                    tmresponse.put("responsecode", "33");
                    tmresponse.put("responsemessage", kernelresponsecode);
                    tmresponse.put("cid", cid);
                    logger.info(phonenumber+"  "+sessid+": otp sent "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("otp sent");
                    tmlog.setActiondetails("otp sent addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else if(kernelresponsecode.equals("M0")){
                    //otp sent
                    String cid =kernelresponse.getString("cid");
                    String transactionref =kernelresponse.getString("transactionref");
                    kernelresponsecode =kernelresponse.getString("responsemessage");
                    tmresponse.put("responsecode", "44");
                    tmresponse.put("responsemessage", kernelresponsecode);
                    tmresponse.put("cid", cid);
                    tmresponse.put("transactionref", transactionref);
                    logger.info(phonenumber+"  "+sessid+": customer needs to enrolphone "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("customer needs to enrolphone");
                    tmlog.setActiondetails("customer needs to enrolphone addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    //reg failed
                    kernelresponsecode =kernelresponse.getString("responsemessage");
                    tmresponse.put("responsecode", "55");
                    tmresponse.put("responsemessage", kernelresponsecode);
                    logger.info(phonenumber+"  "+sessid+": card cannot be added now "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("card cannot be added now");
                    tmlog.setActiondetails("card cannot be added now "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();

                }
            }
            output=tmresponse.toString();
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String addCardValidate(String source, String transid, String sessid,String phonenumber,String cid,String otp)
    {
        Octopuscustomerscards regocustca = new Octopuscustomerscards();
        OctopusOperations op = new OctopusOperations();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        IpgOperations ipg= new IpgOperations();
        Mailer mail = new Mailer();
        JSONObject customerstatresjson ;
        JSONObject kernelresponse ;
        String kernelresponse_str="";
        String kernelresponsecode="";
        JSONObject tmresponse = new JSONObject();
        String firstname="";
        String lastname="";
        String emailaddress="";
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                //is a customer
                //register card
                logger.info(phonenumber+"  "+sessid+":new add card validate request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new add card validate request received");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new add card validate request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                String customerstatres = op.customerStatus(source, transid, sessid, phonenumber, "NG");
                customerstatresjson = new JSONObject(customerstatres);
                firstname = customerstatresjson.getString("firstname");
                lastname = customerstatresjson.getString("lastname");
                emailaddress = customerstatresjson.getString("emailaddress");
                logger.info(phonenumber+"  "+sessid+": calling kernel to register card "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("calling kernel to register card");
                tmlog.setActiondetails("calling kernel to register card "+phonenumber+" " );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                kernelresponse_str=ipg.addcardvalidate(source, transid, sessid, phonenumber, cid, otp);
                System.out.println("kernel response \n"+kernelresponse_str+"\n");
                logger.info(phonenumber+"  "+sessid+": kernel response "+ source+" "+kernelresponse_str );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("kernel response");
                tmlog.setActiondetails("kernel response "+phonenumber+" "+kernelresponse_str );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                kernelresponse= new JSONObject(kernelresponse_str);   
                kernelresponsecode =kernelresponse.getString("responsecode");
                String kernelresponsemessage =kernelresponse.getString("responsemessage");
                if(kernelresponsecode.equals("00")){
                    //reg success
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    String kncid =kernelresponse.getString("cid");
                    String last4 =kernelresponse.getString("last4");
                    String cardbank =kernelresponse.getString("bank");
                    String kncardname =kernelresponse.getString("cardname");
                    String cardcategory =kernelresponse.getString("cardcategory");
                    String cardbrand =kernelresponse.getString("brand");
                    String cardcountry =kernelresponse.getString("cardcountry");
                    String cardcountrycode =kernelresponse.getString("cardcountrycode");
                    String cardtype =kernelresponse.getString("cardtype");
                    regocustca.setCardid(kncid);
                    regocustca.setCardname(kncardname);
                    regocustca.setCardno("xxxx-xxxx-xxxx-"+last4);
                    regocustca.setCardtype(cardtype);
                    regocustca.setCardbank(cardbank);
                    regocustca.setCardbrand(cardbrand);
                    regocustca.setCardcategory(cardcategory);
                    regocustca.setCardcountry(cardcountry);
                    regocustca.setCardcountrycode(cardcountrycode);
                    regocustca.setFirstname(firstname);
                    regocustca.setLastname(lastname);
                    regocustca.setPhonenumber(phonenumber);
                    regocustca.setRecordstatus("active");
                    regocustca.setCreatedat(utils.getDate()+" "+utils.getTime());
                    regocustca.setUpdatedat(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(regocustca);
                    logger.info(phonenumber+"  "+sessid+": card successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("card successfully added");
                    tmlog.setActiondetails("card successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                    mail.addCardNotification(phonenumber, emailaddress, firstname+" "+lastname, cardbank, "xxxx-xxxx-xxxx-"+last4);
                    log.info(appname, classname, "Add Card Notification Mail sent at"+utils.getDate()+" "+utils.getTime(), "Phonenumber"+" "+phonenumber+", Card No "+"xxxx-xxxx-xxxx-"+last4);
                    logger.info(phonenumber+"  "+sessid+":Mail sent. Card successfully added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("Mail sent. Card successfully added");
                    tmlog.setActiondetails("Mail sent. Card successfully addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());

                }else if(kernelresponsecode.equals("T1")){
                    //otp sent
                    tmresponse.put("responsecode", "33");
                    tmresponse.put("responsemessage", "invalid otp "+kernelresponsemessage);
                    logger.info(phonenumber+"  "+sessid+": invalid otp  "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("invalid otp");
                    tmlog.setActiondetails("invalid otp kernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    //reg failed
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "Card cannot be added now "+kernelresponsemessage);
                    logger.info(phonenumber+"  "+sessid+": card cannot be added now "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("card cannot be added now");
                    tmlog.setActiondetails("card cannot be added now "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
            }
            output=tmresponse.toString();
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
    
    public String enrolCardPhone(String source, String transid, String sessid,String phonenumber,String enrolphonenumber,String transactionref,String cid,String otp)
    {
        Octopuscustomerscards regocustca = new Octopuscustomerscards();
        OctopusOperations op = new OctopusOperations();
        Tmlogs tmlog = new Tmlogs();
        Utilities utils = new Utilities();
        IpgOperations ipg= new IpgOperations();
        JSONObject customerstatresjson ;
        JSONObject kernelresponse ;
        String kernelresponse_str="";
        String kernelresponsecode="";
        JSONObject tmresponse = new JSONObject();
        String firstname="";
        String lastname="";
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        String last4="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try{
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                //is a customer
                //register card
                logger.info(phonenumber+"  "+sessid+":new add card validate request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new add card validate request received");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new add card validate request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                String customerstatres = op.customerStatus(source, transid, sessid, phonenumber, "NG");
                customerstatresjson = new JSONObject(customerstatres);
                firstname = customerstatresjson.getString("firstname");
                lastname = customerstatresjson.getString("lastname");
                logger.info(phonenumber+"  "+sessid+": calling kernel to register card "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("calling kernel to register card");
                tmlog.setActiondetails("calling kernel to register card "+phonenumber+" " );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                kernelresponse_str=ipg.enrolphonewithcard(source, transid, sessid, phonenumber, enrolphonenumber, transactionref, cid);
                System.out.println("kernel response \n"+kernelresponse_str+"\n");
                logger.info(phonenumber+"  "+sessid+": kernel response "+ source+" "+kernelresponse_str );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("kernel response");
                tmlog.setActiondetails("kernel response "+phonenumber+" "+kernelresponse_str );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                kernelresponse= new JSONObject(kernelresponse_str);   
                kernelresponsecode =kernelresponse.getString("responsecode");
                if(kernelresponsecode.equals("T0")){
                    //otp sent
                    String kncid =kernelresponse.getString("cid");
                    kernelresponsecode =kernelresponse.getString("responsemessage");
                    tmresponse.put("responsecode", "33");
                    tmresponse.put("responsemessage", kernelresponsecode);
                    tmresponse.put("cid", kncid);
                    logger.info(phonenumber+"  "+sessid+": otp sent "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("otp sent");
                    tmlog.setActiondetails("otp sent addedkernel response "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    //reg failed
                    kernelresponsecode =kernelresponse.getString("responsemessage");
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", kernelresponsecode);
                    logger.info(phonenumber+"  "+sessid+": card cannot be added now "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("card cannot be added now");
                    tmlog.setActiondetails("card cannot be added now "+phonenumber+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
            }
            output=tmresponse.toString();
            
        }catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
    return output;}
     
    
    public String blacklistCustomer(String source,String transid,String sessid,String phonenumber,String blacklistcustomer)
    {
        Tmlogs tmlog = new Tmlogs();
        Octopusblacklist blacklist = new Octopusblacklist();
        List<Octopusblacklist> allblacklist;
        Octopusblacklist perblacklist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        OctopusOperations op = new OctopusOperations();
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        List<Octopusinboxtransactions> ocinboxlist;
        Octopusinboxtransactions ocinbox;
        phonenumber=utils.formatPhone(phonenumber);
        blacklistcustomer=utils.formatPhone(blacklistcustomer);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                allblacklist = DataObjects.getOctopusblacklist(blacklistcustomer);
                logger.info(phonenumber+"  "+sessid+":new blacklist customer request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new blacklist customer  request");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new blacklist customer request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                String existbene = "false";
                int j=0;
                if(allblacklist.isEmpty()==false){
                    for(int i=0;i<allblacklist.size();i++){
                        perblacklist=allblacklist.get(i);
                        if(perblacklist.getBlacklistedby().equals(phonenumber)&&perblacklist.getStatus().equalsIgnoreCase("active")){
                           existbene="true"; 
                        }
                        if(perblacklist.getBlacklistedby().equals(phonenumber)&&perblacklist.getStatus().equalsIgnoreCase("inactive")){
                           existbene="exist"; 
                           j=i;
                        }
                    }
                }
                if(existbene.equals("false")){
                    blacklist.setBlacklistedby(phonenumber);
                    blacklist.setPhonenumber(blacklistcustomer);
                    blacklist.setStatus("active");
                    blacklist.setUpdatedat(utils.getDate()+" "+utils.getTime());
                    blacklist.setCreatedat(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(blacklist);
                    ocinboxlist= DataObjects.getOctopusinboxtransactions(phonenumber);
                    System.out.println("size:"+ ocinboxlist.size());
                    if(ocinboxlist.size()>0){
                        for(int i=0;i<ocinboxlist.size();i++){
                            ocinbox=ocinboxlist.get(i);
                            if(ocinbox.getSenderid().equals(phonenumber)&&ocinbox.getReceiverid().equals(blacklistcustomer)&&ocinbox.getStatus().equalsIgnoreCase("unapproved")){
                                ocinbox.setStatus("deleted");
                                PersistenceAgent.logTransactions(ocinbox);
                            }
                        }
                    }
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "Customer blacklisted successfully");
                    logger.info(phonenumber+"  "+sessid+":save blacklist customer  source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("blacklist customer  success");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":blacklist customer  success "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else if(existbene.equals("exist")){
                    blacklist=allblacklist.get(j);
                    blacklist.setStatus("active");
                    blacklist.setUpdatedat(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(blacklist);
                    ocinboxlist= DataObjects.getOctopusinboxtransactions(phonenumber);
                    System.out.println("size:"+ ocinboxlist.size());
                    if(ocinboxlist.size()>0){
                        for(int i=0;i<ocinboxlist.size();i++){
                            ocinbox=ocinboxlist.get(i);
                            if(ocinbox.getSenderid().equals(phonenumber)&&ocinbox.getReceiverid().equals(blacklistcustomer)&&ocinbox.getStatus().equalsIgnoreCase("unapproved")){
                                ocinbox.setStatus("deleted");
                                PersistenceAgent.logTransactions(ocinbox);
                            }
                        }
                    }
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "Customer blacklisted successfully");
                    logger.info(phonenumber+"  "+sessid+":save blacklist customer  source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("blacklist customer  success");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":blacklist customer  success "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "Customer already blacklisted");
                    logger.info(phonenumber+"  "+sessid+":Customer already blacklisted source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("Customer already blacklisted");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":Customer already blacklisted "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String unBlacklistCustomer(String source,String transid,String sessid,String phonenumber,String blacklistcustomer)
    {
        Tmlogs tmlog = new Tmlogs();
        Octopusblacklist blacklist = new Octopusblacklist();
        List<Octopusblacklist> allblacklist;
        Octopusblacklist perblacklist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        OctopusOperations op = new OctopusOperations();
        String output="";
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        blacklistcustomer=utils.formatPhone(blacklistcustomer);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                allblacklist = DataObjects.getOctopusblacklist(blacklistcustomer);
                logger.info(phonenumber+"  "+sessid+":new blacklist customer request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new blacklist customer  request");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new blacklist customer request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                String existbene = "false";
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "Customer not on your blacklist");
                if(allblacklist.isEmpty()==false){
                    for(int i=0;i<allblacklist.size();i++){
                        perblacklist=allblacklist.get(i);
                        if(perblacklist.getBlacklistedby().equals(phonenumber)){
                            perblacklist.setStatus("inactive");
                            perblacklist.setUpdatedat(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(perblacklist);
                            tmresponse.put("responsecode", "00");
                            tmresponse.put("responsemessage", "Customer removed from blacklist successfully");
                            logger.info(phonenumber+"  "+sessid+":Customer removed from blacklist successfully source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setSource(source);
                            tmlog.setPhonenumber(phonenumber);
                            tmlog.setSessionid(sessid);
                            tmlog.setTransactionid(transid);
                            tmlog.setActionperformed("Customer removed from blacklist successfully");
                            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":Customer removed from blacklist successfully "+ source+" "+tmresponse.toString().replace("\"", ":") );
                            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                            PersistenceAgent.logTransactions(tmlog);
                            tmlog = new Tmlogs();
                        }
                    }
                }
            }
                
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String addBeneficiary(String source,String transid,String sessid,String phonenumber,String benephone,String beneaccount,String benename,String benebank,String benebankcode,String customerid,String paymentcode,String benetype)
    {
        Tmlogs tmlog = new Tmlogs();
        Beneficiaries bene = new Beneficiaries();
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        Beneficiaries benee ;
        List<Beneficiaries> benelist;
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                benelist = DataObjects.getBeneficiaries(phonenumber);
                logger.info(phonenumber+"  "+sessid+":new save beneficiary request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new save beneficiary request");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new save beneficiary request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                String existbene = "false";
                for(int i=0;i<benelist.size();i++){
                    benee=benelist.get(i);
                    if(benetype.equals("ft")){
                        if(benee.getBeneaccount().equals(beneaccount)){
                           existbene="true"; 
                        }
                    }else{
                        if(benee.getCustomerid().equalsIgnoreCase(customerid)){
                           existbene="true"; 
                        }
                    }
                }
                if(existbene.equals("false")){
                    //get biller details based on paymentcode
                    //a
                    iswPaymentitems PaymentCodeDetails = DataObjects.getiswPaymentitems_paymentcodeObject(paymentcode);   
                    if(PaymentCodeDetails.getBiller_name() != null){
                        bene.setAmount(Utilities.round(Double.valueOf(PaymentCodeDetails.getAmount())/100,2));
                        bene.setBillerid(PaymentCodeDetails.getBiller_id());
                        bene.setBillername(PaymentCodeDetails.getBiller_name());
                        bene.setCategoryid(PaymentCodeDetails.getCategory_id());
                        bene.setIsamountfixed(PaymentCodeDetails.getIsamountfixed());
                        bene.setItemfee(Utilities.round(Double.valueOf(PaymentCodeDetails.getItemfee())/100,2));
                        bene.setPaymentitemname(PaymentCodeDetails.getPaymentitem_name());
                        bene.setPaymentitemid(PaymentCodeDetails.getPaymentitem_id());
                    }
                    
                    bene.setBeneaccount(beneaccount);
                    bene.setBenebank(benebank);
                    bene.setBenename(benename);
                    bene.setBenephonenumber(benephone);
                    bene.setPhonenumber(phonenumber);
                    bene.setBenebankcode(benebankcode);
                    bene.setCustomerid(customerid);
                    bene.setPaymentcode(paymentcode);
                    bene.setBenetype(benetype);
                    bene.setStatus("active");
                    bene.setCreatedat(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(bene);
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    logger.info(phonenumber+"  "+sessid+":save beneficiary success source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("save beneficiary success");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":save beneficiary success "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "beneficiary already added");
                    logger.info(phonenumber+"  "+sessid+":beneficiary already added source: "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("beneficiary already added");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":beneficiary already added "+ source+" "+tmresponse.toString().replace("\"", ":") );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String fetchBeneficiary(String source,String transid,String sessid,String phonenumber)
    {
        Tmlogs tmlog = new Tmlogs();
        Beneficiaries bene ;
        List<Beneficiaries> benelist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        JSONArray ary2 = new JSONArray();
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a customer");
           }else{
                logger.info(phonenumber+"  "+sessid+":new beneficiary list request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new beneficiary list request");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new beneficiary list request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                benelist = DataObjects.getBeneficiaries(phonenumber);
                if(benelist.size()<1){
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "no saved beneficiaries"); 
                    logger.info(phonenumber+"  "+sessid+":no saved beneficiaries: "+ source+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("no saved beneficiaries");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no saved beneficiaries "+ source+" " );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    for(int i=0;i<benelist.size();i++){
                        bene=benelist.get(i);
                        String dbbenetype=bene.getBenetype();
                        if(dbbenetype.equals("ft")){
                            JSONObject eachobject = new JSONObject();
                            eachobject.put("benealias", bene.getBenealias()+"");
                            eachobject.put("benephone", bene.getBenephonenumber()+"");
                            eachobject.put("benename", bene.getBenename()+"");
                            eachobject.put("benebank", bene.getBenebank()+"");
                            eachobject.put("beneaccount",bene.getBeneaccount()+"");
                            eachobject.put("benebankcode",bene.getBenebankcode()+"");
                            eachobject.put("sno", bene.getId());
                            ary.put(eachobject);
                        }
                        if(dbbenetype.equals("bp")){
                            JSONObject eachobject2 = new JSONObject();
                            eachobject2.put("benealias", bene.getBenealias()+"");
                            eachobject2.put("benephone", bene.getBenephonenumber()+"");
                            eachobject2.put("customerid", bene.getCustomerid()+"");
                            eachobject2.put("paymentcode", bene.getPaymentcode()+"");
                            eachobject2.put("billername", bene.getBillername()+"");
                            eachobject2.put("paymentitemname", bene.getPaymentitemname()+"");
                            eachobject2.put("billerid", bene.getBillerid()+"");
                            eachobject2.put("amount", bene.getAmount()+"");
                            eachobject2.put("fee", bene.getItemfee()+"");
                            eachobject2.put("amountfixed", bene.getIsamountfixed()+"");
                            eachobject2.put("categoryid", bene.getCategoryid()+"");
                            ary2.put(eachobject2);
                        }
                    }
                    tmresponse.put("fundstransfer", ary);
                    tmresponse.put("billspayment", ary2);
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    logger.info(phonenumber+"  "+sessid+":beneficiary request success: "+ source+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("beneficiary request success");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":beneficiary request success "+ source+" " );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
                output=tmresponse.toString();
            }
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String sourceAccounts(String source,String transid,String sessid,String phonenumber)
    {
        Tmlogs tmlog = new Tmlogs();
        Octopuscustomersaccounts acc ;
        List<Octopuscustomersaccounts> acclist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        OctopusOperations op = new OctopusOperations();
        String loginresponse_str="";
        String loginrescode="";
        JSONObject loginresponse;
        String firstname="";
        String lastname="";
        String accountnumber="";
        String bankname="";
        String bankcode="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            loginresponse_str=op.isCustomer(source, transid, sessid, phonenumber);
            loginresponse = new JSONObject(loginresponse_str);
            loginrescode=loginresponse.getString("responsecode");
            if(loginrescode.equals("11")){
                //not a customer
                tmresponse.put("responsecode", "22");
                tmresponse.put("responsemessage", "not a customer");
            }else if(loginrescode.equals("00")){
                logger.info(phonenumber+"  "+sessid+":new source accounts request source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("new source accounts request");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new source accounts request source: "+ source+" "+phonenumber );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
                acclist = DataObjects.getOctopuscustomersaccounts_rec(phonenumber);
                if(acclist.size()<1){
                    tmresponse.put("responsecode", "11");
                    tmresponse.put("responsemessage", "no account"); 
                    logger.info(phonenumber+"  "+sessid+":no account: "+ source+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("no no account");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no account "+ source+" " );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }else{
                    for(int i=0;i<acclist.size();i++){
                        acc=acclist.get(i);
                        JSONObject eachobject = new JSONObject();
                        firstname=acc.getFirstname();
                        lastname=acc.getLastname();
                        accountnumber=acc.getAccountnumber();
                        bankname=acc.getBankname();
                        bankcode=acc.getBankcode();
                        eachobject.put("firstname", firstname);
                        eachobject.put("lastname", lastname);
                        eachobject.put("accountnumber", accountnumber);
                        eachobject.put("bankname",bankname);
                        eachobject.put("bankcode", bankcode);
                        ary.put(eachobject);
                    }
                    tmresponse.put("result", ary);
                    tmresponse.put("responsecode", "00");
                    tmresponse.put("responsemessage", "success");
                    logger.info(phonenumber+"  "+sessid+":source accounts request success: "+ source+" " );
                    tmlog.setSource(source);
                    tmlog.setPhonenumber(phonenumber);
                    tmlog.setSessionid(sessid);
                    tmlog.setTransactionid(transid);
                    tmlog.setActionperformed("source accounts success");
                    tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":source accounts success "+ source+" " );
                    tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                    PersistenceAgent.logTransactions(tmlog);
                    tmlog = new Tmlogs();
                }
                output=tmresponse.toString();
            }
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String iswCategorys(String source,String transid,String sessid,String phonenumber)
    {
        Tmlogs tmlog = new Tmlogs();
        iswCategorys cat ;
        List<iswCategorys> catlist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        String categoryname="";
        String categoryid="";
        String categorydesc="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new biller categorys request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("biller categorys");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new biller categorys request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            catlist = DataObjects.getiswCategorys();
            if(catlist.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no biller categorys"); 
                logger.info(phonenumber+"  "+sessid+":no biller categorys: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no biller categorys");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no biller categorys "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                for(int i=0;i<catlist.size();i++){
                    cat=catlist.get(i);
                    JSONObject eachobject = new JSONObject();
                    categoryname=cat.getCategory_name();
                    categoryid=cat.getCategory_id();
                    categorydesc=cat.getCategory_desc();
                    eachobject.put("categoryname", categoryname);
                    eachobject.put("categoryid", categoryid);
                    eachobject.put("categorydesc", categorydesc);
                    ary.put(eachobject);
                }
                tmresponse.put("result", ary);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":biller categorys request success: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("biller categorys success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":biller categorys success "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String iswBillers(String source,String transid,String sessid,String phonenumber)
    {
        Tmlogs tmlog = new Tmlogs();
        iswBillers biller ;
        List<iswBillers> billerlist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        String billername="";
        String billerid="";
        String categoryid="";
        String field1="";
        String field2="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new biller list request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("biller list");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new biller list request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            billerlist = DataObjects.getiswBillers();
            if(billerlist.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no biller list"); 
                logger.info(phonenumber+"  "+sessid+":no biller list: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no biller list");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no biller list "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                for(int i=0;i<billerlist.size();i++){
                    biller=billerlist.get(i);
                    JSONObject eachobject = new JSONObject();
                    billername=biller.getBiller_name();
                    billerid=biller.getBiller_id();
                    categoryid=biller.getCategory_id();
                    field1=biller.getCustomerfield1();
                    field2=biller.getCustomerfield2();
                    eachobject.put("billername", billername);
                    eachobject.put("billerid", billerid);
                    eachobject.put("categoryid", categoryid);
                    eachobject.put("field1", field1);
                    eachobject.put("field2", field2);
                    ary.put(eachobject);
                }
                tmresponse.put("result", ary);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":biller list request success: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("biller list success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":biller list success "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String iswBillers_catid(String source,String transid,String sessid,String phonenumber,String categoryid)
    {
        Tmlogs tmlog = new Tmlogs();
        iswBillers biller ;
        List<iswBillers> billerlist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        String billername="";
        String billerid="";
        String field1="";
        String field2="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new biller list request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("biller list");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new biller list request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            billerlist = DataObjects.getiswBillers_billerid(categoryid);
            System.out.println("size ");
            if(billerlist.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no biller list"); 
                logger.info(phonenumber+"  "+sessid+":no biller list: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no biller list");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no biller list "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                for(int i=0;i<billerlist.size();i++){
                    biller=billerlist.get(i);
                    JSONObject eachobject = new JSONObject();
                    billername=biller.getBiller_name();
                    billerid=biller.getBiller_id();
                    categoryid=biller.getCategory_id();
                    field1=biller.getCustomerfield1();
                    field2=biller.getCustomerfield2();
                    eachobject.put("billername", billername);
                    eachobject.put("billerid", billerid);
                    eachobject.put("categoryid", categoryid);
                    eachobject.put("field1", field1);
                    eachobject.put("field2", field2);
                    ary.put(eachobject);
                }
                tmresponse.put("result", ary);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":biller list request success: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("biller list success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":biller list success "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String iswPaymentitems(String source,String transid,String sessid,String phonenumber)
    {
        Tmlogs tmlog = new Tmlogs();
        iswPaymentitems paymentitems ;
        List<iswPaymentitems> paymentitemslist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        String billername="";
        String billerid="";
        String categoryid="";
        String paymentitemid="";
        String paymentcode="";
        String paymentitemname="";
        String isamountfixed="";
        String amount="";
        String itemfee="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("paymentitem list");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            paymentitemslist = DataObjects.getiswPaymentitems();
            if(paymentitemslist.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no paymentitem list"); 
                logger.info(phonenumber+"  "+sessid+":no paymentitem list: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no paymentitem list");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no paymentitem list "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                for(int i=0;i<paymentitemslist.size();i++){
                    paymentitems=paymentitemslist.get(i);
                    JSONObject eachobject = new JSONObject();
                    billername=paymentitems.getBiller_name();
                    billerid=paymentitems.getBiller_id();
                    categoryid=paymentitems.getCategory_id();
                    paymentitemid=paymentitems.getPaymentitem_id();
                    paymentcode=paymentitems.getPaymentcode();
                    paymentitemname=paymentitems.getPaymentitem_name();
                    isamountfixed=paymentitems.getIsamountfixed();
                    amount = paymentitems.getAmount();
                    itemfee = paymentitems.getItemfee();
                    eachobject.put("billername", billername);
                    eachobject.put("billerid", billerid);
                    eachobject.put("categoryid", categoryid);
                    eachobject.put("paymentitemid", paymentitemid);
                    eachobject.put("paymentcode", paymentcode);
                    eachobject.put("paymentitemname", paymentitemname);
                    eachobject.put("isamountfixed", isamountfixed);
                    eachobject.put("amount", amount);
                    eachobject.put("itemfee", itemfee);
                    ary.put(eachobject);
                }
                tmresponse.put("result", ary);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":paymentitem list request success: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("paymentitem list success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":paymentitem list success "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String iswPaymentitems_billerid(String source,String transid,String sessid,String phonenumber,String billerid)
    {
        Tmlogs tmlog = new Tmlogs();
        iswPaymentitems paymentitems ;
        List<iswPaymentitems> paymentitemslist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        String billername="";
        String categoryid="";
        String paymentitemid="";
        String paymentcode="";
        String paymentitemname="";
        String isamountfixed="";
        String amount="";
        String itemfee="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("paymentitem list");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            paymentitemslist = DataObjects.getiswPaymentitems_billerid(billerid);
            if(paymentitemslist.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no paymentitem list"); 
                logger.info(phonenumber+"  "+sessid+":no paymentitem list: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no paymentitem list");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no paymentitem list "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                for(int i=0;i<paymentitemslist.size();i++){
                    paymentitems=paymentitemslist.get(i);
                    JSONObject eachobject = new JSONObject();
                    billername=paymentitems.getBiller_name();
                    billerid=paymentitems.getBiller_id();
                    categoryid=paymentitems.getCategory_id();
                    paymentitemid=paymentitems.getPaymentitem_id();
                    paymentcode=paymentitems.getPaymentcode();
                    paymentitemname=paymentitems.getPaymentitem_name();
                    isamountfixed=paymentitems.getIsamountfixed();
                    amount = paymentitems.getAmount();
                    itemfee = paymentitems.getItemfee();
                    eachobject.put("billername", billername);
                    eachobject.put("billerid", billerid);
                    eachobject.put("categoryid", categoryid);
                    eachobject.put("paymentitemid", paymentitemid);
                    eachobject.put("paymentcode", paymentcode);
                    eachobject.put("paymentitemname", paymentitemname);
                    eachobject.put("isamountfixed", isamountfixed);
                    eachobject.put("amount", amount);
                    eachobject.put("itemfee", itemfee);
                    ary.put(eachobject);
                }
                tmresponse.put("result", ary);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":paymentitem list request success: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("paymentitem list success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":paymentitem list success "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String iswPaymentitems_paymentcode(String source,String transid,String sessid,String phonenumber,String paymentcode)
    {
        Tmlogs tmlog = new Tmlogs();
        iswPaymentitems paymentitems ;
        List<iswPaymentitems> paymentitemslist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        String billername="";
        String categoryid="";
        String paymentitemid="";
        String billerid="";
        String paymentitemname="";
        String isamountfixed="";
        String amount="";
        String itemfee="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("paymentitem list");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            paymentitemslist = DataObjects.getiswPaymentitems_paymentcode(paymentcode);
            if(paymentitemslist.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no paymentitem list"); 
                logger.info(phonenumber+"  "+sessid+":no paymentitem list: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no paymentitem list");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no paymentitem list "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                paymentitems=paymentitemslist.get(0);
                JSONObject eachobject = new JSONObject();
                billername=paymentitems.getBiller_name();
                billerid=paymentitems.getBiller_id();
                categoryid=paymentitems.getCategory_id();
                paymentitemid=paymentitems.getPaymentitem_id();
                paymentcode=paymentitems.getPaymentcode();
                paymentitemname=paymentitems.getPaymentitem_name();
                isamountfixed=paymentitems.getIsamountfixed();
                amount = paymentitems.getAmount();
                itemfee = paymentitems.getItemfee();
                tmresponse.put("billername", billername);
                tmresponse.put("billerid", billerid);
                tmresponse.put("categoryid", categoryid);
                tmresponse.put("paymentitemid", paymentitemid);
                tmresponse.put("paymentcode", paymentcode);
                tmresponse.put("paymentitemname", paymentitemname);
                tmresponse.put("isamountfixed", isamountfixed);
                tmresponse.put("amount", amount);
                tmresponse.put("itemfee", itemfee);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":paymentitem list request success: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("paymentitem list success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":paymentitem list success "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String iswMobileRecharge(String source,String transid,String sessid,String phonenumber)
    {
        Tmlogs tmlog = new Tmlogs();
        iswPaymentitems paymentitems ;
        List<iswPaymentitems> paymentitemslist;
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        JSONArray ary = new JSONArray();
        String billername="";
        String categoryid="";
        String paymentitemid="";
        String paymentcode="";
        String paymentitemname="";
        String isamountfixed="";
        String amount="";
        String itemfee="";
        String billerid="";
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("paymentitem list");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new paymentitem list request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            paymentitemslist = DataObjects.getiswPaymentitems_recharge("3");
            if(paymentitemslist.size()<1){
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "no paymentitem list"); 
                logger.info(phonenumber+"  "+sessid+":no paymentitem list: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("no paymentitem list");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":no paymentitem list "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                for(int i=0;i<paymentitemslist.size();i++){
                    paymentitems=paymentitemslist.get(i);
                    JSONObject eachobject = new JSONObject();
                    billername=paymentitems.getBiller_name();
                    billerid=paymentitems.getBiller_id();
                    categoryid=paymentitems.getCategory_id();
                    paymentitemid=paymentitems.getPaymentitem_id();
                    paymentcode=paymentitems.getPaymentcode();
                    paymentitemname=paymentitems.getPaymentitem_name();
                    isamountfixed=paymentitems.getIsamountfixed();
                    amount = paymentitems.getAmount();
                    itemfee = paymentitems.getItemfee();
                    if(amount.equals("0")){
                        eachobject.put("billername", billername);
                        eachobject.put("billerid", billerid);
                        eachobject.put("categoryid", categoryid);
                        eachobject.put("paymentitemid", paymentitemid);
                        eachobject.put("paymentcode", paymentcode);
                        eachobject.put("paymentitemname", paymentitemname);
                        eachobject.put("isamountfixed", isamountfixed);
                        eachobject.put("amount", amount);
                        eachobject.put("itemfee", itemfee);
                        ary.put(eachobject);
                    }
                }
                tmresponse.put("result", ary);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":paymentitem list request success: "+ source+" " );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("paymentitem list success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":paymentitem list success "+ source+" " );
                tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    public String validateReference(String source,String transid,String sessid,String phonenumber,String custid,String paymentcode)
    {
        
        Tmlogs tmlog = new Tmlogs();
        JSONObject tmresponse = new JSONObject();
        Utilities utils = new Utilities();
        String output="";
        QtServices qts = new QtServices();
        String qtresponse_str="";
        String qtresponsecode="";
        String qtfullname="";
        JSONObject qtresponse;
        phonenumber=utils.formatPhone(phonenumber);
        try {
            tmresponse.put("requestdate", utils.getDate());
            tmresponse.put("requesttime", utils.getTime());
            logger.info(phonenumber+"  "+sessid+":new validate biller reference request source: "+ source+" "+phonenumber+" "+custid+" "+paymentcode );
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("validate biller reference ");
            tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new validate biller reference  request source: "+ source+" "+phonenumber );
            tmlog.setRequesttime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
            tmlog = new Tmlogs();
            
            qtresponse_str=qts.customerValidation(phonenumber, custid, paymentcode);
            qtresponse= new JSONObject(qtresponse_str);
            qtresponsecode=qtresponse.getString("responsecode");
            if(qtresponsecode.equals("00")){
                qtfullname=qtresponse.getString("fullName");
                tmresponse.put("fullname", qtfullname);
                tmresponse.put("responsecode", "00");
                tmresponse.put("responsemessage", "success");
                logger.info(phonenumber+"  "+sessid+":new validate biller reference success source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("validate biller reference success");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new validate biller reference  success source: "+ source+" "+phonenumber );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }else{
                tmresponse.put("responsecode", "11");
                tmresponse.put("responsemessage", "cannot validate reference now");
                logger.info(phonenumber+"  "+sessid+":new validate biller reference failed source: "+ source+" "+phonenumber );
                tmlog.setSource(source);
                tmlog.setPhonenumber(phonenumber);
                tmlog.setSessionid(sessid);
                tmlog.setTransactionid(transid);
                tmlog.setActionperformed("validate biller reference failed");
                tmlog.setActiondetails("request details: "+phonenumber+"  "+sessid+":new validate biller reference failed source: "+ source+" "+phonenumber );
                tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
                PersistenceAgent.logTransactions(tmlog);
                tmlog = new Tmlogs();
            }
            
            output=tmresponse.toString();
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"999\",\"responsemessage\":\"Error parsing json\"}";
            logger.info("error details: "+ e.getMessage());
            logger.info(phonenumber+"  "+sessid+": tm response: "+ output);
            tmlog = new Tmlogs();
            tmlog.setSource(source);
            tmlog.setPhonenumber(phonenumber);
            tmlog.setSessionid(sessid);
            tmlog.setTransactionid(transid);
            tmlog.setActionperformed("build tm response");
            tmlog.setActiondetails("tm responsecode: 999 responsemessage: Error parsing json");
            tmlog.setActiontime(utils.getDate()+" "+utils.getTime());
            PersistenceAgent.logTransactions(tmlog);
        }
        
    return output;}
    
    
    public static void main(String args[])
    {
        OctopusOperations op = new OctopusOperations();
        BankITOperations bio = new BankITOperations();
        //System.out.println(op.registerOctopus("USSD", "654756129990099265733323333434", "654756129990099265733323333434","09032225404", "Samuel","Akpan", "male", "01/02/1997", "akpansamuel53@gmail.com", "NG", "1111", "019234568858","true", "5300004922", "Heritage","030"));
       //System.out.println(op.validateRegisterOctopus("USSD", "654756129990099265733323333434", "654756129990099265733323333434","09032225404", "9697"));
       //System.out.println(op.unBlockCustomer("USSD", "654756129990099265733323333434", "654756129990099265733323333434","07038901111", "8886","1111"));
       //System.out.println(op.loginCustomer("TELEBOT", "6559354756122911", "6559354756122911","07038901111","1211","true"));
       //System.out.println(op.blacklistCustomer("USSD", "6559354756122911", "6559354756122911","08182120030","07038901111"));
       //System.out.println(op.unBlacklistCustomer("USSD", "6559354756122911", "6559354756122911","07066353204","07038901111"));
       //System.out.println(op.iswCategorys("USSD", "6559354756122911", "6559354756122911","07038901111"));
       //System.out.println(op.getInboxTransactions("USSD", "6559354756122911", "6559354756122911","07038901111"));
       //System.out.println(op.addAccount("USSD", "400096559354756122911", "400096559354756122911","07038901111","1111","","2086536811","Uba","033"));
       //System.out.println(op.fetchBeneficiary("USSD", "6559354756122911", "6559354756122911","07038901111"));
       //System.out.println(op.sourceAccounts("USSD", "6559354756122911", "6559354756122911","07038901111"));
       //System.out.println(op.addCardValidate("USSD", "6559354756122911", "6559354756122911","07038901111","5061040000000000181","cee8ece5-25d5-48f8-95c4-161091ae35f2","667220"));
       //System.out.println(op.deleteAccount("USSD", "6559354756122911", "6559354756122911","07038901111","1111","0023116789","Diamond","050"));
       System.out.println(op.deleteCard("USSD", "6559354756122911", "6559354756122911","07038901111","1111","5c0b4f2d-ebf6-453d-be80-94221ada3b6f"));
       //System.out.println(op.getInboxTransactions("USSD", "6559354756122911", "6559354756122911","07066353204"))
       //System.out.println(op.iswBillers_catid("USSD", "6559354756122911", "6559354756122911","07038901111","4"));
//       System.out.println(op.validateReference("", "", "", "07038901111", "07038901111", "10906"));
       //System.out.println(op.fundsRequest("USSD", "123345", "123345", "07038901111", "Omotayo Alimi", "0023113070", "Ecobank","050", "", "", "", "", "", "", "07066353204", "", "", "","", "50", "tedt", "ft", "fundsrequest"));
        // System.out.println(bio.initialize("USSD", "102938475612", "102938475612", "ecobank", "0023113170", "200", "TEST PAYMENT", "07038901111"));
        //bio.sendDebit("USSD", "1029384756", "1029384756", "ecobank", "07038901111", "123456");
    }
    
   
    
    
}
