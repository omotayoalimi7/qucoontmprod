/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.servlets;

import com.qucoon.dbo.Facebook_users;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.State_manager_facebook;
import com.qucoon.dbo.State_manager_telegram;
import com.qucoon.dbo.Telegram_users;
import com.qucoon.operations.TokenDetails;
import com.qucoon.bot.botApi;
import com.qucoon.operations.OctopusOperations;
import com.qucoon.operations.Utilities;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author User001
 */

@WebServlet(name = "servAddCard", urlPatterns = {"/servAddCard"})
public class servAddCard extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        try{
            if(request.getParameter("submit")!=null){
                if(session.getAttribute("identifier")==null){
                    //logout
                    session.setAttribute("msg", "Invalid Request");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                }
                OctopusOperations op = new OctopusOperations();
                Utilities utils = new Utilities();
                String cardname= (request.getParameter("cardname")!=null)?request.getParameter("cardname"):"";
                String cardno= (request.getParameter("cardno")!=null)?request.getParameter("cardno"):"";
                String cardcvv= (request.getParameter("cardcvv")!=null)?request.getParameter("cardcvv"):"";
                String cardpin= (request.getParameter("cardpin")!=null)?request.getParameter("cardpin"):"";
                String expirymonth= (request.getParameter("expirymonth")!=null)?request.getParameter("expirymonth"):"";
                String expiryyear= (request.getParameter("expiryyear")!=null)?request.getParameter("expiryyear"):"";
                String passcode= (request.getParameter("passcode")!=null)?request.getParameter("passcode"):"";
                String identifier = (String)session.getAttribute("identifier");
                String wid = (String)session.getAttribute("wid");
                String transid = utils.generateTransid();
                String sessid = wid;
                botApi botapi = new botApi();
                if(cardname.equals("")||cardno.equals("")||cardcvv.equals("")||cardpin.equals("")||expirymonth.equals("")||expiryyear.equals("")||passcode.equals("")){
                    session.setAttribute("msg", "Please fill all fields");
                    response.sendRedirect("https://octopus.hbng.com/tm/confirmpass.jsp?wid="+wid);
                }
                if(identifier.equals("tele")){
                    Telegram_users tm_users = (Telegram_users)session.getAttribute("tmuserdet");
                    State_manager_telegram tm_stat = (State_manager_telegram)session.getAttribute("tmuser");
                    String phonenumber = tm_users.getPhoneno();
                    String chatid = tm_users.getChatid();
                    String addcardresponse_str=op.addCard("TELEBOT", transid, sessid, phonenumber, passcode, cardno, cardpin, expiryyear+expirymonth, cardcvv, cardname, "NG", "NGN");
                    JSONObject addcardresponse = new JSONObject(addcardresponse_str);
                    String addcardresponsecode=addcardresponse.getString("responsecode");
                    String addcardresponsemessage=addcardresponse.getString("responsemessage");
                    if(addcardresponsecode.equals("00")){
                        session.setAttribute("msg", "Card Added");
                        session.setAttribute("identifier", null);
                        session.setAttribute("tmuserdet", null);
                        session.setAttribute("tmuser", null);
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmpasssuccesstele.jsp");
                    }else if(addcardresponsecode.equals("22")){
                        //login failed
                        session.setAttribute("msg", "Passcode Wrong");
                        response.sendRedirect("https://octopus.hbng.com/tm/cardadd.jsp?wid="+wid);
                    }else if(addcardresponsecode.equals("33")){
                        //Enter token
                        System.out.println("herrrrrr1");
                        session.setAttribute("msg", "Please input token");
                        String addcardcid=addcardresponse.getString("cid");
                        System.out.println("herrrrrr2");
                        TokenDetails tkd = new TokenDetails();
                        tkd.setCid(addcardcid);
                        tkd.setResponsemessage(addcardresponsemessage);
                        session.setAttribute("tokendetails",tkd);
                        System.out.println("herrrrrr3");
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmcardtoken.jsp");
                        System.out.println("herrrrrr4");
                    }else if(addcardresponsecode.equals("66")){
                        //account blocked
                        botapi.sendTeleUnblockMsg(chatid, utils.formatPhone(phonenumber));
                        tm_stat.setNextaction("getblockedtoken");
                        PersistenceAgent.logTransactions(tm_stat);
                        session.setAttribute("identifier", null);
                        session.setAttribute("tmuserdet", null);
                        session.setAttribute("tmuser", null);
                        session.setAttribute("msg", "Your account is blocked. Please input the token sent to "+utils.formatPhone(phonenumber).substring(0,6)+"xxxx"+utils.formatPhone(phonenumber).substring(11,13)+" to verify your account");
                        response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                    }else{
                        //others
                        session.setAttribute("msg", addcardresponsemessage);
                        response.sendRedirect("https://octopus.hbng.com/tm/cardadd.jsp?wid="+wid);
                    }
                }else{
                    Facebook_users tm_users = (Facebook_users)session.getAttribute("fbuserdet");
                    State_manager_facebook tm_stat = (State_manager_facebook)session.getAttribute("fbuser");
                    String phonenumber = tm_users.getPhoneno();
                    String chatid = tm_users.getChatid();
                    String addcardresponse_str=op.addCard("FACEBOT", transid, sessid, phonenumber, passcode, cardno, cardpin, expiryyear+expirymonth, cardcvv, cardname, "NG", "NGN");
                    JSONObject addcardresponse = new JSONObject(addcardresponse_str);
                    String addcardresponsecode=addcardresponse.getString("responsecode");
                    String addcardresponsemessage=addcardresponse.getString("responsemessage");
                    if(addcardresponsecode.equals("00")){
                        session.setAttribute("msg", "Card Added");
                        session.setAttribute("fbuserdet", null);
                        session.setAttribute("fbuser", null);
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmpasssuccessface.jsp");
                    }else if(addcardresponsecode.equals("22")){
                        //login failed
                        session.setAttribute("msg", "Passcode Wrong");
                        response.sendRedirect("https://octopus.hbng.com/tm/cardadd.jsp?wid="+wid);
                    }else if(addcardresponsecode.equals("33")){
                        //Enter token
                        System.out.println("herrrrrr1");
                        session.setAttribute("msg", "Please input token");
                        String addcardcid=addcardresponse.getString("cid");
                        System.out.println("herrrrrr2");
                        TokenDetails tkd = new TokenDetails();
                        tkd.setCid(addcardcid);
                        tkd.setResponsemessage(addcardresponsemessage);
                        session.setAttribute("tokendetails",tkd);
                        System.out.println("herrrrrr3");
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmcardtoken.jsp");
                        System.out.println("herrrrrr4");
                    }else if(addcardresponsecode.equals("66")){
                        //account blocked
                        botapi.sendFaceUnblockMsg(chatid, utils.formatPhone(phonenumber));
                        tm_stat.setNextaction("getblockedtoken");
                        PersistenceAgent.logTransactions(tm_stat);
                        session.setAttribute("identifier", null);
                        session.setAttribute("fbuserdet", null);
                        session.setAttribute("fbuser", null);
                        session.setAttribute("msg", "Your account is blocked. Please input the token sent to "+utils.formatPhone(phonenumber).substring(0,6)+"xxxx"+utils.formatPhone(phonenumber).substring(11,13)+" to verify your account");
                        response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                    }else{
                        //others
                        session.setAttribute("msg", addcardresponsemessage);
                        response.sendRedirect("https://octopus.hbng.com/tm/cardadd.jsp?wid="+wid);
                    }
                }
            }
        }catch(Exception e){
            
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
// </editor-fold>
}
