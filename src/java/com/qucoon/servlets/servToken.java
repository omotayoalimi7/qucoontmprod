/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.servlets;

import com.qucoon.dbo.Facebook_users;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.State_manager_facebook;
import com.qucoon.dbo.State_manager_telegram;
import com.qucoon.dbo.Telegram_users;
import com.qucoon.operations.TokenDetails;
import com.qucoon.bot.botApi;
import com.qucoon.operations.OctopusOperations;
import com.qucoon.operations.Utilities;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author User001
 */

@WebServlet(name = "servToken", urlPatterns = {"/servToken"})
public class servToken extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        try{
            if(request.getParameter("submit")!=null){
                System.out.println("entered");
                String otp= (request.getParameter("otp")!=null)?request.getParameter("otp"):"";
                String identifier="";
                String wid="";
                OctopusOperations op = new OctopusOperations();
                Utilities utils = new Utilities();
                if(session.getAttribute("identifier")==null){
                    //logout
                    session.setAttribute("msg", "Invalid Request");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                }else{
                    identifier = (String)session.getAttribute("identifier");
                    wid = (String)session.getAttribute("wid");
                }
                TokenDetails tkd=null;
                if(session.getAttribute("tokendetails")!=null){
                    tkd = (TokenDetails)session.getAttribute("tokendetails");
                }else{
                    ///logout
                    session.setAttribute("msg", "Invalid Request");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                }
                if(otp.equals("")){
                    session.setAttribute("msg", "Please fill all fields");
                    response.sendRedirect("https://octopus.hbng.com/tm/confirmcardtoken.jsp?wid="+wid);
                }
                String transid = utils.generateTransid();
                String sessid = wid;
                String cid=tkd.getCid();
                botApi botapi = new botApi();
                if(identifier.equals("tele")){
                    Telegram_users tm_users = (Telegram_users)session.getAttribute("tmuserdet");
                    State_manager_telegram tm_stat = (State_manager_telegram)session.getAttribute("tmuser");
                    String phonenumber = tm_users.getPhoneno();
                    String chatid = tm_users.getChatid();
                    String addcardresponse_str=op.addCardValidate("TELEBOT", transid, sessid, phonenumber, cid, otp);
                    JSONObject addcardresponse = new JSONObject(addcardresponse_str);
                    String addcardresponsecode=addcardresponse.getString("responsecode");
                    if(addcardresponsecode.equals("00")){
                        String destination=tm_stat.getRegisterdestination();
                        String responsestr="";
                        botapi.sendTeleMsg(chatid);
                        tm_stat.setNextaction(destination);
                        tm_stat.setStatus("inactive");
                        PersistenceAgent.logTransactions(tm_stat);
                        session.setAttribute("msg", "Card Added");
                        session.setAttribute("identifier", null);
                        session.setAttribute("tokendetails", null);
                        session.setAttribute("tmuserdet", null);
                        session.setAttribute("tmuser", null);
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmpasssuccesstele.jsp");
                    }else if(addcardresponsecode.equals("33")){
                        //invalid otp
                        session.setAttribute("msg", "Invalid Otp");
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmcardtoken.jsp");
                    }else if(addcardresponsecode.equals("66")){
                        //account blocked
                        botapi.sendTeleUnblockMsg(chatid, utils.formatPhone(phonenumber));
                        tm_stat.setNextaction("getblockedtoken");
                        PersistenceAgent.logTransactions(tm_stat);
                        session.setAttribute("identifier", null);
                        session.setAttribute("tokendetails", null);
                        session.setAttribute("tmuserdet", null);
                        session.setAttribute("tmuser", null);
                        session.setAttribute("msg", "Your account is blocked. Please input the token sent to "+utils.formatPhone(phonenumber).substring(0,6)+"xxxx"+utils.formatPhone(phonenumber).substring(11,13)+" to verify your account");
                        response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                    }else{
                        //Cannot add card
                        System.out.println("herrrrrr1");
                        String addcardresponsemessage=addcardresponse.getString("responsemessage");
                        session.setAttribute("msg", addcardresponsemessage);
                        response.sendRedirect("https://octopus.hbng.com/tm/cardadd.jsp?wid="+wid);
                        System.out.println("herrrrrr4");
                    }
                }else{
                    Facebook_users tm_users = (Facebook_users)session.getAttribute("fbuserdet");
                    State_manager_facebook tm_stat = (State_manager_facebook)session.getAttribute("fbuser");
                    String phonenumber = tm_users.getPhoneno();
                    String chatid = tm_users.getChatid();
                    String addcardresponse_str=op.addCardValidate("FACEBOT", transid, sessid, phonenumber, cid, otp);
                    JSONObject addcardresponse = new JSONObject(addcardresponse_str);
                    String addcardresponsecode=addcardresponse.getString("responsecode");
                    if(addcardresponsecode.equals("00")){
                        String destination=tm_stat.getRegisterdestination();
                        String responsestr="";
                        botapi.sendFaceMsg(chatid);
                        tm_stat.setNextaction(destination);
                        tm_stat.setStatus("inactive");
                        PersistenceAgent.logTransactions(tm_stat);
                        session.setAttribute("msg", "Card Added");
                        session.setAttribute("identifier", null);
                        session.setAttribute("tokendetails", null);
                        session.setAttribute("fbuserdet", null);
                        session.setAttribute("fbuser", null);
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmpasssuccessface.jsp");
                    }else if(addcardresponsecode.equals("33")){
                        //invalid otp
                        session.setAttribute("msg", "Invalid Otp");
                        response.sendRedirect("https://octopus.hbng.com/tm/confirmcardtoken.jsp");
                    }else if(addcardresponsecode.equals("66")){
                        //account blocked
                        botapi.sendFaceUnblockMsg(chatid, utils.formatPhone(phonenumber));
                        tm_stat.setNextaction("getblockedtoken");
                        PersistenceAgent.logTransactions(tm_stat);
                        session.setAttribute("identifier", null);
                        session.setAttribute("fbuserdet", null);
                        session.setAttribute("fbuser", null);
                        session.setAttribute("msg", "Your account is blocked. Please input the token sent to "+utils.formatPhone(phonenumber).substring(0,6)+"xxxx"+utils.formatPhone(phonenumber).substring(11,13)+" to verify your account");
                        response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                    }else{
                        //Cannot add card
                        System.out.println("herrrrrr1");
                        String addcardresponsemessage=addcardresponse.getString("responsemessage");
                        session.setAttribute("msg", addcardresponsemessage);
                        response.sendRedirect("https://octopus.hbng.com/tm/cardadd.jsp?wid="+wid);
                        System.out.println("herrrrrr4");
                    }
                }
            }
        }catch(Exception e){
            
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
// </editor-fold>
}
