/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.servlets;

import com.qucoon.dbo.Facebook_users;
import com.qucoon.dbo.PersistenceAgent;
import com.qucoon.dbo.State_manager_facebook;
import com.qucoon.dbo.State_manager_telegram;
import com.qucoon.dbo.Telegram_users;
import com.qucoon.bot.botApi;
import com.qucoon.operations.OctopusOperations;
import com.qucoon.operations.Utilities;
import com.qucoon.slacklogger.SlackLogMessage;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author User001
 */

@WebServlet(name = "servLogin", urlPatterns = {"/servLogin"})
public class servLogin extends HttpServlet {
    SlackLogMessage log = new SlackLogMessage();
    String appname="QucconTM";
    String classname="BotPasscodeValidation"; 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        if(request.getParameter("submit")!=null){
            try{
            String passcode= (request.getParameter("passcode")!=null)?request.getParameter("passcode"):"";
            String identifier = (String)session.getAttribute("identifier");
            String wid = (String)session.getAttribute("wid");
            OctopusOperations op = new OctopusOperations();
            Utilities utils = new Utilities();
            String transid = utils.generateTransid();
            String sessid = wid;
            botApi botapi = new botApi();
            if(session.getAttribute("identifier")==null){
                    //logout
                    session.setAttribute("msg", "Invalid Request");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                    return;
            }
            if(passcode.length()<4){
                session.setAttribute("msg", "Passcode can not be less than 4 characters");
                response.sendRedirect("https://octopus.hbng.com/tm/confirmpass.jsp?wid="+wid);
                return;
            }
            if(identifier.equals("tele")){
                Telegram_users tm_users = (Telegram_users)session.getAttribute("tmuserdet");
                State_manager_telegram tm_stat = (State_manager_telegram)session.getAttribute("tmuser");
                String phonenumber = tm_users.getPhoneno();
                String chatid = tm_users.getChatid();
                String loginresponse_str=op.loginCustomer("TELEBOT",transid,sessid, phonenumber, passcode,"true");
                JSONObject loginresponse = new JSONObject(loginresponse_str);
                String loginresponsecode=loginresponse.getString("responsecode");
                String loginresponsemessage = loginresponse.getString("responsemessage");
                if(loginresponsecode.equals("00")){
                    String destination=tm_stat.getRegisterdestination();
                    String responsestr="";
                    if (destination.equals("continuetransaction_balance")){
                        responsestr="Click to see balances";
                    }else if (destination.equals("performrecharge")){
                        responsestr="Click to perform recharge";
                    }else if (destination.equals("confirmtrans_ft")){
                        responsestr="Click to send money";
                    }else if (destination.equals("confirmbillpayment")){
                        responsestr="Click to pay bill";
                    }else if (destination.equals("collectpasscode_addacc")){
                        responsestr="Click to add account";
                    }else if (destination.equals("collectpass")){
                        responsestr="Click to sign in";
                    }
                    botapi.sendTeleButton(responsestr, passcode, chatid);
                    tm_stat.setNextaction(destination);
                    tm_stat.setRegisterdestination(tm_stat.getRegistersource());
                    PersistenceAgent.logTransactions(tm_stat);
                    session.setAttribute("tmuserdet", null);
                    session.setAttribute("tmuser", null);
                    session.setAttribute("msg", "Passcode Validated");
                    response.sendRedirect("https://octopus.hbng.com/tm/confirmpasssuccesstele.jsp");
                }else if(loginresponsecode.equals("33")){
                    //login failed
                    botapi.sendTeleUnblockMsg(chatid, utils.formatPhone(phonenumber));
                    tm_stat.setNextaction("getblockedtoken");
                    PersistenceAgent.logTransactions(tm_stat);
                    session.setAttribute("tmuserdet", null);
                    session.setAttribute("tmuser", null);
                    session.setAttribute("msg", "Your account is blocked. Please input the token sent to "+utils.formatPhone(phonenumber).substring(0,6)+"xxxx"+utils.formatPhone(phonenumber).substring(11,13)+" to verify your account");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                }else{
                    //login failed
                    session.setAttribute("msg", loginresponsemessage);
                    response.sendRedirect("https://octopus.hbng.com/tm/confirmpass.jsp?wid="+wid);
                }
            }else{
                Facebook_users tm_users = (Facebook_users)session.getAttribute("fbuserdet");
                State_manager_facebook tm_stat = (State_manager_facebook)session.getAttribute("fbuser");
                String phonenumber = tm_users.getPhoneno();
                String chatid = tm_users.getChatid();
                String loginresponse_str=op.loginCustomer("FACEBOT",transid,sessid, phonenumber, passcode,"true");
                JSONObject loginresponse = new JSONObject(loginresponse_str);
                String loginresponsecode= loginresponse.getString("responsecode");
                String loginresponsemessage = loginresponse.getString("responsemessage");
                if(loginresponsecode.equals("00")){
                    String destination=tm_stat.getRegisterdestination();
                    String responsestr="";
                    if(destination.equals("continuetransaction_balance")){
                        responsestr="Click to see balances";
                    }else if (destination.equals("performrecharge")){
                        responsestr="Click to perform recharge";
                    }else if (destination.equals("confirmtrans_ft")){
                        responsestr="Click to send money";
                    }else if (destination.equals("confirmbillpayment")){
                        responsestr="Click to pay bill";
                    }else if (destination.equals("collectpasscode_addacc")){
                        responsestr="Click to add account";
                    }else if (destination.equals("collectpass")){
                        responsestr="Click to sign in";
                    }
                    botapi.sendFaceButton(responsestr, passcode, chatid);
                    tm_stat.setNextaction(destination);
                    tm_stat.setRegisterdestination(tm_stat.getRegistersource());
                    PersistenceAgent.logTransactions(tm_stat);
                    session.setAttribute("msg", "Passcode Validated");
                    session.setAttribute("fbuserdet", null);
                    session.setAttribute("fbuser", null);
                    response.sendRedirect("https://octopus.hbng.com/tm/confirmpasssuccessface.jsp");
                }else if(loginresponsecode.equals("33")){
                    //login failed
                    botapi.sendFaceUnblockMsg(chatid, utils.formatPhone(phonenumber));
                    tm_stat.setNextaction("getblockedtoken");
                    PersistenceAgent.logTransactions(tm_stat);
                    session.setAttribute("fbuserdet", null);
                    session.setAttribute("fbuser", null);
                    session.setAttribute("msg", "Your account is blocked. Please input the token sent to "+utils.formatPhone(phonenumber).substring(0,6)+"xxxx"+utils.formatPhone(phonenumber).substring(11,13)+" to verify your account");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                }else{
                    //login failed
                    session.setAttribute("msg", loginresponsemessage);
                    response.sendRedirect("https://octopus.hbng.com/tm/confirmpass.jsp?wid="+wid);
                }
            }
            }catch(Exception e){
                
            }
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
// </editor-fold>
}
