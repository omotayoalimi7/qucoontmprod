/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.slacklogger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qucoon.operations.PropsReader;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author neptune
 */
public class SlackLogMessage {
private static String slackurlinfo = PropsReader.getProperty("slackurlinfo");//
private static String slackurlerror= PropsReader.getProperty("slackurlerror");//"
private static String slackdispute = PropsReader.getProperty("slackdispute");
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String msg="{\n" +
"\"TerminalId\":\"3QUO0001\",\n" +
"\"paymentCode\":\"10901\",\n" +
"\"customerId\":\"07038901111\",\n" +
"\"customerMobile\":\"07038901111\",\n" +
"\"amount\":\"10000\",\n" +
"\"requestReference\":\"152800000023\"\n" +
"}";
         JSONObject obj = new JSONObject();
        try{
             obj = new JSONObject(msg);
            obj.put("name", "Tope Olakunle");
        
        obj.put("phone", "07066353204");
        obj.put("action", "top-up");
        }catch(Exception e){
            
        }
        SlackLogMessage log = new SlackLogMessage();
        log.info("AppName", "ClassName", "Title Message ", obj.toString());
        System.out.println("fjd");
    }
    
    public  void info(String appname, String classname, String title, String details){
      long unixTimestamp = Instant.now().getEpochSecond();
      if(isJSONValid(details)){
          System.out.println(true);
          details = ConvertJSONtoMapString(details);
          
      }
      details=(details != null) ? details.replace("{", "").replace("}", "").replace("\"", "") : "";
      //System.out.println(details);
        String SlackMsg = "{\n" +
"    \"attachments\": [\n" +
"        {\n" +
"            \"fallback\": \""+title+"\",\n" +
"            \"color\": \"#36a64f\",\n" +
//"            \"pretext\": \"Optional text that appears above the attachment block\",\n" +
"            \"author_name\": \""+appname+" - "+classname+"\",\n" +
"            \"title\": \""+title+"\",\n" +
"            \"text\": \""+details+"\",\n" +
"            \"fields\": [\n" +
"                {\n" +
"                    \"title\": \"Priority\",\n" +
"                    \"value\": \"Low\",\n" +
"                    \"short\": false\n" +
"                }\n" +
"            ],\n" +
"            \"footer\": \""+appname+"\",\n" +
"            \"ts\": "+unixTimestamp+"\n" +
"        }\n" +
"    ]\n" +
"}";
          //System.out.println(SlackMsg);
        try{
            CallSlackAPI api = new CallSlackAPI();
            api.process(SlackMsg,slackurlinfo);
        }catch(Exception s){
            s.printStackTrace();
        }
    }
    
    
        public  void error(String appname, String classname, String title, String details){
      long unixTimestamp = Instant.now().getEpochSecond();
      if(isJSONValid(details)){
          details = ConvertJSONtoMapString(details);
      }
      details=(details != null) ? details.replace("{", "").replace("}", "").replace("\"", "") : "";
        String SlackMsg = "{\n" +
"    \"attachments\": [\n" +
"        {\n" +
"            \"fallback\": \""+title+"\",\n" +
"            \"color\": \"#ff0000\",\n" +
//"            \"pretext\": \"Optional text that appears above the attachment block\",\n" +
"            \"author_name\": \""+appname+" - "+classname+"\",\n" +
"            \"title\": \""+title+"\",\n" +
"            \"text\": \""+details+"\",\n" +
"            \"fields\": [\n" +
"                {\n" +
"                    \"title\": \"Priority\",\n" +
"                    \"value\": \"High\",\n" +
"                    \"short\": false\n" +
"                }\n" +
"            ],\n" +
"            \"footer\": \""+appname+"\",\n" +
"            \"ts\": "+unixTimestamp+"\n" +
"        }\n" +
"    ]\n" +
"}";
        
        try{
            CallSlackAPI api = new CallSlackAPI();
            api.process(SlackMsg,slackurlerror);
        }catch(Exception s){
            s.printStackTrace();
        }
    }

          public  void dispute(String appname, String classname, String title, String details){
      long unixTimestamp = Instant.now().getEpochSecond();
      if(isJSONValid(details)){
          details = ConvertJSONtoMapString(details);
      }
      details=(details != null) ? details.replace("{", "").replace("}", "").replace("\"", "") : "";
        String SlackMsg = "{\n" +
"    \"attachments\": [\n" +
"        {\n" +
"            \"fallback\": \""+title+"\",\n" +
"            \"color\": \"#ff0000\",\n" +
//"            \"pretext\": \"Optional text that appears above the attachment block\",\n" +
"            \"author_name\": \""+appname+" - "+classname+"\",\n" +
"            \"title\": \""+title+"\",\n" +
"            \"text\": \""+details+"\",\n" +
"            \"fields\": [\n" +
"                {\n" +
"                    \"title\": \"Priority\",\n" +
"                    \"value\": \"High\",\n" +
"                    \"short\": false\n" +
"                }\n" +
"            ],\n" +
"            \"footer\": \""+appname+"\",\n" +
"            \"ts\": "+unixTimestamp+"\n" +
"        }\n" +
"    ]\n" +
"}";
        
        try{
            CallSlackAPI api = new CallSlackAPI();
            api.process(SlackMsg,slackdispute);
        }catch(Exception s){
            s.printStackTrace();
        }
    }

    
    private boolean isJSONValid(String data) {
    try {
        new JSONObject(data);
    } catch (JSONException ex) {
        // edited, to include @Arthur's comment
        // e.g. in case JSONArray is valid as well...
        try {
            new JSONArray(data);
        } catch (JSONException ex1) {
            return false;
        }
    }
    return true;
}
    
    private String ConvertJSONtoMapString(String data){
        
            ObjectMapper mapper = new ObjectMapper();
            String json = data.toString();
            Map<String, Object> map = new HashMap<String, Object>();
            try{
            map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
            }catch(Exception s){
                s.printStackTrace();
            }
        
            return map.toString().replace("{", "").replace("}", "");
        
    } 
    
    
}
