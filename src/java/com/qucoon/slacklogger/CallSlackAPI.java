/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.slacklogger;


import com.qucoon.operations.Utilities;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 *
 * @author Temitope
 */
public class CallSlackAPI {

    /**
     * @param args the command line arguments
     */
    
      
    public void process(String jsonPayload, String url){
        String responsemsg = "";
         Utilities utils = new Utilities();
        try{
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, jsonPayload);
            Request request = new Request.Builder()
              .url(url)
              .post(body)
              .build();

            //Response response = client.newCall(request).execute();
            
            client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                // do something wih the result
               System.out.println (response.body().string().toString()+ "---"+utils.getDate()+" "+utils.getTime());
            }
        }
            });
            
            
        }catch(Exception s){
            s.printStackTrace();
        }
        
        //return responsemsg;
    }
//    
//    public String processxx(String jsonPayload, String url){
//        String responsemsg = "";
//        try{
//            
//            String sslClientPwd = "changeit";//Settings.getInstance("").getProperty("JDK_SSLPWD");
//	        	String sslClientFile = "jssecacerts";//Settings.getInstance("").getProperty("JDK_SSLFILE");
//	        	System.setProperty("javax.net.ssl.trustStore", sslClientFile);
//	        	System.setProperty("javax.net.ssl.trustStorePassword", sslClientPwd);
//	        	System.setProperty("javax.net.ssl.keyStore", sslClientFile);
//	        	System.setProperty("javax.net.ssl.keyStorePassword", sslClientPwd);
//	        	//System.setProperty("javax.net.debug", "all");
//	        	System.setProperty("https.protocols", "SSLv3,TLSv1,SSLv2Hello,TLSv1.2");//,SSLv3	
//	        	System.setProperty("-Dhttps.protocols", "SSLv3,TLSv1,SSLv2Hello,TLSv1.2");//,SSLv3
//	        	System.setProperty("-Ddeployment.security.TLSv1", "true");//,SSLv3	
//	        	System.setProperty("-Dhttps.cipherSuites", "SSL_RSA_WITH_RC4_128_MD5");
//	        	;
//	        	
//	        	
//	         HttpClient client = new DefaultHttpClient();; 
//	        client = wrapClient(client);
//	        HttpPost post = new HttpPost(url);
//	        post.setHeader("Content-Type", "application/json");
//                StringEntity params =new StringEntity(jsonPayload);
//                post.setEntity(params);
//                
//                
//                
//	        StringWriter sw = new StringWriter();
//                String xmlString = jsonPayload;
//	        HttpEntity entity = new ByteArrayEntity(xmlString.getBytes("UTF-8"));
//	        post.setEntity(entity);
//	        HttpResponse response2 = client.execute(post);
//	        StringBuilder sb = new StringBuilder();
//	        String line = "";
//	        BufferedReader br = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));
//			while ((line = br.readLine()) != null) {
//				sb.append(line);
//			}
//	            String response = sb.toString();//\
//            
//            System.out.println("response==="+response);
//            //Response response = client.newCall(request).execute();
//            responsemsg = response;//response.body().string().toString();
//        }catch(Exception s){
//            s.printStackTrace();
//        }
//        //retrieve response
//        
//        return responsemsg;
//    }
//    
    
    
    
}
