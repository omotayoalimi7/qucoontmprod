/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qucoon.hbng;

import com.qucoon.operations.PropsReader;
import com.qucoon.operations.Utilities;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONObject;

/**
 *
 * @author User001
 */
public class HbngInterface {
    private static String hbinterfaceurl = PropsReader.getProperty("hbinterfaceurl");
    Utilities utils = new Utilities();
    public String callService(String url, JSONObject requestbody) {
        String output = "";
        try {
            OkHttpClient client = ConnectionClient.getSecureHttpClient();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType,requestbody.toString());
            Request request = new Request.Builder()
                .url(hbinterfaceurl + url)
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "d4cf5316-88d0-39fe-bfb7-72152ce5e4a8")
                .build();
            Response response = client.newCall(request).execute();
           
//            System.out.println("Output from Server .... \n");
            output = response.body().string();
            System.out.println(response.code()+": Output from kernel "+"\n");
//            System.out.println(output);
        } catch (Exception e) {
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }

        return output;
    }
    
    
    public String phonegetname(String source,String sessid,String transid,String auth,String phonenumber){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("phonenumber",phonenumber);
            response=hbng.callService("phonegetname", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    

    public String banknameenquiry(String source,String sessid,String transid,String auth,String bankcode,String accountno,String channelcode){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("bankcode",bankcode);
            requestbody.put("accountno",accountno);
            requestbody.put("channelcode",channelcode);
            response=hbng.callService("accountgetnamenip", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    public String phonereturnaccount(String source,String sessid,String transid,String auth,String phonenumber){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("phonenumber",phonenumber);
            response=hbng.callService("phonegetbalance", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    
    public String accountgetname(String source,String sessid,String transid,String auth,String accountno){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("accountno",accountno);
            response=hbng.callService("accountgetnamehb", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    public String accountgetbalance(String source,String sessid,String transid,String auth,String phonenumber,String accountno){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("userid",phonenumber);
            requestbody.put("accountno",accountno);
            response=hbng.callService("accountgetbal", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    public String phonegetbalance(String source,String sessid,String transid,String auth,String phonenumber){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("phonenumber",phonenumber);
            response=hbng.callService("phonegetbalance", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    public String hbnginternaltrf(String source,String sessid,String transid,String auth,String debitaccount,String creditaccount,String amount,String narration){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
         
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("debitaccount",debitaccount);
            requestbody.put("creditaccount",creditaccount);
            requestbody.put("amount",amount);
            requestbody.put("narration",narration);
            response=hbng.callService("accountdoft", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
        }
    return output;}
    
    
    public String hbnginternaldebit(String source,String sessid,String transid,String auth,String debitaccount,String amount,String narration){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
         
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("debitaccount",debitaccount);
            //requestbody.put("creditaccount",creditaccount);
            requestbody.put("amount",amount);
            requestbody.put("narration",narration);
            response=hbng.callService("accountdodebit", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
        }
    return output;}
    
    public String hbngniptransfer(String source,String destaccount,String destbankcode,String destaccountname,
            String narration,
            String amount,String paymentreference,String sourceaccount,String sourceaccountname){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("destaccount",destaccount);
            requestbody.put("destbankcode",destbankcode);
            requestbody.put("destaccountname",destaccountname);
            requestbody.put("narration",narration);
            requestbody.put("amount",amount);
            requestbody.put("paymentreference",paymentreference);
            requestbody.put("channelcode","8");
            requestbody.put("sourceaccount",sourceaccount);
            requestbody.put("sourceaccountname",sourceaccountname);
            response=hbng.callService("accountdoftnip", requestbody);
            output=response;
        }
        catch(Exception e){
   output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    public String hbnginternalrvs(String source,String sessid,String transid,String auth,String debitaccount,String creditaccount,String amount,String reversalstan,String reversalstamp,String reversalref){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("authid",auth);
            requestbody.put("debitaccount",debitaccount);
            requestbody.put("creditaccount",creditaccount);
            requestbody.put("amount",amount);
            requestbody.put("reversalstan",reversalstan);
            requestbody.put("reversalstamp",reversalstamp);
            requestbody.put("reversalref",reversalref);
            response=hbng.callService("accountdoftreversal", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    public String bvnenquiry(String source,String sessid,String transid,String bvn){
        String output="";
        String response="";
        HbngInterface hbng = new HbngInterface();
        JSONObject requestbody = new JSONObject();
        try{
            requestbody.put("source",source);
            requestbody.put("sessid",sessid);
            requestbody.put("transid",transid);
            requestbody.put("bvn",bvn);
            response=hbng.callService("bvnenquiry", requestbody);
            output=response;
        }
        catch(Exception e){
            output="{\"responsedate\":\""+utils.getDate()+"\",\"responsetime\":\""+utils.getTime()+"\",\"responsecode\":\"96\",\"responsemessage\":\"Error Processing Transaction\"}";
       
        }
    return output;}
    
    
    
}
