/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twilio.servlets;

/**
 *
 * @author User001
 */
import com.twilio.twiml.Gather;
import com.twilio.twiml.Redirect;
import com.twilio.twiml.Say;
import com.twilio.twiml.TwiMLException;
import com.twilio.twiml.VoiceResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class servGather extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Create a TwiML response and add our friendly message.
        VoiceResponse.Builder builder = new VoiceResponse.Builder();

        String digits = request.getParameter("Digits");
        System.out.println("digits  "+digits);
        if (digits != null) {
                    builder.say(new Say.Builder("Ok thanks").build());
              
        }else {
            appendGather(builder);
        } 
        response.setContentType("application/xml");
        try {
            response.getWriter().print(builder.build().toXml());
        } catch (TwiMLException e) {
            throw new RuntimeException(e);
        }
    }
    
    private static void appendGather(VoiceResponse.Builder builder) {
        builder.gather(new Gather.Builder()
                        .numDigits(4)
                        .say(new Say.Builder("Please input your passcode").build())
                        .build()
                )
                .redirect(new Redirect.Builder().url("/Twilio/gather").build());
    }
}
