/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secure;


import java.io.IOException;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FilterHeaders implements Filter {

    //@Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {

        // Assume its HTTP
        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpServletResponse httpRes = (HttpServletResponse)response;
        
    httpRes.setHeader("X-XSS-Protection", "1; mode=block");
    httpRes.setHeader("X-Content-Type-Options", "nosniff");
    httpRes.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubdomains; preload");
    httpRes.setHeader("X-FRAME-OPTIONS","DENY" );
    httpRes.setDateHeader("Last-Modified", 0L);
    httpRes.setHeader("ETag", "1234567890");
    httpRes.setHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0"); 
    httpRes.setHeader("Pragma","no-cache");
    httpRes.setHeader("Expires", "0");
    httpRes.setHeader("Access-Control-Allow-Origin", "*");
    httpRes.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
    httpRes.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

    
        chain.doFilter(request, response);
    
    }

    //@Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

   // @Override
    public void destroy() {
    }
}