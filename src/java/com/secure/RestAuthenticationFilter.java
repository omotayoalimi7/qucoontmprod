/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secure;


import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import serv.jwt.AuthHelper;

public class RestAuthenticationFilter implements javax.servlet.Filter {
	public static final String BODY_TOKEN = "authorization";
        AuthHelper auth = new AuthHelper();
        private static String encoding;
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filter) throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
                        MultiReadHttpServletRequest requestWrapper=new  MultiReadHttpServletRequest(httpServletRequest);
                        String Requestbody = requestWrapper.getReader().lines().reduce("", (accumulator, actual) -> accumulator + actual);
                        Requestbody = (Requestbody != null) ? Requestbody : "";
                        String authorizationData = null;
                        if(isJSONValid(Requestbody)){
                            try {
                                authorizationData = new JSONObject(Requestbody).optString(BODY_TOKEN);
                            } catch (JSONException ex) {
                                Logger.getLogger(RestAuthenticationFilter.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        
                        boolean isauthorization = (authorizationData != null && authorizationData.length() > 1)? true : false;
                        
                        String datavalidation = "";
                        if(!isauthorization){
                            datavalidation = datavalidation + " authorization is Required in payload. ";
                        }
                        
                        //is authorisation token valid
                        boolean isAuthorized = false;
                        if(isauthorization){
                            if(auth.validateJWT(authorizationData).optString("status").equals("validated")){
                                isAuthorized= true;
                            }else{
                                datavalidation = datavalidation + auth.validateJWT(authorizationData).optString("status");

                            }
                        }
                        
                        
                        
                        
			if (isauthorization && isAuthorized) {
                       // if (true) {
                            System.out.println("Request Validation and forwarding to filter");
                            
                            filter.doFilter(requestWrapper, response);
			}else{
                             if (response instanceof HttpServletResponse) {
					HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                                        JSONObject json = new JSONObject();
                                        String rcode = String.valueOf(HttpServletResponse.SC_BAD_REQUEST);
                                 try {
                                     json.put("responsecode", rcode);
                                     json.put("responsemessage", datavalidation);
                                 } catch (JSONException ex) {
                                     Logger.getLogger(RestAuthenticationFilter.class.getName()).log(Level.SEVERE, null, ex);
                                 }
                                        
					httpServletResponse.setContentType("application/json");
                                        httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                        httpServletResponse.getWriter().write(json.toString());
				}
                        }
		}
	}
        
    static String extractPostRequestBody(HttpServletRequest request) {
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        Scanner s = null;
        try {
            s = new Scanner(request.getInputStream(), encoding).useDelimiter("\\A");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s.hasNext() ? s.next() : "";
    }
    return "";
}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
             encoding = arg0.getInitParameter("requestEncoding");
            // System.out.println("encoding=="+encoding);
            if( encoding==null ) encoding="UTF-8";
	}
        
        
    private boolean isJSONValid(String data) {
    try {
        new JSONObject(data);
    } catch (JSONException ex) {
        // edited, to include @Arthur's comment
        // e.g. in case JSONArray is valid as well...
        try {
            new JSONArray(data);
        } catch (JSONException ex1) {
            return false;
        }
    }
    return true;
}
}