package serv.jwt;

import java.util.Calendar;
import java.util.Locale;

import com.google.gson.JsonObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AuthHelper {
	static long  validity = 1;
	static String  checktoken = "7a8e381b55fe672a7d364313f330eeab0566b687337dd04bee";
        static int expiryminutes = 525600;
        public static JSONObject validateJWT(String token){
                JSONObject data =validateJWT.getJWTDataJson(token);
		return data;
	}
	
    public static  String createJWT(JSONObject data, String phonenumber, String requestor){
        String rtn = "";
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        String issuedate = new org.joda.time.Instant(cal.getTimeInMillis()).toString();
        DateTime dateTime = new DateTime();
        String expirydate = new org.joda.time.Instant(dateTime.plusMinutes(expiryminutes)).toString();
        //System.out.println(issuedate);
        //System.out.println(expirydate);
        
            try {
                //Configure request object, which provides information of the item
                data.put("requestor", requestor);
                data.put("issuedate", issuedate);
                data.put("expirydate", expirydate);
                data.put("phonenumber", phonenumber);
            } catch (JSONException ex) {
                Logger.getLogger(AuthHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        String val = generateJWT.process(data.toString());

        return val;
    }
        
    public static void main(String[] args) {
        // TODO code application logic here
        JSONObject data = new JSONObject();
        try {
                //Configure request object, which provides information of the item

                data.put("requestor", "ussd");
                data.put("issuedate", "");
                data.put("expirydate", "");
                data.put("phonenumber", "");
                System.out.println(createJWT(data,"",""));
        } catch (JSONException ex) {
                Logger.getLogger(AuthHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
}
