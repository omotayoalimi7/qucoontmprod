/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv.jwt;

 
import com.google.gson.Gson;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACVerifier;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author tolakunle
 */
public class validateJWT {
	static String  unikey ="7a8e381b55fe672a7d364313f330eeab0566b687337dd04bee";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public static String process(String message){
    String rtn="";    
    String sharedKey = unikey;
    Payload payload = new Payload(message);
    JWSHeader header = new JWSHeader(JWSAlgorithm.HS512);
    header.setContentType("text/plain");
    JWSVerifier verifier = new MACVerifier(sharedKey.getBytes());
        JWSObject jwsObject = new JWSObject(header, payload);
   try {
      jwsObject = JWSObject.parse(message);
    }
    catch (ParseException e) {
      System.out.println("Couldn't parse JWS object: " + e.getMessage());
      return "Couldn't parse Token";
    }

    boolean verifiedSignature = false;
    
    try {
      verifiedSignature = jwsObject.verify(verifier);
    }
    catch (JOSEException e) {
      System.err.println("Couldn't verify signature: " + e.getMessage());
      return "Couldn't verify signature";
    }

    if (verifiedSignature) {
      System.out.println("Verified JWS signature!");
    }
    else {
      //System.out.println("Bad JWS signature!");
      return "Couldn't verify signature";
    }
    try{
     rtn =   jwsObject.getPayload().toString();
     System.out.println(rtn);
    //payload is good.
    //validate the expiry
    Gson gson = new Gson();
    jwtpayload jpay = gson.fromJson(rtn, jwtpayload.class);
    DateTime expirydate = DateTime.parse(jpay.getExpirydate());
   
     Calendar cal = Calendar.getInstance(Locale.ENGLISH);
     String currentdate = new org.joda.time.Instant(cal.getTimeInMillis()).toString();
     DateTime currentdatetime = DateTime.parse(currentdate);
    Duration duration = new Duration(currentdatetime, expirydate);
//    System.out.println(duration.getStandardDays());
//    System.out.println(duration.getStandardHours());
//    System.out.println(duration.getStandardMinutes());
        if(duration.getStandardMinutes() > 0 ){
            rtn="true";
        }else{
            rtn="Authorization Token has Expired";
        }
    }catch(Exception s){
        s.printStackTrace();
        rtn = "Unable to Validate Authorization Code "+s.getMessage();
    }
    return rtn;
    }
    public static jwtpayload getJWTData(String message){
        String rtn="";    
        String sharedKey = unikey;
        Payload payload = new Payload(message);
        jwtpayload jwtd =  new jwtpayload();
        JWSHeader header = new JWSHeader(JWSAlgorithm.HS512);
        header.setContentType("text/plain");
        JWSVerifier verifier = new MACVerifier(sharedKey.getBytes());
            JWSObject jwsObject = new JWSObject(header, payload);
       try {
          jwsObject = JWSObject.parse(message);
        }
        catch (ParseException e) {
          System.out.println("Couldn't parse JWS object: " + e.getMessage());
          return jwtd;//"Couldn't parse Token";
        }

        boolean verifiedSignature = false;
        
        try {
          verifiedSignature = jwsObject.verify(verifier);
        }
        catch (JOSEException e) {
          System.err.println("Couldn't verify signature: " + e.getMessage());
          return jwtd;//"Couldn't verify signature";
        }

        if (verifiedSignature) {
          System.out.println("Verified JWS signature!");
        }
        else {
          //System.out.println("Bad JWS signature!");
          return jwtd;//"Couldn't verify signature";
        }
        try{
         rtn =   jwsObject.getPayload().toString();
         System.out.println(rtn);
        //payload is good.
        //validate the expiry
        Gson gson = new Gson();
        jwtpayload jpay = gson.fromJson(rtn, jwtpayload.class);
        DateTime expirydate = DateTime.parse(jpay.getExpirydate());
         jwtd = jpay;
         Calendar cal = Calendar.getInstance(Locale.ENGLISH);
         String currentdate = new org.joda.time.Instant(cal.getTimeInMillis()).toString();
         DateTime currentdatetime = DateTime.parse(currentdate);
        Duration duration = new Duration(currentdatetime, expirydate);
//        System.out.println(duration.getStandardDays());
//        System.out.println(duration.getStandardHours());
//        System.out.println(duration.getStandardMinutes());
            if(duration.getStandardMinutes() > 0 ){
                rtn=jpay.getUsertype();
            }else{
                rtn="Authorization Token has Expired";
            }
        }catch(Exception s){
            s.printStackTrace();
            rtn = "Unable to Validate Authorization Code "+s.getMessage();
        }
        return jwtd;
        }
 public static JSONObject getJWTDataJson(String message){
        String rtn="";    
        String sharedKey = unikey;
        JSONObject json = new JSONObject();
            try {
                json.put("token", message);
            } catch (JSONException ex) {
                Logger.getLogger(validateJWT.class.getName()).log(Level.SEVERE, null, ex);
            }
        Payload payload = new Payload(message);
        JWSHeader header = new JWSHeader(JWSAlgorithm.HS512);
        header.setContentType("text/plain");
        JWSVerifier verifier = new MACVerifier(sharedKey.getBytes());
            JWSObject jwsObject = new JWSObject(header, payload);
       try {
          jwsObject = JWSObject.parse(message);
        }
        catch (ParseException e) {
          System.out.println("Couldn't parse JWS object: " + e.getMessage());
            try {
                json.put("status", "Couldn't parse JWS object: " + e.getMessage());
            } catch (JSONException ex) {
                Logger.getLogger(validateJWT.class.getName()).log(Level.SEVERE, null, ex);
            }
          return json;//"Couldn't parse Token";
        }

        boolean verifiedSignature = false;
        
        try {
          verifiedSignature = jwsObject.verify(verifier);
        }
        catch (JOSEException e) {
          System.err.println("Couldn't verify signature: " + e.getMessage());
            try {
                json.put("status", "Couldn't verify signature: " + e.getMessage());
            } catch (JSONException ex) {
                Logger.getLogger(validateJWT.class.getName()).log(Level.SEVERE, null, ex);
            }
          return json;//"Couldn't verify signature";
        }

        if (verifiedSignature) {
          System.out.println("Verified JWS signature!");
        }
        else {
            try {
                //System.out.println("Bad JWS signature!");
                json.put("status", "Bad JWS signature!");
            } catch (JSONException ex) {
                Logger.getLogger(validateJWT.class.getName()).log(Level.SEVERE, null, ex);
            }
          return json;//"Couldn't verify signature";
        }
        try{
         rtn =   jwsObject.getPayload().toString();
         System.out.println(rtn);
         json = new JSONObject(rtn);
        
        //payload is good.
        //validate the expiry
//        Gson gson = new Gson();
//        jwtpayload jpay = gson.fromJson(rtn, jwtpayload.class);
        DateTime expirydate = DateTime.parse(json.optString("expirydate"));
        // jwtd = jpay;
         Calendar cal = Calendar.getInstance(Locale.ENGLISH);
         String currentdate = new org.joda.time.Instant(cal.getTimeInMillis()).toString();
         DateTime currentdatetime = DateTime.parse(currentdate);
        Duration duration = new Duration(currentdatetime, expirydate);
//        System.out.println(duration.getStandardDays());
//        System.out.println(duration.getStandardHours());
//        System.out.println(duration.getStandardMinutes());
            if(duration.getStandardMinutes() > 0 ){
                 json.put("status", "validated");
            }else{
                rtn="Authorization Token has Expired";
                 json.put("status", rtn);
            }
        }catch(Exception s){
           
            rtn = "Unable to Validate Authorization Code "+s.getMessage();
            try {
                json.put("status", rtn);
            } catch (JSONException ex) {
                Logger.getLogger(validateJWT.class.getName()).log(Level.SEVERE, null, ex);
            }
             s.printStackTrace();
        }
        return json;
        }

}


