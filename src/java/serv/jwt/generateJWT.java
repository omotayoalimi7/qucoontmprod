/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv.jwt;

 
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;

/**
 *
 * @author tolakunle
 */
public class generateJWT {
	static String  unikey = "7a8e381b55fe672a7d364313f330eeab0566b687337dd04bee";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    public static String process(String message){
        
    Payload payload = new Payload(message);
    //System.out.println("JWS payload message: " + message);
    // Create JWS header with HS256 algorithm
    JWSHeader header = new JWSHeader(JWSAlgorithm.HS512);
    header.setContentType("text/plain");
    //System.out.println("JWS header: " + header.toString());
    // Create JWS object
    JWSObject jwsObject = new JWSObject(header, payload);
    // Create HMAC signer
    String sharedKey = unikey;
    
    JWSSigner signer = new MACSigner(sharedKey.getBytes());
    try {
      jwsObject.sign(signer);
    }
    catch (JOSEException e) {
      System.out.println("Couldn't sign JWS object: " + e.getMessage());
    }

    // Serialise JWS object to compact format
    String s = jwsObject.serialize();

    //System.out.println("Serialised JWS object: " + s);

    // Parse back and check signature
        return s;
    }
}
