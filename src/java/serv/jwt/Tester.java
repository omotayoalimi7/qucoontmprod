/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv.jwt;

import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
 

/**
 *
 * @author tolakunle
 */
public class Tester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        String issuedate = new org.joda.time.Instant(cal.getTimeInMillis()).toString();
        String expirydate = new org.joda.time.Instant(cal.getTimeInMillis() + 1000L * 60L * 60L * 24L * 1).toString();
        System.out.println(issuedate);
        System.out.println(expirydate);
      
        //Configure request object, which provides information of the item
        JSONObject request = new JSONObject();
        try {
            request.put("userid", "tester");
            request.put("ipaddress", "192.168.0.1");
            request.put("usertype", "Terminal");
        } catch (JSONException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String txn = new AuthHelper().createJWT(request, "07066353204", "USSD");
         System.out.println("txn process="+txn);
         System.out.println("TokenDetails="+new AuthHelper().validateJWT(txn).toString());
//        String val = generateJWT.process(request.toString());
//        System.out.println("jwt process="+val);
//        JSONObject dec = validateJWT.getJWTDataJson(val);
//        
//        System.out.println(dec.toString());
    }
    
}
