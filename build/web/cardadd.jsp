<%@page import="com.qucoon.dbo.Facebook_users"%>
<%@page import="com.qucoon.dbo.State_manager_facebook"%>
<%@page import="com.qucoon.dbo.State_manager_telegram"%>
<%@page import="com.qucoon.operations.Operations"%>
<%@page import="com.qucoon.dbo.DataObjects"%>
<%@page import="com.qucoon.dbo.Telegram_users"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>:: Octopus ::</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elem ents and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
 <%
  Telegram_users tm_userdet=null;
  Facebook_users fb_userdet=null;
  State_manager_telegram tm_user=null;
  State_manager_facebook fb_user=null;
  String stat="false";
  if(request.getParameter("wid")!=null){
      String wid = request.getParameter("wid");
      session.setAttribute("wid",wid);
      if(wid.startsWith("0")){
          tm_user= Operations.getCurrentCustomer_tele(wid);
          if(tm_user!=null){
              String chatid =(tm_user.getChatid()!=null)? tm_user.getChatid().trim() :"";
              if(chatid!=""){
                  tm_userdet=Operations.getCustomer_tele(chatid);
                  stat="tele";
                  session.setAttribute("identifier",stat);
                  session.setAttribute("tmuserdet",tm_userdet);
                  session.setAttribute("tmuser",tm_user);
                  
              }else{
                  tm_user=null;
                  tm_userdet=null;
              }
          }else{
              tm_user=null;
              tm_userdet=null; 
          }
      }else{
          fb_user= Operations.getCurrentCustomer_face(wid); 
          if(fb_user!=null){
              String chatid = (fb_user.getChatid()!=null )? fb_user.getChatid().trim() :"";
              if(chatid!=""){
                  fb_userdet=Operations.getCustomer_face(chatid);
                  stat="face";
                  session.setAttribute("identifier",stat);
                  session.setAttribute("fbuserdet",fb_userdet);
                  session.setAttribute("fbuser",fb_user);
              }else{
                  fb_user=null;
                  fb_userdet=null;
              }
          }else{
              tm_user=null;
              tm_userdet=null; 
          }
          
      }
     
    }
  %>
</head>
<body class="cms_sectiontop">
<%if(session.getAttribute("msg")!=null){
        String msg =(String)session.getAttribute("msg"); %>
        <script>
            alert('<%=msg %>');
        </script>
        <%}
  session.setAttribute("msg",null);
  %>
<div class="main_top ">
  <!--header start -->
  <header>
  <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <figure class="logo"><a href="index.html"><img src="images/logo.png" alt="logo" title="OCTOPUS"></a></figure>
            </div>
            <div class="col-sm-8">
                <!-- <div class="branch_button"> <a href="javascript:void(0)" class="btn btn-default mybench" id="bench_event"> My Bench </a> </div> -->
            </div>
        </div>
    </div>
  </header>
  <%  
    if(tm_user==null&&stat=="false"||fb_user==null&&stat=="false"){
        session.setAttribute("msg", "Invalid Request");
        response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
        
    }else{
        String custname="";
        if(stat=="tele"){
            custname=tm_userdet.getCust_firstname();
        }
        if(stat=="face"){
            custname=fb_userdet.getCust_firstname();
        }
    %>
    
  <div class="container" style="padding-left: 5%;padding-right: 5%;">
   
    <div class="cms_section">

      <div class="inner_cms">
       	<div class="middle_title" style="height: 50px;">
           <h2>Add Debit Cards!</h2>
          
           <img src="images/octopus_image.png" alt="image">
        </div>
        <form action="servAddCard" method="post" >
          <div class="recent_addcard"> <img src="images/more_card.png" alt="image">
            <p><%=custname %>, We will save this card for your convenience in the future, you can remove the card any time by visiting Your Account section.</p>
            <div class="row">
              <div class="col-sm-3"> <strong>Name on card</strong>
                <input type="text" class="form-control"  name="cardname" id="cardname" required>
              </div>
              <div class="col-sm-3"> <strong>Card number</strong>
                <input type="text" class="form-control" name="cardno" id="cardno" required>
              </div>
                <div class="col-sm-3"> <strong>CVV</strong>
                <input type="text" class="form-control" name="cardcvv" id="cvv" required>
              </div>
                <div class="col-sm-3"> <strong>PIN</strong>
                <input type="password" class="form-control" name="cardpin" id="pin"  required>
              </div>
              <div class="col-sm-4"> <strong> Expiration date </strong>
                <div class="row">
                  <div class="col-sm-6">
                    <select class="form-control" name="expirymonth">
                      <option selected="selected">01</option>
                      <option>02</option>
                      <option>03</option>
                      <option>04</option>
                      <option>05</option>
                      <option>06</option>
                      <option>07</option>
                      <option>08</option>
                      <option>09</option>
                      <option>10</option>
                      <option>11</option>
                      <option>12</option>
                      
                      
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <select class="form-control"  name="expiryyear">
                      <option selected="selected" value="17">17</option>
                      <option  value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-3"> <strong>Octopus Passcode</strong>
                <input type="password" class="form-control" name="passcode" id="passcode"  required>
              </div>
            </div>
            <div class="button_group">
              <input type="submit" class="btn btn-default" value="Add your card" id="submit" name="submit">
              <button type="button" class="btn btn-default nlpsearch" id="loadingbtn" name="submit" style="display: none;">Processing...</button>
              
            </div>
          </div>
        <form>
      </div>
    </div>
  </div>
  <% }
            %>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/extendservice.js"></script>
<script type="text/javascript">
    $('#submit').click(function(){
        var pass=$('#passcode').val();
        var cardname=$('#cardname').val();
        var cardno=$('#cardno').val();
        var cvv=$('#cvv').val();
        var pin=$('#pin').val();
        if(pass!=""&&cardname!=""&&cardno!=""&&cvv!=""&&pin!=""){
            $('#submit').hide();
            $('#loadingbtn').show();
        }
    })
</script>
<script>
	
	$(document).on('click','.sendreqsend_button', function(){
            			var flag=1;
            			$('.bank_mandatory').each(function(){
            				if($(this).val() ===''){
            						flag=2;
            					$(this).parent().addClass("error_message");
            				}else{
            					$(this).parent().removeClass("error_message");
            				}

            			});
            			//on non empty field it will enter in if
            			if(flag ===1)
            			{
										$('form#transfer').submit();
            					
            			}


            		});
	
	
	
	
	//close bank list
      $(".close_login_modal").click(function(){
        $(".getbankfromaccount_popup").removeClass("modal_popup_active");
        $(".getmyaccount_popup").removeClass("modal_popup_active");
        $(".getbeneficiary_popup").removeClass("modal_popup_active");
      });
      
     
	
	$("#destbank").focusin(function(){
		var accountno = $('#accountno').val();
        console.log("entered accountno == " + accountno);
		//alert('Account Number is Required'); 
		if(accountno == null || accountno === '')
		 {
			 console.log('accountno not provided');
					if($('#accountno').val() ===''){
						$('#accountno').parent().addClass("error_message");
					}
		 }
		
		 $(".getbankfromaccount_popup").addClass("modal_popup_active");
		 $('.loading_content').show();
		
		var endpointurl='sServiceRest';
		var obj = {accountnumber:accountno,action:"nuban"};
		console.log(obj);
		
			 $.ajax({
					  type: "POST",
					  url: endpointurl,
					  contentType:"application/json",
					  dataType:"json",
					  data: JSON.stringify(obj),
					  success: function(data){
						localStorage.removeItem("returnedbanks");
						localStorage.setItem("returnedbanks",JSON.stringify(data.result));
						var returnedbanks = JSON.stringify(data.result);//localStorage.getItem("returnedbanks");
						var banks = $.parseJSON(returnedbanks);
							$.each(banks,function(index, value){
							$('<li><strong><a href="javascript:void(0)" alt="'+value.bankcode+'">'+value.bankname+'</a></strong></li>').appendTo("ul#middle_bankoption");

						  });
						  $("#middle_bankoption li a").click(function() {
							  console.log("bank selected "+$(this).text());
							  console.log("bank selected "+$(this).attr('alt'));
							  var previousval = String($("#accountno").val()+" - ");
							  var bankname = $(this).text();
							  var bankcode = $(this).attr('alt');
							  $("#destbank").val(String(bankname));
							  $("#bankcode").val(String(bankcode));
							
							  $("#banklabel").text("");
							  $("ul#middle_bankoption li").detach();
                                                          //do name enquiry
                                                          
                                                          
							  //close the modal pop up
							  $(".getbankfromaccount_popup").removeClass("modal_popup_active");
                                                          
                                                          getAccountName(accountno,bankcode,bankname).success(function (data) {
                                                                    var returnedcode = data.responsecode;
                                                                    console.log('returnedcode='+returnedcode);
                                                                    if(returnedcode==="00"){
                                                                       
                                                                         $("#accountname").val(String(data.accountname));
                                                                        //$("#accountnamelabel").text("");
                                                                  }
                                                            });
                                                                
                                                                
							});
					  },
					  complete: function(){
						$('.loading-content').hide();
					  }

					}); 
		
		 
		
      });
	
	
	$('#accountno').focusout(function(){

           $('.loading_content').show();
            var accountno = $(this).val();
          
            console.log("account focus out entered "+ $(this).val());
				var endpointurl='sService';
                var obj = {source:"MOBILE",accountnumber:accttosearch,action:"nuban"};
                console.log(obj);
                $.ajax({
                  type: "POST",
                  url: endpointurl,
                  contentType:"application/json",
                  dataType:"json",
                  data: JSON.stringify(obj),
                  success: function(data,status,xHRQ){
                    localStorage.removeItem("returnedbanks");
                    localStorage.setItem("returnedbanks",JSON.stringify(data.result));
                    var returnedbanks = JSON.stringify(data.result);
                    var banks = $.parseJSON(returnedbanks);
                        $.each(banks,function(index, value){
                        $('<li><strong><a href="javascript:void(0)" alt="'+value.bankcode+'">'+value.bankname+'</a></strong></li>').appendTo("ul#middle_bankoption");

                      });
                      $("#middle_bankoption li a").click(function() {
                          console.log("bank selected "+$(this).text());
                          console.log("bank selected "+$(this).attr('alt'));
                          var previousval = String($("#accountno").val()+" - ");
                          var newval = previousval.concat($(this).text());
                          $("#bankname").val(String(newval));
                          $("#banklabel").text("");
                          $("ul#middle_bankoption li").detach();
                          //close the modal pop up
                            $(".getbankfromaccount_popup").removeClass("modal_popup_active");
                        });
                  },
                  complete: function(){
                    $('.loading-content').hide();
                  }

                }); 
});


 
	
	
                  $(document).ready(function(){
                    $("#bench_event").click(function(){
						  $(".modal_popup").addClass("modal_popup_active");
						  	$(".overlay_modal").addClass("c-mask-active");

                    });

					  $("#popup_close").click(function(){
        $(".modal_popup").removeClass("modal_popup_active");
			$(".overlay_modal").removeClass("c-mask-active");

    });
	         $(".overlay_modal").click(function(){
                      $(".modal_popup").removeClass("modal_popup_active");
			$(".overlay_modal").removeClass("c-mask-active");


                    });
                  });
                </script>
</body>
</html>
