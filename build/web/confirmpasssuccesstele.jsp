<%@page import="com.qucoon.dbo.Facebook_users"%>
<%@page import="com.qucoon.dbo.State_manager_facebook"%>
<%@page import="com.qucoon.dbo.State_manager_telegram"%>
<%@page import="com.qucoon.operations.Operations"%>
<%@page import="com.qucoon.dbo.DataObjects"%>
<%@page import="com.qucoon.dbo.Telegram_users"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <script type="text/javascript">
        function closeWindow() {
            window.location.replace("https://telegram.me/octopus10_bot");
        }
        window.onload = closeWindow;
        </script>
        <title>:: Octopus ::</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet">
        <link href="css/owl.carousel.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elem ents and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        
    </head>
    <body>
        <%if(session.getAttribute("msg")!=null){
        String msg =(String)session.getAttribute("msg"); %>
        <script>
            alert('<%=msg %>');
        </script>
        <%}
        session.setAttribute("msg",null);%>
        <div class="main_top">
            <!--header start -->
            <header>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4">
                            <figure class="logo"><a href="index.html"><img src="images/logo.png" alt="logo" title="OCTOPUS"></a></figure>
                        </div>
                        <div class="col-sm-8">
                            <!-- <div class="branch_button"> <a href="javascript:void(0)" class="btn btn-default mybench" id="bench_event"> My Bench </a> </div> -->
                        </div>
                    </div>
                </div>
            </header>
            <div class="row" style="height: 80px;">
            </div>
                    
                    
                
                    <div class="container" style="background-color: white; color: black;">
                        <div class="heading_content">
                            <h1 style="color: black;"> <strong> <b>Success</b> </strong> </h1>
                            
                        </div>
                    </div>
               
            
            
            
            
        </div>
        <!-- maintop div end here -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/bootbox.min.js"></script>

        
    </body>
</html>
