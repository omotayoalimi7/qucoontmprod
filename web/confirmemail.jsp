
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>:: Octopus ::</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elem ents and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="cms_sectiontop">

        
<div class="main_top ">
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <figure class="logo"><a href="index.html"><img src="images/logo.png" alt="logo" title="OCTOPUS"></a></figure>
            </div>
            <div class="col-sm-8">
                <!-- <div class="branch_button"> <a href="javascript:void(0)" class="btn btn-default mybench" id="bench_event"> My Bench </a> </div> -->
            </div>
        </div>
    </div>
</header>
  <!--header start -->
  <div class="container">
   <div style="height: 70px;">
   </div>
    <div class="cms_section">
      <div class="inner_cms">
       			

	<div class="middle_title " style="text-align: center; border-bottom: solid 1px grey; background-color: ">
	   <h2>Email Confirmation!</h2>
	</div>
      
	<div class="" style="text-align: center;"> 
	     <h3>Dear <strong>{name}</strong>, thank you for confirming your email address- <strong>{email_address}</strong>. <br><br>
	     	<p>Details of your account activities shall be sent to you on this email address. </h3></p>
	</div>
	 <div style="height: 30px;">
   	</div>
       </div>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/extendservice.js"></script>
	
</body>
</html>
