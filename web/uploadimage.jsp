<%@page import="com.qucoon.dbo.Facebook_users"%>
<%@page import="com.qucoon.dbo.State_manager_facebook"%>
<%@page import="com.qucoon.dbo.State_manager_telegram"%>
<%@page import="com.qucoon.operations.Operations"%>
<%@page import="com.qucoon.dbo.DataObjects"%>
<%@page import="com.qucoon.dbo.Telegram_users"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>:: Octopus ::</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet">
        <link href="css/owl.carousel.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elem ents and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <%
        Telegram_users tm_userdet=null;
        Facebook_users fb_userdet=null;
        State_manager_telegram tm_user=null;
        State_manager_facebook fb_user=null;
        String stat="false";
        if(request.getParameter("wid")!=null){
            String wid = request.getParameter("wid");
            session.setAttribute("wid",wid);
            if(wid.startsWith("0")){
                tm_user= Operations.getCurrentCustomer_tele(wid);
                if(tm_user!=null){
                    String chatid =(tm_user.getChatid()!=null)? tm_user.getChatid().trim() :"";
                    if(chatid!=""){
                        tm_userdet=Operations.getCustomer_tele(chatid);
                        stat="tele";
                        session.setAttribute("identifier",stat);
                        session.setAttribute("tmuserdet",tm_userdet);
                        session.setAttribute("tmuser",tm_user);
                        
                    }else{
                        tm_user=null;
                        tm_userdet=null;
                    }
                }else{
                    tm_user=null;
                    tm_userdet=null; 
                }
            }else{
                fb_user= Operations.getCurrentCustomer_face(wid); 
                if(fb_user!=null){
                    String chatid = (fb_user.getChatid()!=null )? fb_user.getChatid().trim() :"";
                    if(chatid!=""){
                        fb_userdet=Operations.getCustomer_face(chatid);
                        stat="face";
                        session.setAttribute("identifier",stat);
                        session.setAttribute("fbuserdet",fb_userdet);
                        session.setAttribute("fbuser",fb_user);
                    }else{
                        fb_user=null;
                        fb_userdet=null;
                    }
                }else{
                    fb_user=null;
                    fb_userdet=null; 
                }
                
            }
           
        }
        %>
        
    </head>
    <body>
        <%if(session.getAttribute("msg")!=null){
        String msg =(String)session.getAttribute("msg"); %>
        <script>
            alert('<%=msg %>');
        </script>
        <%}
        session.setAttribute("msg",null);%>
        <div class="main_top">
            <!--header start -->
            <header>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4">
                            <figure class="logo"><a href="index.html"><img src="images/logo.png" alt="logo" title="OCTOPUS"></a></figure>
                        </div>
                        <div class="col-sm-8">
                            <!-- <div class="branch_button"> <a href="javascript:void(0)" class="btn btn-default mybench" id="bench_event"> My Bench </a> </div> -->
                        </div>
                    </div>
                </div>
            </header>
            <div class="row" style="height: 80px;">
            </div>
            <%  
                if(tm_user==null&&stat=="false"||fb_user==null&&stat=="false"){
                    session.setAttribute("msg", "Invalid Request");
                    response.sendRedirect("error.jsp");
                    
                }else{
                    String custname="";
                    if(stat=="tele"){
                        custname=tm_userdet.getCust_firstname();
                    }
                    if(stat=="face"){
                        custname=fb_userdet.getCust_firstname();
                    }
                    %>
                    <div class="container" style="background-color: white; color: black;">
                        <div class="heading_content">
                            <h1 style="color: black;">Hey <%=custname %> <strong>Please Input your  <b>Passcode </b> </strong> </h1>
                            <div class="search_form wow zoomIn ">
                            <form action="servLogin" method="post">
                                <input type="password" class="form-control  inputsearchmob" id="inputsearchbox" id="passcode" name="passcode" placeholder="Passcode" >
                                
                                <button type="submit" class="btn btn-default nlpsearch" id="submit" name="submit">Upload</button>
                            </form>
                            </div>
                        </div>
                    </div>
               <% }
            %>
            
            
            
            
        </div>
        <!-- maintop div end here -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/bootbox.min.js"></script>

        
    </body>
</html>
