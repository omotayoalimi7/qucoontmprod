1<%@page import="com.qucoon.operations.TokenDetails"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>:: Octopus ::</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elem ents and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<%              String identifier="";
                String wid="";
                if(session.getAttribute("identifier")==null){
                    //logout
                    session.setAttribute("msg", "Invalid Request");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                }else{
                    identifier = (String)session.getAttribute("identifier");
                    wid = (String)session.getAttribute("wid");
                }
                TokenDetails tkd=null;
                String resmsg="";
                if(session.getAttribute("tokendetails")!=null){
                    tkd = (TokenDetails)session.getAttribute("tokendetails");
                     resmsg= tkd.getResponsemessage();
                }else{
                    ///logout
                    session.setAttribute("msg", "Invalid Request");
                    response.sendRedirect("https://octopus.hbng.com/tm/error.jsp");
                }
    %>
</head>
<body class="cms_sectiontop">
<%if(session.getAttribute("msg")!=null){
        String msg =(String)session.getAttribute("msg"); %>
        <script>
            alert('<%=msg %>');
        </script>
        <%}
        session.setAttribute("msg",null);%>
        
<div class="main_top ">
<header>
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-4">
              <figure class="logo"><a href="index.html"><img src="images/logo.png" alt="logo" title="OCTOPUS"></a></figure>
          </div>
          <div class="col-sm-8">
              <!-- <div class="branch_button"> <a href="javascript:void(0)" class="btn btn-default mybench" id="bench_event"> My Bench </a> </div> -->
          </div>
      </div>
  </div>
</header>
  <!--header start -->
  <div class="container">
   
    <div class="cms_section">
      <div class="inner_cms">
       			

<div class="middle_title ">
   <h2>Transaction/Service Request Confirmation!</h2>
   <img src="images/octopus_image.png" alt="image">
</div>
      
      <form action="servToken" method="post" >
          <div class="recent_addcard"> 
              <p>Details of your request are:<br><br>
                  
              <p><%=resmsg %></p>
              
              
              </p>
                <div class="row">
                  <div class="col-sm-4"> <strong>Token/OTP</strong>
                    <input type="text" class="form-control"  id="otp" name="otp" required>
                    <input type="hidden" name="action" value="">
                  </div>
                  
                  
                </div>
                <div class="button_group">
                  <input type="submit" class="btn btn-default" id="submit" name="submit" value="Confirm">
                  <button type="button" class="btn btn-default nlpsearch" id="loadingbtn" name="submit" style="display: none;">Processing...</button>
                </div>
              </div>
        <form>
      
      

      </div>

  
       
       
      </div>
    </div>
  </div>
  <div class="overlay_modal"></div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/extendservice.js"></script>
<script type="text/javascript">
    $('#submit').click(function(){
        var otp=$('#otp').val();
        if(otp!=""){
            $('#submit').hide();
            $('#loadingbtn').show();
        }
    })
</script>
</body>
</html>
